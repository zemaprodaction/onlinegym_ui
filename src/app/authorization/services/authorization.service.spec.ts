import {TestBed} from '@angular/core/testing';

import {AuthorizationService} from './authorization.service';
import {NgxsModule, Store} from '@ngxs/store';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AuthorizationStates} from '../store';
import {AuthorizationActions} from '../store/actions/authorization.actions';

describe('AuthorizationService', () => {
  let service: AuthorizationService;
  let store: Store;
  let spyStore;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot(AuthorizationStates), HttpClientTestingModule]
    });
    service = TestBed.inject(AuthorizationService);
    store = TestBed.inject(Store);
    spyStore = jest.spyOn(store, 'dispatch');
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should check if user was login', () => {
    service.checkUserAuthorization();
    expect(spyStore).toHaveBeenCalledWith(expect.any(AuthorizationActions.GetUserAuthorization));
    expect(spyStore).toHaveBeenCalledTimes(1);
  });
});
