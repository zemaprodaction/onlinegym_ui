import {Injectable} from '@angular/core';
import {Store} from '@ngxs/store';
import {AuthorizationActions} from '../store/actions/authorization.actions';
import {UserActions} from '../../core/user-store';
import {UserStatisticActions} from '../../user-progress/store/action/user-state.action';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  constructor(private store: Store) {
  }

  checkUserAuthorization() {
    this.store.dispatch(new AuthorizationActions.GetUserAuthorization());
  }

  logoutUser() {
    this.store.dispatch(new UserActions.ResetState());
    this.store.dispatch(new UserStatisticActions.ResetAction());
    this.store.dispatch(new AuthorizationActions.ResetState());
  }
}
