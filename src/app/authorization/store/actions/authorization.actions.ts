export namespace AuthorizationActions {

  export class GetUserAuthorization {
    public static readonly type = '[Authorization] Get user authorization';

    constructor() {
    }
  }

  export class AuthorizationLoaded {
    public static readonly type = '[Authorization] Authorization Loaded';

    constructor() {
    }
  }

  export class LogoutUser {
    public static readonly type = '[Authorization] Logout User';

    constructor() {
    }
  }

  export class ResetState {
    public static readonly type = '[Authorization] Reset State';

    constructor() {
    }
  }
}
