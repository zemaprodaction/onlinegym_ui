import {State} from '@ngxs/store';
import {Injectable} from '@angular/core';
import {AuthorizationLoadState} from "./authorization-loading.state";
import {AuthorizationUserState} from "./authorization-user.state";

export interface AuthorizationStateModel {

}

@State<AuthorizationStateModel>({
    name: 'authorization',
    defaults: {

    },
    children:[AuthorizationLoadState, AuthorizationUserState]
})
@Injectable()
export class AuthorizationState {


}
