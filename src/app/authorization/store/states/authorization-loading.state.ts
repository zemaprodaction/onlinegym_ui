import {Action, Selector, State, StateContext} from '@ngxs/store';
import {AuthorizationActions} from '../actions/authorization.actions';
import {Injectable} from '@angular/core';

export interface AuthorizationLoadingStateModel {
    loaded: boolean;
    loading: boolean
}

@State<AuthorizationLoadingStateModel>({
    name: 'authorizationLoadingState',
    defaults: {
        loaded: false,
        loading: false
    }
})
@Injectable()
export class AuthorizationLoadState {

    constructor() {
    }

    @Selector([AuthorizationLoadState])
    public static getLoaded(state: AuthorizationLoadingStateModel) {
        return state.loaded;
    }

    @Selector([AuthorizationLoadState])
    public static getLoading(state: AuthorizationLoadingStateModel) {
        return state.loading;
    }

    @Action(AuthorizationActions.AuthorizationLoaded)
    authorizationLoaded(ctx: StateContext<AuthorizationLoadingStateModel>) {
        ctx.patchState({
            loading: false,
            loaded: true
        })
    }

}
