import {Action, Selector, State, StateContext} from '@ngxs/store';
import {User} from '../../../share/models/user';
import {AuthorizationServiceApi} from '../../../core/services/authorization-service-api.service';
import {catchError, tap} from 'rxjs/operators';
import {AuthorizationActions} from '../actions/authorization.actions';
import {Injectable} from '@angular/core';
import {of} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {AuthorizationStateModel} from "./authorization.state";
import AuthorizationLoaded = AuthorizationActions.AuthorizationLoaded;

export interface AuthorizationUserStateModel {
    user: User;
}

@State<AuthorizationUserStateModel>({
    name: 'authorizationUserState',
    defaults: {
        user: null,
    }
})
@Injectable()
export class AuthorizationUserState {

    constructor(private authorizationApi: AuthorizationServiceApi, private http: HttpClient) {
    }

    @Selector([AuthorizationUserState])
    public static getCurrentUser(state: AuthorizationUserStateModel) {
        return state.user;
    }

    @Selector([AuthorizationUserState])
    public static getAuthorizationState(state: AuthorizationUserStateModel) {
        return state;
    }

    @Action(AuthorizationActions.GetUserAuthorization)
    getAuthorization(ctx: StateContext<AuthorizationUserStateModel>) {
        if (!ctx.getState().user || !ctx.getState().user.isAuthorized) {
            return this.authorizationApi.getAuthorization().pipe(
                tap(user => {
                    ctx.patchState({
                        user,
                    });
                    ctx.dispatch(new AuthorizationLoaded())
                }),
                catchError(() => {
                    ctx.patchState({
                        user: {
                            isAuthorized: false,
                            name: 'anonymous',
                            imageUrl: 'anonymous'
                        },
                    });
                    ctx.dispatch(new AuthorizationLoaded())
                    return of('');
                })
            );
        } else {
            return ctx.getState().user
        }
    }

    @Action(AuthorizationActions.LogoutUser)
    logoutuser(ctx: StateContext<AuthorizationStateModel>) {
        return this.http.post(`${environment.api.baseUrl}/logout`, {}).pipe(
            tap(() => ctx.patchState({
                user: {
                    isAuthorized: false,
                    name: 'anonymous',
                    imageUrl: 'anonymous'
                }
            })),
            tap(() => window.location.reload())
        );
    }

    @Action(AuthorizationActions.ResetState)
    resetState(ctx: StateContext<AuthorizationStateModel>) {
        ctx.setState({
            user: {
                isAuthorized: false,
                name: 'anonymous',
                imageUrl: 'anonymous'
            },
        });
    }

}
