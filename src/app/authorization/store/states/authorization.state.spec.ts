import {NgxsModule, Store} from '@ngxs/store';
import {TestBed, waitForAsync} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {AuthorizationConstants} from '../../../share/constants/endpointConstants';
import {User} from '../../../share/models/user';
import {AuthorizationActions} from '../actions/authorization.actions';
import {AuthorizationStates} from '../index';
import {AuthorizationUserState} from "./authorization-user.state";

describe('Authorization state', () => {
  let store: Store;
  let http: HttpTestingController;
  const mockUser: User = {isAuthorized: true, imageUrl: '', name: 'user'};
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot(AuthorizationStates), HttpClientTestingModule]
    }).compileComponents();
    store = TestBed.inject(Store);
    http = TestBed.inject(HttpTestingController);
    store.dispatch(new AuthorizationActions.GetUserAuthorization());
  }));


  it('should select authorization if api return authorization user', done => {
    const req = http.expectOne(`${AuthorizationConstants.AUTHORIZATION}`);
    expect(req.request.method).toEqual('GET');

    req.flush({...mockUser});
    const user = store.select(AuthorizationUserState.getCurrentUser);
    user.subscribe((usr) => {
      expect(usr.name).toEqual('user');
      done();
    });
  });
});
