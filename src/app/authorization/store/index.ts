import {AuthorizationState} from './states/authorization.state';
import {AuthorizationLoadState} from "./states/authorization-loading.state";
import {AuthorizationUserState} from "./states/authorization-user.state";

export const AuthorizationStates = [AuthorizationState, AuthorizationLoadState, AuthorizationUserState];
