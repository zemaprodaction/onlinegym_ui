import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgxsModule} from '@ngxs/store';
import {AuthorizationStates} from './store';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NgxsModule.forFeature(
      AuthorizationStates
    ),
  ]
})
export class AuthorizationModule { }
