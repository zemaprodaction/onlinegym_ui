import {animate, animateChild, group, query, style, transition, trigger} from '@angular/animations';

const initialSteps = [
    style({position: 'relative'}),
];
const slideRight = [
    ...initialSteps,
    query(':enter, :leave', [
        style({
            position: 'absolute',
            top: 0,
            right: 0,
            width: '100%'
        })
    ]),
    query(':enter', [
        style({right: '-100%'})
    ]),
    query(':leave', animateChild()),
    group([
        query(':leave', [
            animate('300ms ease-out', style({right: '100%'}))
        ]),
        query(':enter', [
            animate('300ms ease-out', style({right: '0%'}))
        ])
    ]),
    query(':enter', animateChild()),
];

const slideLeft = [
    ...initialSteps,
    query(':enter, :leave', [
        style({
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%'
        })
    ]),
    query(':enter', [
        style({left: '-100%'})
    ]),
    query(':leave', animateChild()),
    group([
        query(':leave', [
            animate('300ms ease-out', style({left: '100%'}))
        ]),
        query(':enter', [
            animate('300ms ease-out', style({left: '0%'}))
        ])
    ]),
    query(':enter', animateChild()),
];


const fadeIn = [
    style({position: 'relative'}),
    query(':enter, :leave', [
        style({
            position: 'absolute',
            top: 0,
            right: 0,
            width: '100%'
        })
    ]),
    query(':enter', [style({opacity: 0, transform: 'scale(0.92)'})]),
    query(':leave', [style({opacity: 0, transform: 'scale(0.92)'})]),
    query(':leave', animateChild()),
    group([
        query(':leave', [animate('75ms ease-out', style({opacity: 0}))]),
        query(':enter', [animate('225ms ease-in', style({opacity: 1, transform: 'scale(1)'}))])
    ]),
    query(':enter', animateChild()),
]

const slideRightWithFadeIn = [
    ...initialSteps,
    group([
        query(':self', [
            animate('200ms ease-out', style({transform: 'translateX(-100%)'})),
            style({
                opacity: 0,
                transform: 'translateX(0)'
            }),
            animate('300ms ease-out', style({opacity: 1})),
        ])
    ]),
    query(':enter', animateChild()),
];

const slideLeftWithFadeIn = [
    ...initialSteps,
    group([
        query(':self', [
            animate('200ms ease-out', style({transform: 'translateX(100%)'})),
            style({
                opacity: 0,
                transform: 'translateX(0)'
            }),
            animate('300ms ease-out', style({opacity: 1})),
        ])
    ]),
    query(':enter', animateChild()),
];
export const routeTransitionAnimations = trigger('routerTransition', [
    transition('workout-programs <=> user-workout-program, statistic <=> user-workout-program, workout-programs <=> statistic', fadeIn),
    transition('* <=> workout-result-page', fadeIn),
    transition('workout-programs => workout-program-detail', slideRight
    ),
    transition('workout-program-detail => workout-programs', slideLeft),
    transition('pre-training-info <=> user-workout-program', fadeIn),
]);
export const tabAnimation = trigger('tabAnimation', [
    transition(':increment', slideRight),
    transition(':decrement', slideLeft)
]);
export const workoutSetNavigation = trigger('workoutSetNavigation', [
    transition(':increment', slideRightWithFadeIn),
    transition(':decrement', slideLeftWithFadeIn)
]);


