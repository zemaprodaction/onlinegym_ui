import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {WorkoutProgramEntity} from '../../../share/models/workout-programs';


@Component({
  selector: 'app-training-list',
  templateUrl: './workout-programs-list-display.component.html',
  styleUrls: ['./workout-programs-list-display.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkoutProgramsListDisplayComponent implements OnInit {
  @Input()
  workoutPrograms: WorkoutProgramEntity[];
  @Output()
  enrollWorkoutProgram = new EventEmitter<WorkoutProgramEntity>();

  constructor() {
  }

  ngOnInit(): void {
  }

  onEnroll(workoutProgram: WorkoutProgramEntity) {
    this.enrollWorkoutProgram.emit(workoutProgram);
  }
}
