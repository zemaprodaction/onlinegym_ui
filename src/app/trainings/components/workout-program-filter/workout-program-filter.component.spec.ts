import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {WorkoutProgramFilterComponent} from './workout-program-filter.component';
import {NgxsModule, Store} from '@ngxs/store';
import {AppStates} from '../../../core/store';
import {MatChipsModule} from '@angular/material/chips';
import {Component, EventEmitter, Input, Output} from '@angular/core';
import {WorkoutProperty} from '../../../share/models/workout-propert';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {WorkoutProgramAction} from '../../../core/training-store/actions/workout-programs.action';
import AddWorkoutProgramFilter = WorkoutProgramAction.AddWorkoutProgramFilter;
import RemoveWorkoutProgramFilter = WorkoutProgramAction.RemoveWorkoutProgramFilter;

@Component({
  selector: 'app-filter-chips-component',
  template: ''
})
class MockFilterChipsComponent {
  @Output()
  filterAdded = new EventEmitter<WorkoutProperty>();
  @Output()
  filterRemoved = new EventEmitter<WorkoutProperty>();

  @Input()
  filters: Map<string, WorkoutProperty[]>;
}

describe('Toolbar component', () => {

  let component;
  let fixture: ComponentFixture<WorkoutProgramFilterComponent>;
  let store: Store;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        WorkoutProgramFilterComponent, MockFilterChipsComponent],
      imports: [
        HttpClientTestingModule,
        MatChipsModule,
        NgxsModule.forRoot(AppStates)]
    }).compileComponents();
    fixture = TestBed.createComponent(WorkoutProgramFilterComponent);
    component = fixture.componentInstance;
    store = TestBed.inject(Store);
    jest.spyOn(store, 'dispatch');
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should dispatch action when filter was chosen', fakeAsync(() => {
    component.onFilterAdded({workoutPropertyTypeCode: 'test', value: 'test_value'});
    tick();
    expect(store.dispatch).toHaveBeenCalledWith(expect.any(AddWorkoutProgramFilter));
  }));

  it('should remove filter when filter was unchecked', fakeAsync(() => {
    component.onFilterRemoved({workoutPropertyTypeCode: 'test', value: 'test_value'});
    tick();
    expect(store.dispatch).toHaveBeenCalledWith(expect.any(RemoveWorkoutProgramFilter));
  }));
});






