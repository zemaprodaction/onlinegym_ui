import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {FilterService} from '../../../core/services/filter.service';
import {Store} from '@ngxs/store';
import {WorkoutProgramAction} from '../../../core/training-store/actions/workout-programs.action';
import {CharacteristicPropertyFilter} from '../../../share/models/characteristic-property-filter';
import {CharacteristicPropertyFilterChosen} from '../../../share/models/characteristic-property-filter-chosen';
import AddWorkoutProgramFilter = WorkoutProgramAction.AddWorkoutProgramFilter;
import RemoveWorkoutProgramFilter = WorkoutProgramAction.RemoveWorkoutProgramFilter;
import ResetFilters = WorkoutProgramAction.ResetFilters;

@Component({
  selector: 'app-workout-program-filter',
  templateUrl: './workout-program-filter.component.html',
  styleUrls: ['./workout-program-filter.component.scss']
})
export class WorkoutProgramFilterComponent implements OnInit {

  $filters: Observable<CharacteristicPropertyFilter[]>;

  constructor(private store: Store, private filterService: FilterService) {
  }

  ngOnInit(): void {
    this.$filters = this.filterService.getAllFiltersForWorkoutProgram();
  }

  onFilterAdded(workoutProperty: CharacteristicPropertyFilterChosen) {
    this.store.dispatch(new AddWorkoutProgramFilter(workoutProperty));
  }

  onFilterRemoved(workoutProperty: CharacteristicPropertyFilterChosen) {
    this.store.dispatch(new RemoveWorkoutProgramFilter(workoutProperty));
  }

  resetFilters() {
    this.store.dispatch(new ResetFilters());
  }
}
