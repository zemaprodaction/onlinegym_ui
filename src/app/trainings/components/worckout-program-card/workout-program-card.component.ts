import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {WorkoutProgram} from '../../../share/models/workout-programs';

@Component({
  selector: 'app-workout-program-card',
  templateUrl: './workout-program-card.component.html',
  styleUrls: ['./workout-program-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkoutProgramCardComponent implements OnInit {

  @Input()
  workoutProgram: WorkoutProgram;
  @Input()
  withActionButtons = true;
  @Output()
  enrollWorkoutProgram = new EventEmitter<WorkoutProgram>();

  difficultyValue = '';
  workoutPlace;
  required = '';

  changeText: boolean;

  constructor() {
    this.changeText = false;
  }

  ngOnInit() {
    if (this.workoutProgram) {
      this.difficultyValue = this.workoutProgram.characteristic.workoutDifficulty.nativeValue as string;
      this.workoutPlace = this.workoutProgram.characteristic.workoutPlaces.value as string[];
      this.required = this.workoutProgram.characteristic.equipmentNecessity.value as string;
    }
  }

  onEnroll(workoutProgram: WorkoutProgram) {
    this.enrollWorkoutProgram.emit(workoutProgram);
  }
}
