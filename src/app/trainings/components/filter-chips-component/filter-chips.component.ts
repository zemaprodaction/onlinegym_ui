import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {CharacteristicPropertyFilter} from '../../../share/models/characteristic-property-filter';
import {CharacteristicPropertyFilterChosen} from '../../../share/models/characteristic-property-filter-chosen';

@Component({
  selector: 'app-filter-chips-component',
  templateUrl: './filter-chips.component.html',
  styleUrls: ['./filter-chips.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilterChipsComponent implements OnInit, OnDestroy {

  @Output()
  filterAdded = new EventEmitter<CharacteristicPropertyFilterChosen>();
  @Output()
  filterRemoved = new EventEmitter<CharacteristicPropertyFilterChosen>();

  @Output()
  filterComponentDestroy = new EventEmitter();

  @Input()
  filters: CharacteristicPropertyFilter[];

  selectable = true;

  selectedFilter: CharacteristicPropertyFilterChosen[] = [];

  constructor() {
  }

  ngOnDestroy(): void {
    this.filterComponentDestroy.emit();
  }

  isSelected(filterPath: string, value: string): boolean {
    const index = this.findFilter(filterPath, value);
    return index >= 0;
  }

  selectFilter(field: CharacteristicPropertyFilter, value: string): void {

    const index = this.findFilter(field.filterPath, value);
    const filterChosen: CharacteristicPropertyFilterChosen = {
      filterPath: field.filterPath,
      value
    };
    if (index >= 0) {
      this.filterRemoved.emit(filterChosen);
      this.selectedFilter.splice(index, 1);
    } else {
      this.filterAdded.emit(filterChosen);
      this.selectedFilter.push(filterChosen);
    }
  }

  ngOnInit(): void {
  }

  private findFilter(field: string, value: string) {
    return this.selectedFilter.map(f => f.filterPath + f.value).indexOf(field + value);
  }
}
