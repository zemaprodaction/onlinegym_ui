import {Component, NO_ERRORS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, fakeAsync, TestBed, tick, waitForAsync} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {FilterChipsComponent} from './filter-chips.component';

@Component({
  selector: 'mat-chip-list',
  template: ''
})
class FakeChipListComponent {
}

describe('TimelineComponent', () => {
  let component: FilterChipsComponent;
  let fixture: ComponentFixture<FilterChipsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [FilterChipsComponent, FakeChipListComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {

    fixture = TestBed.createComponent(FilterChipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should button on workout history card redirect to result page of this workout', fakeAsync(() => {
    const allWorkoutDayHistoryBlocks = fixture.debugElement.queryAll(By.css('.workout-day'));
    allWorkoutDayHistoryBlocks.forEach((workoutDayBlock) => {
      workoutDayBlock
        .query(By.css('a'))
        .nativeElement.click();
      tick();
    });
  }));
});



