import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BackdropNavLink} from '../../../share/models/backdrop-nav-link';
import {AppService} from '../../../core/store/app.service';
import {BackdropSetting} from '../../../core/type/backdrop-setting';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-backdrop-tab-navigation',
  templateUrl: './backdrop-tab-navigation.component.html',
  styleUrls: ['./backdrop-tab-navigation.component.scss']
})
export class BackdropTabNavigationComponent implements OnInit {
  isViewInitialized = false;

  backdropSettings$: Observable<BackdropSetting>;

  navLinks$: Observable<BackdropNavLink[]>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private appService: AppService
  ) {
  }

  ngOnInit() {
    this.backdropSettings$ = this.appService.getBackdropSettings();
    this.navLinks$ = this.backdropSettings$.pipe(
      map(settings => settings.navLinks)
    );
  }

  getAbsoluteUrlRoute(url: string) {
    return this.router.url.replace(/\/([^/]*)$/, '/' + url);
  }
}
