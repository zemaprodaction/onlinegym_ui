 import {WorkoutProgramCardComponent} from './worckout-program-card/workout-program-card.component';
import {WorkoutProgramsListDisplayComponent} from './workout-programs-list-display/workout-programs-list-display.component';
import {FilterChipsComponent} from './filter-chips-component/filter-chips.component';
import {WorkoutProgramFilterComponent} from './workout-program-filter/workout-program-filter.component';
import {BackdropTabNavigationComponent} from './workout-program-detail-tab-navigation/backdrop-tab-navigation.component';

export const components = [WorkoutProgramCardComponent, WorkoutProgramsListDisplayComponent,
  FilterChipsComponent, WorkoutProgramFilterComponent,
  BackdropTabNavigationComponent];

export * from './worckout-program-card/workout-program-card.component';
export * from './workout-programs-list-display/workout-programs-list-display.component';
export * from './filter-chips-component/filter-chips.component';
export * from './workout-program-filter/workout-program-filter.component';
export * from './workout-program-detail-tab-navigation/backdrop-tab-navigation.component';
