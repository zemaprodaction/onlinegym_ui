import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {Store} from '@ngxs/store';
import {WorkoutProgramService} from '../../core/services/workout-program-service.service';
import {filter, switchMap, tap} from 'rxjs/operators';
import {OpenTabNavigationBackdropGuard} from '../../core/guards/open-tab-navigation-backdrop-guard.service';


@Injectable({
  providedIn: 'root'
})
export class WorkoutProgramSetUpPageName implements CanActivate {
  constructor(private workoutProgramService: WorkoutProgramService,
              private store: Store, private backdropGuard: OpenTabNavigationBackdropGuard) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    const workoutProgramId = route.params.workoutProgramId;
    const workoutProgram = this.workoutProgramService.getWorkoutProgramById(workoutProgramId);

    return workoutProgram.pipe(
      filter(wp => wp !== null && wp !== undefined),
      tap(_ => {
        route.data = {...route.data, namePage: 'Подробнее'};
      }),
      switchMap(() => this.backdropGuard.canActivateWithoutGuard(route)));
  }
}
