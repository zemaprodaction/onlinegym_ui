import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate} from '@angular/router';
import {Observable} from 'rxjs';
import {Store} from '@ngxs/store';
import {WorkoutProgramAction} from '../../core/training-store/actions/workout-programs.action';
import {WorkoutProgramService} from '../../core/services/workout-program-service.service';
import {filter, map} from 'rxjs/operators';


@Injectable()
export class WorkoutProgramExistsGuard implements CanActivate {
  constructor(private workoutProgramService: WorkoutProgramService,
              private store: Store) {
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    const workoutProgramId = route.params.workoutProgramId;
    const workoutProgram = this.workoutProgramService.getWorkoutProgramById(workoutProgramId);
    this.store.dispatch(new WorkoutProgramAction.LoadWorkoutProgramById(workoutProgramId));
    return workoutProgram.pipe(
      filter(wp => wp !== null && wp !== undefined),
      map(() => true));
  }
}
