import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate} from '@angular/router';
import {Observable, of} from 'rxjs';
import {Store} from '@ngxs/store';

@Injectable()
export class WorkoutExistsGuard implements CanActivate {

  constructor(private store: Store) {
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    // return this.util.checkStore(fromStore.getWorkoutsLoaded,
    //   fromStore.loadWorkouts()).pipe(
    //   switchMap(() => {
    //     const id = route.params.workoutId;
    //     return this.util.hasEntityInState(id, fromStore.getWorkoutEntities);
    //   })
    // );
    return of(false);
  }
}
