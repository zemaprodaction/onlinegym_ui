import {Injectable} from '@angular/core';
import {CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {Store} from '@ngxs/store';
import {FabBecomeResetable} from '../../core/store/actions/app-ui.actions';
import {map, tap} from 'rxjs/operators';
import {Backdrop} from '../../core/store/actions/backdrop.actions';

@Injectable({
  providedIn: 'root'
})
export class ResetFabGuard implements CanDeactivate<unknown> {

  constructor(private store: Store) {
  }

  canDeactivate(
    component: unknown,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.store.dispatch(new FabBecomeResetable()).pipe(
      tap(_ => this.store.dispatch(new Backdrop.StateBecomeResetable())),
      map(_ => true)
    );
  }
}
