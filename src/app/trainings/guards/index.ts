import { WorkoutProgramExistsGuard } from './workout-program-exists-guard.service';
import { WorkoutProgramsGuard } from './workout-programs-guard.service';
import {WorkoutExistsGuard} from './workout-exists.guard';
import {ResetFabGuard} from './reset-fab.guard';

export const guards: any[] = [WorkoutProgramsGuard, WorkoutProgramExistsGuard, WorkoutExistsGuard, ResetFabGuard];

export * from './workout-programs-guard.service';
export * from './workout-program-exists-guard.service';
export * from './workout-exists.guard';
