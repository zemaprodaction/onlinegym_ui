import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';

import {catchError, switchMap} from 'rxjs/operators';
import {Observable, of} from 'rxjs';
import {Store} from '@ngxs/store';


@Injectable()
export class WorkoutProgramsGuard implements CanActivate {

    constructor( private store: Store) {}

    canActivate(): Observable<boolean> {
        return this.checkStore().pipe(
            switchMap(() => of(true)),
            catchError(() => of(false))
        );
    }

    checkStore(): Observable<boolean> {

      // return this.store.select(fromStore.getWorkoutProgramsLoaded).pipe(
      //       tap(loaded => {
      //           if (!loaded) {
      //             this.store.dispatch(fromStore.loadWorkoutPrograms());
      //           }
      //       }),
      //       filter(loaded => loaded),
      //       take(1)
      //   );
      return of(false);
    }
}

