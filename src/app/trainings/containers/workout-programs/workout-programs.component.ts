import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {WorkoutProgramEntity} from '../../../share/models/workout-programs';
import {Store} from '@ngxs/store';
import {WorkoutProgramAction} from '../../../core/training-store/actions/workout-programs.action';
import {WorkoutProgramState} from '../../../core/training-store/states/workout-program/workout-program.state';
import {UserActions} from '../../../core/user-store';
import {Backdrop} from '../../../core/store/actions/backdrop.actions';
import {WorkoutProgramFilterState} from '../../../core/training-store/states/workout-program/workout-program-filter.state';
import {filter, switchMap, takeUntil, withLatestFrom} from 'rxjs/operators';
import {ActivatedRoute, NavigationEnd, Router, RouterEvent} from '@angular/router';

@Component({
    templateUrl: './workout-programs.component.html',
    styleUrls: ['./workout-programs.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkoutProgramsComponent implements OnInit, OnDestroy {
    workoutPrograms$: Observable<WorkoutProgramEntity[]>;
    filterValues$: Observable<string[]>;
    $isLoading: Observable<boolean>;
    public destroyed = new Subject<any>();
    defaultArray = new Array(10).fill(null);
    param: string;

    constructor(private _store: Store, private route: ActivatedRoute, private router: Router) {
        this.param = route.snapshot.params['workoutProgramId'];
        this.workoutPrograms$ = this._store.select(WorkoutProgramState.getFilteredWorkoutPrograms);
        this.filterValues$ = this._store.select(WorkoutProgramFilterState.getAllFilterValue);
        this.$isLoading = this._store.select(WorkoutProgramState.getLoading());
        this._store.dispatch(new WorkoutProgramAction.LoadWorkoutPrograms());
        this.fetchData();
    }

    ngOnInit() {
    }

    onEnroll(workoutProgram: WorkoutProgramEntity) {
        this._store.dispatch(new UserActions.EnrollAction(workoutProgram.id));

    }

    ngOnDestroy(): void {
        this.destroyed.next();
        this.destroyed.complete();
    }


    private fetchData() {
        this.router.events.pipe(
            filter((event: RouterEvent) => event instanceof NavigationEnd),
            takeUntil(this.destroyed),
            switchMap(_ => this.workoutPrograms$),
            withLatestFrom(this.filterValues$)
        ).subscribe(([workoutPrograms, filterValuse]) => {
            const openTitle = $localize`:@@foundPrograms:Found programs` + ' ' + workoutPrograms.length.toString();
          if (filterValuse.length === 0) {
                this._store.dispatch(new Backdrop.SetUpCloseBackdropSubHeaderTitle({
                    closeTitle: $localize`:@@allWorkoutPrograms:All workout programs`,
                    openTitle
                }));
            } else {
                this._store.dispatch(new Backdrop.SetUpCloseBackdropSubHeaderTitle({
                    closeTitle: openTitle,
                    openTitle
                }));
            }
        });
    }
}
