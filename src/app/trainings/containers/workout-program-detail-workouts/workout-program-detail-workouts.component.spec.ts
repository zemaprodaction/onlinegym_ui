import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkoutProgramDetailWorkoutsComponent } from './workout-program-detail-workouts.component';

describe('WorkoutProgramDetailWorkoutsComponent', () => {
  let component: WorkoutProgramDetailWorkoutsComponent;
  let fixture: ComponentFixture<WorkoutProgramDetailWorkoutsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkoutProgramDetailWorkoutsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkoutProgramDetailWorkoutsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
