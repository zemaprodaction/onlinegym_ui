import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {WorkoutEntity} from '../../../share/models/workout/workoutEntity';
import {WorkoutInstancePhaseEntity} from '../../../share/models/workout/workout-instance-phase-entity';
import {ActivatedRoute} from '@angular/router';
import {WorkoutProgram} from '../../../share/models/workout-programs';
import {Store} from '@ngxs/store';
import {WorkoutProgramState} from '../../../core/training-store/states/workout-program/workout-program.state';
import {filter, map, switchMap} from 'rxjs/operators';
import {WorkoutState} from '../../../core/training-store/states/workout.state';
import {WorkoutInstanceEntity} from '../../../share/models/workout/workout-instance-entity';
import {Location} from "@angular/common";

@Component({
  selector: 'app-workout-detail-page',
  templateUrl: './workout-instance-detail-page.component.html',
  styleUrls: ['./workout-instance-detail-page.component.scss']
})
export class WorkoutInstanceDetailPageComponent implements OnInit {


  $workoutData: Observable<{ workout: WorkoutEntity }>;
  $workoutInstance: Observable<WorkoutInstanceEntity>;
  $listOfWorkoutPhases: Observable<WorkoutInstancePhaseEntity[]>;
  private wp: Observable<WorkoutProgram>;

  constructor(private activateRoute: ActivatedRoute, private store: Store,  private _location: Location) {
  }

  ngOnInit(): void {
    const workoutInstanceId = this.activateRoute.snapshot.params['workoutInstanceId'];
    const wpId = this.activateRoute.snapshot.params['workoutProgramId'];
    this.wp = this.store.select(WorkoutProgramState.getWorkoutProgramById(wpId));
    this.$workoutInstance = this.wp.pipe(
      filter(wp => wp !== undefined && wp !== null),
      map(wp => wp.listOfWeeks.flatMap(w => w.listOfDays).flatMap(d => d.listOfScheduledWorkouts)),
      map(wiList => wiList.find(wi => wi.id === workoutInstanceId)),
    );
    this.$listOfWorkoutPhases = this.$workoutInstance.pipe(
      map(wi => wi.listOfWorkoutPhases)
    );
    this.$workoutData = this.$workoutInstance.pipe(
      switchMap(wi => this.store.select(WorkoutState.getWorkoutById(wi.workoutId))),
      map(w => ({workout: w}))
    );

  }

}
