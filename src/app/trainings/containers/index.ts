import {WorkoutProgramsComponent} from './workout-programs/workout-programs.component';
import {WorkoutProgramDetailWorkoutsComponent} from './workout-program-detail-workouts/workout-program-detail-workouts.component';
import {WorkoutProgramDetailSchedulerComponent} from './workout-program-detail-scheduler/workout-program-detail-scheduler.component';
import {WorkoutInstanceDetailPageComponent} from './workout-detail-page/workout-instance-detail-page.component';


export const containers = [WorkoutProgramsComponent,
  WorkoutProgramDetailWorkoutsComponent, WorkoutProgramDetailSchedulerComponent, WorkoutInstanceDetailPageComponent];

export * from './workout-programs/workout-programs.component';
export * from './workout-program-detail-workouts/workout-program-detail-workouts.component';
export * from './workout-program-detail-scheduler/workout-program-detail-scheduler.component';
export * from './workout-detail-page/workout-instance-detail-page.component';
