import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkoutProgramDetailSchedulerComponent } from './workout-program-detail-scheduler.component';

describe('WorkoutProgramDetailSchedulerComponent', () => {
  let component: WorkoutProgramDetailSchedulerComponent;
  let fixture: ComponentFixture<WorkoutProgramDetailSchedulerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkoutProgramDetailSchedulerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkoutProgramDetailSchedulerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
