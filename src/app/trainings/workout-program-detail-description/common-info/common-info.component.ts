import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {BaseWorkoutProgramDetail} from '../base-workout-program-detail';
import {ActivatedRoute, Router} from '@angular/router';
import {Exercise} from '../../../share/models/exercise/exercise';
import {ExerciseState} from '../../../core/training-store/states/exercise.state';
import {Backdrop} from '../../../core/store/actions/backdrop.actions';
import {ShowFabWithoutReset} from '../../../core/store/actions/app-ui.actions';
import {UserActions} from '../../../core/user-store';

@Component({
    selector: 'app-common-info',
    templateUrl: './common-info.component.html',
    styleUrls: ['./common-info.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CommonInfoComponent extends BaseWorkoutProgramDetail implements OnInit {

    equipmentString: string;

    constructor(private router: Router, protected route: ActivatedRoute) {
        super(route);
    }

    ngOnInit(): void {
        this.store.dispatch(new Backdrop.StateBecomeNotResetable());
        this.workoutProgramId = this.route.parent.snapshot.params.workoutProgramId;
        this.store.dispatch(new ShowFabWithoutReset({
            name: 'Записаться',
            icon: 'add',
            handler: this.onEnroll(this.workoutProgramId)
        }));
        const exercise = this.workoutProgram.listOfWeeks
            .flatMap(w => w.listOfDays)
            .flatMap(d => d.listOfScheduledWorkouts)
            .flatMap(s => s.listOfWorkoutPhases)
            .flatMap(p => p.listOfSets)
            .map(s => s.exerciseId);
        this.equipmentString = Array.from(new Set(this.store.selectSnapshot(ExerciseState.getEntityByListId<Exercise>(exercise))
            .flatMap(ex => ex.additionalInfo.equipment).map(eq => eq.name))).join(', ');
    }


    onEnroll(workoutProgramId: string) {
        return () => {
            this.store.dispatch(new UserActions.EnrollAction((this.workoutProgramId)));
        };
    }

    swipeLeft() {
        this.router.navigate(["../scheduler"], {relativeTo: this.route});
    }
}
