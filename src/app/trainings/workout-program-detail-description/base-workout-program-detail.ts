import {AppInjector} from '../../core/services/app/app-injector.service';
import {Store} from '@ngxs/store';
import {WorkoutProgram} from '../../share/models/workout-programs';
import {ActivatedRoute} from '@angular/router';
import {WorkoutProgramState} from '../../core/training-store/states/workout-program/workout-program.state';

export class BaseWorkoutProgramDetail {

  workoutProgram: WorkoutProgram;
  store: Store;
  workoutProgramId: string;

  constructor(protected route: ActivatedRoute) {
    const injector = AppInjector.getInjector();
    this.store = injector.get(Store);
    this.workoutProgramId = this.route.parent.snapshot.params.workoutProgramId;

    this.workoutProgram = this.store.selectSnapshot(WorkoutProgramState.getWorkoutProgramById(this.workoutProgramId));
  }
}
