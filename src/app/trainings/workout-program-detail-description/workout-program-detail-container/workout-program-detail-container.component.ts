import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {BaseWorkoutProgramDetail} from '../base-workout-program-detail';
import {ActivatedRoute, RouterOutlet} from '@angular/router';
import {tabAnimation} from "../../../route-transition-animations";

@Component({
  selector: 'app-workout-program-detail',
  templateUrl: './workout-program-detail-container.component.html',
  styleUrls: ['./workout-program-detail-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [tabAnimation]
})
export class WorkoutProgramDetailContainerComponent extends BaseWorkoutProgramDetail implements OnInit {

  constructor(protected route: ActivatedRoute) {
    super(route);

  }

  ngOnInit() {

  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet &&
      outlet.activatedRouteData &&
      outlet.activatedRouteData['animationState'];
  }

}
