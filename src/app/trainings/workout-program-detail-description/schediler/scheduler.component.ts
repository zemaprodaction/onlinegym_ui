import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {BaseWorkoutProgramDetail} from '../base-workout-program-detail';
import {ActivatedRoute, Router} from '@angular/router';
import {WorkoutService} from '../../../core/services/workout.service';

@Component({
  selector: 'app-schediler',
  templateUrl: './scheduler.component.html',
  styleUrls: ['./scheduler.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SchedulerComponent extends BaseWorkoutProgramDetail implements OnInit {

  constructor(private router: Router, protected route: ActivatedRoute, private workoutService: WorkoutService) {
    super(route);
  }

  ngOnInit(): void {
  }

  getWorkoutById(id: string){
    return this.workoutService.getWorkoutSnapshotById(id);
  }

  swipeRight() {
    this.router.navigate(["../common"], {relativeTo: this.route});
  }
}
