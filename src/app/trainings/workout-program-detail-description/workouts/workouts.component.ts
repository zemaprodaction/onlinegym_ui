import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {BaseWorkoutProgramDetail} from '../base-workout-program-detail';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './workouts.component.html',
  styleUrls: ['./workouts.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkoutsComponent extends BaseWorkoutProgramDetail implements OnInit {

  constructor(protected route: ActivatedRoute) {
    super(route);
  }

  ngOnInit(): void {
  }

}
