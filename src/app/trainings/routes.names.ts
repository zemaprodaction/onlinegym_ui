export const trainingRoutesNames = {
  WORKOUT_PROGRAM: 'all',
  WORKOUT_PROGRAM_DETAIL: ':workoutProgramId',
  WORKOUT_PROGRAM_ID_PARAM: 'workout-program/:workoutProgramId',
  WORKOUT: 'workout',
  WORKOUT_DETAIL: 'workout/:workoutId',
  WORKOUT_INSTANCE_ID: 'workout_instance/:workoutInstanceId',
  EXERCISE: 'exercise',
  EXERCISE_ID: 'exercise/:exerciseId',
  USER_WORKOUTS: 'my-workouts',
  WORKOUT_RESULT_ID: 'statistic/:workoutResultId'
};
