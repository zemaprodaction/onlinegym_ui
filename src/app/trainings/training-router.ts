import {RouterModule, Routes} from '@angular/router';
import * as fromContainers from './containers';
import {ModuleWithProviders} from '@angular/core';
import {trainingRoutesNames} from './routes.names';
import {AddActionToToolbarGuard} from '../core/guards/add-action-to-toolbar.guard';
import {ToolBarButton} from '../share/models/tool-bar-button';
import {BackdropStatus} from '../core/store';
import {WorkoutProgramFilterComponent} from './components';
import {SetupCurrentNamePageGuard} from '../core/guards/setup-current-name-page.guard';
import {WorkoutProgramExistsGuard} from './guards';
import {WorkoutProgramDetailContainerComponent} from './workout-program-detail-description/workout-program-detail-container/workout-program-detail-container.component';
import {CommonInfoComponent} from './workout-program-detail-description/common-info/common-info.component';
import {SchedulerComponent} from './workout-program-detail-description/schediler/scheduler.component';
import {WorkoutProgramSetUpPageName} from './guards/workout-program-set-up-page-name';
import {CloseBackdropGuard} from '../core/guards/close-backdrop.guard';
import {ResetFabGuard} from './guards/reset-fab.guard';
import {AddBackToToolbarGuard} from "../core/guards/add-back-to-toolbar.guard";
import {AddCustomBackActionToToolbarGuard} from "../core/guards/add-custom-back-action-to-toolbar.guard";

const dataNavigateToAllWorkoutProgramPage = {
  goToUrl: ['workout-programs']
}
const TRAINING_ROUTES: Routes = [
  {path: '', pathMatch: 'full', redirectTo: `${trainingRoutesNames.WORKOUT_PROGRAM}`},
  {
    path: `${trainingRoutesNames.WORKOUT_PROGRAM}`,
    component: fromContainers.WorkoutProgramsComponent,
    canActivate: [AddActionToToolbarGuard, SetupCurrentNamePageGuard],
    runGuardsAndResolvers: 'always',
    data: {
      currentNamePage: $localize`:@@page-name.workout-programs:Workout programs`,
      action: {
        button: new ToolBarButton($localize`:@@filters:Filters`, '', 'filter_alt'),
        status: BackdropStatus.OPEN,
        isUseBackdropContext: true
      },
      component: WorkoutProgramFilterComponent,
      backdropSettings: {
        webDownToHeight: 160,
        mobileDownToHeight: 386,
        nameBackdropPage: $localize`:@@filters:Filters`
      },
      animationState: 'workout-programs'
    }
  },
  {
    path: `${trainingRoutesNames.WORKOUT_PROGRAM_DETAIL}`,
    component: WorkoutProgramDetailContainerComponent,
    children: [
      {path: '', redirectTo: 'common'},
      {path: 'common', component: CommonInfoComponent, data: {animationState: '1', ...dataNavigateToAllWorkoutProgramPage}, canActivate: [AddCustomBackActionToToolbarGuard],},
      {path: 'scheduler', component: SchedulerComponent, data: {animationState: '2', ...dataNavigateToAllWorkoutProgramPage}, canActivate: [AddCustomBackActionToToolbarGuard]},
    ],
    canActivate: [WorkoutProgramExistsGuard, WorkoutProgramSetUpPageName],
    canDeactivate: [CloseBackdropGuard, ResetFabGuard],
    data: {
      navLinks: [
        {
          path: 'common',
          label: $localize`:@@common:Common`,

        },
        {
          path: 'scheduler',
          label: $localize`:@@scheduler:Scheduler`,
          canActivate: [WorkoutProgramExistsGuard, WorkoutProgramSetUpPageName],
        }
      ],
      animationState: 'workout-program-detail'
    },
  },
  {
    path: `${trainingRoutesNames.WORKOUT_PROGRAM_DETAIL}/${trainingRoutesNames.WORKOUT_INSTANCE_ID}`,
    component: fromContainers.WorkoutInstanceDetailPageComponent,
    canActivate: [WorkoutProgramExistsGuard, AddBackToToolbarGuard]
  },

];


export const routing: ModuleWithProviders<RouterModule> = RouterModule.forChild(TRAINING_ROUTES);
