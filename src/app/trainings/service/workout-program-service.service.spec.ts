import {TestBed} from '@angular/core/testing';

import {NgxsModule, Store} from '@ngxs/store';
import {TrainingStates} from '../../core/training-store';
import {of} from 'rxjs';
import {workoutProgram} from '../../share/unit-test-utils/unit-test-utils.spec';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {WorkoutProgramService} from '../../core/services/workout-program-service.service';

describe('WorkoutProgramServiceService', () => {
  let service: WorkoutProgramService;
  let store: Store;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot(TrainingStates), HttpClientTestingModule]
    }).compileComponents();
    service = TestBed.inject(WorkoutProgramService);
    store = TestBed.inject(Store);
    jest.spyOn(store, 'select').mockReturnValue(of(workoutProgram));
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('when user select workout program from store', () => {

    it('should return denormilize workout program', done => {
      service.getWorkoutProgramById(workoutProgram.id).subscribe(val => {
        expect(val).toEqual(workoutProgram);
        done();
      });
    });
  });
});
