import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {WorkoutEntity} from '../../share/models/workout/workoutEntity';
import {CrudService} from './crud.service';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WorkoutServiceApi extends CrudService<WorkoutEntity, string> {

  constructor(protected _http: HttpClient) {
    super(_http, `${environment.api.baseUrl}/workout-program/workout`, 'workouts');
  }

  findAll123(): Observable<any> {
    return this._http.get<any>(`${environment.api.baseUrl}/statistic/workout/mostPopular`);
  }

}
