import {Injectable} from '@angular/core';
import {CrudService} from './crud.service';
import {Exercise} from '../../share/models/exercise/exercise';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ExerciseServiceApi extends CrudService<Exercise, string> {

  constructor(protected _http: HttpClient) {
    super(_http, `${environment.api.baseUrl}/workout-program/exercise`, 'exercises');
  }
}
