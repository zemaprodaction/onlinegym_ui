import {NgModule} from '@angular/core';

import {CommonModule} from '@angular/common';
// containers
import * as fromContainers from './containers';
import * as fromComponents from './components';
import {FilterChipsComponent, WorkoutProgramCardComponent, WorkoutProgramsListDisplayComponent} from './components';
import * as fromGuards from './guards';

import {routing} from './training-router';
import {MatChipsModule} from '@angular/material/chips';
import {MatCardModule} from '@angular/material/card';
import {MatStepperModule} from '@angular/material/stepper';
import {MatDividerModule} from '@angular/material/divider';
import {MatIconModule} from '@angular/material/icon';
import {FlexModule, GridModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {ShareModule} from '../share/share.module';
import {FormsModule} from '@angular/forms';
import {MatTabsModule} from '@angular/material/tabs';
import {MatListModule} from '@angular/material/list';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatExpansionModule} from '@angular/material/expansion';
import {WorkoutProgramDetailContainerComponent} from './workout-program-detail-description/workout-program-detail-container/workout-program-detail-container.component';
import {WorkoutsComponent} from './workout-program-detail-description/workouts/workouts.component';
import {CommonInfoComponent} from './workout-program-detail-description/common-info/common-info.component';
import {SchedulerComponent} from './workout-program-detail-description/schediler/scheduler.component';


@NgModule({
  declarations: [...fromContainers.containers,
    WorkoutProgramDetailContainerComponent,
    WorkoutsComponent,
    CommonInfoComponent,
    SchedulerComponent,
    ...fromComponents.components],
  imports: [
    CommonModule,
    routing,
    MatTabsModule,
    MatExpansionModule,
    MatChipsModule,
    MatCardModule,
    MatStepperModule,
    MatDividerModule,
    MatIconModule,
    FlexModule,
    MatButtonModule,
    GridModule,
    ShareModule,
    FormsModule,
    MatTabsModule,
    MatListModule,
    MatCheckboxModule,
  ],
  providers: [...fromGuards.guards],
  exports: [...fromContainers.containers, WorkoutProgramsListDisplayComponent,
    FilterChipsComponent, WorkoutProgramCardComponent]
})
export class TrainingsModule {
}
