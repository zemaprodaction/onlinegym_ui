// @ts-ignore
import {Component} from '@angular/core';
import {Routes} from '@angular/router';
import {trainingRoutesNames} from '../../trainings/routes.names';
import {Observable, of} from 'rxjs';
import {UserProgress} from '../../user-progress/types/user-progress';
import {WorkoutProgramEntity} from '../models/workout-programs';
import {WorkoutEntity} from '../models/workout/workoutEntity';
import {CrudService} from '../../trainings/service/crud.service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Store} from '@ngxs/store';
import {WorkoutPlace} from '../../user/training-process/type/workout-place.enum';
import {WorkoutInstanceEntity} from '../models/workout/workout-instance-entity';
import {WorkoutSetEntity} from '../models/workout/workout-set-entity';

@Component({
  selector: 'app-test-component',
  template: '<router-outlet></router-outlet>'
})
export class MockComponent {
}

export const TEST_ROUTERS: Routes = [
  {
    path: `${trainingRoutesNames.EXERCISE_ID}`,
    component: MockComponent
  }
];

export const userProgress: Observable<UserProgress> = of({
  workoutProgramPercentProgress: 97,
  groupingStatistic: [{name: 'test', value: 55, measurePostfix: 'раз'}, {
    name: 'test2',
    value: 56,
    measurePostfix: 'минут'
  }, {
    name: 'test2',
    value: 56,
    measurePostfix: 'минут'
  }],
  workoutHistory: [{
    numberRestDaysBeforeWorkout: 3,
    workoutInstanceId: 'workout 33',
    workoutCompletePercent: 90,
    workoutId: 'test_workout_id'
  },
    {
      numberRestDaysBeforeWorkout: 0,
      workoutInstanceId: 'workout 14',
      workoutCompletePercent: 50,
      workoutId: 'test_workout_id'
    },
    {
      numberRestDaysBeforeWorkout: 0,
      workoutInstanceId: 'workout 14',
      workoutCompletePercent: 50,
      workoutId: 'test_workout_id'
    }]
});

export function getMockWorkoutList() {
  const workout1: WorkoutEntity = {
    id: 'workout_1',
    listOfPhases: [
      {
        name: 'phase_1',
        listOfExercises: ['exercise_1', 'exercise_1']
      }
    ],
    common: {}
  };
  const workout2: WorkoutEntity = {
    id: 'workout_2',
    listOfPhases: [
      {
        name: 'phase_1',
        listOfExercises: ['exercise_2']
      }
    ],
    common: {}
  };
  return [workout1, workout2];
}

export function initDefaultTestState(store: Store) {
  store.reset({
    ...store.snapshot(),
    metadata: {
      measures: {
        ids: ['measure_1'],
        entities: {
          measure_1: {
            id: 'measure_1',
            actualValue: 2
          }
        }
      }
    },
    training: {
      ...store.snapshot().training,
      workout: {
        entities: {
          test_workout_id: {
            common: {
              workoutPlace: {
                value: WorkoutPlace.HOME
              }
            }
          }
        },
        ids: ['test_workout_id']
      },
      exercise: {
        ids: ['exercise_1', 'exercise_2'],
        entities: {
          exercise_1: {
            id: 'exercise_1',
            name: 'name_1'
          },
          exercise_2: {
            id: 'exercise_2',
            name: 'name_2'
          }
        }
      }
    }
  });
}

export const workoutProgram: WorkoutProgramEntity = {
  id: 'test_workout_program',
};

export class CrudServiceMockImplementation extends CrudService<any, any> {

  constructor(protected _http: HttpClient) {
    super(_http, `${environment.api.baseUrl}/exercise`);
  }

  delete(id: any): Observable<any> {
    return undefined;
  }

  findAll(): Observable<any[]> {
    return undefined;
  }

  findOne(id: any): Observable<any> {
    return undefined;
  }

  save(t: any): Observable<any> {
    return undefined;
  }

  update(id: any, t: any): Observable<any> {
    return undefined;
  }

  findByIdList(idList: any[]) {
    return of(getMockWorkoutList());
  }
}

const testWorkoutSet1: WorkoutSetEntity = {exerciseId: 'exerciseId',
  listOfMeasures: [{measureTypeId: 'test', expectedValue: 3}]};
export const validWorkoutInstance: WorkoutInstanceEntity = {
  id: 'workoutInstance1',
  workoutId: 'work1',
  listOfWorkoutPhases: [
    {name: 'workout_Phase1', listOfSets: [testWorkoutSet1, testWorkoutSet1, testWorkoutSet1]},
    {name: 'workoutPhase2', listOfSets: [testWorkoutSet1, testWorkoutSet1, testWorkoutSet1]}
  ]
};

describe('Toolbar component', () => {
  it('fake test', () => {
    expect(true).toEqual(true);
  });
});
