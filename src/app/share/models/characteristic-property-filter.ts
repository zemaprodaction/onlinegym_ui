export interface CharacteristicPropertyFilter {
  name: string;
  id: string;
  values: string[];
  filterPath: string;
}
