import {ExerciseSummary} from './exercise-summary';

class RestTime {
  actualRestTime: number;
  planedRestTime: number
}

export class WorkoutResult {
  date?: string;
  exerciseCount?: string;
  exerciseSummary?: ExerciseSummary[];
  id: string;
  percentOfCompletedWork?: number;
  restTime: RestTime
  totalWork?: string;
  totalWorkoutTime?: string;
  trainingWorkoutInstanceId?: string;
  workoutProgress?: number[];
  userWorkoutInstanceRowStatisticId: string
}
