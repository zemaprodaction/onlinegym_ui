export class ActionButton {
  name?: string;
  action?: () => void;
  color?: string;
  icon?: string;
}
