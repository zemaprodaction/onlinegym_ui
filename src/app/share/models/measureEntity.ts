import {MeasuresType} from './measuresType';

export class MeasureEntity {
    expectedValue: number;
    actualValue?: number;
    measureTypeId?: string;
    measurePrefix?: string;
    measurePrefixLocalized?: string;

  constructor(expectedValue: number, actualValue?: number, measureTypeId?: string) {
    this.expectedValue = expectedValue;
    this.actualValue = actualValue;
    this.measureTypeId = measureTypeId;
  }
}

export class Measure {
  measureType?: MeasuresType;
  expectedValue: number;
  actualValue?: number;
  currentValue?: number;

    static convertToMeasureEntity(measure: Measure): MeasureEntity {
        return {
            ...measure,
            measureTypeId: measure.measureType.id,
            measurePrefix: measure.measureType.measurePrefix,
            measurePrefixLocalized: measure.measureType.measurePrefixLocalized
        };
    }
}
