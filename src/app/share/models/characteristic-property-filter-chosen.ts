export interface CharacteristicPropertyFilterChosen {
  value: string;
  filterPath: string;
}
