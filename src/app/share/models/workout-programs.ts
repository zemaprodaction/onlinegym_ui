import {WorkoutInstanceEntity} from './workout/workout-instance-entity';
import {Range} from './range';
import {WorkoutCommon} from './workout/workout-common';
import {Workout} from './workout/workoutEntity';
import {CharacteristicProperty} from './characteristic-property';

export enum WorkoutProgramCharacteristicConstants {
  workoutType = 'workoutType',
  workoutDifficulty = 'workoutDifficulty',
  equipmentNecessity = 'equipmentNecessity',
  workoutPlaces = 'workoutPlaces',
}

export interface WorkoutProgramCharacteristic {
  workoutType: CharacteristicProperty;
  workoutDifficulty: CharacteristicProperty;
  equipmentNecessity: CharacteristicProperty;
  workoutPlaces: CharacteristicProperty;
}

export class WorkoutProgramEntity {
  id: string;
  common?: WorkoutCommon;
  listOfWeeks?: WorkoutWeekEntity[];
  workoutsOnProgram?: string[];
  amountOfDays?: number;
  amountWorkoutInWeek?: Range;
  characteristic?: WorkoutProgramCharacteristic;
}

export class WorkoutProgram {
  id: string;
  common?: WorkoutCommon;
  listOfWeeks?: WorkoutWeekEntity[];
  workoutsOnProgram?: Workout[];
  amountOfDays?: number;
  amountWorkoutInWeek?: Range;
  characteristic?: WorkoutProgramCharacteristic;
}

export interface WorkoutWeekEntity {
  listOfDays: WorkoutDayEntity[];
  weekNumber?: number;
  isComplete?: string;
}

export interface WorkoutDayEntity {
  listOfScheduledWorkouts?: WorkoutInstanceEntity[];
  dayIndex?: number;
  type?: string;
  unFinishedWorkouts?: number;
  numberOfDay?: number;
}





