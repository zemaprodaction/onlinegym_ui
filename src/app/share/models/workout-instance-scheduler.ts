import {UserWorkoutInstanceEntity, WorkoutInstanceEntity} from './workout/workout-instance-entity';

export interface WorkoutInstanceScheduler {
  day: string;
  workoutInstance: UserWorkoutInstanceEntity;
}
