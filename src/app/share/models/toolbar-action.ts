import {ToolBarButton} from './tool-bar-button';
import {BackdropStatus} from '../../core/store';

export class ToolbarAction {
  constructor(public button: ToolBarButton,
              public component?: any,
              public status?: BackdropStatus,
              public isUseBackdropContext?: boolean) {
  }
}
