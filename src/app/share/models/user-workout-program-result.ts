export interface UserWorkoutStatistic {
  workoutFinishedAmount: number;
  workoutMarkMapToCount: Map<string, number>;
}

export interface UserWorkoutProgramResult {
  daysExpired: number;
  finishedDay: string;
  startDate: string;
  takesDays: number;
  workoutProgramMark: number;
  averagePercentWorkoutComplete: number;
  workoutStatistic: UserWorkoutStatistic;
}
