import {WorkoutStatistic} from './workout-statistic';
import {WorkoutDataDto} from './workout-data-dto';
import {WorkoutSet} from './workout/workout-set-entity';
import {WorkoutInstanceEntity} from './workout/workout-instance-entity';
import {ExerciseOnWorkout} from './exercise-on-workout';
import {WorkoutProgram} from './workout-programs';
import {Measure} from './measureEntity';
import {Store} from "@ngxs/store";
import {ExerciseState} from "../../core/training-store/states/exercise.state";

export class WorkoutProgramStatistic {
    constructor(public workoutInstanceId: string,
                public exercisesResultList: ExerciseOnWorkout[],
                public workoutDifficalt: number,
                public totalWorkoutTime: number) {
    }

    static createdWorkoutProgramStatistic(workoutSetList: WorkoutSet[],
                                          workoutInstance: WorkoutInstanceEntity,
                                          workoutDifficalt: number,
                                          totalWorkoutTime: number, store: Store) {
        const exerciseStatistic = workoutSetList
            .map(value => {
                const listOfMeasurseEntity = value.listOfMeasures.map(m => Measure.convertToMeasureEntity(m));
                const ex = store.selectSnapshot(ExerciseState.selectExerciseById(value.exercise.id))
                return new ExerciseOnWorkout(ex.exerciseType, listOfMeasurseEntity, value.exercise.id, value.restTime, value.actualRestTime);
            });
        return new WorkoutProgramStatistic(workoutInstance.id, exerciseStatistic, workoutDifficalt, totalWorkoutTime);
    }
}
