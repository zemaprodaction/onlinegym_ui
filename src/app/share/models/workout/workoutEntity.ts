import {WorkoutPhase, WorkoutPhaseEntity} from './workout-phase-entity';
import {WorkoutCommon} from './workout-common';
import {WorkoutProperty} from '../workout-propert';
import {WorkoutProgramCharacteristic} from '../workout-programs';

export class WorkoutEntity {
  id: string;
  common?: WorkoutCommon;
  listOfPhases: WorkoutPhaseEntity[];
  duration?: number;
  characteristic?: WorkoutProgramCharacteristic;
}

export class Workout {
  id: string;
  common?: WorkoutCommon;
  characteristic?: WorkoutProgramCharacteristic;
  listOfPhases: WorkoutPhase[];
  duration?: number;
}






