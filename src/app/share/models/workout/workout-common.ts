import {ImageBundle} from '../image-bundle';

export class WorkoutCommon {
  name?: string;
  description?: string;
  imageBundle?: ImageBundle = null;
}
