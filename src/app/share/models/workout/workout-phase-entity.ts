import {Exercise} from '../exercise/exercise';

export class WorkoutPhaseEntity {

  listOfExercises: string[];
  name: string;

  constructor(listOfExercises: string[], name: string) {
    this.listOfExercises = listOfExercises;
    this.name = name;
  }

}

export class WorkoutPhase extends WorkoutPhaseEntity {
  listOfExercisesFetched: Exercise[];
}
