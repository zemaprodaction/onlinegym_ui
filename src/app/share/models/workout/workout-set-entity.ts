import {Measure, MeasureEntity} from '../measureEntity';
import {Exercise} from '../exercise/exercise';

export class WorkoutSetEntity {
  id?: string;
  restTime?: number;
  actualRestTime?: number;
  listOfMeasures?: MeasureEntity[];
  exerciseId?: string;
  active?: boolean;
  isDone?: boolean;
  number?: number
}

export class WorkoutSet {
  id?: string;
  restTime?: number;
  actualRestTime?: number;
  listOfMeasures?: Measure[];
  exercise?: Exercise;
  active?: boolean;
  isDone?: boolean;
  number?: number
  static calculateIdByWorkoutPhaseAndIndex(workoutPhase: string, index: number): string {
    return workoutPhase + '_' + index;
  }
}
