import {WorkoutSet, WorkoutSetEntity} from './workout-set-entity';

export interface WorkoutInstancePhaseEntity {
  name: string;
  listOfSets?: WorkoutSetEntity[];
  next?: string;
  isSkipped?: boolean;
  isOptional?: boolean;
}

export interface WorkoutInstancePhase {
  name: string;
  listOfSets: WorkoutSet[];
  next?: string;
  isSkipped?: boolean;
  isOptional?: boolean;
  enable: boolean;
}
