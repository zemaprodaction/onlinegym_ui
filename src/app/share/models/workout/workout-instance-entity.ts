import {WorkoutInstancePhase, WorkoutInstancePhaseEntity} from './workout-instance-phase-entity';
import {Workout} from './workoutEntity';

export class WorkoutInstanceEntity {
  id: string;
  name?: string;
  workoutId?: string;
  listOfWorkoutPhases?: WorkoutInstancePhaseEntity[];
  workoutProgramId?: string;
  isFinished?: boolean;
  workoutDuration?: string;
  actualDate?: string;
  percentComplete?: number
}

export class WorkoutInstance {
  id: string;
  name?: string;
  workoutId?: string;
  workout?: Workout;
  listOfWorkoutPhases?: WorkoutInstancePhase[];
  workoutProgramId?: string;
  isFinished?: boolean;
  workoutDuration?: string;
  actualDate?: string;
}

export class UserWorkoutInstanceEntity {
    id: string;
    trainingWorkoutId?: string;
    userId?: string;
    listOfWorkoutPhases?: WorkoutInstancePhaseEntity[];
    status?: "NOT_STARTED" | "IN_PROGRESS" | "FINISHED";
    name?: string;
    percentComplete: number
}
