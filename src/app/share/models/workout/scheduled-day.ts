import {UserWorkoutInstanceEntity} from './workout-instance-entity';

export interface ScheduledDay {
  id: string,
  userWorkoutInstanceList: UserWorkoutInstanceEntity[];
  date: string
}
