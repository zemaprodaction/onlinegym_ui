export interface WorkoutProgramProgress {
  percentComplete: number;
  workoutAllNumber: number;
  workoutCompletedNumber: number;
}
