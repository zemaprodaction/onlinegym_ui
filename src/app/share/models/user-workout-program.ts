import {WorkoutProgramScheduler} from './workout-program-scheduler';
import {WorkoutProgramEntity} from "./workout-programs";

export interface UserWorkoutProgram {
  id: string;
  workoutProgramScheduler: WorkoutProgramScheduler;
  status: 'NOT_STARTED' | 'IN_PROGRESS' | 'FINISHED';
  userWorkoutProgramId: string;
  trainingWorkoutProgramId: string;
}

export interface UserWorkoutProgramDto {
  id: string;
  workoutProgramScheduler: WorkoutProgramScheduler;
  status: 'NOT_STARTED' | 'IN_PROGRESS' | 'FINISHED';
  userWorkoutProgramId: string;
  trainingWorkoutProgram: WorkoutProgramEntity;
}
