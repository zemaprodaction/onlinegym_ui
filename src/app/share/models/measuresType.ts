export class MeasuresType {
  id: string;
  name: string;
  measurePrefix: string;
  measurePrefixLocalized: string;
}
