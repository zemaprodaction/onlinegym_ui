import {UserWorkoutProgram} from './user-workout-program';

export interface UserTrainingInfo {
  workoutProgramId: string;
  userWorkoutProgram: UserWorkoutProgram;
}
