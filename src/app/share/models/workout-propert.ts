export class WorkoutProperty {
  constructor(
              public workoutPropertyType: string,
              public id: string,
              public name?: string,
              ) {
  }
}
