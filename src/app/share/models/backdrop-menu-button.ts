export interface BackdropMenuButton {
  icon: string;
  handler?(): void;
}
