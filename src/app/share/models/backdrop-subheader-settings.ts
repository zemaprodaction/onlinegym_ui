export interface BackdropSubheaderSettings {
  closeTitle: string;
  openTitle: string;
}
