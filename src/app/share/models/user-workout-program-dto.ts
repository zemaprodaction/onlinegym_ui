export interface UserWorkoutProgramDto {
  id: string;
  startDate: string;
  endDate: string;
  userWorkoutProgramId: string;
}
