import {WorkoutDataDto} from './workout-data-dto';

export class WorkoutStatistic {
  constructor(public workoutId: string, public workoutData: WorkoutDataDto) {
  }
}


