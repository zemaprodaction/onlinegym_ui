import {ScheduledDay} from './workout/scheduled-day';

export interface WorkoutProgramScheduler {
  id: String,
  listOfDays: ScheduledDay[];
}
