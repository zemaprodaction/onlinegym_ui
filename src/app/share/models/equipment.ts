export interface Equipment {
  name: string;
  imageUrl: string;
}
