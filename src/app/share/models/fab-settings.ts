export interface FabSettings {
  isShow?: boolean;
  name: string;
  icon: string;
  isExtend?: boolean;
  handler(data: any): void;
}
