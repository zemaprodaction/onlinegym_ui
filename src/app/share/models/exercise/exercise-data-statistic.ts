import {MeasureEntity} from '../measureEntity';

export interface ExerciseDataStatistic {
  id: string;
  measures: MeasureEntity[];
}
