export interface ExerciseGroupingStatistic {
  strength?: number;
  cardio?: number;
  flexibility?: number;
}
