import {ImageBundle} from '../image-bundle';

export class Video {
    url: string
}

export interface Exercise {
    id: string;
    name?: string;
    exerciseType?: string;
    imageBundle?: ImageBundle;
    video?: Video;
    detailedDescription: string;
    additionalInfo?: AdditionalInfo;

}

class Muscle {
    id: string;
    name: string;
}

export interface AdditionalInfo {
    targetMuscle: Array<Muscle>;
    musclesInvolved: Array<Muscle>;
    equipment: Array<Equipment>;
}

export interface Equipment {
    id: string;
    name: string;
}
