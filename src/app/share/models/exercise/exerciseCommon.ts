interface ExerciseCommon {
  description: string;
  group: string;
  id: string;
  imageUrl: string;
  name: string;
  starRating: string;
  tags: string[];
}
