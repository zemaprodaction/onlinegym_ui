import {ToolbarContextHostDirective} from '../../core/directives';

export class ToolBarButton {

  constructor(public name?: string, public link?: string, public icon?: string,
              public callback?: (toolbarContextHostDirective: ToolbarContextHostDirective) => void) {
  }
}
