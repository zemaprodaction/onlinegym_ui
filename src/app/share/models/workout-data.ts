import {Workout} from './workout/workoutEntity';

export interface WorkoutData {
  workoutInstanceId: string;
  workoutId: string;
  workout: Workout;
  workoutProgramId: string;
  date: string;
  totalWorkoutTime: number;
  exerciseIdList: string[];
  percentOfCompletedWork: number;
}
