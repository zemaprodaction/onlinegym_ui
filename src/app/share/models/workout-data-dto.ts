import {ExerciseOnWorkout} from './exercise-on-workout';

export class WorkoutDataDto {
  constructor(public exercises: ExerciseOnWorkout[], public workoutInstanceId: string,
              public totalWorkoutTime: number = 0) {}
}
