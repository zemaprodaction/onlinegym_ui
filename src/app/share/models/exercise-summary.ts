import {Exercise} from './exercise/exercise';

export interface ExerciseSummary {
   exerciseId: string;
   exercise: Exercise;
   summary: Map<string, number>[];
   percentOfCompletedWork: number;
}
