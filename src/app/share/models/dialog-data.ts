export interface DialogData {
  value: string;
  message: string;
}
