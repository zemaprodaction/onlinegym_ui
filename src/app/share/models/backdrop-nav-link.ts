export interface BackdropNavLink {
  path: string;
  label: string;
}
