import {MeasureEntity} from './measureEntity';

export class ExerciseOnWorkout {
  constructor(public exerciseType: string, public measures: MeasureEntity[], public exerciseId: string, public restTime: number,
              public actualRestTime: number) {
  }
}
