import {Pipe, PipeTransform} from '@angular/core';
import {Equipment} from '../models/equipment';

@Pipe({
  name: 'equipmentString'
})
export class EquipmentStringPipe implements PipeTransform {

  transform(value: Equipment[]): any {
    return value.length ? value.map(equipment => equipment.name).join(', ')
      : 'Оборудование не нужно';
  }

}
