import {EquipmentStringPipe} from './equipment-string.pipe';

export const pipes = [EquipmentStringPipe];

export * from './equipment-string.pipe';
