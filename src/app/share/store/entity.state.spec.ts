import {NgxsModule, Store} from '@ngxs/store';
import {TestBed, waitForAsync} from '@angular/core/testing';
import {NgxsRouterPluginModule} from '@ngxs/router-plugin';
import {TrainingStates} from '../../core/training-store';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {defaultsEntityState, EntityState} from './entity.state';
import {RouterTestingModule} from '@angular/router/testing';
import {CrudServiceMockImplementation} from '../unit-test-utils/unit-test-utils.spec';


describe('Entity state', () => {
  let store: Store;
  let http: HttpClient;
  let crudService: CrudServiceMockImplementation;
  let entityState;
  const ctx = {
    setState: jest.fn(),
    getState: jest.fn(),
    patch: jest.fn(),
    dispatch: jest.fn(),
    patchState: jest.fn()
  };


  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot(TrainingStates,
        {
          selectorOptions: {
            suppressErrors: false,
            injectContainerState: false
          }
        }),
        NgxsRouterPluginModule.forRoot(),
        HttpClientModule,
        RouterTestingModule]
    }).compileComponents();

    store = TestBed.inject(Store);
    http = TestBed.inject(HttpClient);
    crudService = new CrudServiceMockImplementation(http);
  }));

  describe('when call loadByIdList methode', () => {
    beforeEach(waitForAsync(() => {
      jest.spyOn(crudService, 'findByIdList');
      ctx.getState = jest.fn(() => defaultsEntityState);
      entityState = new EntityState(crudService);
    }));
    it('should call service methode and insert result into state', () => {
      const idList = ['workout_1', 'workout_2'];

      entityState.loadByIdList(ctx, idList);
      expect(crudService.findByIdList).toHaveBeenCalledWith(idList);
    });

    describe('when send none uniqe id', () => {
      it('should remove duplicates', () => {
        const idList = ['workout_1', 'workout_2', 'workout_2'];
        entityState.loadByIdList(ctx, idList);
        expect(crudService.findByIdList).toHaveBeenCalledWith(['workout_1', 'workout_2']);
      });
    });
  });
});
