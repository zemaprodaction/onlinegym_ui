import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {combineLatest, Observable} from 'rxjs';
import {map, skipWhile, tap} from 'rxjs/operators';
import {YouShouldLoginComponent} from '../ui-components/dialogs';
import {ModalService} from '../../core/services/modal.service';
import {Select, Store} from '@ngxs/store';
import {AuthorizationLoadState} from "../../authorization/store/states/authorization-loading.state";
import {AuthorizationUserState, AuthorizationUserStateModel} from "../../authorization/store/states/authorization-user.state";

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {

    @Select(AuthorizationUserState.getAuthorizationState)
    private currentUser$: Observable<AuthorizationUserStateModel>;

    constructor(private store: Store,
                private modalService: ModalService) {
    }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot):
        Observable<boolean> {
        return combineLatest([this.currentUser$, this.store.select(AuthorizationLoadState.getLoaded)]).pipe(
            skipWhile(([authState, loaded]) => !loaded),
            tap(([authState, loaded]) => {
                if (!authState.user.isAuthorized) {
                    this.modalService.openDialog(YouShouldLoginComponent);
                }
            }),
            map(([authState, loaded]) => authState.user !== undefined && authState.user !== null && authState.user.isAuthorized)
        );
    }

}
