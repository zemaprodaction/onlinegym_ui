import {Component, Input, OnInit} from '@angular/core';
import {MatBottomSheet} from "@angular/material/bottom-sheet";

@Component({
  selector: 'app-close-bottom-sheet-button',
  templateUrl: './close-bottom-sheet-button.component.html',
  styleUrls: ['./close-bottom-sheet-button.component.scss']
})
export class CloseBottomSheetButtonComponent implements OnInit {

  @Input()
  bottomSheet: MatBottomSheet

  constructor() { }

  ngOnInit(): void {
  }

  close(){
    this.bottomSheet.dismiss()
  }

}
