import {Component, Input, OnInit} from '@angular/core';
import {BackdropStatus} from '../../../core/store';
import {BackdropSubheaderSettings} from '../../models/backdrop-subheader-settings';

@Component({
  selector: 'app-backdrop-header',
  templateUrl: './backdrop-header.component.html',
  styleUrls: ['./backdrop-header.component.scss']
})
export class BackdropHeaderComponent implements OnInit {

  @Input()
  header: BackdropSubheaderSettings;

  @Input()
  backdropStatus: BackdropStatus;

  constructor() {
  }

  ngOnInit(): void {
  }

  calculateTitle() {
    if (this.backdropStatus === BackdropStatus.CLOSE || this.backdropStatus === BackdropStatus.TAB_NAVIGATION_OPEN) {
      return this.header.closeTitle || this.header.openTitle;
    } else {
      return this.header.openTitle || this.header.closeTitle;
    }
  }
}
