import * as fromCharts from './charts';
import * as fromCards from './cards';
import * as fromDialogs from './dialogs';

export const uiComponents = [...fromCharts.charts, ...fromCards.cards, ...fromDialogs.dialogs];

