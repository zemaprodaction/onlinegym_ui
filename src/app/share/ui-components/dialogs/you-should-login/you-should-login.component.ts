import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {DialogData} from '../../../models/dialog-data';
import {environment} from '../../../../../environments/environment';
import {Store} from '@ngxs/store';
import {Navigate} from '@ngxs/router-plugin';

@Component({
  selector: 'app-you-should-login',
  templateUrl: './you-should-login.component.html',
  styleUrls: ['./you-should-login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class YouShouldLoginComponent {

  loginUrl = `${environment.api.baseUrl}/oauth2/authorization/keycloak`;

  constructor(
    public dialogRef: MatDialogRef<YouShouldLoginComponent>,
    private store: Store,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
  }

  onNoClick(): void {
    this.dialogRef.close();
    this.store.dispatch(new Navigate(['/workout-programs']));
  }

}
