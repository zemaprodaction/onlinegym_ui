import {Component, Input, OnInit} from '@angular/core';
import {WorkoutSetEntity} from '../../models/workout/workout-set-entity';
import {ExerciseState} from '../../../core/training-store/states/exercise.state';
import {MeasuresType} from '../../models/measuresType';
import {MeasureTypeState} from '../../../core/store/states/metadata-state/measure-types.state';
import {MeasureEntity} from '../../models/measureEntity';
import {Store} from '@ngxs/store';

@Component({
  selector: 'app-exercise-list',
  templateUrl: './exercise-list.component.html',
  styleUrls: ['./exercise-list.component.scss']
})
export class ExerciseListComponent implements OnInit {

  @Input()
  workoutSetList: WorkoutSetEntity[];

  constructor(private store: Store) {
  }

  ngOnInit(): void {
  }

  getExerciseById(exerciseId: string) {
    return this.store.selectSnapshot(ExerciseState.selectExerciseById(exerciseId));
  }

  getMeasureById(measureID: string): MeasuresType {
    return this.store.selectSnapshot(MeasureTypeState.getEntityById(measureID));
  }

  getMeasureString(listOfMeasures: MeasureEntity[]) {
    return listOfMeasures.map(m => m.expectedValue + ' ' +this.getMeasureById(m.measureTypeId).name).join('/');
  }

}
