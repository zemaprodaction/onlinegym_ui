import {Component, Input, OnInit} from '@angular/core';
import {WorkoutInstancePhaseEntity} from '../../models/workout/workout-instance-phase-entity';

@Component({
  selector: 'app-workout-instance-training-plan',
  templateUrl: './workout-instance-training-plan.component.html',
  styleUrls: ['./workout-instance-training-plan.component.scss']
})
export class WorkoutInstanceTrainingPlanComponent implements OnInit {

  @Input()
  workoutPhaseList: WorkoutInstancePhaseEntity[];

  constructor() {
  }

  ngOnInit(): void {
  }
}
