import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-complete-percent-card',
  templateUrl: './complete-percent-card.component.html',
  styleUrls: ['./complete-percent-card.component.scss']
})
export class CompletePercentCardComponent implements OnInit {

  @Input()
  percentCompleatWork!: number;

  constructor() {
  }

  ngOnInit(): void {
  }

}
