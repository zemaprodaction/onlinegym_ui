import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {WorkoutCharateristic} from '../../../models/workout-charateristic';

@Component({
  selector: 'app-image-card-with-characteristic',
  templateUrl: './image-card-with-characteristic.component.html',
  styleUrls: ['./image-card-with-characteristic.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageCardWithCharacteristicComponent implements OnInit {

  @Input()
  characteristics: WorkoutCharateristic[];
  @Input()
  image: string;
  @Input()
  date: string;

  constructor() {
  }

  ngOnInit() {

  }

}
