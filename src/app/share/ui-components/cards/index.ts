import {ImageCardWithCharacteristicComponent} from './image-card-with-characteristic/image-card-with-characteristic.component';
import {CompletePercentCardComponent} from './complete-percent-card/complete-percent-card.component';
import {WorkoutCardComponent} from './workout-card/workout-card.component';

export const cards = [ImageCardWithCharacteristicComponent, CompletePercentCardComponent, WorkoutCardComponent];

export * from './image-card-with-characteristic/image-card-with-characteristic.component';
export * from './complete-percent-card/complete-percent-card.component';
export * from './workout-card/workout-card.component';
