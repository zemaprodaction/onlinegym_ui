import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-bumbbell-icon',
  templateUrl: './bumbbell-icon.component.html',
  styleUrls: ['./bumbbell-icon.component.scss']
})
export class BumbbellIconComponent implements OnInit {

  @Input()
  isContrast = false;

  @Input()
  required = '';

  constructor() { }

  ngOnInit(): void {
  }

}
