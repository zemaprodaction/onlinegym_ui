import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-hard-detect-icon',
  templateUrl: './hard-detect-icon.component.html',
  styleUrls: ['./hard-detect-icon.component.scss']
})
export class HardDetectIconComponent implements OnInit {

  @Input()
  difficultyName: string;

  @Input()
  isContrast = false;

  constructor() {
  }

  ngOnInit(): void {
    this.difficultyName = this.difficultyName.toLowerCase();
  }

}
