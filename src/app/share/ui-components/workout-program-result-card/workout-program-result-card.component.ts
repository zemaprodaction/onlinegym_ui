import {ChangeDetectionStrategy, Component, EventEmitter, Inject, Input, LOCALE_ID, OnInit, Output} from '@angular/core';
import {UserWorkoutProgramResult} from '../../models/user-workout-program-result';
import {DatePipe} from "@angular/common";
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-workout-program-result-card',
  templateUrl: './workout-program-result-card.component.html',
  styleUrls: ['./workout-program-result-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkoutProgramResultCardComponent implements OnInit {

  @Input()
  result: UserWorkoutProgramResult;

  @Input()
  header = $localize`:@@result:Result`;

  @Input()
  repeatButtonNeed = true;
  @Input()
  withoutShadow = false;

  @Output()
  public repeateSubscription = new EventEmitter();

  constructor(@Inject(LOCALE_ID) private locale: string) {
  }

  ngOnInit(): void {
  }

  repeatSubscribe() {
    this.repeateSubscription.emit();
  }

  getRangeDate() {
    if (this.result.startDate) {
      if (this.result.finishedDay) {
        return formatDate(this.result.startDate, 'yyyy-MM-dd', this.locale) + ' - '
            + formatDate(this.result.finishedDay, 'yyyy-MM-dd', this.locale);
      } else {
        return formatDate(this.result.startDate, 'yyyy-MM-dd', this.locale)+ ' - ';
      }
    } else {
      return 'Не начата';
    }
  }
}
