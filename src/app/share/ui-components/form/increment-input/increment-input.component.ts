import {AfterViewInit, ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ThemePalette} from '@angular/material/core';

@Component({
  selector: 'app-increment-input',
  templateUrl: './increment-input.component.html',
  styleUrls: ['./increment-input.component.scss']
})
export class IncrementInputComponent implements OnInit, AfterViewInit {

  @Input()
  parent: FormGroup;

  _value = 0;
  _step = 1;
  _min = 1;
  _max = Infinity;
  _wrap = false;
  color: ThemePalette = 'primary';

  constructor(private cdr: ChangeDetectorRef, private fb: FormBuilder) {
  }

  @Input('value')
  set inputValue(_value: number) {
    this._value = this.parseNumber(_value);
  }

  @Input()
  set step(_step: number) {
    this._step = this.parseNumber(_step);
  }

  @Input()
  set min(_min: number) {
    this._min = this.parseNumber(_min);
  }

  @Input()
  set max(_max: number) {
    this._max = this.parseNumber(_max);
  }

  @Input()
  set wrap(_wrap: boolean) {
    this._wrap = this.parseBoolean(_wrap);
  }

  private parseNumber(num: any): number {
    return +num;
  }

  private parseBoolean(bool: any): boolean {
    return !!bool;
  }

  setColor(color: ThemePalette): void {
    this.color = color;
  }

  getColor(): ThemePalette {
    return this.color;
  }

  incrementValue(step: number = 1): void {

    let inputValue = this._value + step;

    if (this._wrap) {
      inputValue = this.wrappedValue(inputValue);
    }

    this._value = inputValue;

    this.parent.setControl('actualValue', this.fb.control(this._value));
    this.parent.value.actualValue = this._value;
    this.cdr.detectChanges();
  }

  private wrappedValue(inputValue: number): number {
    if (inputValue > this._max) {
      return this._min + inputValue - this._max;
    }

    if (inputValue < this._min) {

      if (this._max === Infinity) {
        return 0;
      }

      return this._max + inputValue;
    }

    return inputValue;
  }

  shouldDisableDecrement(inputValue: number): boolean {
    return !this._wrap && inputValue <= this._min;
  }

  shouldDisableIncrement(inputValue: number): boolean {
    return !this._wrap && inputValue >= this._max;
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
  }
}
