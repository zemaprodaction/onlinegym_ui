import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';

@Component({
  selector: 'app-charts-result-work-with-joules',
  templateUrl: './chart-result-work-with-joules.component.html',
  styleUrls: ['./chart-result-work-with-joules.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class ChartResultWorkWithJoulesComponent implements OnInit {

  @Input() chartResultData: { labels: string[]; data: number[] };

  public barChartOptions: ChartOptions = {
    responsive: true,
    title: {
      display: false
    },
    legend: {
      position: 'bottom',
      labels: {
        usePointStyle: true
      }
    },
    scales: {
      xAxes: [{
        gridLines: {
          display: false
        }
      }]
    }
  };

  public barChartType: ChartType = 'line';
  public barChartLegend = true;

  public barChartData: ChartDataSets[];

  constructor() {
  }

  ngOnInit() {
    if (this.chartResultData) {
      this.barChartData = [
        {
          data: this.chartResultData?.data, label: 'Работы выполнено', backgroundColor: '#7F22FD',
          borderColor: '#7F22FD', pointBackgroundColor: '#03DAC6',
          hoverBackgroundColor: '#7F22FD', minBarLength: 74
        },
      ];
    }
  }

}
