import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {ChartDataSets, ChartType} from 'chart.js';
import {Color, Label} from 'ng2-charts';

@Component({
  selector: 'app-radar-chart',
  templateUrl: './radar-chart.component.html',
  styleUrls: ['./radar-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RadarChartComponent implements OnInit {

  @Input()
  allData: ChartDataSets;

  public arrPeriods = ['Day', 'Month', 'Year', 'All time'];
  public radarChartLabels: Label[] = ['Strength', 'Endurance', 'Stretching'];
  public radarChartData: ChartDataSets[];
  public radarChartType: ChartType = 'radar';
  public radarChartBackgroundColor: Color[] = [{backgroundColor: 'rgb(2, 190, 178, 50%)'}];


  constructor() {
  }

  ngOnInit(): void {
  }

  getRadarOption() {
    return {
      responsive: true,
      scale: {
        angleLines: {
          display: false
        },
        ticks: {
          suggestedMin: (this.allData.data as number[]).reduce((min, shot) => shot <= min ? shot : min) - 2,
          suggestedMax: (this.allData.data as number[]).reduce((max, shot) => shot >= max ? shot : max) + 2,
          precision: 0
        }
      }
    };
  }

}
