import {Component, Input, OnInit} from '@angular/core';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import {Label} from 'ng2-charts';
import {WorkoutResult} from '../../../models/workout-result';

@Component({
  selector: 'app-rest-chart',
  templateUrl: './rest-chart.component.html',
  styleUrls: ['./rest-chart.component.scss']
})
export class RestChartComponent implements OnInit {

  @Input()
  workoutResult: WorkoutResult;

  public barChartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    title: {
      display: false
    },
    tooltips: {
      enabled: false
    },
    legend: {
      position: 'bottom',
      labels: {
        usePointStyle: true
      }
    },
    scales: {
      xAxes: [{
        gridLines: {
          // color: "rgba(0, 0, 0, 0)",
        }
      }],
      yAxes: [{
        gridLines: {
          display: false
        }
      }]
    }
  };
  public barChartLabels: Label[] = [''];
  public barChartType: ChartType = 'horizontalBar';
  public barChartLegend = true;

  public barChartData: ChartDataSets[] = [];

  constructor() {
  }

  ngOnInit() {
    if (this.workoutResult) {
      this.barChartData = [
        {
          data: [this.workoutResult.restTime.planedRestTime, 1], label: 'Запланированный', backgroundColor: '#03DAC6',
          hoverBackgroundColor: '#03DAC6', borderColor: '#03DAC6', minBarLength: 0
        },
        {
          data: [this.workoutResult.restTime.actualRestTime, 1], label: 'Фактический', backgroundColor: '#7F22FD',
          borderColor: '#7F22FD',
          hoverBackgroundColor: '#7F22FD', minBarLength: 0
        },
      ];
    }
  }
}
