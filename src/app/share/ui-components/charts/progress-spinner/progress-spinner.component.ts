import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-progress-spinner',
  templateUrl: './progress-spinner.component.html',
  styleUrls: ['./progress-spinner.component.scss']
})
export class ProgressSpinnerComponent implements OnInit {

  @Input()
  percentComplete: number = 0;

  @Input()
  size: 'small' | 'medium' | 'big' = 'medium';
  @Input()
  color: 'accent' | 'warn' | 'primary' = 'primary';

  diameter: number;

  constructor() {
  }

  ngOnInit(): void {

    this.percentComplete = this.percentComplete || 0
    this.diameter = this.size === 'small' ? 56 : this.size === 'medium' ? 72 : 128;
  }

}
