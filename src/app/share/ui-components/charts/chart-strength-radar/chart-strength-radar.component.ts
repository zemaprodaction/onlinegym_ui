import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {ChartDataSets} from 'chart.js';
import {ExerciseProgressService, StatisticDateRange} from '../../../../core/services/exercise-progress.service';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-chart-strength-radar',
  templateUrl: './chart-strength-radar.component.html',
  styleUrls: ['./chart-strength-radar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChartStrengthRadarComponent implements OnInit {

  characteristicBasedOnExercise$: Observable<ChartDataSets>;

  public arrPeriods = ['Week', 'Month', 'Year', 'All time'];
  public selected = 'All time';

  constructor(private exerciseProgressService: ExerciseProgressService) {
    this.characteristicBasedOnExercise$ = this.getCharacteristicBasedOnExercise$();
  }

  getDateRange(str: string) {
    if (str === 'All time') {
      return StatisticDateRange.allTime;
    }
    return StatisticDateRange[str.replace(/\s/g, '_').toLowerCase()];
  }

  ngOnInit(): void {

  }

  onChange() {
    this.characteristicBasedOnExercise$ =
      this.getCharacteristicBasedOnExercise$();
  }

  private getCharacteristicBasedOnExercise$() {
    return this.exerciseProgressService.getCharacteristicBaseOnExercise(this.getDateRange(this.selected)).pipe(
      map(val => {
        const label = 'Chart';
        const data = [val['strength'] || 0, val['cardio'] || 0, val['stretching'] || 0];
        return {label, data};
      })
    );
  }
}
