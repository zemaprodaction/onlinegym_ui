import {RestChartComponent} from './rest-chart/rest-chart.component';
import {ChartResultWorkWithJoulesComponent} from './chart-result-work-with-joules/chart-result-work-with-joules.component';
import {ProgressSpinnerComponent} from './progress-spinner/progress-spinner.component';

export const charts = [ChartResultWorkWithJoulesComponent, RestChartComponent, ProgressSpinnerComponent];

export * from './chart-result-work-with-joules/chart-result-work-with-joules.component';
export * from './rest-chart/rest-chart.component';
export * from './progress-spinner/progress-spinner.component';
