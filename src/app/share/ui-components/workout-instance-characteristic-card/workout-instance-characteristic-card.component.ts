import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {WorkoutEntity} from '../../models/workout/workoutEntity';

@Component({
  selector: 'app-workout-instance-characteristic-card',
  templateUrl: './workout-instance-characteristic-card.component.html',
  styleUrls: ['./workout-instance-characteristic-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkoutInstanceCharacteristicCardComponent implements OnInit {

  @Input()
  workoutData: { workout: WorkoutEntity};

  constructor() {
  }

  ngOnInit(): void {
  }

  getCountOfExercises() {
    const size = this.workoutData.workout.listOfPhases.flatMap(ph => ph.listOfExercises).length;
    return size + ' ' + $localize`:@@exercises:exercises`;
  }
}
