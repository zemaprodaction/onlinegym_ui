import {environment} from '../../../environments/environment';

class RootConstants {
  public static GATEWAY = `${environment.api.baseUrl}`;
}

export class TrainingParamConstants {
  public static WORKOUT_PROGRAM_ID_TEMPLATE = '{workoutProgramId}';
  static WORKOUT_INSTANCE_ID = 'workoutInstanceId';
  static WORKOUT_PROGRAM_ID = 'workoutProgramId';
  static EXERCISE_ID = 'exerciseId';
  static WORKOUT_RESULT_ID = 'workoutResultId';
}

export class TrainingEndpointConstants {
  public static ROOT_WORKOUT_PROGRAM_SERVICE = `${RootConstants.GATEWAY}/workout-program`;
  public static WORKOUT_PROGRAM = `${TrainingEndpointConstants.ROOT_WORKOUT_PROGRAM_SERVICE}/workoutProgram`;
  public static WORKOUT = `${TrainingEndpointConstants.ROOT_WORKOUT_PROGRAM_SERVICE}/workout`;
  public static EXERCISES = `${TrainingEndpointConstants.ROOT_WORKOUT_PROGRAM_SERVICE}/exercise`;
  public static WORKOUT_PROGRAM_FILTERS = `${TrainingEndpointConstants.ROOT_WORKOUT_PROGRAM_SERVICE}/filter/workout_program`;
  public static METADATA = `${TrainingEndpointConstants.ROOT_WORKOUT_PROGRAM_SERVICE}/metadata`;
  public static METADATA_MEASURETYPE = `${TrainingEndpointConstants.ROOT_WORKOUT_PROGRAM_SERVICE}/metadata/measureTypes`;
  public static WORKOUT_INSTANCE = `${TrainingEndpointConstants.WORKOUT_PROGRAM}/${TrainingParamConstants.WORKOUT_PROGRAM_ID_TEMPLATE}/workoutInstance`;
}

export class StatisticConstants {
  public static ROOT_STATISTIC_SERVICE = `${RootConstants.GATEWAY}/statistic`;
  public static ROOT_STATISTIC_EXERCISE = `${StatisticConstants.ROOT_STATISTIC_SERVICE}/exercise`;
  public static ROOT_STATISTIC_WORKOUT_PROGRAM = `${StatisticConstants.ROOT_STATISTIC_SERVICE}/user_workout_instance/finish`;
  public static STATISTIC_WORKOUT_PROGRAM = `${StatisticConstants.ROOT_STATISTIC_SERVICE}/userWorkoutProgramStatistic`;
  public static ROOT_STATISTIC_WORKOUT = `${StatisticConstants.ROOT_STATISTIC_SERVICE}/workout`;
  public static ROOT_STATISTIC_WORKOUT_INSTANCE = `${StatisticConstants.ROOT_STATISTIC_SERVICE}/workout-instance`;
  public static WORKOUT_RATING = `${StatisticConstants.ROOT_STATISTIC_WORKOUT}/most_popular`;
  public static WORKOUT_HISTORY = `${StatisticConstants.ROOT_STATISTIC_WORKOUT}/history`;
  public static USER_PROGRESS = `${StatisticConstants.ROOT_STATISTIC_SERVICE}/common_user_statistic`;
  public static USER_PROGRESS_CURRENT_WORKOUT_PROGRAM = `${StatisticConstants.ROOT_STATISTIC_SERVICE}/current_workout_program_statistic`;
}


export class AuthorizationConstants {
  public static AUTHORIZATION = `${RootConstants.GATEWAY}/user_profile/me`;
}

export class UserEndpointConstants {
  public static USER = '/user/workout-program';
  public static USER_PROFILE = `${StatisticConstants.ROOT_STATISTIC_SERVICE}/user/training-info`;
  public static START_WORKOUT_PROGRAM =
    `${StatisticConstants.ROOT_STATISTIC_SERVICE}/userWorkoutProgram/start`;
  public static ENROLL = `${StatisticConstants.ROOT_STATISTIC_SERVICE}/subscribe`;
  public static UNSUBSCRIBE =
    `${StatisticConstants.ROOT_STATISTIC_SERVICE}/unsubscribe`;
  public static RESTART_WP =
    `${StatisticConstants.ROOT_STATISTIC_SERVICE}/workout-program/restart`;

  public static USER_WORKOUT_PROGRAM_RESULT =
    `${StatisticConstants.ROOT_STATISTIC_SERVICE}${UserEndpointConstants.USER}/result/active`;
  public static USER_ARCHIVE_WORKOUT_PROGRAM_RESULT =
    `${StatisticConstants.ROOT_STATISTIC_SERVICE}${UserEndpointConstants.USER}/result`;
  public static USER_WORKOUT_PROGRAM_ARCHIVE =
    `${StatisticConstants.ROOT_STATISTIC_SERVICE}${UserEndpointConstants.USER}/archive`;
}
