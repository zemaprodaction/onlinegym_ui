
import {BackgroundCoverDirective} from './background-cover.directive';

export const directives = [ BackgroundCoverDirective];

export * from './background-cover.directive';

