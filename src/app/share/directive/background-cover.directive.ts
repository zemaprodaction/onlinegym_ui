import {Directive, ElementRef, Input, OnInit} from '@angular/core';

@Directive({
  selector: '[appBackgroundCover]'
})
export class BackgroundCoverDirective implements OnInit {

  @Input('appBackgroundCover') imgHref: string;

  constructor(private el: ElementRef) {
  }

  ngOnInit(): void {
    this.el.nativeElement.style.background = 'url(' + this.imgHref + ')';
    this.el.nativeElement.style.backgroundSize = 'cover';
  }
}
