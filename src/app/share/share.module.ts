import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import * as fromDirectives from './directive';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import * as fromUiComponents from './ui-components';
import * as fromPipes from './pipes';
import {MatCardModule} from '@angular/material/card';
import {MatDividerModule} from '@angular/material/divider';
import {MatIconModule} from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {FlexModule} from '@angular/flex-layout';
import {IncrementInputComponent} from './ui-components/form/increment-input/increment-input.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {ChartsModule} from 'ng2-charts';
import {BumbbellIconComponent} from './ui-components/icons/bumbbell-icon/bumbbell-icon.component';
import {HardDetectIconComponent} from './ui-components/icons/hard-detect-icon/hard-detect-icon.component';
import {BackdropHeaderComponent} from './ui-components/backdrop-header/backdrop-header.component';
import {ChartStrengthRadarComponent} from './ui-components/charts/chart-strength-radar/chart-strength-radar.component';
import {MatSelectModule} from '@angular/material/select';
import {RadarChartComponent} from './ui-components/charts/radar-chart/radar-chart.component';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {TruncatePipe} from '../share/pipes/truncate.pipe';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {ExerciseListComponent} from './ui-components/exercise-list/exercise-list.component';
import {MatListModule} from '@angular/material/list';
import {WorkoutInstanceCharacteristicCardComponent} from './ui-components/workout-instance-characteristic-card/workout-instance-characteristic-card.component';
import {WorkoutInstanceTrainingPlanComponent} from './ui-components/workout-instance-training-plan/workout-instance-training-plan.component';
import {WorkoutProgramResultCardComponent} from './ui-components/workout-program-result-card/workout-program-result-card.component';
import {NgxSkeletonLoaderModule} from 'ngx-skeleton-loader';
import { CloseBottomSheetButtonComponent } from './ui-components/close-bottom-sheet-button/close-bottom-sheet-button.component';

@NgModule({
  declarations: [
    ...fromDirectives.directives,
    ...fromUiComponents.uiComponents,
    ...fromPipes.pipes,
    IncrementInputComponent, BumbbellIconComponent,
    HardDetectIconComponent,
    BackdropHeaderComponent,
    ChartStrengthRadarComponent,
    RadarChartComponent,
    TruncatePipe,
    WorkoutInstanceTrainingPlanComponent,
    ExerciseListComponent,
    WorkoutInstanceCharacteristicCardComponent,
    WorkoutProgramResultCardComponent,
    CloseBottomSheetButtonComponent
  ],
  imports: [
    CommonModule,
    MatSnackBarModule,
    MatCardModule,
    MatDividerModule,
    MatIconModule,
    MatDialogModule,
    MatButtonModule,
    FlexModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    ChartsModule,
    MatSelectModule,
    MatBottomSheetModule,
    MatProgressSpinnerModule,
    MatListModule,
    NgxSkeletonLoaderModule,

  ],
    exports: [...fromDirectives.directives, ...fromUiComponents.uiComponents, ...fromPipes.pipes,
        IncrementInputComponent,
        WorkoutInstanceCharacteristicCardComponent,
        WorkoutProgramResultCardComponent,
        TruncatePipe,
        WorkoutInstanceTrainingPlanComponent,
        BumbbellIconComponent,
        NgxSkeletonLoaderModule,
        MatProgressSpinnerModule,
        HardDetectIconComponent, BackdropHeaderComponent, ChartStrengthRadarComponent, ExerciseListComponent, CloseBottomSheetButtonComponent]
})
export class ShareModule {
}
