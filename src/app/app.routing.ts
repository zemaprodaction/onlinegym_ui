import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {appRoutesNames} from './routes.names';

export const APP_ROUTES: Routes = [
  {
    path: `${appRoutesNames.WORKOUT_PROGRAMS}`,
    loadChildren: () => import('./trainings/trainings.module').then(m => m.TrainingsModule),
  },
  {
    path: `${appRoutesNames.USER}`,
    loadChildren: () => import('./user/user.module').then(m => m.UserModule),
  },
  {
    path: `${appRoutesNames.PROGRESS}`,
    loadChildren: () => import('./user-progress/user-progress.module').then(m => m.UserProgressModule)
  },
  {path: '**', pathMatch: 'full', redirectTo: `${appRoutesNames.USER}`},
];

export const routing: ModuleWithProviders<RouterModule> = RouterModule.forRoot(
  APP_ROUTES,
  {
    onSameUrlNavigation: 'reload',
  }
);
