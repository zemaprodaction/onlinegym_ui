import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable, of} from 'rxjs';
import {Store} from '@ngxs/store';
import {Backdrop} from "../../core/store/actions/backdrop.actions";

@Injectable({
    providedIn: 'root'
})
export class StoreBecomeNotResatable implements CanActivate {

    constructor(private store: Store) {
    }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        this.store.dispatch(new Backdrop.StateBecomeNotResetable())
        return of(true)
    }

}
