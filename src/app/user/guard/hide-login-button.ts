import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable, of} from 'rxjs';
import {Store} from '@ngxs/store';
import {Toolbar} from "../../core/store/actions/toolbar.action";

@Injectable({
    providedIn: 'root'
})
export class HideLoginButton implements CanActivate {

    constructor(private store: Store) {
    }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        this.store.dispatch(new Toolbar.HideLoginButton())
        return of(true)
    }

}
