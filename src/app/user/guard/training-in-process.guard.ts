import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {Store} from '@ngxs/store';
import {map} from 'rxjs/operators';
import {WorkoutProcessStatusEnum} from '../training-process/model/workout-process-status.enum';
import {TrainingProcessStateCore} from '../training-process/store/state/training-process-core.state';
import {Navigate} from "@ngxs/router-plugin";
import {TrainingProcessCoreActions} from "../training-process/store/actions/training-process-core.actions";
import {AuthorizationUserState} from "../../authorization/store/states/authorization-user.state";

@Injectable({
    providedIn: 'root'
})
export class TrainingInProcessGuard implements CanActivate {

    constructor(private store: Store) {
    }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return this.store.select(TrainingProcessStateCore.getStatus).pipe(
            map((status) => {
                const currentUser = this.store.selectSnapshot(AuthorizationUserState.getCurrentUser)
                if (status === WorkoutProcessStatusEnum.inProgress) {
                    if (!currentUser || !currentUser.isAuthorized) {
                        this.store.dispatch(new TrainingProcessCoreActions.ResetState())
                    } else {
                        const activeWs = this.store.selectSnapshot(TrainingProcessStateCore.getActiveWorkoutSet)
                        this.store.dispatch(new Navigate(['user', 'training-process', activeWs.id]));
                    }
                    return false;
                } else {
                    return true;
                }
            })
        );
    }

}
