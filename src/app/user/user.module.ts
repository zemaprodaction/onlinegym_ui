import {NgModule} from '@angular/core';
import {routing} from './user-router';
import * as fromContainers from './containers';
import * as fromComponents from './component';
import {CommonModule} from '@angular/common';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatCardModule} from '@angular/material/card';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {NgxsModule} from '@ngxs/store';
import {TrainingProcessStateList} from './training-process/store';
import {ShareModule} from '../share/share.module';
import {UnsubscribeButtonComponent} from './component/unsubscribe-button/unsubscribe-button.component';
import {MatListModule} from '@angular/material/list';
import {WorkoutProgramPresenterComponent} from './component/workout-program-presenter/workout-program-presenter.component';
import {WorkoutProgramResultComponent} from './containers/workout-program-result/workout-program-result.component';
import {TrainingProcessWidgetStateList} from './training-process/widgets';
import { WorkoutPorgramSchedulerComponent } from './component/workout-porgram-scheduler/workout-porgram-scheduler.component';
import {OffsetTopDirective} from "./directive/offset-top.directive";
import {ScrollableDirective} from "./directive/scrollable.directive";
import {ScrollingModule} from "@angular/cdk/scrolling";
import { SchedulerDayItemComponent } from './component/scheduler-day-item/scheduler-day-item.component';


@NgModule({
    declarations: [...fromContainers.containers, ...fromComponents.components, UnsubscribeButtonComponent,
        WorkoutProgramPresenterComponent,
        WorkoutProgramResultComponent,
        OffsetTopDirective,
        ScrollableDirective,
        WorkoutPorgramSchedulerComponent,
        SchedulerDayItemComponent],
    imports: [
        routing,
        NgxsModule.forFeature([...TrainingProcessStateList, ...TrainingProcessWidgetStateList]),
        CommonModule,
        FlexLayoutModule,
        MatCardModule,
        MatIconModule,
        MatButtonModule,
        MatBottomSheetModule,
        ShareModule,
        MatListModule,
        ScrollingModule,
    ]
})
export class UserModule {
}
