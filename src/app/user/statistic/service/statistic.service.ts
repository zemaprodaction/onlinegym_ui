import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import * as fromConstants from '../../../share/constants/endpointConstants';
import {StatisticConstants} from '../../../share/constants/endpointConstants';
import {catchError, map} from 'rxjs/operators';
import {WorkoutSetEntity} from '../../../share/models/workout/workout-set-entity';
import {Statistic} from '../../../share/models/statistic';
import {ExerciseDataStatistic} from '../../../share/models/exercise/exercise-data-statistic';
import {Measure} from '../../../share/models/measureEntity';
import {TrainingProcessWorkoutSetState} from '../../training-process/store/state/training-process-workout-set.state';
import {Store} from '@ngxs/store';

@Injectable({
  providedIn: 'root'
})
export class StatisticService {

  constructor(private http: HttpClient, private store: Store) {
  }

  public getStatistic(): Observable<Statistic[]> {
    return this.http.get<Statistic[]>(StatisticConstants.ROOT_STATISTIC_WORKOUT_PROGRAM)
      .pipe(catchError((error: any) => throwError(error.json())));
  }

  public updateExerciseStatistic(workoutSet: WorkoutSetEntity) {
    return this.http.put<WorkoutSetEntity>(
      `${fromConstants.StatisticConstants.ROOT_STATISTIC_EXERCISE}`,
      {exerciseId: workoutSet.exerciseId, exerciseData: {listOfMeasures: workoutSet.listOfMeasures}})
      .pipe(catchError((err => throwError(err))));
  }

  public getRecordForExercise(exerciseId: string) {
    return this.http.get<ExerciseDataStatistic>(
      `${fromConstants.StatisticConstants.ROOT_STATISTIC_EXERCISE}/${exerciseId}/record`)
      .pipe(
        map(exData => this.getMoreRecord(exerciseId, exData.measures)),
        catchError((err => throwError(err))));
  }

  private getMoreRecord(exId: string, measureRecord: Measure[]) {
    const localRecordWorkoutSet = this.store.selectSnapshot(TrainingProcessWorkoutSetState.getRecordForExercise(exId));
    const localRecord = TrainingProcessWorkoutSetState.calculateMeasure(localRecordWorkoutSet.listOfMeasures);
    const remoteRecord = TrainingProcessWorkoutSetState.calculateMeasure(measureRecord);
    return localRecord > remoteRecord ? localRecordWorkoutSet.listOfMeasures : measureRecord;
  }
}
