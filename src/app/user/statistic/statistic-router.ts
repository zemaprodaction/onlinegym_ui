import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';

export const USER_ROUTES: Routes = [
  // {
  //   path: `${statisticRouteNames.USER_STATISTICS}`,
  //   component: fromContainers.StatisticComponent,
  // },
];

export const routing: ModuleWithProviders<RouterModule> = RouterModule.forChild(USER_ROUTES);
