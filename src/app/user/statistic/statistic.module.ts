import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {routing} from './statistic-router';

@NgModule({
  declarations: [],
  imports: [
    routing,
    CommonModule,
  ]
})
export class StatisticModule {
}
