import {Component, OnInit} from '@angular/core';
import {UserService} from '../../../core/services/user-service.service';
import {Observable} from 'rxjs';
import {filter, map, switchMap, take} from 'rxjs/operators';
import {WorkoutService} from '../../../core/services/workout.service';
import {WorkoutEntity} from '../../../share/models/workout/workoutEntity';
import {WorkoutInstanceScheduler} from '../../../share/models/workout-instance-scheduler';
import {Store} from '@ngxs/store';
import {WorkoutInstancePhaseEntity} from '../../../share/models/workout/workout-instance-phase-entity';
import {SetCurrentPageName, ShowFab} from '../../../core/store/actions/app-ui.actions';
import {TrainingProcessCoreActions} from '../../training-process/store/actions/training-process-core.actions';
import {Navigate} from "@ngxs/router-plugin";
import {Backdrop} from "../../../core/store/actions/backdrop.actions";

@Component({
  selector: 'app-workout-instance-pre-training',
  templateUrl: './workout-instance-pre-training.component.html',
  styleUrls: ['./workout-instance-pre-training.component.scss']
})
export class WorkoutInstancePreTrainingComponent implements OnInit {

  $workoutData: Observable<{ workout: WorkoutEntity; workoutInstance: WorkoutInstanceScheduler }>;
  $listOfWorkoutPhases: Observable<WorkoutInstancePhaseEntity[]>;

  constructor(private userService: UserService, private workoutService: WorkoutService, private store: Store) {

  }

  ngOnInit(): void {
    this.$workoutData = this.userService.getWorkoutInstanceByParamFromRoute().pipe(
      take(1),
      switchMap(workoutInstanceScheduler => this.workoutService
        .getWorkoutById(workoutInstanceScheduler.workoutInstance.trainingWorkoutId).pipe(
          map(workout => ({workout, workoutInstance: workoutInstanceScheduler}))
        ))
    );
    this.$listOfWorkoutPhases = this.$workoutData.pipe(
      filter(wd => wd !== undefined && wd !== null),
      map(wd => wd.workoutInstance.workoutInstance.listOfWorkoutPhases)
    );
    $localize`:@@login:Login`
    this.store.dispatch(new SetCurrentPageName($localize`:@@workoutPlan:Workout plan`));
    const callBack = () => this.store.dispatch(new Navigate(['user']));
    this.store.dispatch(new Backdrop.UseNavigateBackButton(callBack));
    this.store.dispatch(new ShowFab({
      name: $localize`:@@start:Start`,
      icon: 'play_arrow',
      handler: this.startWorkoutInstance(this.$workoutData),
      isExtend: true
    }));
  }

  startWorkoutInstance(data: Observable<{ workout: WorkoutEntity; workoutInstance: WorkoutInstanceScheduler }>) {
    return () => {
      data.pipe(
        take(1),
      ).subscribe(workoutData => {
        this.store.dispatch(new TrainingProcessCoreActions.StartWorkoutInstanceAction(workoutData.workoutInstance));
      });
    };
  }
}
