import {ComponentFixture, TestBed} from '@angular/core/testing';

import {WorkoutInstancePreTrainingComponent} from './workout-instance-pre-training.component';
import {By} from '@angular/platform-browser';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {NgxsModule} from '@ngxs/store';
import {WorkoutState} from '../../../core/training-store/states/workout.state';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('WorkoutInstancePreTrainingComponent', () => {
  let component: WorkoutInstancePreTrainingComponent;
  let fixture: ComponentFixture<WorkoutInstancePreTrainingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WorkoutInstancePreTrainingComponent],
      imports: [NgxsModule.forRoot([WorkoutState]),
        HttpClientTestingModule
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkoutInstancePreTrainingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contains app-workout-instance-characteristic-card', () => {
    const header = fixture.debugElement
      .query(By.css('app-workout-instance-characteristic-card'));
    expect(header).toBeTruthy();
  });
});
