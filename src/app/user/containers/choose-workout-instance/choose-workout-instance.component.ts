import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

import {Observable} from 'rxjs';
import {filter, map, switchMap} from 'rxjs/operators';
import {Select, Store} from '@ngxs/store';
import {UserState, UserStateModel} from '../../../core/user-store/states/user.state';
import {UserWorkoutProgramStatisticService} from '../../../core/services/statistic/user-workout-program-statistic.service';
import {UserActions} from '../../../core/user-store';
import {ScheduledDay} from '../../../share/models/workout/scheduled-day';
import {ActionButton} from '../../../share/models/action-button';
import {Navigate} from '@ngxs/router-plugin';
import {SetCurrentPageName} from '../../../core/store/actions/app-ui.actions';
import {UserWorkoutProgramDto} from "../../../share/models/user-workout-program";

@Component({
    selector: 'app-process',
    templateUrl: './choose-workout-instance.component.html',
    styleUrls: ['./choose-workout-instance.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChooseWorkoutInstanceComponent implements OnInit {

    @Select(UserState.getUserTrainingProcessInfo)
    userTrainingInfo$: Observable<UserStateModel>;
    @Select(UserState.getToday)
    today: Observable<ScheduledDay>;
    presenterActionButton: ActionButton;
    currentWorkoutProgram$: Observable<UserWorkoutProgramDto>;
    workoutProgramPercentDone$: Observable<number>;

    workoutProgramName$: Observable<string>;
    isDisabled = true
    state: string;
    constructor(private store: Store, private workoutProgramStatisticService: UserWorkoutProgramStatisticService) {
    }

    ngOnInit(): void {
        this.presenterActionButton = {
            action: () => this.store.dispatch(new UserActions.UnsubscribeWorkoutProgramAction()),
            name: $localize`:@@unsubscribe:Unsubscribe`,
            icon: 'delete'
        };
        this.store.dispatch(new UserActions.LoadUserTrainingProcessInfo());
        this.userTrainingInfo$.subscribe(wp => this.initActionButton(wp));
        this.store.dispatch(new SetCurrentPageName($localize`:@@scheduler:Scheduler`));
        this.currentWorkoutProgram$ = this.store.select(UserState.getUserWorkoutProgram);
        this.workoutProgramPercentDone$ = this.currentWorkoutProgram$.pipe(
            filter(wp => wp !== undefined),
            switchMap(currentWp => this.workoutProgramStatisticService.getUserWorkoutProgramStatistic(currentWp.id).pipe(
                map(stat => stat?.percentDone || 0)
            ))
        );
        this.workoutProgramName$ = this.currentWorkoutProgram$.pipe(
            map(wp => wp?.trainingWorkoutProgram.common.name)
        );
    }

    unsubscribe() {
        this.store.dispatch(new UserActions.UnsubscribeWorkoutProgramAction());
    }

    repeateSubscription() {
        this.store.dispatch(new UserActions.EnrollAction(this.store.selectSnapshot(UserState.getTrainingWorkoutProgram).id));
    }

    private initActionButton(wp: UserStateModel) {
        if (wp.userWorkoutProgram) {
            if (wp.userWorkoutProgram.status !== 'FINISHED') {
                this.presenterActionButton = {
                    action: () => this.store.dispatch(new UserActions.UnsubscribeWorkoutProgramAction()),
                    name: $localize`:@@unsubscribe:Unsubscribe`,
                    icon: 'delete'
                };
            } else {
                this.presenterActionButton = {
                    action: () => this.store.dispatch(new Navigate(['workout-programs'])),
                    name: $localize`:@@chooseAnotherProgram:Choose another program`,
                    icon: 'moving'
                };
            }
        }
    }

    restartWp() {
        this.store.dispatch(new UserActions.UserWorkoutProgramRestartAction());
    }
    startWorkoutProgram() {
        this.isDisabled = false
        this.state = "start"
        this.store.dispatch(new UserActions.StartWorkoutProgramAction());
    }
}
