import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {UserWorkoutProgramServiceApi} from '../../../core/services/user-workout-program-api.service';
import {Observable} from 'rxjs';
import {UserWorkoutProgramResult} from '../../../share/models/user-workout-program-result';

@Component({
  selector: 'app-workout-program-result',
  templateUrl: './workout-program-result.component.html',
  styleUrls: ['./workout-program-result.component.scss']
})
export class WorkoutProgramResultComponent implements OnInit {

  @Output()
  public repeateSubscription = new EventEmitter();

  results$: Observable<UserWorkoutProgramResult>;

  constructor(private service: UserWorkoutProgramServiceApi) {
  }

  ngOnInit(): void {
    this.results$ = this.service.getActiveUserWorkoutProgramResult();
  }

  onRepeateSubscription() {
    this.repeateSubscription.emit();
  }
}
