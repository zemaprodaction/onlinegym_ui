import {ChooseWorkoutInstanceComponent} from './choose-workout-instance/choose-workout-instance.component';
import {WorkoutInstancePreTrainingComponent} from './workout-instance-pre-training/workout-instance-pre-training.component';

export const containers = [
  ChooseWorkoutInstanceComponent,
  WorkoutInstancePreTrainingComponent
];

export * from './choose-workout-instance/choose-workout-instance.component';
export * from './workout-instance-pre-training/workout-instance-pre-training.component';

