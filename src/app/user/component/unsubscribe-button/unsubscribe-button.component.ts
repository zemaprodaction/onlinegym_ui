import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-unsubscribe-button',
  templateUrl: './unsubscribe-button.component.html',
  styleUrls: ['./unsubscribe-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UnsubscribeButtonComponent implements OnInit {

  @Input()
  workoutProgram: string;

  @Output()
  clickUnSubscribe = new EventEmitter();

  constructor() {
  }

  ngOnInit(): void {
  }

  unsubscribe() {
    this.clickUnSubscribe.emit();
  }

}
