import {ComponentFixture, TestBed} from '@angular/core/testing';

import {UnsubscribeButtonComponent} from './unsubscribe-button.component';
import {By} from '@angular/platform-browser';

function getMatButton(fixture: ComponentFixture<UnsubscribeButtonComponent>) {
  return fixture.debugElement.query(By.css('button'));
}

describe('UnsubscribeButtonComponent', () => {
  let component: UnsubscribeButtonComponent;
  let fixture: ComponentFixture<UnsubscribeButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UnsubscribeButtonComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UnsubscribeButtonComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  describe('when there is workout program', () => {
    beforeEach(() => {
      component.workoutProgram = 'testId';

    });
    it('should contains button', () => {
      fixture.detectChanges();
      const matButton = getMatButton(fixture);
      expect(matButton).toBeTruthy();
    });
    describe('when click on button', () => {
      it('should emit value', () => {
        fixture.detectChanges();
        jest.spyOn(component.clickUnSubscribe, 'emit');
        const matButton = getMatButton(fixture);
        matButton.nativeElement.dispatchEvent(new Event('click'));

        fixture.detectChanges();

        expect(component.clickUnSubscribe.emit).toHaveBeenCalledTimes(1);
      });
    });

    describe('when workoutProgramId is empty', () => {
      it('should not contains button', () => {
        component.workoutProgram = '';
        fixture.detectChanges();
        const matButton = getMatButton(fixture);
        expect(matButton).toBeFalsy();
      });
    });

  });

  describe('when there is not workout program', () => {
    it('should contains button', () => {
      fixture.detectChanges();
      const matButton = getMatButton(fixture);
      expect(matButton).toBeFalsy();
    });
  });
});
