import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ScheduledDay} from "../../../share/models/workout/scheduled-day";
import {Observable} from "rxjs";
import {Store} from "@ngxs/store";
import {WorkoutState} from "../../../core/training-store/states/workout.state";
import {map} from "rxjs/operators";

@Component({
    selector: 'app-scheduler-day-item',
    templateUrl: './scheduler-day-item.component.html',
    styleUrls: ['./scheduler-day-item.component.scss']
})
export class SchedulerDayItemComponent implements OnInit {

    @Input()
    schedulerDay: ScheduledDay;
    @Input()
    isLock: boolean;
    @Input()
    enableStartButton: boolean;

    @Output()
    onStartWorkoutInstance = new EventEmitter<string>()

    name$: Observable<String>

    constructor(private store: Store) {
    }

    ngOnInit(): void {
        this.name$ = this.store.select(WorkoutState.getWorkoutById(this.schedulerDay.userWorkoutInstanceList[0].trainingWorkoutId)).pipe(
            map(workout => workout.common.name)
        )
    }

    startWorkoutInstance(id: string) {
        this.onStartWorkoutInstance.emit(id)
    }
}
