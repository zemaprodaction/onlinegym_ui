import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {WorkoutProgramEntity} from '../../../share/models/workout-programs';
import {ActionButton} from '../../../share/models/action-button';

@Component({
    selector: 'app-workout-program-presenter',
    templateUrl: './workout-program-presenter.component.html',
    styleUrls: ['./workout-program-presenter.component.scss']
})
export class WorkoutProgramPresenterComponent implements OnInit {

    @Input()
    workoutProgram: WorkoutProgramEntity;
    @Input()
    workoutProgramPercentDone: number;
    @Input()
    action: ActionButton;

    @Output()
    unsubscribe = new EventEmitter();

    @Output()
    restart = new EventEmitter<WorkoutProgramEntity>();

    constructor() {
    }

    ngOnInit(): void {
    }

    onUnsubscribe() {
        this.unsubscribe.emit();
    }

    restartWorkoutProgram() {
      this.restart.emit(this.workoutProgram)
    }
}
