import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-start-workout-program-button',
  templateUrl: './start-workout-program-button.component.html',
  styleUrls: ['./start-workout-program-button.component.scss']
})
export class StartWorkoutProgramButtonComponent implements OnInit {

  @Output()
  startWP = new EventEmitter();

  constructor() {
  }

  ngOnInit(): void {
  }

  onStart() {
    this.startWP.emit();
  }

}
