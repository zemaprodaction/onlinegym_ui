import {
    AfterViewInit,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    Input,
    OnChanges,
    OnInit,
    SimpleChanges,
    ViewChild,
    ViewEncapsulation
} from '@angular/core';
import {ScheduledDay} from "../../../share/models/workout/scheduled-day";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {SnackBarService} from "../../../core/services/snack-bar.service";
import {OffsetTopDirective} from "../../directive/offset-top.directive";
import {ScrollableDirective} from "../../directive/scrollable.directive";
import {DateService} from "../../../share/utils/date.service";
import {UserState} from "../../../core/user-store/states/user.state";

@Component({
    selector: ' app-workout-porgram-scheduler',
    templateUrl: './workout-porgram-scheduler.component.html',
    styleUrls: ['./workout-porgram-scheduler.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
})
export class WorkoutPorgramSchedulerComponent implements OnInit, AfterViewInit, OnChanges {
    @ViewChild(OffsetTopDirective) listItems: OffsetTopDirective;
    @ViewChild(ScrollableDirective) list: ScrollableDirective;

    @Input()
    calendar: ScheduledDay[]

    @Input()
    today: ScheduledDay

    state: string;
    isDisabled = true

    lockDays = 0
    // availableDays: ScheduledDay[]

    currentIndex = 0
    isRest = false

    constructor(private cdr: ChangeDetectorRef, private store: Store, private snackBarService: SnackBarService) {

    }

    ngOnInit(): void {
        let todayDate = DateService.convertDataToBackEndPattern(new Date())
        let todayWorkoutDay = this.store.selectSnapshot(UserState.getToday)
        this.currentIndex = this.calendar.findIndex(day => {
            if (day) {
                return DateService.convertDataToBackEndPattern(new Date(day.date + 'Z')) >= todayDate
            }
        })
        if (todayWorkoutDay == null) {
            this.calendar = this.insert(this.calendar, this.currentIndex, null)
        }
    }

    startWorkoutInstance(dayIndex: string, isLock = false) {
        if (isLock) {
            this.snackBarService.openSnackBar('Эта тренировка еще недоступна')
        } else {
            this.store.dispatch(new Navigate(['user', 'training-info',
                dayIndex]));
        }
    }

    ngAfterViewInit(): void {
        if (this.calendar) {
            this.list.scrollTop = this.listItems.offsetTop;
        }
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.ngOnInit()
    }

    insert(arr, index, newItem) {
        return [
            // part of the array before the specified index
            ...arr.slice(0, index),
            // inserted item
            newItem,
            // part of the array after the specified index
            ...arr.slice(index)
        ]
    }

}
