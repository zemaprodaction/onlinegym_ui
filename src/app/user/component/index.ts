
import {StartWorkoutProgramButtonComponent} from './start-workout-program-button/start-workout-program-button.component';
import {WorkoutRestDayCardComponent} from './workout-rest-day-card/workout-rest-day-card.component';

export const components: any[] = [StartWorkoutProgramButtonComponent, WorkoutRestDayCardComponent];

export * from './start-workout-program-button/start-workout-program-button.component';
export * from './workout-rest-day-card/workout-rest-day-card.component';
