import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {userRouteNames} from './routes.name';
import * as fromContainers from './containers';
import {appRoutesNames} from '../routes.names';
import {trainingRoutesNames} from '../trainings/routes.names';
import {TrainingInProcessGuard} from './guard/training-in-process.guard';
import {AuthGuard} from '../share/guards/auth.guard';
import {StoreBecomeNotResatable} from "./guard/store-become-not-resatable";
import {HideLoginButton} from "./guard/hide-login-button";

export const USER_ROUTES: Routes = [
    {
        path: `${userRouteNames.USER_WORKOUT_PROGRAMS}`,
        component: fromContainers.ChooseWorkoutInstanceComponent,
        canActivate: [AuthGuard, TrainingInProcessGuard],
        canActivateChild: [AuthGuard],
        runGuardsAndResolvers: 'always',
        data: {
            backdropSubheaderSettings: {
                closeTitle: 'Вы занимаетесь по программе',
                openTitle: 'Вы занимаетесь по программе'
            },
            currentNamePage: 'Тренировка',
            animationState: 'user-workout-program'
        }
    },
    {
        path: `${appRoutesNames.TRAINING_PROCESS}`,
        loadChildren: () => import('./training-process/training-process.module').then(m => m.TrainingProcessModule),
        canActivate: [StoreBecomeNotResatable, HideLoginButton],
        data: {preload: true}
    },
    {
        path: `${userRouteNames.PRE_TRAINING_INFO}/:dayIndex`,
        canActivate: [],
        component: fromContainers.WorkoutInstancePreTrainingComponent,
        data: {animationState: 'pre-training-info'}
    },
    {
        path: '',
        pathMatch: 'full',
        redirectTo: `${trainingRoutesNames.WORKOUT_PROGRAM}`
    },
];

export const routing: ModuleWithProviders<RouterModule> = RouterModule.forChild(USER_ROUTES);
