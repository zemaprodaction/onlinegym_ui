import {Injectable} from '@angular/core';
import {WorkoutSet} from '../../../share/models/workout/workout-set-entity';
import {EditWorkoutSetComponent} from '../dialogs/edit-workout-set/edit-workout-set.component';
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {MatDialog} from '@angular/material/dialog';

@Injectable({
  providedIn: 'root'
})
export class AuxiliaryComponentService {

  dialogRef;

  constructor(private _bottomSheet: MatBottomSheet, public dialog: MatDialog) {
  }

  openEditWindow(set: WorkoutSet): MatBottomSheetRef {
    return this._bottomSheet.open(EditWorkoutSetComponent, {
      closeOnNavigation: true,
      backdropClass: 'transparent',
      data: set, panelClass: ['edit-bottom-sheet',
        'glass-morphism']
    });
  }

  infoBottomSheet(component, data?) {
    this._bottomSheet.open(component, {
      data,
      backdropClass: 'transparent',
      panelClass: ['bottom-sheet-full-width', 'glass-morphism']
    });
  }

  openFullWindowDialog(component, data?) {
    return this.dialog.open(component, {
      width: '100vw',
      maxWidth: '100vw',
      backdropClass: 'transparent',
      height: '100vh',
      panelClass: 'glass-morphism',
      data
    });
  }
}
