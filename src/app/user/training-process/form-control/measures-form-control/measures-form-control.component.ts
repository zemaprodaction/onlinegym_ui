import {Component, Input, OnInit} from '@angular/core';
import {AbstractFormControlComponent} from '../abstract-form-control/abstract-form-control.component';
import {Measure} from '../../../../share/models/measureEntity';
import {FormArray, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-measures-form-control',
  templateUrl: './measures-form-control.component.html',
  styleUrls: ['./measures-form-control.component.scss']
})
export class MeasuresFormControlComponent extends AbstractFormControlComponent implements OnInit {

  @Input()
  measures: Measure[];

  constructor() {
    super();
  }

  ngOnInit(): void {
    (this.parent as FormGroup).setControl(this.controlName, this.initForm());
  }

  private initForm() {
    const measuresArray = this.fb.array([]);
    this.measures.forEach(m => measuresArray.push(
      this.fb.group({
        ...m,
        actualValue: this.fb.control(m.actualValue ? m.actualValue : m.expectedValue)
      })));
    return measuresArray;
  }

  get measuresControl(): any[] {
    return (this.parent.get(this.controlName) as FormArray).controls;
  }

}
