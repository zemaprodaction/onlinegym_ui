import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {AppInjector} from '../../../../core/services/app/app-injector.service';

@Component({
  selector: 'app-abstract-form-control',
  templateUrl: './abstract-form-control.component.html',
  styleUrls: ['./abstract-form-control.component.scss']
})
export class AbstractFormControlComponent implements OnInit {

  @Input()
  parent: any;

  @Input()
  controlName: string;

  fb: FormBuilder;

  constructor() {
    const injector = AppInjector.getInjector();
    this.fb = injector.get(FormBuilder);
  }

  ngOnInit(): void {

  }

}
