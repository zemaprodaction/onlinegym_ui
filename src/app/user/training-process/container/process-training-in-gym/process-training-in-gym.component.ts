import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {BaseComponent} from '../base-process-training/base-process-training.component';
import {WorkoutSet} from '../../../../share/models/workout/workout-set-entity';
import {WorkoutInstancePhase} from '../../../../share/models/workout/workout-instance-phase-entity';
import {TrainingProcessWorkoutSetActions} from '../../store/actions/training-process-workout-set.action';
import {BackdropService} from '../../../../core/services/backdrop/backdrop.service';
import {Select} from '@ngxs/store';
import {BehaviorSubject, Observable, of, Subject, Subscription, timer} from 'rxjs';
import {Measure} from '../../../../share/models/measureEntity';
import {TrainingProcessStateCore} from '../../store/state/training-process-core.state';
import {WorkoutProcessStatusEnum} from '../../model/workout-process-status.enum';
import {TrainingProcessCoreActions} from '../../store/actions/training-process-core.actions';
import {TrainingProcessTimerActions} from '../../widgets/timer/actions/training-process-timer.action';
import {TrainingProcessRestState} from '../../widgets/rest/state/training-process-rest.state';
import {TrainingProcessRestActions} from '../../widgets/rest/actions/training-process-rest.action';
import {filter, map, switchMap, take, takeUntil, withLatestFrom} from 'rxjs/operators';
import {SetCurrentPageName} from '../../../../core/store/actions/app-ui.actions';
import {AuxiliaryComponentService} from '../../services/auxiliary-component.service';
import {PersonalRecordComponent} from '../../dialogs/personal-record/personal-record.component';
import {ActionButton} from '../../../../share/models/action-button';
import {Toolbar} from '../../../../core/store/actions/toolbar.action';
import {ToolbarAction} from '../../../../share/models/toolbar-action';
import {ToolBarButton} from '../../../../share/models/tool-bar-button';
import {BackdropStatus} from '../../../../core/store';
import {WorkoutSchedulerListComponent} from '../../component/workout-scheduler-list/workout-scheduler-list.component';
import {TrainingProcessTimerState} from "../../widgets/timer/state/training-process-timer.state";
import {Backdrop} from "../../../../core/store/actions/backdrop.actions";
import {TrainingProcessTimerComponent} from "../../component/training-process-timer/training-process-timer.component";
import {workoutSetNavigation} from "../../../../route-transition-animations";
import {Location} from '@angular/common';
import {ProgressController} from "../../component/progress-bar/progress-bar.component";
import {FormBuilder, FormGroup} from "@angular/forms";

@Component({
    selector: 'app-process-training-in-gym',
    templateUrl: './process-training-in-gym.component.html',
    styleUrls: ['./process-training-in-gym.component.scss'],
    animations: [workoutSetNavigation]
})
export class ProcessTrainingInGymComponent extends BaseComponent implements OnInit, OnDestroy {

    @Select(TrainingProcessStateCore.getCurrentWorkoutPhase)
    $activeWorkoutPhase: Observable<WorkoutInstancePhase>;

    @Select(TrainingProcessStateCore.getActiveWorkoutSet)
    $activeWorkoutSet: Observable<WorkoutSet>;

    @Select(TrainingProcessStateCore.getStatus)
    $status: Observable<WorkoutProcessStatusEnum>;

    @Select(TrainingProcessRestState.getRestTime)
    $rest: Observable<number>;

    $progress: Observable<number>;

    $measures: BehaviorSubject<Measure[]> = new BehaviorSubject<Measure[]>([]);

    $progressControlers: Observable<ProgressController[]>;

    fab: ActionButton = {};
    subscription: Subscription;
    pauseMeasureTimeButton: ActionButton;
    startMeasureTimerButton: ActionButton;
    workoutSetDoneButton: ActionButton;
    timerSubscription: Subscription;
    workoutSetTimerComplete = new Subject<number>();
    totalTime = 0;
    timerPercentValue: Observable<number>;
    status: 'time_measure' | 'usual_measure' = 'usual_measure';
    timeFinished = false;
    animationState = new BehaviorSubject(0)

    constructor(private backdropService: BackdropService,
                private cdr: ChangeDetectorRef,
                private _location: Location,
                private fb: FormBuilder,
                private auxiliaryComponentService: AuxiliaryComponentService) {
        super();
    }

    stopMeasureTimer = () => {
        this.fab = this.startMeasureTimerButton;
        this.$measures.pipe(
            take(1),
            withLatestFrom(this.$activeWorkoutSet)).subscribe(([a1, a2]) => {
            let m = a2.listOfMeasures.find(mes => mes.measureType.id === 'time');
            m = {...m, actualValue: a1.find(mes => mes.measureType.id === 'time').currentValue}
            this.store.dispatch(
                new TrainingProcessWorkoutSetActions.UpdateActualValue({
                    workoutSetId: a2.id,
                    measures: [m, ...a2.listOfMeasures.filter(mes => mes.measureType.id !== 'time')]
                }));
        });
        this.store.dispatch(new TrainingProcessCoreActions.FinishActiveWorkoutSet());
        this.totalTime = 0;
        this.workoutSetTimerComplete.next(this.totalTime);
        this.timerSubscription.unsubscribe();
        this.timerSubscription = undefined;
        this.status = 'usual_measure';

        this.timeFinished = false;

        this.timerPercentValue = of(0);
        this.cdr.detectChanges();
    };

    finishedWorkoutSet = () => {
        this.totalTime = 0;
        this.store.dispatch(new TrainingProcessCoreActions.FinishActiveWorkoutSet());
    };
    startMeasureTimer = () => {
        this.store.dispatch(new Toolbar.ResetToolbarButton());
        this.fab = this.pauseMeasureTimeButton;
        this.status = 'time_measure';
        if (!this.timerSubscription) {
            this.timerSubscription = timer(1000, 1000).pipe(
                takeUntil(this.workoutSetTimerComplete)
            ).subscribe(_ => {
                if (!this.isPause) {
                    this.totalTime = this.totalTime + 1;
                    let measures = this.$measures.getValue();
                    let timeMeasure = {...measures.find(m => m.measureType.id === 'time')};
                    timeMeasure = {...timeMeasure, actualValue: 0};
                    if (timeMeasure.expectedValue === 0 || this.timeFinished) {
                        measures = measures.filter(m => m.measureType.id !== 'time');
                        this.$measures.next([...measures, {...timeMeasure, expectedValue: this.totalTime, currentValue: this.totalTime}]);
                        this.timerPercentValue = of(100);
                        this.timeFinished = true;
                    } else {
                        measures = measures.filter(m => m.measureType.id !== 'time');
                        this.$measures.next([...measures, {
                            ...timeMeasure,
                            expectedValue: timeMeasure.expectedValue - 1,
                            currentValue: this.totalTime
                        }]);
                    }
                }
            });
        }
        this.cdr.detectChanges();
    };

    ngOnInit(): void {
        super.ngOnInit();
        const toolbarAction: ToolbarAction = {
            button: new ToolBarButton($localize`:@@scheduler:Scheduler`, '', 'list',
                () => this.openTrainingScheduler()),
            status: BackdropStatus.OPEN,
            isUseBackdropContext: true
        };

        this.backdropService.addActionToBackdrop(toolbarAction);

        this.store.dispatch(new TrainingProcessCoreActions.WorkoutProcessLoaded())

        this.pauseMeasureTimeButton = {icon: 'done', action: this.stopMeasureTimer};
        this.startMeasureTimerButton = {icon: 'play_arrow', action: this.startMeasureTimer};
        this.workoutSetDoneButton = {icon: 'done', action: this.finishedWorkoutSet};
        this.$activeWorkoutPhase.pipe(
            takeUntil(this.$status.pipe(filter(status =>
                status === WorkoutProcessStatusEnum.finished || status === undefined || status === null)))
        ).subscribe(phase => {
            if (phase) {
                this.store.dispatch(new SetCurrentPageName(phase.name));
            }
        });
        const showTimerAction: ToolbarAction = {
            button: new ToolBarButton($localize`:@@generalTime:General Time`, '', 'query_builder',
                () => this.openTrainingTimer()),
            status: BackdropStatus.OPEN,
            isUseBackdropContext: true
        };
        this.backdropService.addActionToBackdrop(showTimerAction);

        this.$progress = this.$activeWorkoutPhase.pipe(
            filter(phase => phase !== undefined && phase !== null),
            map(phase => {
                const allWorkoutSet = phase.listOfSets.length;
                const doneWorkoutSet = phase.listOfSets.filter(ws => ws.isDone).length;
                return (doneWorkoutSet / allWorkoutSet) * 100;
            })
        );
        this.subscription = this.$activeWorkoutSet.subscribe(ws => {
            if (ws) {
                this.timerPercentValue = this.$measures.pipe(
                    filter(m => m.find(mes => mes.measureType.id === 'time') !== undefined),
                    map(m => m.find(mes => mes.measureType.id === 'time').expectedValue),
                    map(val => (this.totalTime / (this.totalTime + val)) * 100),
                );
                this.$measures.next(ws.listOfMeasures);
                const timeMeasure = ws.listOfMeasures.find(m => m.measureType.id === 'time');
                if (timeMeasure) {
                    this.fab = this.startMeasureTimerButton;
                } else {
                    this.fab = this.workoutSetDoneButton;
                }
            }
        });
        this.subscription.add(this.$activeWorkoutSet.subscribe(ws => {
            if (ws) {
                this.store.dispatch(new Backdrop.SetUpCloseBackdropSubHeaderTitle({
                    closeTitle: ws?.exercise?.name,
                    openTitle: ws?.exercise?.name
                }));
            }
        }))
        this.isPause = this.store.selectSnapshot(TrainingProcessTimerState.getIsPause)
        this.$progressControlers = this.$activeWorkoutPhase.pipe(
            withLatestFrom(this.$activeWorkoutSet),
            filter(([phase]) => phase !== undefined),
            map(([phase, activeSet]) => phase.listOfSets.map(set => activeSet.id === set.id ? {state: "active"}
                : set.isDone ? {state: "finished"} : {state: 'non_active'}))
        )
    }

    openBottomSheet(): void {
        this.subscription.add(this.$activeWorkoutSet.pipe(
            take(1),
            switchMap(set => this.auxiliaryComponentService.openEditWindow(set).afterDismissed())).subscribe(
            val => {
                if (val) {
                    this.updateWorkoutSet({
                        workoutSetId: this.store.selectSnapshot(TrainingProcessStateCore.getActiveWorkoutSetId),
                        listOfMeasures: val.workoutSet.listOfMeasures
                    });
                }
            }
        ));
    }


    updateWorkoutSet(workoutSet: { workoutSetId: string; listOfMeasures: Measure[] }) {
        this.store.dispatch(
            new TrainingProcessWorkoutSetActions.UpdateActualValue({
                workoutSetId: workoutSet.workoutSetId,
                measures: workoutSet.listOfMeasures
            }));
    }

    skipCurrentWorkoutSet() {
        if (this.timerSubscription) {
            this.timerSubscription.unsubscribe()
        }
        this.totalTime = 0;
        this.store.dispatch(new TrainingProcessCoreActions.SkipCurrentWorkoutSet());
    }

    pause() {
        if (this.isPause) {
            this.store.dispatch(new TrainingProcessTimerActions.ResumeTimer());
        } else {
            this.store.dispatch(new TrainingProcessTimerActions.PauseTimer());
        }
        this.isPause = !this.isPause;
    }

    openPersonalRecord(workoutSet: Observable<WorkoutSet>) {
        workoutSet.pipe(take(1)).subscribe(ws => {
            this.auxiliaryComponentService.infoBottomSheet(PersonalRecordComponent, ws.exercise);
        });

    }


    breakRest($event: number) {
        this.store.dispatch(new TrainingProcessRestActions.StopRest($event));
    }

    get formatPause() {
        return this.isPause ? 'play_arrow' : 'pause';
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        if (this.timerSubscription) {
            this.timerSubscription.unsubscribe();
        }
    }

    back() {
        this._location.back()
    }

    private openTrainingScheduler() {
        this.backdropService.openBackdropWithComponentCallback(WorkoutSchedulerListComponent, {
            webDownToHeight: 356,
            mobileDownToHeight: 356,
            nameBackdropPage: $localize`:@@scheduler:Scheduler`,
            customHeightOfBackdrop: 64
        });
    }

    private openTrainingTimer() {
        this.backdropService.openBackdropWithComponentCallback(TrainingProcessTimerComponent, {
            webDownToHeight: 64,
            mobileDownToHeight: 64,
            nameBackdropPage: $localize`:@@trainingTime:Training Time`,
            customHeightOfBackdrop: 64
        });
    }

}
