import {BaseComponent} from './base-process-training.component';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {Injector} from '@angular/core';
import {AppInjector} from '../../../../core/services/app/app-injector.service';
import {NgxsModule, Store} from '@ngxs/store';
import {AppStates} from '../../../../core/store';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {timer} from 'rxjs';
import {TrainingProcessStateList} from '../../store';
import {TimerService} from '../../../../core/services/timer.service';
import {WorkoutProcessStatusEnum} from '../../model/workout-process-status.enum';

describe('Process training component', () => {
  let component: BaseComponent;
  let fixture: ComponentFixture<BaseComponent>;
  let store: Store;
  let timerService: TimerService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        BaseComponent
      ],
      imports: [
        NgxsModule.forRoot([...AppStates, ...TrainingProcessStateList]),
        HttpClientTestingModule
      ]
    }).compileComponents().then(() => {
      AppInjector.setInjector(TestBed.inject(Injector));
      fixture = TestBed.createComponent(BaseComponent);
      component = fixture.componentInstance;
      store = TestBed.inject(Store);
      timerService = TestBed.inject(TimerService);
    });
  });

  it('should be created', () => {
    expect(BaseComponent).toBeTruthy();
  });

});
