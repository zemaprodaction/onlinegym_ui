import {Component, OnDestroy, OnInit} from '@angular/core';
import {AppInjector} from '../../../../core/services/app/app-injector.service';
import {Observable, Subscription} from 'rxjs';
import {TimerService} from '../../../../core/services/timer.service';
import {Select, Store} from '@ngxs/store';
import {WorkoutInstancePhase} from '../../../../share/models/workout/workout-instance-phase-entity';
import {WorkoutInstancePhaseState} from '../../store/state/workout-instance-phase-state';
import {TrainingProcessCoreActions} from '../../store/actions/training-process-core.actions';

@Component({
  template: ''
})
export class BaseComponent implements OnInit, OnDestroy {

  @Select(WorkoutInstancePhaseState.selectAllWorkoutInstancePhase)
  workoutPhases$: Observable<WorkoutInstancePhase[]>;

  timerSubscription!: Subscription;
  timer!: Observable<number>;
  isPause = false;
  shoudStop = false;

  protected store: Store;


  private timerService: TimerService;

  constructor() {
    const injector = AppInjector.getInjector();
    this.timerService = injector.get(TimerService);
    this.store = injector.get(Store);

  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
    }
  }
}
