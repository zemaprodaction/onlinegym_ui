import {WorkoutInstanceEntity} from '../../../../share/models/workout/workout-instance-entity';
import {Measure} from '../../../../share/models/measureEntity';
import {WorkoutInstanceScheduler} from '../../../../share/models/workout-instance-scheduler';


export namespace TrainingProcessCoreActions {

  export class StartWorkoutInstanceAction {

    public static readonly type = '[Training Process] Start Workout Instance';

    constructor(public payload: WorkoutInstanceScheduler) {
    }
  }

  export class SetUpNewActiveWorkoutPhase {

    public static readonly type = '[Training Process] Set Up New Active Workout Phase';

    constructor(public workoutPhaseId: string) {
    }
  }
  export class SetUpNewActiveWorkoutPhaseManual {

    public static readonly type = '[Training Process] Set Up New Active Workout Phase Manual';

    constructor(public workoutPhaseId: string) {
    }
  }

  export class InitWorkoutPhase {

    public static readonly type = '[Training Process] Init workout phase';

    constructor(public workoutPhaseId: string) {
    }
  }


  export class ChangeDifficalty {

    public static readonly type = '[Training Process] Change Workout Difficalty';

    constructor(public difficultLevel: number) {
    }
  }

  export class SetUpNextActiveWorkoutPhase {

    public static readonly type = '[Training Process] Set Up Next Active Workout Phase';

  }

  export class SetUpStatus {

    public static readonly type = '[Training Process] Set Up Status';

    constructor(public status: string) {
    }
  }
  export class WorkoutProcessLoaded {

    public static readonly type = '[Training Process] Workout Process Loaded';
  }

  export class StartActiveWorkoutPhase {

    public static readonly type = '[Training Process] Start current workout phase';

  }

  export class StartActiveWorkoutSet {

    public static readonly type = '[Training Process] Start active workout set';
  }

  export class ExitFromTrainingProcess {

    public static readonly type = '[Training Process] Exit From Training Process';
  }


  export class SetUpNewActiveWorkoutSet {

    public static readonly type = '[Training Process] Set up new active workout set';

    constructor(public workoutSetId: string) {
    }
  }
  export class SetUpNewActiveWorkoutSetManual {

    public static readonly type = '[Training Process] Set up new active workout set manual';

    constructor(public workoutSetId: string) {
    }
  }

  export class FinishActiveWorkoutSet {

    public static readonly type = '[Training Process] Finish active workout set';
  }

  export class FinishActiveWorkoutSetWithCustomMeasure {

    public static readonly type = '[Training Process] Finish active workout set with custom measure';
  }

  export class FinishedWorkoutInstance {

    public static readonly type = '[Training Process] Finish Workout Instance';
  }

  export class SendWorkoutInstanceStatistic {

    public static readonly type = '[Training Process] Send Workout Instance Statistic';

    constructor(public payload: WorkoutInstanceEntity) {
    }
  }

  export class ChangeWorkoutSetAndWorkoutPhase {
    public static readonly type = '[Training Process] Change Workout Set And Workout Phase';

    constructor(public workoutSetId: string) {
    }
  }

  export class FinishedWorkoutSetFromBackdrop {

    public static readonly type = '[Training Process] Finished Workout Set From Backdrop';

    constructor(public payload: { idWorkoutSet: string; measure: Measure[] }) {
    }
  }

  export class SkipCurrentWorkoutSet {
    public static readonly type = '[Training Process] Skip current workout set';

  }

  export class GoToNextWorkoutPhase {
    public static readonly type = '[Training Process] Go To Next Workout Phase';

  }

  export class GoToNextWorkoutPhaseManually {
    public static readonly type = '[Training Process] Go To Next Workout Phase Manually';

  }

  export class ContinuousTraining {

    public static readonly type = '[Training Process] Continuous Training';
  }

  export class ResetState {

    public static readonly type = '[Training Process] Reset State';
  }
}








