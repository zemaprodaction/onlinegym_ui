import {Measure} from '../../../../share/models/measureEntity';
import {WorkoutInstancePhaseEntity} from '../../../../share/models/workout/workout-instance-phase-entity';

export namespace TrainingProcessWorkoutSetActions {

  export class DoneWorkoutSet {
    public static readonly type = '[Training Process] Update Workout Set Done';

    constructor(public workoutSetId: string) {
    }
  }

  export class AddWorkoutSetToTrainingProcess {
    public static readonly type = '[Training Process] Add Workout Set To Training Process';

    constructor(public payload: WorkoutInstancePhaseEntity[]) {
    }
  }

  export class FinishAllWorkoutSet {
    public static readonly type = '[Training Process] Finish All WorkoutSet';

    constructor(public payload: string[]) {
    }
  }

  //-----------------------------------------------------------


  export class UpdateActualValue {
    public static readonly type = '[Training Process] Update Actual Value For Workout Set';

    constructor(public payload: {
      workoutSetId: string;
      measures: Measure[];
    }) {
    }
  }

  export class CancelWorkoutSet {

    public static readonly type = '[Training Process] Cancel Workout Set';

    constructor(public workoutSetId: string) {
    }
  }

  export class UpdateRestTime {

    public static readonly type = '[Training Process] Update Rest Time';

    constructor(public payload: { idWorkoutSet: string; actualRestTime: number }) {
    }
  }

}



