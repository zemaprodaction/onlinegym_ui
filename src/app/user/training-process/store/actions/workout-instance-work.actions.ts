export namespace WorkoutInstanceWorkAction {

  export class SetUpActiveWorkoutInstancePhase {

    public static readonly type = '[Training Process] Set Up Active Workout Instance Phase';

    constructor(public payload: string) {
    }
  }

    export class SetUpActiveWorkoutSet {
      public static readonly type = '[Training Process] Set Up Active Workout Set';

      constructor(public payload: {workoutPhaseName: string, workoutSetIndex}) {
      }
    }
}








