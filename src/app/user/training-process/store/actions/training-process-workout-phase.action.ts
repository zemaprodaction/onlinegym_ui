import {WorkoutInstancePhaseEntity} from '../../../../share/models/workout/workout-instance-phase-entity';

export namespace TrainingProcessWorkoutPhaseActions {

  export class AddWorkoutPhaseToTrainingProcess {
    public static readonly type = '[Training Process Workout Phase] Add Workout Phase List To Training Process';

    constructor(public payload: WorkoutInstancePhaseEntity[]) {
    }
  }
  export class SkipPhase {
    public static readonly type = '[Training Process Workout Phase] Skip workout phase';

    constructor(public workoutPhaseId: string) {
    }
  }
  export class DisablePhase {
    public static readonly type = '[Training Process Workout Phase] Disable phase';

    constructor(public workoutPhaseId: string) {
    }
  }
  export class FinishedAllWorkoutSetInPhaseBatch {

    public static readonly type = '[Training Process Workout Phase] Finished Bactch Workout Set';

    constructor(public phaseId: string) {
    }
  }
}



