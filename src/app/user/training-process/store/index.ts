import {WorkoutInstancePhaseState} from './state/workout-instance-phase-state';
import {TrainingProcessWorkoutSetState} from './state/training-process-workout-set.state';
import {TrainingProcessStateCore} from './state/training-process-core.state';


export const TrainingProcessStateList = [TrainingProcessStateCore,
  WorkoutInstancePhaseState, TrainingProcessWorkoutSetState];

