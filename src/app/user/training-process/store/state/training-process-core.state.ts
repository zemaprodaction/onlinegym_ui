import {Action, createSelector, Selector, State, StateContext, Store} from '@ngxs/store';
import {Injectable} from '@angular/core';
import {WorkoutProcessStatusEnum} from '../../model/workout-process-status.enum';
import {WorkoutInstanceEntity} from '../../../../share/models/workout/workout-instance-entity';
import {WorkoutInstancePhaseState, WorkoutInstancePhaseStateModel} from './workout-instance-phase-state';
import {TrainingProcessWorkoutSetState, TrainingProcessWorkoutSetStateModel} from './training-process-workout-set.state';
import {TrainingProcessWorkoutPhaseActions} from '../actions/training-process-workout-phase.action';
import {TrainingProcessWorkoutSetActions} from '../actions/training-process-workout-set.action';
import {WorkoutInstanceScheduler} from '../../../../share/models/workout-instance-scheduler';
import {tap} from 'rxjs/operators';
import {TrainingProcessCoreActions} from '../actions/training-process-core.actions';
import {Navigate, RouterState} from '@ngxs/router-plugin';
import {ExerciseState} from '../../../../core/training-store/states/exercise.state';
import {MeasureTypeState} from '../../../../core/store/states/metadata-state/measure-types.state';
import {UserActions} from '../../../../core/user-store';
import {WorkoutProcessService} from '../../../../core/services/workout-process.service';
import {UserStatisticActions} from '../../../../user-progress/store/action/user-state.action';
import {WorkoutProgramStatistic} from '../../../../share/models/workout-program-statistic';
import {TrainingProcessTimerState} from '../../widgets/timer/state/training-process-timer.state';
import {WorkoutInstancePhase} from '../../../../share/models/workout/workout-instance-phase-entity';
import {TrainingProcessTimerActions} from "../../widgets/timer/actions/training-process-timer.action";
import {WidgetActions} from "../../widgets/store/widget.actions";
import {RouterStateModel} from "../../../../router-state.model";

export interface TrainingProcessCoreStateModel {
    status: string;
    workoutInstanceEntity: WorkoutInstanceEntity;
    activeWorkoutPhaseId: string;
    activeWorkoutSetIndex: number;
    date: string;
    isStarted: boolean;
    howWorkoutDifficult: number;
}

export const initialState: TrainingProcessCoreStateModel = {
    status: null,
    workoutInstanceEntity: null,
    activeWorkoutPhaseId: null,
    activeWorkoutSetIndex: null,
    date: null,
    isStarted: false,
    howWorkoutDifficult: 3
};

@State<TrainingProcessCoreStateModel>({
    name: 'trainingProcess',
    defaults: {
        ...initialState
    },
    children: [WorkoutInstancePhaseState,
        TrainingProcessWorkoutSetState]
})
@Injectable()
export class TrainingProcessStateCore {

    constructor(private store: Store, private service: WorkoutProcessService) {
    }

    static getNextUnDoneWorkoutSetInPhase(phaseId: string) {
        return createSelector([TrainingProcessStateCore, WorkoutInstancePhaseState.selectWorkoutInstancePhaseByName(phaseId), RouterState.state],
            (state: TrainingProcessCoreStateModel, phase: WorkoutInstancePhase, routerState: RouterStateModel) => {
                const indexCurrentWorkoutSet = routerState.params['workoutSetId'].split('_')[1]
                let nextWs = phase.listOfSets.slice(Number(indexCurrentWorkoutSet) + 1).find(ws => !ws.isDone);
                if (!nextWs) {
                    nextWs = phase.listOfSets.find(ws => !ws.isDone);
                }
                return nextWs;
            }
        );
    }

    static getNexWorkoutSetInPhase(phaseId: string) {
        return createSelector([TrainingProcessStateCore, WorkoutInstancePhaseState.selectWorkoutInstancePhaseByName(phaseId), RouterState.state],
            (state: TrainingProcessCoreStateModel, phase: WorkoutInstancePhase, routerState: RouterStateModel) => {
                const indexCurrentWorkoutSet = routerState.params['workoutSetId'].split('_')[1]
                let nextWs = phase.listOfSets.slice(Number(indexCurrentWorkoutSet) + 1)[0]
                if (!nextWs) {
                    nextWs = phase.listOfSets[0];
                }
                return nextWs;
            }
        );
    }

    public static getNextWorkoutSetInPhaseInCurrentPhase() {
        return createSelector([TrainingProcessStateCore, WorkoutInstancePhaseState, TrainingProcessWorkoutSetState],
            (state: TrainingProcessCoreStateModel, phaseState: WorkoutInstancePhaseStateModel, wsSet: TrainingProcessWorkoutSetStateModel) => {
                const phase = WorkoutInstancePhaseState.selectWorkoutInstancePhaseByName(state.activeWorkoutPhaseId)(phaseState, wsSet);
                let nextWs = phase.listOfSets.slice(state.activeWorkoutSetIndex + 1).find(ws => !ws.isDone);
                if (!nextWs) {
                    nextWs = phase.listOfSets.find(ws => !ws.isDone);
                }
                return nextWs;
            }
        );
    }

    @Selector([TrainingProcessStateCore])
    public static getDifficult(state: TrainingProcessCoreStateModel) {
        return state.howWorkoutDifficult;
    }

    @Selector([TrainingProcessStateCore])
    public static getStatus(state: TrainingProcessCoreStateModel) {

        return state.status;
    }

    @Selector([TrainingProcessStateCore, RouterState.state])
    public static getActiveWorkoutSetId(state: TrainingProcessCoreStateModel, router: RouterStateModel) {

        return this.parseWorkoutSetIdParam(router);
    }

    private static parseWorkoutSetIdParam(router: RouterStateModel) {
        return router.params['workoutSetId']?.split('_');
    }

    @Selector([TrainingProcessStateCore])
    public static getState(state: TrainingProcessCoreStateModel) {
        return state;
    }

    @Selector([TrainingProcessStateCore])
    static getWorkoutInstance(state: TrainingProcessCoreStateModel) {
        return state.workoutInstanceEntity;
    }

    @Selector([TrainingProcessStateCore, RouterState.state])
    static getCurrentWorkoutPhaseId(state: TrainingProcessCoreStateModel, router: RouterStateModel) {
        const parsedArr = this.parseWorkoutSetIdParam(router)
        if (parsedArr) {
            return parsedArr[0]
        }
    }

    @Selector([TrainingProcessStateCore, RouterState.state])
    static getCurrentWorkoutIndex(state: TrainingProcessCoreStateModel, router: RouterStateModel) {
        const parsedArr = this.parseWorkoutSetIdParam(router)
        if (parsedArr) {
            return parsedArr[1]
        }
    }

    @Selector([TrainingProcessStateCore, WorkoutInstancePhaseState, TrainingProcessWorkoutSetState, RouterState.state])
    static getCurrentWorkoutPhase(state: TrainingProcessCoreStateModel, phaseState: WorkoutInstancePhaseStateModel,
                                  workoutSetState: TrainingProcessWorkoutSetStateModel, router: RouterStateModel) {
        if (router.params['workoutSetId']) {
            const activeWorkoutPhaseId = router.params['workoutSetId'].split('_')[0];
            return WorkoutInstancePhaseState
                .selectWorkoutInstancePhaseByName(activeWorkoutPhaseId)(phaseState, workoutSetState);
        } else {
            return WorkoutInstancePhaseState
                .selectWorkoutInstancePhaseByName(state.activeWorkoutPhaseId)(phaseState, workoutSetState);
        }
    }

    @Selector([TrainingProcessStateCore, WorkoutInstancePhaseState, TrainingProcessWorkoutSetState, RouterState.state])
    static getFirstPhase(state: TrainingProcessCoreStateModel, phaseState: WorkoutInstancePhaseStateModel,
                         workoutSetState: TrainingProcessWorkoutSetStateModel) {
        const activeWorkoutPhase = phaseState.entities[phaseState.ids[0]]
        return WorkoutInstancePhaseState
            .selectWorkoutInstancePhaseByName(activeWorkoutPhase.name)(phaseState, workoutSetState);
    }

    @Selector([TrainingProcessStateCore])
    static getIsStarted(state: TrainingProcessCoreStateModel) {
        return state.isStarted
    }

    @Selector([TrainingProcessStateCore.getActiveWorkoutSetId,
        TrainingProcessWorkoutSetState, ExerciseState, MeasureTypeState, RouterState.state])
    static getActiveWorkoutSet(activeWorkoutSetId: string, workoutSetState, exercisseState, measuresTypeState, router: RouterStateModel) {
        if (router?.params) {
            const activeWorkoutSetIdFromRoute = router.params['workoutSetId']
            return TrainingProcessWorkoutSetState
                .selectWorkoutSetById(activeWorkoutSetIdFromRoute)(workoutSetState, exercisseState, measuresTypeState);
        } else {
            return TrainingProcessWorkoutSetState
                .selectWorkoutSetById(activeWorkoutSetId)(workoutSetState, exercisseState, measuresTypeState);
        }
    }

    @Selector([TrainingProcessStateCore.getActiveWorkoutSetId,
        TrainingProcessWorkoutSetState, ExerciseState, MeasureTypeState])
    static getActiveWorkoutSetFromStore(activeWorkoutSetId: string, workoutSetState, exercisseState, measuresTypeState) {

        return TrainingProcessWorkoutSetState
            .selectWorkoutSetById(activeWorkoutSetId)(workoutSetState, exercisseState, measuresTypeState);
    }

    @Action(TrainingProcessCoreActions.StartWorkoutInstanceAction)
    public setUpWorkoutInstance(ctx: StateContext<TrainingProcessCoreStateModel>,
                                {payload}: TrainingProcessCoreActions.StartWorkoutInstanceAction) {
        ctx.patchState({
            workoutInstanceEntity: payload.workoutInstance,
            date: payload.day
        });
        const formatPhase = this.getFormatingPhase(payload);
        this.store.dispatch(new WidgetActions.InitializeWidgets());
        ctx.dispatch(new TrainingProcessTimerActions.StartTimer())
        return this.store.dispatch(new TrainingProcessWorkoutPhaseActions
            .AddWorkoutPhaseToTrainingProcess(formatPhase)).pipe(
            tap(_ => this.store.dispatch(
                new TrainingProcessWorkoutSetActions.AddWorkoutSetToTrainingProcess(formatPhase))),
            tap(_ => ctx.dispatch(new TrainingProcessCoreActions.SetUpStatus(WorkoutProcessStatusEnum.inProgress))),
            tap(_ => this.store.dispatch(new TrainingProcessCoreActions
                .InitWorkoutPhase(payload.workoutInstance.listOfWorkoutPhases[0].name))),
            tap(_ => this.navigateToNextState(ctx, payload.workoutInstance.listOfWorkoutPhases[0].name, 0)),
            tap(_ => this.store.dispatch(new TrainingProcessCoreActions.StartActiveWorkoutPhase())),
            tap(_ => this.store.dispatch(new TrainingProcessCoreActions.StartActiveWorkoutSet()))
        );
    }


    @Action(TrainingProcessCoreActions.InitWorkoutPhase)
    public initWorkoutPhase(ctx: StateContext<TrainingProcessCoreStateModel>,
                            {workoutPhaseId}: TrainingProcessCoreActions.InitWorkoutPhase) {
        ctx.patchState({
            activeWorkoutPhaseId: workoutPhaseId,
            activeWorkoutSetIndex: 0
        });
    }


    @Action(TrainingProcessCoreActions.ChangeDifficalty)
    public changeDifficalty(ctx: StateContext<TrainingProcessCoreStateModel>,
                            {difficultLevel}: TrainingProcessCoreActions.ChangeDifficalty) {
        ctx.patchState({
            howWorkoutDifficult: difficultLevel,
        });
    }

    @Action(TrainingProcessCoreActions.SetUpNewActiveWorkoutPhase)
    public setUpNewActiveWorkoutPhase(ctx: StateContext<TrainingProcessCoreStateModel>,
                                      {workoutPhaseId}: TrainingProcessCoreActions.SetUpNewActiveWorkoutPhase) {
        const phase = this.store.selectSnapshot(TrainingProcessStateCore.getCurrentWorkoutPhase);
        if (phase.isOptional) {
            this.store.dispatch(new TrainingProcessWorkoutPhaseActions.DisablePhase(phase.name));
        }
        ctx.patchState({
            activeWorkoutPhaseId: workoutPhaseId,
            activeWorkoutSetIndex: 0
        });
    }

    @Action(TrainingProcessCoreActions.SetUpNewActiveWorkoutPhaseManual)
    public setUpNewActiveWorkoutPhaseManual(ctx: StateContext<TrainingProcessCoreStateModel>,
                                            {workoutPhaseId}: TrainingProcessCoreActions.SetUpNewActiveWorkoutPhaseManual) {
        this.setUpNewActiveWorkoutPhase(ctx, {workoutPhaseId})
    }

    @Action(TrainingProcessCoreActions.SetUpStatus)
    public setUpStatus(ctx: StateContext<TrainingProcessCoreStateModel>,
                       {status}: TrainingProcessCoreActions.SetUpStatus) {
        if (ctx.getState().status !== WorkoutProcessStatusEnum.finished) {
            ctx.patchState({
                status
            });
        }
    }

    @Action(TrainingProcessCoreActions.WorkoutProcessLoaded)
    public workoutProcessLoaded(ctx: StateContext<TrainingProcessCoreStateModel>) {
        ctx.patchState({
            isStarted: true
        })
    }


    @Action(TrainingProcessCoreActions.SetUpNewActiveWorkoutSet)
    public setUpNewWorkoutSetIndex(ctx: StateContext<TrainingProcessCoreStateModel>,
                                   {workoutSetId}: TrainingProcessCoreActions.SetUpNewActiveWorkoutSet) {
        const index = Number(workoutSetId.split('_')[1])
        ctx.patchState({
            activeWorkoutSetIndex: index
        });
        ctx.dispatch(new Navigate(['user', 'training-process', workoutSetId]))
    }

    @Action(TrainingProcessCoreActions.SetUpNewActiveWorkoutSetManual)
    public setUpNewWorkoutSetIndexManual(ctx: StateContext<TrainingProcessCoreStateModel>,
                                         {workoutSetId}: TrainingProcessCoreActions.SetUpNewActiveWorkoutSetManual) {
        this.setUpNewWorkoutSetIndex(ctx, {workoutSetId});
    }

    @Action(TrainingProcessCoreActions.FinishActiveWorkoutSet)
    public finishActiveWorkoutSet() {
        const activeWorkoutSet = this.store.selectSnapshot(TrainingProcessStateCore.getActiveWorkoutSet);
        this.store.dispatch(new TrainingProcessWorkoutSetActions.DoneWorkoutSet(activeWorkoutSet.id));
    }

    @Action(TrainingProcessCoreActions.FinishedWorkoutSetFromBackdrop)
    public finishActiveWorkoutSetFromBackdrop(ctx: StateContext<TrainingProcessCoreStateModel>,
                                              {payload}: TrainingProcessCoreActions.FinishedWorkoutSetFromBackdrop) {
        this.store.dispatch(new TrainingProcessWorkoutSetActions.DoneWorkoutSet(payload.idWorkoutSet));
    }

    @Action(TrainingProcessCoreActions.StartActiveWorkoutPhase)
    public startActiveWorkoutSet(ctx: StateContext<TrainingProcessCoreStateModel>) {
        ctx.patchState({
            status: WorkoutProcessStatusEnum.inProgress
        });
    }

    @Action(TrainingProcessCoreActions.FinishedWorkoutInstance)
    public finishedWorkoutInstance(ctx: StateContext<TrainingProcessCoreStateModel>) {
        ctx.patchState({status: WorkoutProcessStatusEnum.finished, isStarted: false});
        ctx.dispatch(new UserActions
            .WorkoutInstanceFinished(ctx.getState().workoutInstanceEntity.id, ctx.getState().date));
        this.store.dispatch(new TrainingProcessTimerActions.StopTimer())
        return ctx.dispatch(new TrainingProcessCoreActions
            .SendWorkoutInstanceStatistic(ctx.getState().workoutInstanceEntity));
    }

    @Action(TrainingProcessCoreActions.SendWorkoutInstanceStatistic)
    public sendWorkoutInstanceStatistic(ctx: StateContext<TrainingProcessCoreStateModel>) {
        const wpStatistic = this.initializeWorkoutProgramStatistic(ctx);
        return this.service.updateStatistic(wpStatistic).pipe(
            tap((result) => {
                this.store.dispatch(new UserStatisticActions.AddWorkoutResult(result));
            }),
            tap((result) => {
                this.store.dispatch(new UserStatisticActions.AddWorkoutResult(result));
                return ctx.dispatch(new Navigate(['progress', 'workout_result', result.userWorkoutInstanceRowStatisticId]));
            })
        );
    }

    @Action(TrainingProcessCoreActions.ChangeWorkoutSetAndWorkoutPhase)
    public changeWorkoutSetAndWorkoutRest(ctx: StateContext<TrainingProcessCoreStateModel>,
                                          {workoutSetId}: TrainingProcessCoreActions.ChangeWorkoutSetAndWorkoutPhase) {
        const splitIdArray = workoutSetId.split('_');
        const workoutSetIndex = splitIdArray[splitIdArray.length - 1];
        ctx.patchState({
            activeWorkoutSetIndex: Number(workoutSetIndex),
        });
        this.setUpNewWorkoutSetIndex(ctx, {workoutSetId})
    }

    @Action(TrainingProcessCoreActions.ExitFromTrainingProcess)
    public exitFromTrainingProcess(ctx: StateContext<TrainingProcessCoreStateModel>) {
        this.store.dispatch(new TrainingProcessTimerActions.StopTimer())
        ctx.setState({
            ...initialState
        });
        this.store.dispatch(new Navigate(['user']));
    }

    @Action(TrainingProcessCoreActions.ContinuousTraining)
    public continuousTrainingProcess(ctx: StateContext<TrainingProcessCoreStateModel>) {
        const totalTime = this.store.selectSnapshot(TrainingProcessTimerState.getTotalTime)
        this.store.dispatch(new TrainingProcessTimerActions.StartTimer(totalTime));
    }

    @Action(TrainingProcessCoreActions.GoToNextWorkoutPhase)
    public goToNextWorkoutPhase(ctx: StateContext<TrainingProcessCoreStateModel>) {
        this.goToTheNextPhase(ctx);
    }

    @Action(TrainingProcessCoreActions.GoToNextWorkoutPhaseManually)
    public goToNextWorkoutPhaseManually(ctx: StateContext<TrainingProcessCoreStateModel>) {
        this.goToTheNextPhase(ctx);
    }

    @Action(TrainingProcessCoreActions.ResetState)
    public resetState(ctx: StateContext<TrainingProcessCoreStateModel>) {
        ctx.setState({
            ...initialState
        })
    }

    private goToTheNextPhase(ctx: StateContext<TrainingProcessCoreStateModel>) {
        const currentWorkoutPhase = this.store.selectSnapshot(WorkoutInstancePhaseState
            .selectWorkoutInstancePhaseByName(ctx.getState().activeWorkoutPhaseId));
        const nextWorkoutPhase = this.getNextPhase(this.store, currentWorkoutPhase).name;
        if (currentWorkoutPhase.isOptional) {
            this.store.dispatch(new TrainingProcessWorkoutPhaseActions.DisablePhase(currentWorkoutPhase.name));
        }
        const firstUndoneWorkoutSet =
            this.store.selectSnapshot(WorkoutInstancePhaseState.selectFirstUndoneWorkoutSetInPhase(nextWorkoutPhase));
        this.navigateToNextState(ctx, nextWorkoutPhase, Number(firstUndoneWorkoutSet.id.split('_')[1]))
        ctx.patchState({
            activeWorkoutPhaseId: nextWorkoutPhase,
            activeWorkoutSetIndex: Number(firstUndoneWorkoutSet.id.split('_')[1])
        });
    }

    private getNextPhase(store: Store, phase: WorkoutInstancePhase) {
        const nextPhase = store.selectSnapshot(WorkoutInstancePhaseState.selectWorkoutInstancePhaseByName(phase.next));
        const nextPhaseNotIsDone = nextPhase.listOfSets.find(set => !set.isDone);
        return nextPhase.enable && nextPhaseNotIsDone ? nextPhase : this.getNextPhase(store, nextPhase);
    }

    private initializeWorkoutProgramStatistic(ctx: StateContext<TrainingProcessCoreStateModel>): WorkoutProgramStatistic {
        const requiredWorkoutPhase = this.store.selectSnapshot(WorkoutInstancePhaseState.selectAllRequiredWorkoutInstancePhase);
        const workoutSet = requiredWorkoutPhase.flatMap(ph => ph.listOfSets);
        const workoutInstance = ctx.getState().workoutInstanceEntity;
        const totalWorkoutTime = this.store.selectSnapshot(TrainingProcessTimerState.getTotalTime);
        const workoutDifficalt = this.store.selectSnapshot(TrainingProcessStateCore.getDifficult);
        return WorkoutProgramStatistic.createdWorkoutProgramStatistic(workoutSet,
            workoutInstance, workoutDifficalt, totalWorkoutTime, this.store);
    }


    private navigateToNextState(ctx: StateContext<TrainingProcessCoreStateModel>, phaseName: string, workoutSetNumber: number) {
        return ctx.dispatch(new Navigate(['user', 'training-process', phaseName + '_' + workoutSetNumber]));
    }

    private getFormatingPhase(payload: WorkoutInstanceScheduler) {
        return payload.workoutInstance.listOfWorkoutPhases.map(ph => {
            const listOfSets = ph.listOfSets.map((s, index) => {
                const formatedMeasures = s.listOfMeasures.map(m => ({...m, actualValue: undefined}))
                const id = ph.name + '_' + index;
                return {...s, id, listOfMeasures: formatedMeasures};
            });
            return {...ph, listOfSets};
        });
    }

}

