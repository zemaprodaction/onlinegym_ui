import {Action, createSelector, Selector, State, StateContext} from '@ngxs/store';
import {Injectable} from '@angular/core';
import {defaultsEntityState, EntityState, EntityStateModel} from '../../../../share/store/entity.state';
import {WorkoutSet, WorkoutSetEntity} from '../../../../share/models/workout/workout-set-entity';
import {TrainingProcessWorkoutSetActions} from '../actions/training-process-workout-set.action';
import {ExerciseState, ExerciseStateModel} from '../../../../core/training-store/states/exercise.state';
import {Exercise} from '../../../../share/models/exercise/exercise';
import {
    MeasureTypesStateModel,
    MeasureTypeState
} from '../../../../core/store/states/metadata-state/measure-types.state';
import {Measure} from '../../../../share/models/measureEntity';

export type TrainingProcessWorkoutSetStateModel = EntityStateModel<WorkoutSetEntity>;

@State<TrainingProcessWorkoutSetStateModel>({
    name: 'workoutSet',
    defaults: {
        ...defaultsEntityState,
        routerSelectedParams: ''
    },
})
@Injectable()
export class TrainingProcessWorkoutSetState extends EntityState<WorkoutSetEntity> {

    constructor() {
        super(null);
    }

    static calculateMeasure(measureList: Measure[]) {
        if (!measureList || measureList.length === 0) {
            return 0;
        }
        return measureList.map(m => m.actualValue)
            .reduce(((previousValue, currentValue) => previousValue * currentValue));
    }

    static selectWorkoutSetById<T>(entityId: string) {
        return createSelector([TrainingProcessWorkoutSetState, ExerciseState, MeasureTypeState],
            (state: TrainingProcessWorkoutSetStateModel, exerciseState: EntityStateModel<Exercise>,
             measureTypeState: MeasureTypesStateModel) => {
                if (state) {
                    const ws = state.entities[entityId];
                    if (ws) {
                        const listOfMeasures = ws.listOfMeasures ? ws.listOfMeasures : [];
                        const formattedListMeasures = listOfMeasures.map(measure => ({
                            ...measure,
                            measureType: measureTypeState.entities[measure.measureTypeId]
                        }));
                        return {
                            ...ws,
                            exercise: exerciseState.entities[state.entities[entityId].exerciseId],
                            listOfMeasures: formattedListMeasures
                        };
                    }
                }
            });
    }

    static getListWorkoutByListId(idList: string[]) {
        return createSelector([TrainingProcessWorkoutSetState, ExerciseState],
            (state: EntityStateModel<WorkoutSetEntity>, exerciseState: EntityStateModel<Exercise>): WorkoutSet[] => idList.map(id => ({
                ...state.entities[id],
                exercise: exerciseState.entities[state.entities[id].exerciseId]
            })));
    }

    static getRecordForExercise(exerciseId: string) {
        return createSelector([TrainingProcessWorkoutSetState],
            (state: EntityStateModel<WorkoutSetEntity>): WorkoutSet => {
                const wsForExercise = state.ids.map(id => state.entities[id]).filter(ws => ws.exerciseId === exerciseId);
                return wsForExercise.reduce((max, ws) =>
                    TrainingProcessWorkoutSetState.calculateMeasure(ws.listOfMeasures)
                    > TrainingProcessWorkoutSetState.calculateMeasure(max.listOfMeasures) ? ws : max);
            });
    }

    @Selector([TrainingProcessWorkoutSetState])
    public static selectAllDoneWorkoutSet(state: TrainingProcessWorkoutSetStateModel) {
        return state.ids.filter(id => state.entities[id].isDone).map(id => state.entities[id]);
    }

    @Selector([TrainingProcessWorkoutSetState])
    public static getAmount(state: TrainingProcessWorkoutSetStateModel) {
        return state.ids.length;
    }

    @Selector([TrainingProcessWorkoutSetState.getEntitiesAsArray(), ExerciseState, MeasureTypeState])
    public static getAllWorkoutSets(state: WorkoutSetEntity[],
                                    exState: ExerciseStateModel,
                                    measureTypeState: MeasureTypesStateModel): WorkoutSet[] {
        return state.map(ws => {
                let listOfMeasures = [];
                if (ws.listOfMeasures) {
                    listOfMeasures = ws.listOfMeasures;
                }
                const formattedListMeasures = listOfMeasures.map(measure => ({
                    ...measure,
                    measureType: measureTypeState.entities[measure.measureTypeId]
                }));
                return {...ws, exercise: exState.entities[ws.exerciseId], listOfMeasures: formattedListMeasures};
            }
        );
    }


    @Action(TrainingProcessWorkoutSetActions.AddWorkoutSetToTrainingProcess)
    public addWorkoutSetToTrainingProcess(ctx: StateContext<EntityStateModel<WorkoutSet>>,
                                          {payload}: TrainingProcessWorkoutSetActions.AddWorkoutSetToTrainingProcess) {
        let number = 0
        const wsList = payload.flatMap(phase => {

            return phase.listOfSets.map(set => {
              return {...set, number: number++}
            })
        });
        return this.addAll(ctx, wsList);
    }

    @Action(TrainingProcessWorkoutSetActions.FinishAllWorkoutSet)
    public finishedWAllWorkoutSet(ctx: StateContext<EntityStateModel<WorkoutSet>>,
                                  {payload}: TrainingProcessWorkoutSetActions.FinishAllWorkoutSet) {
        const state = ctx.getState();
        const newEntity = {};
        payload.map(s => state.entities[s]).filter(s => !s.isDone).forEach(ws => {
            const listOfMeasures = ws.listOfMeasures.map(m => ({...m, actualValue: 0}));
            newEntity[ws.id] = {...ws, isDone: true, listOfMeasures};
        });
        return ctx.patchState({
            entities: {...state.entities, ...newEntity}
        });
    }

    @Action(TrainingProcessWorkoutSetActions.DoneWorkoutSet)
    public doneWorkoutSet(ctx: StateContext<EntityStateModel<WorkoutSet>>,
                          {workoutSetId}: TrainingProcessWorkoutSetActions.DoneWorkoutSet) {
        const entities = ctx.getState().entities;
        let measureFormatted;
        const ws = ctx.getState().entities[workoutSetId];
        if (ws.listOfMeasures && ws.listOfMeasures.length !== 0) {
            measureFormatted = ws.listOfMeasures.map(m => {
                if (m.actualValue === undefined) {
                    m = {...m, actualValue: m.expectedValue};
                }
                return m;
            });
        }
        const updateEntity = {...entities[workoutSetId], isDone: true, listOfMeasures: measureFormatted};
        return ctx.patchState({
            entities: {
                ...entities, [workoutSetId]: updateEntity
            }
        });
    }

    //-------------------------------------

    @Action(TrainingProcessWorkoutSetActions.UpdateRestTime)
    public updateRestValue(ctx: StateContext<EntityStateModel<WorkoutSet>>,
                           {payload}: TrainingProcessWorkoutSetActions.UpdateRestTime) {
        const entities = ctx.getState().entities;
        ctx.patchState({
            entities: {
                ...entities,
                [payload.idWorkoutSet]: {...entities[payload.idWorkoutSet], actualRestTime: payload.actualRestTime}
            }
        });
    }


    @Action(TrainingProcessWorkoutSetActions.UpdateActualValue)
    public updateMeasureForWorkoutSetList(ctx: StateContext<EntityStateModel<WorkoutSet>>,
                                          {payload}: TrainingProcessWorkoutSetActions.UpdateActualValue) {
        const entities = ctx.getState().entities;
        ctx.patchState({
            entities: {
                ...entities,
                [payload.workoutSetId]: {...entities[payload.workoutSetId], listOfMeasures: payload.measures}
            }
        });
    }

    @Action(TrainingProcessWorkoutSetActions.CancelWorkoutSet)
    public cancelWorkoutSet(ctx: StateContext<TrainingProcessWorkoutSetStateModel>,
                            {workoutSetId}: TrainingProcessWorkoutSetActions.CancelWorkoutSet) {
        ctx.patchState({
            entities: {
                ...ctx.getState().entities,
                [workoutSetId]: {...ctx.getState().entities[workoutSetId], isDone: false}
            }
        });
    }


}
