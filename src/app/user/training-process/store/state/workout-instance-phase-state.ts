import {Action, createSelector, Selector, State, StateContext} from '@ngxs/store';
import {Injectable} from '@angular/core';
import {defaultsEntityState, EntityState, EntityStateModel} from '../../../../share/store/entity.state';
import {TrainingProcessWorkoutPhaseActions} from '../actions/training-process-workout-phase.action';
import {
  TrainingProcessWorkoutSetState,
  TrainingProcessWorkoutSetStateModel
} from './training-process-workout-set.state';
import {
  WorkoutInstancePhase,
  WorkoutInstancePhaseEntity
} from '../../../../share/models/workout/workout-instance-phase-entity';
import {WorkoutSet} from '../../../../share/models/workout/workout-set-entity';
import {TrainingProcessWorkoutSetActions} from '../actions/training-process-workout-set.action';

interface NormalizeWorkoutPhase {
  listOfSets: string[];
  name: string;
  next: string;
  isOptional: boolean;
  enable: boolean;
}

export type WorkoutInstancePhaseStateModel = EntityStateModel<NormalizeWorkoutPhase>;

@State<WorkoutInstancePhaseStateModel>({
  name: 'workoutInstancePhase',
  defaults: {
    ...defaultsEntityState
  },
})
@Injectable()
export class WorkoutInstancePhaseState extends EntityState<NormalizeWorkoutPhase> {
  constructor() {
    super(null);
  }

  static selectWorkoutInstancePhaseByName(workoutInstancePhaseId: string) {
    return createSelector([WorkoutInstancePhaseState, TrainingProcessWorkoutSetState],
      (state: WorkoutInstancePhaseStateModel, wsState: TrainingProcessWorkoutSetStateModel):
      WorkoutInstancePhase => {
        if (state) {
          return {
            ...state.entities[workoutInstancePhaseId],
            listOfSets: state.entities[workoutInstancePhaseId].listOfSets.map(s => wsState.entities[s])
          };
        }
      });
  }

  static selectFirstUndoneWorkoutSetInPhase(workoutInstancePhaseId: string) {
    return createSelector([WorkoutInstancePhaseState.selectWorkoutInstancePhaseByName(workoutInstancePhaseId),
        TrainingProcessWorkoutSetState],
      (phase: WorkoutInstancePhase, wsState: TrainingProcessWorkoutSetStateModel):
      WorkoutSet => {
        if (phase) {
          return phase.listOfSets.find(ws => !ws.isDone);
        }
      });
  }


  @Selector([WorkoutInstancePhaseState.getEntitiesAsArray(), TrainingProcessWorkoutSetState])
  static getAllWorkoutPhase(workoutPhaseArray: NormalizeWorkoutPhase[], workoutSetState: TrainingProcessWorkoutSetStateModel) {
    return workoutPhaseArray.map(wp => ({
      ...wp,
      listOfSets: wp.listOfSets.map(setId => workoutSetState.entities[setId])
    }));
  }

  @Selector([WorkoutInstancePhaseState.getEntitiesAsArray(), TrainingProcessWorkoutSetState.getAllWorkoutSets])
  public static selectAllWorkoutInstancePhase(state: NormalizeWorkoutPhase[], wsState: WorkoutSet[]) {
    return state.map(nwp => ({
      ...nwp, listOfSets: nwp.listOfSets.map(id =>
        wsState.find(ws => ws.id === id))
    }));
  }

  @Selector([WorkoutInstancePhaseState.getEntitiesAsArray(), TrainingProcessWorkoutSetState.getAllWorkoutSets])
  public static selectAllRequiredWorkoutInstancePhase(state: NormalizeWorkoutPhase[], wsState: WorkoutSet[]) {
    return state.filter(ph => !ph.isOptional).map(nwp => ({
      ...nwp, listOfSets: nwp.listOfSets.map(id =>
        wsState.find(ws => ws.id === id))
    }));
  }

  @Selector([WorkoutInstancePhaseState.getAllWorkoutPhase])
  public static selectFirstUnDoneWorkoutSetId(phaseList: WorkoutInstancePhase[]) {
    return phaseList.filter(ph => ph.enable)
      .flatMap(ph => ph.listOfSets).find(set => !set.isDone);
  }

  @Action(TrainingProcessWorkoutPhaseActions.AddWorkoutPhaseToTrainingProcess)
  public addWorkoutSetToTrainingProcess(ctx: StateContext<EntityStateModel<NormalizeWorkoutPhase>>,
                                        {payload}: TrainingProcessWorkoutPhaseActions.AddWorkoutPhaseToTrainingProcess) {
    const normilizeWorkoutPhase
      = payload.map((ph, index) =>
      ({
        ...ph, listOfSets: ph.listOfSets.map(set => set.id),
        next: this.getNextPhase(payload, index), isOptional: ph.isOptional, enable: true
      }));
    return this.addAllWithCustomId(ctx, normilizeWorkoutPhase, 'name');
  }

  @Action(TrainingProcessWorkoutPhaseActions.DisablePhase)
  public disablePhase(ctx: StateContext<EntityStateModel<NormalizeWorkoutPhase>>,
                      {workoutPhaseId}: TrainingProcessWorkoutPhaseActions.DisablePhase) {
    const phase = {...ctx.getState().entities[workoutPhaseId], enable: false};
    ctx.patchState({
      entities: {
        ...ctx.getState().entities,
        [workoutPhaseId]: phase
      }
    });
  }

  @Action(TrainingProcessWorkoutPhaseActions.FinishedAllWorkoutSetInPhaseBatch)
  public finishedAllWorkoutSetInPhaseBatch(ctx: StateContext<EntityStateModel<NormalizeWorkoutPhase>>,
                                           {phaseId}: TrainingProcessWorkoutPhaseActions.FinishedAllWorkoutSetInPhaseBatch) {
    return ctx.dispatch(new TrainingProcessWorkoutSetActions
      .FinishAllWorkoutSet(ctx.getState().entities[phaseId].listOfSets));
  }

  private getNextPhase(workoutPhaseList: WorkoutInstancePhaseEntity[], index: number) {
    return index + 1 >= workoutPhaseList.length ? workoutPhaseList[0].name : workoutPhaseList[index + 1].name;
  }
}
