import {ComponentFixture, TestBed} from '@angular/core/testing';
import {EditWorkoutPhaseComponent} from './edit-workout-phase.component';
import {MatCardModule} from '@angular/material/card';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Component, Input} from '@angular/core';
import {MeasureEntity} from '../../../../share/models/measureEntity';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetModule} from '@angular/material/bottom-sheet';

@Component({
  selector: 'app-measures-form-control',
  template: '',
})
export class MeasuresFormControlMockComponent {

  @Input()
  measures: MeasureEntity[];

  @Input()
  parent: any;
}

describe('EditWorkoutPhaseComponent', () => {

  let component;
  let fixture: ComponentFixture<EditWorkoutPhaseComponent>;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        EditWorkoutPhaseComponent, MeasuresFormControlMockComponent],
      imports: [
        MatCardModule,
        FormsModule,
        ReactiveFormsModule,
        MatBottomSheetModule
      ],
      providers: [
        {provide: MAT_BOTTOM_SHEET_DATA, useValue: {}}
      ]
    }).compileComponents();
    fixture = TestBed.createComponent(EditWorkoutPhaseComponent);
    component = fixture.componentInstance;

  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  describe('when app start', () => {

  });
});
