import {Component, Inject, OnInit} from '@angular/core';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheet} from '@angular/material/bottom-sheet';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {WorkoutInstancePhase} from '../../../../share/models/workout/workout-instance-phase-entity';

@Component({
  selector: 'app-edit-workout-phase',
  templateUrl: './edit-workout-phase.component.html',
  styleUrls: ['./edit-workout-phase.component.scss']
})
export class EditWorkoutPhaseComponent implements OnInit {

  form: FormGroup;

  constructor(@Inject(MAT_BOTTOM_SHEET_DATA) public data: WorkoutInstancePhase,
              private fb: FormBuilder, private _bottomSheet: MatBottomSheet) {
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      workoutPhase: this.fb.group({
        ...this.data, listOfSets: this.initWorkoutSetControls()
      })
    });
  }

  initWorkoutSetControls(): FormArray {
    const wsArray = this.fb.array([]);
    this.data.listOfSets.forEach(ws => wsArray.push(this.fb.group({...ws})));
    return wsArray;
  }

  get workoutSetListControl(): FormArray {
    return this.form.get(['workoutPhase', 'listOfSets']) as FormArray;
  }

  closeBottomSheet() {
    this._bottomSheet.dismiss({...this.form.value});
  }
}
