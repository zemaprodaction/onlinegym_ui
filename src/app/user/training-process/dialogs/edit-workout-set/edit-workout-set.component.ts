import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheet} from '@angular/material/bottom-sheet';
import {WorkoutSet} from '../../../../share/models/workout/workout-set-entity';

@Component({
  selector: 'app-edit-workout-set',
  templateUrl: './edit-workout-set.component.html',
  styleUrls: ['./edit-workout-set.component.scss']
})
export class EditWorkoutSetComponent implements OnInit {

  form: FormGroup;
  workoutSetFormGroup: FormGroup;

  constructor(@Inject(MAT_BOTTOM_SHEET_DATA) public data: WorkoutSet,
              private fb: FormBuilder, public _bottomSheet: MatBottomSheet) {
  }

  ngOnInit(): void {
    this.workoutSetFormGroup = this.fb.group({});
    this.form = this.fb.group({
      workoutSet: this.workoutSetFormGroup
    });
  }

  closeBottomSheet() {
    this._bottomSheet.dismiss({...this.form.value});
  }

  close() {
    this._bottomSheet.dismiss()
  }
}
