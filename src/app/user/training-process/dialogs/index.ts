import {DialogEditWorkoutSetValuesComponent} from './dialog-edit-workout-set-values/dialog-edit-workout-set-values.component';
import {EditWorkoutPhaseComponent} from './edit-workout-phase/edit-workout-phase.component';
import {AreYouSureComponent} from "./are-you-sure/are-you-sure.component";

export const dialogs = [DialogEditWorkoutSetValuesComponent, EditWorkoutPhaseComponent, AreYouSureComponent];
