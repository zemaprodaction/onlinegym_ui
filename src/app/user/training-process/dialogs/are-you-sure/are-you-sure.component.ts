import {ChangeDetectionStrategy, Component} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';


@Component({
  selector: 'app-you-should-login',
  templateUrl: './are-you-sure.component.html',
  styleUrls: ['./are-you-sure.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AreYouSureComponent {

  constructor(
    public dialogRef: MatDialogRef<AreYouSureComponent>,) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
