import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MeasureEntity} from '../../../../share/models/measureEntity';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-dialog-edit-workout-set-values',
  templateUrl: './dialog-edit-workout-set-values.component.html',
  styleUrls: ['./dialog-edit-workout-set-values.component.scss']
})
export class DialogEditWorkoutSetValuesComponent implements OnInit {

  form: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<DialogEditWorkoutSetValuesComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogEditWorkoutSetValuesData,
    private fb: FormBuilder) {
    this.form = fb.group({});
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  getWorkoutSetWithActualValue() {
    if (this.data.measures) {
      this.data.measures = this.data.measures.map((m, index) => ({
        ...m,
        actualValue: this.form.value.measures[index].actualValue
      }));
    }
    return this.data;
  }

  ngOnInit(): void {
  }

}

interface DialogEditWorkoutSetValuesData {
  measures: MeasureEntity[];
}
