import {Component, Inject, OnInit} from '@angular/core';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheet} from '@angular/material/bottom-sheet';
import {Exercise} from '../../../../share/models/exercise/exercise';
import {Observable} from 'rxjs';
import {MeasureEntity} from '../../../../share/models/measureEntity';
import {StatisticService} from '../../../statistic/service/statistic.service';
import {map} from 'rxjs/operators';

@Component({
    selector: 'app-personal-record',
    templateUrl: './personal-record.component.html',
    styleUrls: ['./personal-record.component.scss']
})
export class PersonalRecordComponent implements OnInit {

    recordMeasures: Observable<MeasureEntity[]>;

    constructor(@Inject(MAT_BOTTOM_SHEET_DATA) public data: Exercise,
                public bottomSheet: MatBottomSheet,
                private statistic: StatisticService) {
    }

    ngOnInit(): void {
        this.recordMeasures = this.statistic.getRecordForExercise(this.data.id).pipe(
            map(measures => measures)
        );
    }

    close() {
        this.bottomSheet.dismiss()
    }
}
