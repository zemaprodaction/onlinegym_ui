import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-process-training-timer',
  templateUrl: './process-training-timer.component.html',
  styleUrls: ['./process-training-timer.component.scss']
})
export class ProcessTrainingTimerComponent implements OnInit {

  @Input()
  totalFormatTotalWorkoutTime: string;

  constructor() {
  }

  ngOnInit(): void {
  }

}
