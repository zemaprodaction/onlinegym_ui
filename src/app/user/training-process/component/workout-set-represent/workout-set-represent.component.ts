import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {WorkoutSet} from '../../../../share/models/workout/workout-set-entity';
import {WorkoutInstancePhase} from '../../../../share/models/workout/workout-instance-phase-entity';
import {Measure} from '../../../../share/models/measureEntity';
import {Subscription} from 'rxjs';
import {Exercise} from '../../../../share/models/exercise/exercise';
import {AuxiliaryComponentService} from '../../services/auxiliary-component.service';
import {InfoWorkoutSetWorkComponent} from '../info-workout-set-work/info-workout-set-work.component';
import {TrainingProcessCoreActions} from '../../store/actions/training-process-core.actions';
import {Store} from '@ngxs/store';

@Component({
  selector: 'app-workout-set-represent',
  templateUrl: './workout-set-represent.component.html',
  styleUrls: ['./workout-set-represent.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkoutSetRepresentComponent implements OnInit, OnDestroy {

  @Input()
  workoutSet: WorkoutSet;

  @Input()
  currentWorkoutSetIndex: string;

  @Input()
  workoutPhase: WorkoutInstancePhase;

  @Output()
  updateWorkoutSet = new EventEmitter<{ workoutSetId: string; listOfMeasures: Measure[] }>();

  subscr: Subscription;

  constructor(private auxiliaryComponentService: AuxiliaryComponentService, private store: Store) {
  }

  ngOnInit(): void {
  }

  openBottomSheet(set: WorkoutSet): void {
    this.subscr = this.auxiliaryComponentService.openEditWindow(set)
        .afterDismissed().subscribe(
            val => {
              if (val) {
                this.updateWorkoutSet.emit({
                  workoutSetId: this.workoutSet.id,
                  listOfMeasures: val.workoutSet.listOfMeasures
                });
              }
            }
        );
  }

  infoBottomSheet(exercise: Exercise) {
    this.auxiliaryComponentService.infoBottomSheet(InfoWorkoutSetWorkComponent, exercise);
  }

  ngOnDestroy(): void {
    if (this.subscr) {
      this.subscr.unsubscribe();
    }
  }

  exitFromTrainingProcess() {
    this.store.dispatch(new TrainingProcessCoreActions.ExitFromTrainingProcess());
  }
}
