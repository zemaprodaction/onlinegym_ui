import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {WorkoutSetEntity} from '../../../../share/models/workout/workout-set-entity';
import {WorkoutInstancePhaseEntity} from '../../../../share/models/workout/workout-instance-phase-entity';

@Component({
  selector: 'app-workout-phase-list',
  templateUrl: './workout-phase-list.component.html',
  styleUrls: ['./workout-phase-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkoutPhaseListComponent implements OnInit {

  @Input()
  workoutPhases: WorkoutInstancePhaseEntity[] = [];
  @Input()
  activeWorkoutSet!: WorkoutSetEntity;

  constructor() { }

  ngOnInit() {
  }


}
