import {Component, Input, OnInit} from '@angular/core';
import {WorkoutSet} from '../../../../share/models/workout/workout-set-entity';

@Component({
  selector: 'app-workout-set',
  templateUrl: './workout-set.component.html',
  styleUrls: ['./workout-set.component.scss']
})
export class WorkoutSetComponent implements OnInit {

  @Input()
  workoutSets: WorkoutSet[] = [];

  @Input()
  activeWorkoutSet!: WorkoutSet;

  constructor() {
  }

  ngOnInit() {
  }

}
