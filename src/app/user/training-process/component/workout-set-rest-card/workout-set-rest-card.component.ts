import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, NgZone, OnInit} from '@angular/core';
import {Observable, Subscription, timer} from 'rxjs';
import {ThemePalette} from '@angular/material/core';
import {Measure} from '../../../../share/models/measureEntity';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {WorkoutSet} from '../../../../share/models/workout/workout-set-entity';
import {Select, Store} from "@ngxs/store";
import {TrainingProcessStateCore} from "../../store/state/training-process-core.state";
import {TrainingProcessCoreActions} from "../../store/actions/training-process-core.actions";

@Component({
  selector: 'app-workout-set-rest-card',
  templateUrl: './workout-set-rest-card.component.html',
  styleUrls: ['./workout-set-rest-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkoutSetRestCardComponent implements OnInit {
  restTime: number;

  subscribeTimer: any;
  timerValuePercent = 0;
  actualTime: number;
  colorOfChart: ThemePalette = 'primary';
  nextWorkoutSet: WorkoutSet;
  recordMeasures: Observable<Measure[]>;
  finishedButtonName: string;
  private subscription: Subscription;

  @Select(TrainingProcessStateCore.getDifficult)
  difficaltValue: Observable<number>

  constructor(private cdr: ChangeDetectorRef,
              private ngZone: NgZone,
              private store: Store,
              public dialogRef: MatDialogRef<WorkoutSetRestCardComponent>,
              @Inject(MAT_DIALOG_DATA) public data: {
                rest: number;
                recordMeasures: Observable<Measure[]>;
                nextWorkoutSet: WorkoutSet;
              }) {
    this.restTime = data.rest;
    this.nextWorkoutSet = data.nextWorkoutSet;
    this.recordMeasures = data.recordMeasures;
  }

  breakRest() {
    this.ngZone.run(() => {
      this.dialogRef.close();
    });
  }

  ngOnInit(): void {
    const percent = 100 / this.restTime;
    const source = timer(0, 1000);
    this.finishedButtonName = this.nextWorkoutSet ? $localize`:@@startTraining:Start training` : $localize`:@@finish:Finish`
    this.subscription = source.subscribe(val => {
      this.actualTime = val;
      this.timerValuePercent = this.timerValuePercent + percent;
      this.subscribeTimer = this.restTime - val;
      if (this.subscribeTimer < 0) {
        this.colorOfChart = 'warn';
        this.subscribeTimer = '+' + (this.subscribeTimer * -1);
      }
      this.cdr.detectChanges();
    });
  }

  getMeasuresFormatString(measure: Measure[]) {
    return measure.map(m => (m.actualValue || m.expectedValue) + ' ' + m.measureType.name).join(' / ');
  }

  changeDifficalt($event: number) {
    this.store.dispatch(new TrainingProcessCoreActions.ChangeDifficalty($event))
  }
}
