import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Measure} from '../../../../share/models/measureEntity';

@Component({
  selector: 'app-exercise-measure',
  templateUrl: './exercise-measure.component.html',
  styleUrls: ['./exercise-measure.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExerciseMeasureComponent implements OnInit {

  @Input()
  measure: Measure = {expectedValue: 0};

  @Input()
  parent!:  FormGroup;

  minWidth = 36;
  width: number = this.minWidth;

  constructor() {
  }

  ngOnInit() {
    const textCount = this.measure.expectedValue + '';
    this.width = textCount.length * 32;
  }

}
