import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {WorkoutSet, WorkoutSetEntity} from '../../../../share/models/workout/workout-set-entity';
import {MeasureEntity} from '../../../../share/models/measureEntity';
import {DialogEditWorkoutSetValuesComponent} from '../../dialogs/dialog-edit-workout-set-values/dialog-edit-workout-set-values.component';
import {MatDialog} from '@angular/material/dialog';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import {InfoWorkoutSetWorkComponent} from '../info-workout-set-work/info-workout-set-work.component';

@Component({
  selector: 'app-workout-set-work-card',
  templateUrl: './workout-set-work-card.component.html',
  styleUrls: ['./workout-set-work-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkoutSetWorkCardComponent implements OnInit {

  @Input()
  set!: WorkoutSet;

  form: FormGroup;

  @Output()
  exerciseFinished: EventEmitter<WorkoutSetEntity> = new EventEmitter();

  constructor(private fb: FormBuilder, public dialog: MatDialog,
              private _bottomSheet: MatBottomSheet
  ) {
  }

  ngOnInit() {
    if (this.set !== null) {
      this.form = this.initForm(this.set.listOfMeasures);
    }
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogEditWorkoutSetValuesComponent, {
      width: 'auto',
      data: {measures: this.set.listOfMeasures}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.set = {...this.set, listOfMeasures: result.measures};
      this.next();
    });
  }

  next() {
    return this.exerciseFinished.emit(this.set);
  }

  private initForm(measures: MeasureEntity[]) {
    return this.fb.group({
      measures: this.intMeasuresForm(measures)
    });
  }

  private intMeasuresForm(measures: MeasureEntity[]) {
    const measureForm = this.fb.array([]);
    measures.forEach(value => {
      measureForm.push(this.fb.group({actualValue: this.fb.control(value.expectedValue)}));
    });
    return measureForm;
  }

  infoBottomSheet() {
    this._bottomSheet.open(InfoWorkoutSetWorkComponent, {
        data: this.set.exercise.id
      }
    );
  }
}


