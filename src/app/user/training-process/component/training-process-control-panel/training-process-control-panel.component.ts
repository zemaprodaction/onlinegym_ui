import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TrainingProgress} from '../../model/training-progress';
import {WorkoutSetEntity} from '../../../../share/models/workout/workout-set-entity';

@Component({
  selector: 'app-training-process-control-panel',
  templateUrl: './training-process-control-panel.component.html',
  styleUrls: ['./training-process-control-panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TrainingProcessControlPanelComponent implements OnInit {

  @Input()
  isPause!: boolean;
  @Input()
  progress!: TrainingProgress;
  @Input()
  currentWorkoutSet!: WorkoutSetEntity;
  @Input()
  nextWorkoutSet!: string;

  @Output()
  pauseClick = new EventEmitter<boolean>();
  @Output()
  skipClick = new EventEmitter<WorkoutSetEntity>();
  @Output()
  finishedWorkoutIstance = new EventEmitter();

  constructor() {
  }

  ngOnInit(): void {
  }

  get formatPause() {
    return this.isPause ? 'play_arrow' : 'pause';
  }

  pause() {
    this.pauseClick.emit(this.isPause);
  }

  clickSkip() {
    this.skipClick.emit(this.currentWorkoutSet);
  }

  get formatProgress() {
    return `${this.progress.finished + 1}/${this.progress.total}`;
  }

  finishedWorkoutInstance() {
    return this.finishedWorkoutIstance.emit();
  }
}
