import { Component, OnInit } from '@angular/core';
import {TrainingProcessTimerState} from "../../widgets/timer/state/training-process-timer.state";
import {map} from "rxjs/operators";
import {Store} from "@ngxs/store";

@Component({
  selector: 'app-training-process-timer',
  templateUrl: './training-process-timer.component.html',
  styleUrls: ['./training-process-timer.component.scss']
})
export class TrainingProcessTimerComponent implements OnInit {

  constructor(private store: Store) { }

  ngOnInit(): void {
  }

  get totalFormatTotalWorkoutTime() {
    return this.store.select(TrainingProcessTimerState.getTotalTime).pipe(
        map(totalTime => {
          const totalMinutes = 100 + (totalTime / 60);
          const totalHour = 100 + ((totalTime / 60) / 60);
          const totalSeconds = totalTime > 60 ? totalTime % 60 : totalTime;
          const totalSecFormat = (totalSeconds + 100).toString().substring(1);
          return `${totalHour.toFixed().substring(1)} : ${totalMinutes.toFixed().substring(1)} : ${totalSecFormat}`;
        })
    );
  }

}
