import {Component, Input, OnInit, EventEmitter, Output, SimpleChanges, OnChanges} from '@angular/core';
import {Measure, MeasureEntity} from '../../../../share/models/measureEntity';
import {Store} from '@ngxs/store';
import {MeasureTypeState} from '../../../../core/store/states/metadata-state/measure-types.state';


@Component({
  selector: 'app-measures-editable-list',
  templateUrl: './measures-editable-list.component.html',
  styleUrls: ['./measures-editable-list.component.scss']
})
export class MeasuresEditableListComponent implements OnInit, OnChanges {

  @Input()
  measureList: (Measure | MeasureEntity)[] = [];

  @Output()
  editMeasures = new EventEmitter();

  measures: {
    name: string;
    value: number;
    prefix: string;
    prefixLocalized: string;
  }[];

  constructor(private store: Store) {
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.measureList) {
      this.measures = this.measureList.map((m: Measure | MeasureEntity) => ({
        name: m['measureType'] ? m['measureType']?.name : this.getMeasureType(m),
        value: m.actualValue || m.expectedValue,
        prefix: this.getMeasureType(m)['measurePrefix'],
        prefixLocalized: this.getMeasureType(m)['measurePrefixLocalized']
      }));
    }
  }

  editMeasure() {
    this.editMeasures.emit();
  }

  private getMeasureType(m: Measure | MeasureEntity) {
    const measure = this.store.selectSnapshot(MeasureTypeState.getEntityById(m['measureTypeId']));
    return measure ? measure['name'] : '';
  }
}
