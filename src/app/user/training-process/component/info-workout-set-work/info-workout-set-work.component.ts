import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {Exercise} from '../../../../share/models/exercise/exercise';

@Component({
    selector: 'app-info-workout-set-work',
    templateUrl: './info-workout-set-work.component.html',
    styleUrls: ['./info-workout-set-work.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InfoWorkoutSetWorkComponent {

    targetMuscle: string;
    musclesInvolved: string;

    constructor(
        @Inject(MAT_BOTTOM_SHEET_DATA) public data: Exercise,
        private _bottomSheetRef: MatBottomSheetRef<InfoWorkoutSetWorkComponent>,
        public bottomSheet: MatBottomSheet,
    ) {
        this.targetMuscle = data.additionalInfo.targetMuscle.map(value => value.name).join(', ');
        this.musclesInvolved = data.additionalInfo.musclesInvolved.map(value => value.name).join(', ');
    }

    getEquipment() {
        return this.data.additionalInfo.equipment.map(eq => eq.name).join(', ');
    }

    close() {
        this.bottomSheet.dismiss()
    }
}
