import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {WorkoutInstancePhase} from "../../../../share/models/workout/workout-instance-phase-entity";
import {Measure} from "../../../../share/models/measureEntity";
import {AuxiliaryComponentService} from "../../services/auxiliary-component.service";
import {Store} from "@ngxs/store";
import {WorkoutSet} from "../../../../share/models/workout/workout-set-entity";
import {Exercise} from "../../../../share/models/exercise/exercise";
import {InfoWorkoutSetWorkComponent} from "../info-workout-set-work/info-workout-set-work.component";
import {TrainingProcessCoreActions} from "../../store/actions/training-process-core.actions";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-training-process-menu',
  templateUrl: './training-process-menu.component.html',
  styleUrls: ['./training-process-menu.component.scss']
})
export class TrainingProcessMenuComponent implements OnInit {

  @Input()
  workoutSet: WorkoutSet;
  
  @Input()
  currentWorkoutSetIndex: string;

  @Input()
  workoutPhase: WorkoutInstancePhase;

  @Output()
  updateWorkoutSet = new EventEmitter<{ workoutSetId: string; listOfMeasures: Measure[] }>();

  constructor(private auxiliaryComponentService: AuxiliaryComponentService, private store: Store) { }
  subscr: Subscription;
  ngOnInit(): void {
  }

  openBottomSheet(set: WorkoutSet): void {
    this.subscr = this.auxiliaryComponentService.openEditWindow(set)
        .afterDismissed().subscribe(
            val => {
              if (val) {
                this.updateWorkoutSet.emit({
                  workoutSetId: this.workoutSet.id,
                  listOfMeasures: val.workoutSet.listOfMeasures
                });
              }
            }
        );
  }

  infoBottomSheet(exercise: Exercise) {
    this.auxiliaryComponentService.infoBottomSheet(InfoWorkoutSetWorkComponent, exercise);
  }

  exitFromTrainingProcess() {
    this.store.dispatch(new TrainingProcessCoreActions.ExitFromTrainingProcess());
  }

}
