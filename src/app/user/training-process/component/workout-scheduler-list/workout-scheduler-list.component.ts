import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Select, Store} from '@ngxs/store';
import {Observable} from 'rxjs';
import {WorkoutInstancePhase} from '../../../../share/models/workout/workout-instance-phase-entity';
import {WorkoutSet} from '../../../../share/models/workout/workout-set-entity';
import {WorkoutInstancePhaseState} from '../../store/state/workout-instance-phase-state';
import {TrainingProcessStateCore} from '../../store/state/training-process-core.state';
import {TrainingProcessCoreActions} from '../../store/actions/training-process-core.actions';
import {MatCheckboxChange} from '@angular/material/checkbox';
import {TrainingProcessWorkoutSetActions} from '../../store/actions/training-process-workout-set.action';

@Component({
    selector: 'app-workout-scheduler-list',
    templateUrl: './workout-scheduler-list.component.html',
    styleUrls: ['./workout-scheduler-list.component.scss']
})
export class WorkoutSchedulerListComponent implements OnInit {

    @Select(WorkoutInstancePhaseState.selectAllRequiredWorkoutInstancePhase)
    $filteredWorkoutPhase: Observable<WorkoutInstancePhase[]>;

    @Select(TrainingProcessStateCore.getCurrentWorkoutPhaseId)
    workoutPhaseActiveId: Observable<string>;

    @Select(TrainingProcessStateCore.getActiveWorkoutSet)
    workoutSetActiveId: Observable<WorkoutSet>;

    constructor(private store: Store, private cdr: ChangeDetectorRef) {

    }

    ngOnInit(): void {
        this.workoutSetActiveId.subscribe((ws) => {
          this.cdr.detectChanges()
        })
    }

    chooseWorkoutSet(workoutSetId: string) {
        this.store.dispatch(new TrainingProcessCoreActions.ChangeWorkoutSetAndWorkoutPhase(workoutSetId));
    }

    changeWorkoutSetDone(ws: WorkoutSet, event: MatCheckboxChange) {
        if (event.checked) {
            this.store.dispatch(new TrainingProcessCoreActions.FinishedWorkoutSetFromBackdrop({
                idWorkoutSet: ws.id,
                measure: ws.listOfMeasures
            }));
        } else {
            this.store.dispatch(new TrainingProcessWorkoutSetActions.CancelWorkoutSet(
                ws.id));
        }
    }
}
