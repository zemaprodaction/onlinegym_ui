import {WorkoutPhaseListComponent} from './workout-phase/workout-phase-list.component';
import {WorkoutSetComponent} from './workout-set/workout-set.component';
import {ExerciseMeasureComponent} from './exercise-measure/exercise-measure.component';
import {WorkoutSetRestCardComponent} from './workout-set-rest-card/workout-set-rest-card.component';
import {WorkoutSetWorkCardComponent} from './workout-set-work-card/workout-set-work-card.component';
import {TrainingProcessControlPanelComponent} from './training-process-control-panel/training-process-control-panel.component';
import {InfoWorkoutSetWorkComponent} from './info-workout-set-work/info-workout-set-work.component';
import {ProcessTrainingTimerComponent} from './process-training-timer/process-training-timer.component';

export const components: any[] = [
  WorkoutPhaseListComponent,
  WorkoutSetComponent,
  ExerciseMeasureComponent,
  WorkoutSetRestCardComponent,
  WorkoutSetWorkCardComponent,
  TrainingProcessControlPanelComponent,
  InfoWorkoutSetWorkComponent,
  ProcessTrainingTimerComponent
];

export * from './workout-phase/workout-phase-list.component';
export * from './workout-set/workout-set.component';
export * from './exercise-measure/exercise-measure.component';
export * from './workout-set-rest-card/workout-set-rest-card.component';
export * from './workout-set-work-card/workout-set-work-card.component';
export * from '../../../share/ui-components/cards/complete-percent-card/complete-percent-card.component';
export * from './training-process-control-panel/training-process-control-panel.component';
export * from './info-workout-set-work/info-workout-set-work.component';
