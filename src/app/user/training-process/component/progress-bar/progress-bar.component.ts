import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-progress-bar',
    templateUrl: './progress-bar.component.html',
    styleUrls: ['./progress-bar.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProgressBarComponent implements OnInit {

    @Input()
    progressController: ProgressController[] = [];

    constructor() {
    }

    ngOnInit(): void {
    }

}

export interface ProgressController {
    state: 'active' | 'finished' | 'non_active'
}
