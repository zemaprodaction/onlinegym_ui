import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ProgressBarComponent, ProgressController} from './progress-bar.component';
import {By} from "@angular/platform-browser";

function cheeckControls(fixture: ComponentFixture<ProgressBarComponent>, numberOfControls: number = 0) {
    const controls = fixture.debugElement.queryAll(By.css('.control'))
    expect(controls.length).toEqual(numberOfControls)
}

describe('ProgressBarComponent', () => {
    let component: ProgressBarComponent;
    let fixture: ComponentFixture<ProgressBarComponent>;
    const activeControl: ProgressController = {state: "active"}
    const nonActiveControl: ProgressController = {state: "non_active"}
    const finishedControl: ProgressController = {state: "finished"}
    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [ProgressBarComponent]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(ProgressBarComponent);
        component = fixture.componentInstance;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    describe('when receive 1 nav controller', () => {
        it('should contains 1 div control', () => {
            component.progressController = [{state: "active"}]
            fixture.detectChanges();
            cheeckControls(fixture, 1);
        })
        describe('when there is active control', () => {
            it('should contains 1 div active control', () => {
                component.progressController = [activeControl]
                fixture.detectChanges();
                const controls = fixture.debugElement.queryAll(By.css('.active-control'))
                expect(controls.length).toEqual(1)
            })
        });
    })
    describe('when receive 0 nav controller', () => {
        it('should contains 0 div control', () => {
            component.progressController = []
            fixture.detectChanges();
            cheeckControls(fixture, 0);
        })
    });
    describe('when receive 5 nav controller', () => {
        it('should contains 5 div control', () => {
            component.progressController = [nonActiveControl, nonActiveControl, nonActiveControl, nonActiveControl, nonActiveControl]
            fixture.detectChanges();
            cheeckControls(fixture, 5);
        })
        it('should contains 1 active', () => {
            component.progressController = [nonActiveControl, nonActiveControl, activeControl, nonActiveControl, nonActiveControl]
            fixture.detectChanges();
            const controls = fixture.debugElement.queryAll(By.css('.active-control'))
            expect(controls.length).toEqual(1)
        })
        describe('when the first 3 was done', () => {
            it('should contains 3 finished container', () => {
                component.progressController = [finishedControl, finishedControl, finishedControl, activeControl, nonActiveControl]
                fixture.detectChanges();
                const controls = fixture.debugElement.queryAll(By.css('.finished-control'))
                expect(controls.length).toEqual(3)
            })
        })
    });

});
