import {ChangeDetectionStrategy, Component, Input, OnInit, EventEmitter, Output} from '@angular/core';
import {MatSliderChange} from "@angular/material/slider";

@Component({
  selector: 'app-difficalty-slider',
  templateUrl: './difficalty-slider.component.html',
  styleUrls: ['./difficalty-slider.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DifficaltySliderComponent implements OnInit {

  @Input()
  value: number

  @Output()
  onChange = new EventEmitter<number>()

  constructor() { }

  ngOnInit(): void {
  }

  getDifficalty(value) {
    switch (value){
      case 1: {
        return "Очень легко"
      }
      case 2: {
        return  "Легко"
      }
      case 3: {
        return "Нормально"
      }
      case 4: {
        return  "Сложно"
      }
      case 5: {
        return "Очень сложно"
      }
    }
  }

  change($event: MatSliderChange) {
    this.onChange.emit($event.value)
  }
}
