import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {AuthGuard} from '../../share/guards/auth.guard';
import {ProcessTrainingInGymComponent} from './container/process-training-in-gym/process-training-in-gym.component';
import {TrainingContiniousGuard, TrainingInProgressGuard} from './guards/training-in-progress.guard';
import {SureExitTrainingProcessGuard} from "./guards/sure-exit-training-process.guard";

export const TRAINING_ROUTES: Routes = [
    {
        path: '',
        pathMatch: 'full',
        canActivate: [AuthGuard, TrainingInProgressGuard],
        runGuardsAndResolvers: 'always',
    },
    {
        canActivate: [AuthGuard, TrainingContiniousGuard],
        canDeactivate: [SureExitTrainingProcessGuard],
        path: ':workoutSetId',
        component: ProcessTrainingInGymComponent,
    }
];

export const routing: ModuleWithProviders<RouterModule> = RouterModule.forChild(
    TRAINING_ROUTES
);
