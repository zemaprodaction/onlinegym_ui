import {Injectable} from '@angular/core';
import {CanActivate, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {Store} from '@ngxs/store';
import {Navigate} from '@ngxs/router-plugin';
import {TrainingProcessStateCore} from '../store/state/training-process-core.state';
import {WorkoutProcessStatusEnum} from '../model/workout-process-status.enum';
import {TrainingProcessCoreActions} from "../store/actions/training-process-core.actions";
import ContinuousTraining = TrainingProcessCoreActions.ContinuousTraining;

@Injectable({
    providedIn: 'root'
})
export class TrainingInProgressGuard implements CanActivate {
    constructor(private store: Store) {
    }

    canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        const status = this.store.selectSnapshot(TrainingProcessStateCore.getStatus);
        const isStarted = this.store.selectSnapshot(TrainingProcessStateCore.getIsStarted);
        if (status === WorkoutProcessStatusEnum.finished || status === null) {
            this.store.dispatch(new Navigate(['user']));
        } else {
            if (isStarted) {
                const currentWorkoutSet = this.store.selectSnapshot(TrainingProcessStateCore.getActiveWorkoutSetFromStore)
                this.store.dispatch(new ContinuousTraining())
                this.store.dispatch(new Navigate(['user', 'training-process', currentWorkoutSet.id]))
            }
        }
        return true;
    }
}


@Injectable({
    providedIn: 'root'
})
export class TrainingContiniousGuard implements CanActivate {
    constructor(private store: Store) {
    }

    canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        const status = this.store.selectSnapshot(TrainingProcessStateCore.getStatus);
        const isStarted = this.store.selectSnapshot(TrainingProcessStateCore.getIsStarted);
        if (status === WorkoutProcessStatusEnum.finished || status === null) {
            this.store.dispatch(new Navigate(['user']));
        } else {
            if (isStarted) {
                this.store.dispatch(new ContinuousTraining())
            }
        }
        return true;
    }
}
