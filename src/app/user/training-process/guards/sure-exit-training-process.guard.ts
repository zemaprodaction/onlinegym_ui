import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable, of} from 'rxjs';
import {ProcessTrainingInGymComponent} from "../container/process-training-in-gym/process-training-in-gym.component";
import {ModalService} from "../../../core/services/modal.service";
import {tap} from "rxjs/operators";
import {AreYouSureComponent} from "../dialogs/are-you-sure/are-you-sure.component";
import {TrainingProcessCoreActions} from "../store/actions/training-process-core.actions";
import {Store} from "@ngxs/store";
import {TrainingProcessStateCore} from "../store/state/training-process-core.state";
import {WorkoutProcessStatusEnum} from "../model/workout-process-status.enum";
import {Backdrop} from "../../../core/store/actions/backdrop.actions";
import {Toolbar} from "../../../core/store/actions/toolbar.action";

@Injectable({
    providedIn: 'root'
})
export class SureExitTrainingProcessGuard implements CanDeactivate<ProcessTrainingInGymComponent> {
    constructor(private modalService: ModalService, private store: Store) {
    }

    canDeactivate(component: ProcessTrainingInGymComponent, currentRoute: ActivatedRouteSnapshot,
                  currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        const status = this.store.selectSnapshot(TrainingProcessStateCore.getStatus)
        if (nextState.url.includes('training-process/')) {
            return of(true)
        } else if (status === null || status === WorkoutProcessStatusEnum.finished) {
            this.store.dispatch(new Backdrop.StateBecomeResetable())
            this.store.dispatch(new Toolbar.ResetToolbarButton())
            this.store.dispatch(new Backdrop.ResetSubHeader());
            this.store.dispatch(new Toolbar.ShowLoginButton());
            return of(true)
        }
        return this.modalService.openDialogWithCallBack(AreYouSureComponent).afterClosed().pipe(
            tap(result => {
                if (result) {
                    this.store.dispatch(new TrainingProcessCoreActions.ResetState())
                    this.store.dispatch(new Backdrop.StateBecomeResetable())
                    this.store.dispatch(new Toolbar.ResetToolbarButton())
                    this.store.dispatch(new Backdrop.ResetSubHeader());
                    this.store.dispatch(new Toolbar.ShowLoginButton());
                }
            })
        )
    }
}
