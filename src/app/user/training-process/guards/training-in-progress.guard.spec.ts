import {fakeAsync, TestBed, tick} from '@angular/core/testing';

import {TrainingInProgressGuard} from './training-in-progress.guard';
import {Location} from '@angular/common';
import {NgxsModule, Store} from '@ngxs/store';
import {AppStates} from '../../../core/store';
import {TrainingProcessStateList} from '../store';
import {BaseComponent} from '../container/base-process-training/base-process-training.component';
import {OnInit} from '@angular/core';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {NgxsRouterPluginModule} from '@ngxs/router-plugin';

export class ProcessTrainingMockComponent extends BaseComponent implements OnInit {

  ngOnInit() {
    super.ngOnInit();
  }
}

describe('TrainingInProgressGuard', () => {
  let guard: TrainingInProgressGuard;
  let location: Location;
  let store: Store;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        BaseComponent,
        ProcessTrainingMockComponent
      ],
      imports: [
        HttpClientTestingModule,
        NgxsModule.forRoot([...AppStates, ...TrainingProcessStateList]),
        NgxsRouterPluginModule.forRoot(),
        RouterTestingModule.withRoutes([{
            path: `user/training-process/gym`,
            component: ProcessTrainingMockComponent
          },
            {
              path: `user`,
              component: ProcessTrainingMockComponent
            }]
        )
      ]
    });
    guard = TestBed.inject(TrainingInProgressGuard);
    store = TestBed.inject(Store);
    location = TestBed.inject(Location);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
  describe('when training process is not progress', () => {
    it('should redirect to user page', fakeAsync(() => {
      guard.canActivate();
      tick();
      expect(location.path()).toEqual('/user');
    }));
  });
});
