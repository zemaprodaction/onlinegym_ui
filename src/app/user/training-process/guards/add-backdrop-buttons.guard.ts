import {Injectable} from '@angular/core';
import {CanActivate, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {WorkoutSchedulerListComponent} from '../component/workout-scheduler-list/workout-scheduler-list.component';
import {ToolbarAction} from '../../../share/models/toolbar-action';
import {ToolBarButton} from '../../../share/models/tool-bar-button';
import {BackdropStatus} from '../../../core/store';
import {BackdropService} from '../../../core/services/backdrop/backdrop.service';

@Injectable({
  providedIn: 'root'
})
export class AddBackdropButtonsGuard implements CanActivate {
  constructor(private backdropService: BackdropService) {
  }

  canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const toolbarAction: ToolbarAction = {
      button: new ToolBarButton($localize`:@@scheduler:Scheduler`, '', 'list',
        () => this.openTrainingScheduler()),
      status: BackdropStatus.OPEN,
      isUseBackdropContext: true
    };
    this.backdropService.addActionToBackdrop(toolbarAction);
    return true;
  }

  private openTrainingScheduler() {
    this.backdropService.openBackdropWithComponentCallback(WorkoutSchedulerListComponent, {
      webDownToHeight: 356,
      mobileDownToHeight: 356,
      nameBackdropPage: $localize`:@@scheduler:Scheduler`,
      customHeightOfBackdrop: 64
    });
  }
}
