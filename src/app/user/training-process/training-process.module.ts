import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {routing} from './training-process.routing';
import {MatCardModule} from '@angular/material/card';
import {ShareModule} from '../../share/share.module';
import * as fromComponents from './component';
import * as fromContainers from './container';
import * as fromDialogs from './dialogs';
import * as fromFormControl from './form-control';
import {MatFormFieldModule} from '@angular/material/form-field';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatListModule} from '@angular/material/list';
import {MatTabsModule} from '@angular/material/tabs';
import {FlexLayoutModule, FlexModule, GridModule} from '@angular/flex-layout';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {NgxsModule} from '@ngxs/store';
import {TrainingProcessStateList} from './store';
import {WorkoutSchedulerListComponent} from './component/workout-scheduler-list/workout-scheduler-list.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {WorkoutSetRepresentComponent} from './component/workout-set-represent/workout-set-represent.component';
import {EditWorkoutSetComponent} from './dialogs/edit-workout-set/edit-workout-set.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {UserStatisticState} from '../../user-progress/store/state/user-statistic.state';
import {PhasePreviewComponent} from './widgets/phasePreview/component/phase-preview/phase-preview.component';
import {TrainingProcessWidgetStateList} from './widgets';
import {MatMenuModule} from '@angular/material/menu';
import {MeasuresEditableListComponent} from './component/measures-editable-list/measures-editable-list.component';
import {PersonalRecordComponent} from './dialogs/personal-record/personal-record.component';
import { TrainingProcessMenuComponent } from './component/training-process-menu/training-process-menu.component';
import { TrainingProcessTimerComponent } from './component/training-process-timer/training-process-timer.component';
import { ProgressBarComponent } from './component/progress-bar/progress-bar.component';
import {MatRadioModule} from "@angular/material/radio";
import {MatSliderModule} from "@angular/material/slider";
import { DifficaltySliderComponent } from './component/difficalty-slider/difficalty-slider.component';

@NgModule({
  declarations: [...fromComponents.components, ...fromContainers.containers, ...fromDialogs.dialogs,
    ...fromFormControl.formComponent,
    WorkoutSchedulerListComponent,
    WorkoutSetRepresentComponent,
    EditWorkoutSetComponent,
    PhasePreviewComponent,
    MeasuresEditableListComponent,
    PersonalRecordComponent,
    TrainingProcessMenuComponent,
    TrainingProcessTimerComponent,
    ProgressBarComponent,
    DifficaltySliderComponent],
    imports: [
        ShareModule,
        CommonModule,
        routing,
        NgxsModule.forFeature([...TrainingProcessWidgetStateList, ...TrainingProcessStateList, UserStatisticState,]),
        MatCardModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatListModule,
        MatTabsModule,
        FlexModule,
        MatInputModule,
        MatButtonModule,
        MatIconModule,
        MatDialogModule,
        MatBottomSheetModule,
        FormsModule,
        GridModule,
        MatCheckboxModule,
        MatToolbarModule,
        FlexLayoutModule,
        MatMenuModule,
        MatRadioModule,
        MatSliderModule,
    ]
})
export class TrainingProcessModule {
}
