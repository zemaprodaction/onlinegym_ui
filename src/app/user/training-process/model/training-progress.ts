export class TrainingProgress {

  constructor(public total: number, public finished: number) {
  }
}
