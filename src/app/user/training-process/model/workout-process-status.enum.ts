export enum WorkoutProcessStatusEnum {
  inProgress = 'inProgress',
  rest = 'rest',
  finished = 'finished',
}
