import {WorkoutSetEntity} from '../../../share/models/workout/workout-set-entity';

export interface WorkoutInstanceWork {
  workoutPhaseList: WorkoutInstancePhaseNormalization[];
  workoutSetList: WorkoutSetEntity;
}

export interface WorkoutInstancePhaseNormalization {
  name: string;
  listOfSets: string[];
}
