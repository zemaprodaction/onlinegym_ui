import {Action, Selector, State, StateContext} from '@ngxs/store';
import {Injectable} from '@angular/core';
import {TrainingProcessTimerActions} from '../actions/training-process-timer.action';
import {TimerService} from '../../../../../core/services/timer.service';
import {Subscription} from "rxjs";
import ChangeTime = TrainingProcessTimerActions.ChangeTime;

export interface TrainingProcessTimerStateModel {
    totalTime: number;
    isPause: boolean;
}

export const initialState: TrainingProcessTimerStateModel = {
    totalTime: 0,
    isPause: false
};

@State<TrainingProcessTimerStateModel>({
    name: 'timer',
    defaults: {
        ...initialState
    }
})
@Injectable()
export class TrainingProcessTimerState {

    constructor(private timerService: TimerService) {
    }

    private subscription: Subscription

    @Selector([TrainingProcessTimerState])
    public static getTotalTime(state: TrainingProcessTimerStateModel) {
        return state.totalTime;
    }
    @Selector([TrainingProcessTimerState])
    public static getIsPause(state: TrainingProcessTimerStateModel) {
        return state.isPause;
    }

    @Action(TrainingProcessTimerActions.StartTimer)
    public startTrainingTimer(ctx: StateContext<TrainingProcessTimerStateModel>,
                              {time}: TrainingProcessTimerActions.StartTimer) {
        ctx.patchState({
            totalTime: time
        })
        if (!this.subscription || this.subscription.closed) {
            this.subscription = this.timerService.startTraining()
                .subscribe(_ => {
                    ctx.getState().isPause
                    if (!ctx.getState().isPause) {
                        ctx.dispatch(new ChangeTime(ctx.getState().totalTime + 1))
                    }
                });
        }
    }

    @Action(TrainingProcessTimerActions.StopTimer)
    public stopTimer(ctx: StateContext<TrainingProcessTimerStateModel>) {
        this.timerService.timerComplete();
        this.subscription.unsubscribe()
        ctx.patchState({
            ...initialState
        });
    }

    @Action(TrainingProcessTimerActions.PauseTimer)
    public pauseTimer(ctx: StateContext<TrainingProcessTimerStateModel>) {
        ctx.patchState({
            isPause: true
        })
        this.timerService.timerPause();
    }

    @Action(TrainingProcessTimerActions.ResumeTimer)
    public resumeTimer(ctx: StateContext<TrainingProcessTimerStateModel>) {
        ctx.patchState({
            isPause: false
        })
        this.timerService.timerResume();
    }

    @Action(TrainingProcessTimerActions.ChangeTime)
    public changeTime(ctx: StateContext<TrainingProcessTimerStateModel>, {time}: TrainingProcessTimerActions.ChangeTime) {
        ctx.patchState({
            totalTime: time
        })
    }

}

