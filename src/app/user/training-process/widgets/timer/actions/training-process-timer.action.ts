export namespace TrainingProcessTimerActions {

  export class StartTimer {

    public static readonly type = '[Training Process Timer] Start Timer';
    constructor(public time: number = 0) {
    }

  }

  export class StopTimer {

    public static readonly type = '[Training Process Timer] Stop Timer';

  }

  export class PauseTimer {

    public static readonly type = '[Training Process Timer] Pause Timer';

  }
  export class ResumeTimer {

    public static readonly type = '[Training Process Timer] Resume Timer';

  }
  export class ChangeTime {

    public static readonly type = '[Training Process Timer] Change Time';
    constructor(public time: number) {
    }
  }
}








