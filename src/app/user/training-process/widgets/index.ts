import {TrainingProcessProcessFinishActiveWorkoutSetState} from '../widgets/processFinishActiveWorkoutSet/state/training-process-process-finish-active-workout-set.state';
import {TrainingProcessTimerState} from '../widgets/timer/state/training-process-timer.state';
import {TrainingProcessRestState} from '../widgets/rest/state/training-process-rest.state';
import {TrainingProcessWidgetState} from './store/training-process-widget.state';
import {TrainingProcessPhasePreviewState} from './phasePreview/state/training-process-phase-preview.state';


export const TrainingProcessWidgetStateList = [
  TrainingProcessWidgetState,
  TrainingProcessTimerState,
  TrainingProcessRestState,
  TrainingProcessPhasePreviewState,
  TrainingProcessProcessFinishActiveWorkoutSetState];

