import {Action, Selector, State, StateContext, Store} from '@ngxs/store';
import {Injectable} from '@angular/core';
import {FinishWorkoutInctanceInterceptor} from '../interceptor/finish-workout-inctance-interceptor.service';
import {TrainingProcessRestActions} from '../actions/training-process-rest.action';
import {TrainingProcessCoreActions} from '../../../store/actions/training-process-core.actions';
import {RestStatusEnum} from '../rest-status.enum';
import {WorkoutProcessStatusEnum} from '../../../model/workout-process-status.enum';
import {TrainingProcessWorkoutSetActions} from '../../../store/actions/training-process-workout-set.action';

export interface TrainingProcessRestStateModel {
  restTime: number;
  workoutSetIdAfterRest: string;
}

export const initialState: TrainingProcessRestStateModel = {
  restTime: 0,
  workoutSetIdAfterRest: null
};

@State<TrainingProcessRestStateModel>({
  name: 'rest',
  defaults: {
    ...initialState
  }
})
@Injectable()
export class TrainingProcessRestState {

  constructor(private handler: FinishWorkoutInctanceInterceptor, private store: Store) {
    handler.startWatching();
  }

  @Selector([TrainingProcessRestState])
  public static getRestTime(state: TrainingProcessRestStateModel) {
    return state.restTime;
  }

  @Action(TrainingProcessRestActions.StartRest)
  public startRest(ctx: StateContext<TrainingProcessRestStateModel>,
                   {payload}: TrainingProcessRestActions.StartRest) {
    ctx.patchState({
      restTime: payload.timeRest,
      workoutSetIdAfterRest: payload.workoutSetId
    });
    this.store.dispatch(new TrainingProcessCoreActions.SetUpStatus(RestStatusEnum.rest));
  }

  @Action(TrainingProcessRestActions.StopRest)
  public stopRest(ctx: StateContext<TrainingProcessRestStateModel>,
                  {actualRestTime}: TrainingProcessRestActions.StopRest) {
    this.store.dispatch(new TrainingProcessWorkoutSetActions.UpdateRestTime({
      idWorkoutSet: ctx.getState().workoutSetIdAfterRest,
      actualRestTime
    }));
    this.store.dispatch(new TrainingProcessCoreActions.SetUpStatus(WorkoutProcessStatusEnum.inProgress));
    ctx.patchState({
      ...initialState
    });
  }
}

