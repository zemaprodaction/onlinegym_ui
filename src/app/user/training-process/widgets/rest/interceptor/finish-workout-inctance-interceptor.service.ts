import {Injectable} from '@angular/core';
import {Actions, ofActionDispatched, Store} from '@ngxs/store';
import {TrainingProcessCoreActions} from '../../../store/actions/training-process-core.actions';
import {TrainingProcessStateCore} from '../../../store/state/training-process-core.state';
import {TrainingProcessRestActions} from '../actions/training-process-rest.action';
import {delay} from 'rxjs/operators';
import {WorkoutSetRestCardComponent} from '../../../component';
import {AuxiliaryComponentService} from '../../../services/auxiliary-component.service';
import {TrainingProcessWorkoutSetState} from '../../../store/state/training-process-workout-set.state';
import {StatisticService} from '../../../../statistic/service/statistic.service';
import {WorkoutSet} from '../../../../../share/models/workout/workout-set-entity';
import {WorkoutSetFinishHandlerService} from '../../processFinishActiveWorkoutSet/service/workout-set-finish-handler.service';

@Injectable({providedIn: 'root'})
export class FinishWorkoutInctanceInterceptor {

    isOpen = false;

    constructor(private actions$: Actions,
                private store: Store,
                private statisticService: StatisticService,
                private workoutSetFinishHandlerService: WorkoutSetFinishHandlerService,
                public auxiliaryComponentService: AuxiliaryComponentService) {

    }

    startWatching() {
        this.actions$
            .pipe(ofActionDispatched(TrainingProcessCoreActions.FinishActiveWorkoutSet),
                delay(100))
            .subscribe(() => {
                const workoutSet = this.store.selectSnapshot(TrainingProcessStateCore.getActiveWorkoutSet);
                const nextWorkoutSetId = this.workoutSetFinishHandlerService.getHandler().handle(this.store);
                const nextWorkoutSet = this.store.selectSnapshot(TrainingProcessWorkoutSetState.selectWorkoutSetById(nextWorkoutSetId));
                this.openDialog(workoutSet, nextWorkoutSet);
                this.store.dispatch(new TrainingProcessRestActions.StartRest({
                    workoutSetId: workoutSet.id,
                    timeRest: workoutSet.restTime
                }));
            });
    }

    openDialog(workoutSet: WorkoutSet, nextWorkoutSet): void {
        if (!this.isOpen) {
            let recordMeasures = null
            if (nextWorkoutSet) {
                recordMeasures = this.statisticService.getRecordForExercise(nextWorkoutSet.exercise.id);
            }
            const dialogRef = this.auxiliaryComponentService.openFullWindowDialog(WorkoutSetRestCardComponent, {
                rest: workoutSet.restTime,
                nextWorkoutSet,
                recordMeasures
            });
            dialogRef.afterClosed().subscribe(result => {
                this.isOpen = false;
                this.store.dispatch(new TrainingProcessRestActions.StopRest(result));
            });
            this.isOpen = true;
        }
    }
}
