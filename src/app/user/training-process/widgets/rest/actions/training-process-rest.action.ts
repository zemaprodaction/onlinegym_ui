export namespace TrainingProcessRestActions {

  export class StartRest {

    public static readonly type = '[Training Process] Start Rest';

    constructor(public payload: { timeRest: number; workoutSetId: string }) {
    }
  }

  export class StopRest {

    public static readonly type = '[Training Process] Stop Rest';

    constructor(public actualRestTime: number) {
    }
  }
}








