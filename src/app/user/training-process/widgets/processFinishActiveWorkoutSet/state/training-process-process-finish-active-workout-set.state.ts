import {Injectable} from '@angular/core';
import {WorkoutSetFinishHandlerService} from '../service/workout-set-finish-handler.service';
import {State} from '@ngxs/store';

@State({
  name: 'trainingProcessFinishActiveWorkoutSet',
})
@Injectable()
export class TrainingProcessProcessFinishActiveWorkoutSetState {

  constructor(private service: WorkoutSetFinishHandlerService) {
    service.startWatching();
  }

}

