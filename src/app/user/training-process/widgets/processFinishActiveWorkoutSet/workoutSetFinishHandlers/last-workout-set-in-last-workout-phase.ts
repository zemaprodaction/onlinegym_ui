import {Store} from '@ngxs/store';
import {AbstractHandler} from './abstract-handler';
import {TrainingProcessStateCore} from '../../../store/state/training-process-core.state';

export class LastWorkoutSetInLastWorkoutPhase extends AbstractHandler {

    public handle(store: Store): string {
        const phase = store.selectSnapshot(TrainingProcessStateCore.getCurrentWorkoutPhase);
        const activeWorkoutIndex = store.selectSnapshot(TrainingProcessStateCore.getCurrentWorkoutIndex);
        if (phase.listOfSets.length - 1 === Number(activeWorkoutIndex)) {
            if (phase.next === undefined) {
              return store.selectSnapshot(TrainingProcessStateCore.getFirstPhase).name + '_' + 0
            }
            return phase.next + '_' + 0
        }
        return super.handle(store);
    }
}
