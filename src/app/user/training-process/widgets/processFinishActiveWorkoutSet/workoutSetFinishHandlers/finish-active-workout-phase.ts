import {Store} from '@ngxs/store';
import {AbstractHandler} from './abstract-handler';
import {WorkoutSet} from '../../../../../share/models/workout/workout-set-entity';
import {TrainingProcessStateCore} from '../../../store/state/training-process-core.state';
import {TrainingProcessCoreActions} from '../../../store/actions/training-process-core.actions';
import {WorkoutInstancePhaseState} from '../../../store/state/workout-instance-phase-state';
import {WorkoutInstancePhase} from '../../../../../share/models/workout/workout-instance-phase-entity';

export class FinishActiveWorkoutPhase extends AbstractHandler {

  public handle(store: Store): string {
    const phase = store.selectSnapshot(TrainingProcessStateCore.getCurrentWorkoutPhase);
    const activeWorkoutId = store.selectSnapshot(TrainingProcessStateCore.getActiveWorkoutSetId);
    const activeWorkoutIndex = store.selectSnapshot(TrainingProcessStateCore.getCurrentWorkoutIndex);
    const isLastWorkoutSetInWorkoutPhase = activeWorkoutIndex + 1 === phase.listOfSets.length;
    if (this.isLastUndoneWorkoutSetInPhase(phase.listOfSets, activeWorkoutId)
      || isLastWorkoutSetInWorkoutPhase) {
      return this.getNextWorkoutSet(store);
    }
    return super.handle(store);
  }



  private isLastUndoneWorkoutSetInPhase(phaseWorkoutSetList: WorkoutSet[], activeWorkoutId: string) {
    return phaseWorkoutSetList.filter(ws => !ws.isDone && ws.id !== activeWorkoutId).length === 0;
  }
  private getNextWorkoutSet(store: Store) {
    const currentWorkoutPhase = store.selectSnapshot(TrainingProcessStateCore.getCurrentWorkoutPhase);
    const nextWorkoutPhase = this.getNextPhase(store, currentWorkoutPhase).name;
    return store.selectSnapshot(WorkoutInstancePhaseState.selectFirstUndoneWorkoutSetInPhase(nextWorkoutPhase)).id;
  }

  private getNextPhase(store: Store, phase: WorkoutInstancePhase) {
    const nextPhase = store.selectSnapshot(WorkoutInstancePhaseState.selectWorkoutInstancePhaseByName(phase.next));
    const nextPhaseNotIsDone = nextPhase.listOfSets.find(set => !set.isDone);
    return nextPhase.enable && nextPhaseNotIsDone ? nextPhase : this.getNextPhase(store, nextPhase);
  }
}

export class FinishActiveWorkoutPhaseFromBackdrop extends AbstractHandler {

  public handle(store: Store): string {
    const phase = store.selectSnapshot(TrainingProcessStateCore.getCurrentWorkoutPhase);
    const activeWorkoutId = store.selectSnapshot(TrainingProcessStateCore.getActiveWorkoutSetId);
    const activeWorkoutIndex = store.selectSnapshot(TrainingProcessStateCore.getCurrentWorkoutIndex);
    const isLastWorkoutSetInWorkoutPhase = activeWorkoutIndex + 1 === phase.listOfSets.length;
    if (this.isLastUndoneWorkoutSetInPhase(phase.listOfSets, activeWorkoutId)
      || isLastWorkoutSetInWorkoutPhase) {
      store.dispatch(new TrainingProcessCoreActions.GoToNextWorkoutPhaseManually());
      return null;
    }
    return super.handle(store);
  }



  private isLastUndoneWorkoutSetInPhase(phaseWorkoutSetList: WorkoutSet[], activeWorkoutId: string) {
    return phaseWorkoutSetList.filter(ws => !ws.isDone && ws.id !== activeWorkoutId).length === 0;
  }
}
