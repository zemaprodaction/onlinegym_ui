import {Store} from '@ngxs/store';
import {AbstractHandler} from './abstract-handler';
import {TrainingProcessStateCore} from '../../../store/state/training-process-core.state';
import {WorkoutInstancePhaseState} from '../../../store/state/workout-instance-phase-state';

export class LastUnDoneWorkoutSetInLastWorkoutPhase extends AbstractHandler {

  public handle(store: Store): string {
    const phase = store.selectSnapshot(TrainingProcessStateCore.getCurrentWorkoutPhase);
    const activeWorkoutIndex = store.selectSnapshot(TrainingProcessStateCore.getCurrentWorkoutIndex);
    if (phase.next === undefined && phase.listOfSets.length - 1 === activeWorkoutIndex) {
      const lastUnDoneWorkoutSet = store.selectSnapshot(WorkoutInstancePhaseState.selectFirstUnDoneWorkoutSetId);
      return lastUnDoneWorkoutSet.id;
    }
    return super.handle(store);
  }
}
