import {Store} from '@ngxs/store';
import {AbstractHandler} from './abstract-handler';
import {TrainingProcessStateCore} from '../../../store/state/training-process-core.state';

export class SkipActiveWorkoutSet extends AbstractHandler {

    public handle(store: Store): string {
        const currentPhase = store.selectSnapshot(TrainingProcessStateCore.getCurrentWorkoutPhase);
        const nextWs = store.selectSnapshot(TrainingProcessStateCore.getNexWorkoutSetInPhase(currentPhase.name));
        return nextWs.id;
    }
}
