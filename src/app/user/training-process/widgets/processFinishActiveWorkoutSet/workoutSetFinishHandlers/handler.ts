import {Store} from '@ngxs/store';

export interface Handler {
  setNext(handler: Handler): Handler;

  handle(store: Store): string;
}
