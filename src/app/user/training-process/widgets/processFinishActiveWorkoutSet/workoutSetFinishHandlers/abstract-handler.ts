import {Store} from '@ngxs/store';
import {Handler} from './handler';

export abstract class AbstractHandler implements Handler {

  private nextHandler: Handler;

  public setNext(handler: Handler): Handler {
    this.nextHandler = handler;
    return handler;
  }

  public handle(store: Store): string {
    if (this.nextHandler) {
      return this.nextHandler.handle(store);
    }

    return null;
  }
}
