import {Store} from '@ngxs/store';
import {AbstractHandler} from './abstract-handler';
import {WorkoutSet} from '../../../../../share/models/workout/workout-set-entity';
import {TrainingProcessStateCore} from '../../../store/state/training-process-core.state';
import {WorkoutInstancePhaseState} from '../../../store/state/workout-instance-phase-state';

export class WorkoutInstanceFinishedHandler extends AbstractHandler {

  public handle(store: Store): string {
    const activeWorkoutId = store.selectSnapshot(TrainingProcessStateCore.getActiveWorkoutSetId);
    const allWorkoutSet = store.selectSnapshot(WorkoutInstancePhaseState.selectAllRequiredWorkoutInstancePhase)
      .flatMap(ph => ph.listOfSets);
    if (this.isLastUndoneWorkoutSetInWorkoutInstance(allWorkoutSet, activeWorkoutId)) {
      return null;
    }

    return super.handle(store);
  }

  private isLastUndoneWorkoutSetInWorkoutInstance(phaseWorkoutSetList: WorkoutSet[], activeWorkoutId: string) {
    return phaseWorkoutSetList.filter(ws => !ws.isDone && ws.id !== activeWorkoutId).length === 0;
  }
}
