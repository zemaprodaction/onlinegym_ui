import {Injectable} from '@angular/core';
import {Handler} from '../workoutSetFinishHandlers/handler';
import {FinishActiveWorkoutSet} from '../workoutSetFinishHandlers/finish-active-workout-set';
import {FinishActiveWorkoutPhase, FinishActiveWorkoutPhaseFromBackdrop} from '../workoutSetFinishHandlers/finish-active-workout-phase';
import {WorkoutInstanceFinishedHandler} from '../workoutSetFinishHandlers/workout-instance-finished-handler';
import {Actions, ofActionSuccessful, Store} from '@ngxs/store';
import {TrainingProcessCoreActions} from '../../../store/actions/training-process-core.actions';
import {LastUnDoneWorkoutSetInLastWorkoutPhase} from '../workoutSetFinishHandlers/last-un-done-workout-set-in-last-workout-phase';
import {Subscription} from 'rxjs';
import {TrainingProcessRestActions} from '../../rest/actions/training-process-rest.action';
import {TrainingProcessWorkoutSetActions} from '../../../store/actions/training-process-workout-set.action';
import {TrainingProcessStateCore} from '../../../store/state/training-process-core.state';
import {WorkoutProcessStatusEnum} from '../../../model/workout-process-status.enum';
import {TrainingProcessWorkoutPhaseActions} from '../../../store/actions/training-process-workout-phase.action';
import {WorkoutInstancePhase} from "../../../../../share/models/workout/workout-instance-phase-entity";
import {LastWorkoutSetInLastWorkoutPhase} from "../workoutSetFinishHandlers/last-workout-set-in-last-workout-phase";
import {SkipActiveWorkoutSet} from "../workoutSetFinishHandlers/skip-active-workout-set";

@Injectable({
    providedIn: 'root'
})
export class WorkoutSetFinishHandlerService {

    handler: Handler;

    skipWorkoutSetHandler: Handler;
    subscription: Subscription;

    constructor(private actions$: Actions, private store: Store) {
        const wsFinish = new FinishActiveWorkoutSet();

        const workoutPhaseFinished = new FinishActiveWorkoutPhase();
        const workoutPhaseFinishedFromBackdrop = new FinishActiveWorkoutPhaseFromBackdrop();
        workoutPhaseFinished.setNext(wsFinish);

        const lastWorkoutSetInLastWorkoutPhase = new LastUnDoneWorkoutSetInLastWorkoutPhase();
        const lastWorkoutSetInLastWorkoutPhaseForBackdrop = new LastUnDoneWorkoutSetInLastWorkoutPhase();

        lastWorkoutSetInLastWorkoutPhase.setNext(workoutPhaseFinished);
        lastWorkoutSetInLastWorkoutPhaseForBackdrop.setNext(workoutPhaseFinishedFromBackdrop);

        this.handler = new WorkoutInstanceFinishedHandler();
        this.handler.setNext(lastWorkoutSetInLastWorkoutPhase);

        this.skipWorkoutSetHandler = new LastWorkoutSetInLastWorkoutPhase();
        this.skipWorkoutSetHandler.setNext(new SkipActiveWorkoutSet());

    }

    startWatching() {
        if (!this.subscription) {

            this.subscription = this.actions$
                .pipe(ofActionSuccessful(TrainingProcessRestActions.StopRest,
                    TrainingProcessWorkoutSetActions.FinishAllWorkoutSet))
                .subscribe(() => {
                    const nextWorkoutSet = this.handler.handle(this.store);
                    if (nextWorkoutSet === null) {
                        this.store.dispatch(new TrainingProcessCoreActions.FinishedWorkoutInstance());
                        this.store.dispatch(new TrainingProcessCoreActions.SetUpStatus(WorkoutProcessStatusEnum.finished));
                    } else {
                        const [phaseId, workoutSetIndex] = nextWorkoutSet.split('_');
                        const currentWorkoutPhase = this.store.selectSnapshot(TrainingProcessStateCore.getCurrentWorkoutPhase);
                        this.setUpNewWorkoutSetAndWorkoutPhase(currentWorkoutPhase, phaseId, nextWorkoutSet);
                    }
                });
            this.subscription.add(this.actions$
                .pipe(ofActionSuccessful(TrainingProcessCoreActions.FinishedWorkoutSetFromBackdrop))
                .subscribe(() => {
                    const nextWorkoutSet = this.handler.handle(this.store);
                    if (nextWorkoutSet === null) {
                        this.store.dispatch(new TrainingProcessCoreActions.FinishedWorkoutInstance());
                        this.store.dispatch(new TrainingProcessCoreActions.SetUpStatus(WorkoutProcessStatusEnum.finished));
                    } else {
                        const [phaseId, workoutSetIndex] = nextWorkoutSet.split('_');
                        const currentWorkoutPhase = this.store.selectSnapshot(TrainingProcessStateCore.getCurrentWorkoutPhase);
                        if (currentWorkoutPhase.name === phaseId) {
                            this.store.dispatch(new TrainingProcessCoreActions.SetUpNewActiveWorkoutSetManual(nextWorkoutSet));
                        } else {
                            if (currentWorkoutPhase.isOptional) {
                                this.store.dispatch(new TrainingProcessWorkoutPhaseActions.DisablePhase(currentWorkoutPhase.name));
                            }
                            this.store.dispatch(new TrainingProcessCoreActions.SetUpNewActiveWorkoutPhaseManual(phaseId));
                            this.store.dispatch(new TrainingProcessCoreActions.SetUpNewActiveWorkoutSetManual(nextWorkoutSet));
                        }
                        this.store.dispatch(new TrainingProcessCoreActions.SetUpStatus(WorkoutProcessStatusEnum.inProgress));
                    }
                }));
            this.subscription.add(this.actions$
                .pipe(ofActionSuccessful(TrainingProcessCoreActions.SkipCurrentWorkoutSet))
                .subscribe(() => {
                    const nextWorkoutSet = this.skipWorkoutSetHandler.handle(this.store);
                    const [phaseId, workoutSetIndex] = nextWorkoutSet.split('_');
                    const currentWorkoutPhase = this.store.selectSnapshot(TrainingProcessStateCore.getCurrentWorkoutPhase);
                    this.setUpNewWorkoutSetAndWorkoutPhase(currentWorkoutPhase, phaseId, nextWorkoutSet);
                }));
        }
    }

    private setUpNewWorkoutSetAndWorkoutPhase(currentWorkoutPhase: WorkoutInstancePhase, phaseId: string, workoutSetId: string) {
        if (currentWorkoutPhase.name === phaseId) {
            this.store.dispatch(new TrainingProcessCoreActions.SetUpNewActiveWorkoutSet(workoutSetId));
        } else {
            if (currentWorkoutPhase.isOptional) {
                this.store.dispatch(new TrainingProcessWorkoutPhaseActions.DisablePhase(currentWorkoutPhase.name));
            }
            this.store.dispatch(new TrainingProcessCoreActions.SetUpNewActiveWorkoutPhase(phaseId));
            this.store.dispatch(new TrainingProcessCoreActions.SetUpNewActiveWorkoutSet(workoutSetId));
        }
        this.store.dispatch(new TrainingProcessCoreActions.SetUpStatus(WorkoutProcessStatusEnum.inProgress));
    }

    getHandler() {
        return this.handler;
    }


}
