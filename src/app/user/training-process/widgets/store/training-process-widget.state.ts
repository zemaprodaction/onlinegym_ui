import {Action, State, StateContext} from '@ngxs/store';
import {Injectable} from '@angular/core';
import {TrainingProcessTimerState} from '../../widgets/timer/state/training-process-timer.state';
import {
  TrainingProcessRestState,
  TrainingProcessRestStateModel
} from '../../widgets/rest/state/training-process-rest.state';
import {TrainingProcessProcessFinishActiveWorkoutSetState} from '../processFinishActiveWorkoutSet/state/training-process-process-finish-active-workout-set.state';
import {TrainingProcessPhasePreviewState} from '../phasePreview/state/training-process-phase-preview.state';
import {WidgetActions} from './widget.actions';
import {PhasePreviewHandler} from '../phasePreview/interceptor/start-workout-inctance.interceptor';
import {FinishWorkoutInctanceInterceptor} from '../rest/interceptor/finish-workout-inctance-interceptor.service';
import {StartWorkoutInctanceInterceptor} from "../timer/interceptor/start-workout-inctance.interceptor";

export interface TrainingProcessWidgetStateModel {

}

export const initialState: TrainingProcessWidgetStateModel = {};

@State<TrainingProcessWidgetStateModel>({
  name: 'trainingProcessWidgetState',
  defaults: {},
  children: [TrainingProcessRestState,
    TrainingProcessTimerState,
    TrainingProcessPhasePreviewState,
    TrainingProcessProcessFinishActiveWorkoutSetState]
})
@Injectable()
export class TrainingProcessWidgetState {

  constructor(private service: PhasePreviewHandler,
              private timerHandler: StartWorkoutInctanceInterceptor,
              private service2: FinishWorkoutInctanceInterceptor) {
  }

  @Action(WidgetActions.InitializeWidgets)
  public initWidget(ctx: StateContext<TrainingProcessRestStateModel>) {
    this.service.startWatching();
    this.service2.startWatching();
    this.timerHandler.startWatching();
  }
}
