export namespace WidgetActions {

  export class InitializeWidgets {

    public static readonly type = '[Training Process Widgets] Initialize widgets';
  }
}
