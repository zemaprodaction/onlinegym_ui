import {Action, createSelector, State, StateContext, Store} from '@ngxs/store';
import {Injectable} from '@angular/core';
import {TrainingProcessPhasePreviewActions} from '../action/training-process-phase-preview.action';
import {TrainingProcessCoreActions} from '../../../store/actions/training-process-core.actions';
import {WorkoutProcessStatusEnum} from '../../../model/workout-process-status.enum';
import {TrainingProcessStateCore} from '../../../store/state/training-process-core.state';
import {TrainingProcessWorkoutPhaseActions} from '../../../store/actions/training-process-workout-phase.action';


export interface TrainingProcessPhasePreviewStateModel {
    ids: string[];
    phases: {
        [phaseName: string]: {
            name: string;
            status: string;
        };
    };
}

export const initialState: TrainingProcessPhasePreviewStateModel = {
    ids: [],
    phases: {}
};

@State<TrainingProcessPhasePreviewStateModel>({
    name: 'phasePreviewState',
    defaults: {
        ...initialState
    }
})

@State({
    name: 'trainingProcessPhasePreview',
})
@Injectable()
export class TrainingProcessPhasePreviewState {

    constructor(private store: Store) {
    }

    static getPhaseStateByPhaseName(phaseId: string) {
        return createSelector([TrainingProcessPhasePreviewState],
            (state: TrainingProcessPhasePreviewStateModel) => state.phases[phaseId]
        );
    }

    @Action(TrainingProcessPhasePreviewActions.StartPhase)
    public startPhase() {
        this.store.dispatch(new TrainingProcessCoreActions.SetUpStatus(WorkoutProcessStatusEnum.inProgress));
    }

    @Action(TrainingProcessPhasePreviewActions.SkipPhase)
    public skipPhase(ctx: StateContext<TrainingProcessPhasePreviewStateModel>) {
        const activeWorkoutPhase = this.store.selectSnapshot(TrainingProcessStateCore.getCurrentWorkoutPhase);
        const state = ctx.getState();
        const isPhaseWasSkipped = state.phases[activeWorkoutPhase.name] !== undefined;
        if (!isPhaseWasSkipped) {
            ctx.patchState({
                ids: [...state.ids, activeWorkoutPhase.name],
                phases: {
                    ...state.phases,
                    [activeWorkoutPhase.name]: {status: 'skipped', name: activeWorkoutPhase.name}
                }
            });
        }
        this.store.dispatch(new TrainingProcessCoreActions.SetUpStatus(WorkoutProcessStatusEnum.inProgress));
        const status = this.store.selectSnapshot(TrainingProcessStateCore.getStatus);
        if (status !== WorkoutProcessStatusEnum.finished) {
            this.store.dispatch(new TrainingProcessCoreActions.GoToNextWorkoutPhase());
        }
    }

    @Action(TrainingProcessPhasePreviewActions.SkipPhasePermanently)
    public skipPhasePermanently() {
        const activeWorkoutPhase = this.store.selectSnapshot(TrainingProcessStateCore.getCurrentWorkoutPhase);
        this.store.dispatch(new TrainingProcessWorkoutPhaseActions.FinishedAllWorkoutSetInPhaseBatch(activeWorkoutPhase.name));
        this.store.dispatch(new TrainingProcessCoreActions.SetUpStatus(WorkoutProcessStatusEnum.inProgress));
    }

    @Action(TrainingProcessPhasePreviewActions.ResetState)
    public resetState(ctx: StateContext<TrainingProcessPhasePreviewStateModel>) {
        ctx.setState({
            ...initialState
        })
    }
}

