import {Component, Inject, NgZone} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Store} from '@ngxs/store';
import {TrainingProcessPhasePreviewActions} from '../../action/training-process-phase-preview.action';
import {Observable} from 'rxjs';
import {TrainingProcessPhasePreviewState} from '../../state/training-process-phase-preview.state';
import {TrainingProcessStateCore} from '../../../../store/state/training-process-core.state';
import {map, switchMap} from 'rxjs/operators';
import {WorkoutInstancePhase} from '../../../../../../share/models/workout/workout-instance-phase-entity';
import {WorkoutProcessStatusEnum} from '../../../../model/workout-process-status.enum';

@Component({
  selector: 'app-phase-preview',
  templateUrl: './phase-preview.component.html',
  styleUrls: ['./phase-preview.component.scss']
})
export class PhasePreviewComponent {

  $phaseStatus: Observable<{
    name: string;
    status: string;
  }>;
  $phaseIsDirty: Observable<boolean>;
  $currentPhase: Observable<WorkoutInstancePhase>;
  $trainingProcessStatus: Observable<WorkoutProcessStatusEnum>;

  constructor(
    private store: Store,
    private ngZone: NgZone,
    public dialogRef: MatDialogRef<PhasePreviewComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.$phaseStatus = this.store.select(TrainingProcessStateCore.getCurrentWorkoutPhaseId).pipe(
      switchMap(currentPhaseId =>
        this.store.select(TrainingProcessPhasePreviewState.getPhaseStateByPhaseName(currentPhaseId)))
    );
    this.$currentPhase = this.store.select(TrainingProcessStateCore.getCurrentWorkoutPhase);
    this.$phaseIsDirty = this.store.select(TrainingProcessStateCore.getCurrentWorkoutPhase).pipe(
      map(phase => phase.listOfSets.find(s => s.isDone) !== undefined)
    );
  }

  start(): void {
    this.ngZone.run(() => {
      this.dialogRef.close('start');
    });
  }

  skip() {
    this.ngZone.run(() => {
      this.dialogRef.close('skip');
    });
  }

  skipPermanent() {
    this.ngZone.run(() => {
      this.dialogRef.close('skip_permanent');
    });
  }
}
