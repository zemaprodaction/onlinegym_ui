import {Injectable} from '@angular/core';
import {Actions, ofActionSuccessful, Store} from '@ngxs/store';
import {TrainingProcessCoreActions} from '../../../store/actions/training-process-core.actions';
import {PhasePreviewStatusEnum} from '../phase-preview-status.enum';
import {PhasePreviewComponent} from '../component/phase-preview/phase-preview.component';
import {TrainingProcessStateCore} from '../../../store/state/training-process-core.state';
import {TrainingProcessPhasePreviewState} from '../state/training-process-phase-preview.state';
import {TrainingProcessPhasePreviewActions} from '../action/training-process-phase-preview.action';
import {WorkoutProcessStatusEnum} from '../../../model/workout-process-status.enum';
import {AuxiliaryComponentService} from '../../../services/auxiliary-component.service';
import SetUpNewActiveWorkoutPhase = TrainingProcessCoreActions.SetUpNewActiveWorkoutPhase;

@Injectable({providedIn: 'root'})
export class PhasePreviewHandler {

    isOpen = false;

    constructor(private actions$: Actions, private store: Store,
                public auxiliaryComponentService: AuxiliaryComponentService) {

    }

    startWatching() {
        this.actions$
            .pipe(ofActionSuccessful(TrainingProcessCoreActions.StartActiveWorkoutPhase,
                TrainingProcessCoreActions.GoToNextWorkoutPhase, SetUpNewActiveWorkoutPhase))
            .subscribe(() => {
                const status = this.store.selectSnapshot(TrainingProcessStateCore.getStatus);
                const activePhase = this.store.selectSnapshot(TrainingProcessStateCore.getCurrentWorkoutPhase);
                const phaseState = this.store.selectSnapshot(TrainingProcessPhasePreviewState
                    .getPhaseStateByPhaseName(activePhase.name));
                if (status !== PhasePreviewStatusEnum.phasePreview && status !== WorkoutProcessStatusEnum.finished) {
                    this.store.dispatch(new TrainingProcessCoreActions.SetUpStatus(PhasePreviewStatusEnum.phasePreview));
                    if (phaseState?.status === 'skipped' && activePhase.isOptional) {
                        this.store.dispatch(new TrainingProcessPhasePreviewActions.SkipPhase());
                    }
                    this.openDialog();
                }
            });
        this.actions$
            .pipe(ofActionSuccessful(TrainingProcessCoreActions.ExitFromTrainingProcess,
                TrainingProcessCoreActions.FinishedWorkoutInstance))
            .subscribe(() => {
                this.store.dispatch(new TrainingProcessPhasePreviewActions.ResetState())
            });
    }

    openDialog(): void {
        if (!this.isOpen) {
            const dialogRef = this.auxiliaryComponentService.openFullWindowDialog(PhasePreviewComponent);
            dialogRef.afterClosed().subscribe(result => {
                this.isOpen = false;
                if (result === 'skip') {
                    this.store.dispatch(new TrainingProcessPhasePreviewActions.SkipPhase());
                } else if (result === 'skip_permanent') {
                    this.store.dispatch(new TrainingProcessPhasePreviewActions.SkipPhasePermanently());
                } else {
                    this.store.dispatch(new TrainingProcessPhasePreviewActions.StartPhase());
                }
            });
            this.isOpen = true;
        }
    }
}
