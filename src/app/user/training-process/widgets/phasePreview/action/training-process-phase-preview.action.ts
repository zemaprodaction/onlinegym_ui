export namespace TrainingProcessPhasePreviewActions {

  export class StartPhase {

    public static readonly type = '[Training Process Phase Preview] Start Phase';

  }

  export class SkipPhase {

    public static readonly type = '[Training Process Phase Preview] Skip Phase';

  }

  export class SkipPhasePermanently {

    public static readonly type = '[Training Process Phase Preview] Skip Phase Permanently';

  }
  export class ResetState {

    public static readonly type = '[Training Process Phase Preview] Reset State';

  }
}








