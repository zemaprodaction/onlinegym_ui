import {Component, OnInit} from '@angular/core';
import {Router, RoutesRecognized} from '@angular/router';
import {IconService} from './core/services/icons.service';
import {Store} from '@ngxs/store';
import {AuthorizationService} from './authorization/services/authorization.service';
import {Toolbar} from './core/store/actions/toolbar.action';
import {MetadataActions} from './core/store/actions/metadata.actions';
import {UserActions} from './core/user-store';
import {Backdrop} from './core/store/actions/backdrop.actions';
import {filter, pairwise, withLatestFrom} from 'rxjs/operators';
import {BackdropState, BackdropStatus} from './core/store';
import {ExerciseActions} from './core/training-store/actions/exercise.action';
import {ChangePrevUrl, HideFab} from './core/store/actions/app-ui.actions';
import {WorkoutAction} from './core/training-store/actions/workouts.action';
import {WorkoutProgramAction} from './core/training-store/actions/workout-programs.action';
import {routeTransitionAnimations} from './route-transition-animations';
import ResetMenuButton = Backdrop.ResetMenuButton;

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    animations: [routeTransitionAnimations]
})
export class AppComponent implements OnInit {

    constructor(private iconService: IconService,
                private _store: Store,
                private authorizationService: AuthorizationService,
                private router: Router,
    ) {
        router.events.pipe(
            filter((evt: any) => evt instanceof RoutesRecognized),
            pairwise(),
            withLatestFrom(this._store.select(BackdropState.getStatus)),
        ).subscribe(([event, status]) => {
            this._store.dispatch(new Toolbar.ResetToolbarButton());
            this._store.dispatch(new Backdrop.ResetSubHeader());
            this._store.dispatch(new HideFab());
            this._store.dispatch(new ResetMenuButton());
            if (!status || status !== BackdropStatus.TAB_NAVIGATION_OPEN) {
                this._store.dispatch(new Backdrop.HardClose());
            }

            if (event[0]) {
                this._store.dispatch(new ChangePrevUrl(event[0].url))
            }
        });
        iconService.initializeRequredIcons();
    }


    ngOnInit(): void {
        this.authorizationService.checkUserAuthorization();
        this._store.dispatch(new UserActions.LoadRequiredDataAction());
        this._store.dispatch(new MetadataActions.LoadMeasuresTypes());
        this._store.dispatch(new ExerciseActions.LoadExerciseAction());
        this._store.dispatch(new WorkoutAction.LoadWorkouts());
        this._store.dispatch(new WorkoutProgramAction.LoadWorkoutPrograms());
    }
}
