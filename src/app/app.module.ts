import {BrowserModule, HAMMER_GESTURE_CONFIG, HammerGestureConfig, HammerModule} from '@angular/platform-browser';
import {Injectable, NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {routing} from './app.routing';
import {AuthorizationModule} from './authorization/authorization.module';
import {CoreModule} from './core/core.module';
import {NgxsReduxDevtoolsPluginModule} from '@ngxs/devtools-plugin';
import {NgxsModule} from '@ngxs/store';
import {AppStates} from './core/store';
import {NgxsRouterPluginModule, RouterStateSerializer} from '@ngxs/router-plugin';
import {CustomRouterStateSerializer} from './router-state.serializer';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {MatTabsModule} from '@angular/material/tabs';
import {HttpClientXsrfModule} from '@angular/common/http';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {NgxsStoragePluginModule} from "@ngxs/storage-plugin";
import * as Hammer from 'hammerjs';

@Injectable()
export class HammerConfig extends HammerGestureConfig {
    overrides = <any>{
        'swipe': {direction: Hammer.DIRECTION_ALL}
    };
}

@NgModule({
    declarations: [
        AppComponent],
    imports: [
        CoreModule,
        BrowserModule,
        AuthorizationModule,
        routing,
        HttpClientXsrfModule,
        HammerModule,
        NgxsModule.forRoot(
            AppStates
            ,
            {
                developmentMode: !environment.production,
                selectorOptions: {
                    suppressErrors: false,
                    injectContainerState: false
                }
            },
        ),
        NgxsRouterPluginModule.forRoot(),
        NgxsStoragePluginModule.forRoot({
            key: ['trainingProcess', 'trainingProcessWidgetState', 'user', 'authorizationUserState', 'measureType']
        }),
        NgxsReduxDevtoolsPluginModule.forRoot({
            name: 'NGXS store',
            disabled: environment.production
        }),
        ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
        MatTabsModule,
        MatIconModule,
        MatButtonModule,
    ],
    providers: [
        {provide: RouterStateSerializer, useClass: CustomRouterStateSerializer},
        {
            provide: HAMMER_GESTURE_CONFIG,
            useClass: HammerConfig
        }
    ],
    bootstrap: [AppComponent],
})
export class AppModule {
}
