import { Injectable } from '@angular/core';
import {Store} from '@ngxs/store';
import {SetCurrentPageName} from './actions/app-ui.actions';
import {BackdropState} from './states/backdrop.state';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private store: Store) { }

  setCurrentPageName(testPageName: string) {
    this.store.dispatch(new SetCurrentPageName(testPageName));
  }

  getBackdropSettings() {
    return this.store.select(BackdropState.getSetting);
  }
}
