import {State} from '@ngxs/store';
import {AppUiState} from './app-ui.state';
import {Injectable} from '@angular/core';
import {MetadataState} from './metadata-state/metadata.state';

@State({
  name: 'app',
  children: [AppUiState, MetadataState]
})
@Injectable()
export class AppState {

}
