import {Action, Selector, State, StateContext} from '@ngxs/store';
import {BackdropSetting} from '../../type/backdrop-setting';
import {Backdrop} from '../actions/backdrop.actions';
import {Injectable} from '@angular/core';
import {BackdropMenuButton} from '../../../share/models/backdrop-menu-button';
import {BackdropSubheaderSettings} from '../../../share/models/backdrop-subheader-settings';

export enum BackdropStatus {
  OPEN = 'open',
  CLOSE = 'close',
  FULL_WIDTH = 'full_width',
  TAB_NAVIGATION_OPEN = 'tab_navigation_open'
}

export interface BackdropStateModel {
  backdropStateStatus: BackdropStatus;
  backdropSetting: BackdropSetting;
  previousBackdropSetting: BackdropSetting;
  menuButton: BackdropMenuButton;
  backdropSubHeader: BackdropSubheaderSettings;
  isSubPage: boolean;
  isNotResetState: boolean;
}

const menuButton = {icon: 'menu'};
const subHeaderInitial: BackdropSubheaderSettings = {closeTitle: null, openTitle: null};

@State<BackdropStateModel>({
  name: 'backdrop',
  defaults: {
    backdropStateStatus: BackdropStatus.CLOSE,
    backdropSetting: {},
    previousBackdropSetting: null,
    menuButton,
    backdropSubHeader: subHeaderInitial,
    isSubPage: false,
    isNotResetState: false
  },
})
@Injectable()
export class BackdropState {

  constructor() {
  }

  @Selector()
  public static getState(state: BackdropStateModel) {
    return state;
  }


  @Selector()
  public static isBackdropNotResatable(state: BackdropStateModel) {
    return state.isNotResetState;
  }

  @Selector([BackdropState])
  public static getStatus(state: BackdropStateModel) {
    return state.backdropStateStatus;
  }

  @Selector([BackdropState])
  public static getPreviousSettings(state: BackdropStateModel) {
    return state.previousBackdropSetting;
  }


  @Selector()
  public static getSetting(state: BackdropStateModel) {
    return state.backdropSetting;
  }

  @Selector()
  public static getMenuButton(state: BackdropStateModel) {
    return state.menuButton;
  }


  @Selector()
  public static getBackdroSubHeader(state: BackdropStateModel) {
    return state.backdropSubHeader;
  }

  @Action(Backdrop.Open)
  public openBackdrop(ctx: StateContext<BackdropStateModel>, {payload}: Backdrop.Open) {
    ctx.patchState(({
      backdropStateStatus: BackdropStatus.OPEN,
      backdropSetting: payload,
    }));
  }

  @Action(Backdrop.OpenMenu)
  public openMenu(ctx: StateContext<BackdropStateModel>) {
    const status = ctx.getState().backdropStateStatus;
    let config = null;
    if (status === BackdropStatus.TAB_NAVIGATION_OPEN) {
      config = ctx.getState().backdropSetting;
    }
    ctx.patchState(({
      backdropStateStatus: BackdropStatus.OPEN,
      backdropSetting: {
        webDownToHeight: 200,
        mobileDownToHeight: 200,
        nameBackdropPage: 'Меню',
        isCloseable: true,
        isNavigable: false
      },
      previousBackdropSetting: config
    }));
  }

  @Action(Backdrop.ResetPrevConfig)
  public resetPreviousConfig(ctx: StateContext<BackdropStateModel>) {
    ctx.patchState(({
      previousBackdropSetting: null
    }));
  }

  @Action(Backdrop.OpenTabNavigations)
  public openTabNavigationBackdrop(ctx: StateContext<BackdropStateModel>, {payload}: Backdrop.OpenTabNavigations) {
    ctx.patchState(({
      backdropStateStatus: BackdropStatus.TAB_NAVIGATION_OPEN,
      backdropSetting: {...payload, isNeedMenu: true}
    }));
  }

  @Action(Backdrop.SetUpBackdropPageName)
  public setUpBackdropPageName(ctx: StateContext<BackdropStateModel>, {payload}: Backdrop.SetUpBackdropPageName) {
    ctx.patchState(({
      backdropSetting: {...ctx.getState(), nameBackdropPage: payload}
    }));
  }

  @Action(Backdrop.CloseMenu)
  public closeMenu(ctx: StateContext<BackdropStateModel>) {
    const currentPageSettings = '';
    if (currentPageSettings) {
      this.openTabNavigationBackdrop(ctx, {payload: currentPageSettings});
    } else {
      ctx.patchState(({backdropStateStatus: BackdropStatus.CLOSE}));
    }
  }

  @Action(Backdrop.HardClose)
  public closeHard(ctx: StateContext<BackdropStateModel>) {
    ctx.patchState(({backdropStateStatus: BackdropStatus.CLOSE, previousBackdropSetting: null}));
  }

  @Action(Backdrop.FullBackdrop)
  public openBackdropFull(ctx: StateContext<BackdropStateModel>) {
    ctx.patchState(({backdropStateStatus: BackdropStatus.FULL_WIDTH}));
  }

  @Action(Backdrop.UseNavigateBackButton)
  public useCloseButtonInsteadOfMenu(ctx: StateContext<BackdropStateModel>, {callback}: Backdrop.UseNavigateBackButton) {
    ctx.patchState(({
      menuButton: {
        icon: 'keyboard_backspace', handler: callback
      }
    }));
  }

  @Action(Backdrop.ResetMenuButton)
  public resetMenuButton(ctx: StateContext<BackdropStateModel>) {
    if (!ctx.getState().isNotResetState) {
      ctx.patchState(({menuButton}));
    }
  }

  @Action(Backdrop.StateBecomeNotResetable)
  public stateBecomeNotResetable(ctx: StateContext<BackdropStateModel>) {
    ctx.patchState({
      isNotResetState: true
    });

  }
  @Action(Backdrop.StateBecomeResetable)
  public stateBecomeResetable(ctx: StateContext<BackdropStateModel>) {
    ctx.patchState({
      isNotResetState: false
    });
    this.resetMenuButton(ctx);
  }

  @Action(Backdrop.SetUpCloseBackdropSubHeaderTitle)
  public setUpBackdropSubheaderCloseTitle(ctx: StateContext<BackdropStateModel>,
                                          {backdropSubHeaderSettings}: Backdrop.SetUpCloseBackdropSubHeaderTitle) {
    ctx.patchState(({
      backdropSubHeader: backdropSubHeaderSettings
    }));
  }

  @Action(Backdrop.ResetSubHeader)
  public resetSubHeader(ctx: StateContext<BackdropStateModel>) {
    if(!ctx.getState().isNotResetState) {
      ctx.patchState(({
        backdropSubHeader: subHeaderInitial
      }));
    }
  }
}
