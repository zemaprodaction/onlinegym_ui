import {State, Selector, Action, StateContext} from '@ngxs/store';
import {ChangePrevUrl, FabBecomeResetable, HideFab, SetCurrentPageName, ShowFab, ShowFabWithoutReset} from '../actions/app-ui.actions';
import {BackdropState} from './backdrop.state';
import {ToolbarState} from './toolbar.state';
import {Injectable} from '@angular/core';
import {FabSettings} from '../../../share/models/fab-settings';

export interface AppUiStateModel {
    currentNamePage: string;
    fabButton: FabSettings;
    notResetButton: boolean;
    prevUrl: string
}

const fabInitial = {
    isShow: false,
    handler: null,
    name: '',
    icon: '',
};

@State<AppUiStateModel>({
    name: 'appUi',
    defaults: {
        currentNamePage: '',
        fabButton: fabInitial,
        notResetButton: false,
        prevUrl: null
    },
    children: [BackdropState, ToolbarState]
})
@Injectable()
export class AppUiState {

    @Selector()
    public static getState(state: AppUiStateModel) {
        return state;
    }

    @Selector()
    static currentPageName(state: AppUiStateModel) {
        return state.currentNamePage;
    }

    @Selector()
    static fabSettings(state: AppUiStateModel) {
        return state.fabButton;
    }

    @Selector()
    static getPrevUrl(state: AppUiStateModel) {
        return state.prevUrl;
    }

    @Action(SetCurrentPageName)
    public add(ctx: StateContext<AppUiStateModel>, {payload}: SetCurrentPageName) {
        ctx.patchState(({currentNamePage: payload}));
    }

    @Action(ShowFabWithoutReset)
    public showFabWithoutReset(ctx: StateContext<AppUiStateModel>, {fab}: ShowFab) {
        ctx.patchState(({
            fabButton: {
                isShow: true,
                ...fab
            },
            notResetButton: true
        }));
    }

    @Action(ShowFab)
    public showFab(ctx: StateContext<AppUiStateModel>, {fab}: ShowFab) {
        ctx.patchState(({
            fabButton: {
                isShow: true,
                ...fab
            }
        }));
    }

    @Action(ChangePrevUrl)
    public changePrevUrl(ctx: StateContext<AppUiStateModel>, {url}: ChangePrevUrl) {
        ctx.patchState(({
            prevUrl: url
        }));
    }

    @Action(FabBecomeResetable)
    public fabBecomeResetable(ctx: StateContext<AppUiStateModel>) {
        ctx.patchState(({
            notResetButton: false
        }));
        this.hideFab(ctx);
    }

    @Action(HideFab)
    public hideFab(ctx: StateContext<AppUiStateModel>) {
        if (!ctx.getState().notResetButton) {
            ctx.patchState(({
                fabButton: fabInitial
            }));
        }
    }

}
