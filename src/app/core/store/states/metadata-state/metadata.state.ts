import {State} from '@ngxs/store';
import {Injectable} from '@angular/core';
import {MeasureTypeState} from './measure-types.state';


@State({
  name: 'metadata',
  defaults: {},
  children: [MeasureTypeState]
})
@Injectable()
export class MetadataState {

}
