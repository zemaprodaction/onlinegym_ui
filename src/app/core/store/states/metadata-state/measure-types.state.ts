import {Action, Selector, State, StateContext} from '@ngxs/store';
import {Injectable} from '@angular/core';
import {defaultsEntityState, EntityState, EntityStateModel} from '../../../../share/store/entity.state';
import {MeasureServiceApi} from '../../../services/api/measure-service-api.service';
import {MeasuresType} from '../../../../share/models/measuresType';
import {MetadataActions} from '../../actions/metadata.actions';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface MeasureTypesStateModel extends EntityStateModel<MeasuresType> {
}

@State<MeasureTypesStateModel>({
  name: 'measureType',
  defaults: {
    ...defaultsEntityState,
  },
})
@Injectable()
export class MeasureTypeState extends EntityState<MeasuresType> {

  constructor(private measureServiceApi: MeasureServiceApi) {
    super(measureServiceApi);
  }

  @Selector()
  public static getState(state: MeasureTypesStateModel) {
    return state;
  }

  @Action(MetadataActions.LoadMeasuresTypes)
  loadMeasureType(ctx: StateContext<EntityStateModel<MeasuresType>>) {
    return this.load(ctx);
  }
}
