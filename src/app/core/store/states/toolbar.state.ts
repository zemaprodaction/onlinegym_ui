import {Action, Selector, State, StateContext, Store} from '@ngxs/store';
import {ToolbarAction} from '../../../share/models/toolbar-action';
import {Toolbar} from '../actions/toolbar.action';
import {append, patch} from '@ngxs/store/operators';
import {Injectable} from '@angular/core';
import {BackdropState} from "./backdrop.state";

export interface ToolbarStateModel {
    actions: ToolbarAction[];
    logAction: ToolbarAction;
    isShowLoginButton: boolean;
}

@State<ToolbarStateModel>({
    name: 'toolbar',
    defaults: {
        actions: [],
        logAction: null,
        isShowLoginButton: true
    }
})
@Injectable()
export class ToolbarState {

    constructor(private store: Store) {
    }

    @Selector()
    public static getState(state: ToolbarAction) {
        return state;
    }

    @Selector()
    static actions(state: ToolbarStateModel) {
        return state.actions;
    }

    @Selector()
    static getIsShowLoginButton(state: ToolbarStateModel) {
        return state.isShowLoginButton;
    }

    @Action(Toolbar.LoadAction)
    addActionToolbar(ctx: StateContext<ToolbarStateModel>, {payload}: Toolbar.LoadAction) {
        ctx.setState(
            patch({
                actions: append([payload])
            }));
    }

    @Action(Toolbar.ResetToolbarButton)
    resetToolbarActions(ctx: StateContext<ToolbarStateModel>) {
        const isBackdropResatable = this.store.selectSnapshot(BackdropState.isBackdropNotResatable)
        if (!isBackdropResatable) {
            ctx.patchState({actions: []});
        }
    }

    @Action(Toolbar.HideLoginButton)
    hideLoginButton(ctx: StateContext<ToolbarStateModel>) {
        ctx.patchState({
            isShowLoginButton: false
        })
    }

    @Action(Toolbar.ShowLoginButton)
    showLoginButton(ctx: StateContext<ToolbarStateModel>) {
        ctx.patchState({
            isShowLoginButton: true
        })
    }
}
