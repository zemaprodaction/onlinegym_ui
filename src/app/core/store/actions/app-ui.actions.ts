import {FabSettings} from '../../../share/models/fab-settings';

export class SetCurrentPageName {
  public static readonly type = '[App] Set Current Page Name';

  constructor(public payload: string) {
  }
}

export class ShowFab {
  public static readonly type = '[App] Show Fab';

  constructor(public fab: FabSettings) {
  }
}

export class ChangePrevUrl {
  public static readonly type = '[App] Change Prev Url';

  constructor(public url: string) {
  }
}
export class ShowFabWithoutReset {
  public static readonly type = '[App] Show Fab Without Reset';

  constructor(public fab: FabSettings) {
  }
}
export class HideFab {
  public static readonly type = '[App] Hide Fab';
}
export class FabBecomeResetable {
  public static readonly type = '[App] Fab Become Resetable';

  constructor() {
  }
}
