import {ToolbarAction} from '../../../share/models/toolbar-action';

export namespace Toolbar {

  export class LoadAction {
    public static readonly type = '[Toolbar] Add Toolbar Action';

    constructor(public payload: ToolbarAction) {
    }
  }

  export class ResetToolbarButton {
    public static readonly type = '[Toolbar] Reset All Toolbar Actions';

    constructor() {
    }
  }
  export class HideLoginButton {
    public static readonly type = '[Toolbar] Hide login button';

    constructor() {
    }
  }
  export class ShowLoginButton {
    public static readonly type = '[Toolbar] Show Login Button';

    constructor() {
    }
  }
}
