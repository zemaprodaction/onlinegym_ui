import {BackdropSetting} from '../../type/backdrop-setting';
import {BackdropSubheaderSettings} from '../../../share/models/backdrop-subheader-settings';

export namespace Backdrop {
  export class Open {
    public static readonly type = '[Backdrop] Open backdrop';

    constructor(public payload: BackdropSetting) {
    }
  }

  export class OpenTabNavigations {
    public static readonly type = '[Backdrop] Open Tab navigations';

    constructor(public payload: BackdropSetting) {
    }
  }

  export class CloseMenu {
    public static readonly type = '[Backdrop] Close backdrop';
  }

  export class HardClose {
    public static readonly type = '[Backdrop] Hard close backdrop';
  }

  export class FullBackdrop {
    public static readonly type = '[Backdrop] Open Full backdrop';
  }

  export class SetUpBackdropPageName {
    public static readonly type = '[Backdrop] Set Up Backdrop Page Name';

    constructor(public payload: string) {
    }
  }

  export class OpenMenu {
    public static readonly type = '[Backdrop] Open Menu';
  }

  export class ResetPrevConfig {
    public static readonly type = '[Backdrop] Reset Previous Config';
  }

  export class UseNavigateBackButton {
    public static readonly type = '[Backdrop] Use Close Button Instead Of Menu';

    constructor(public callback: () => void) {
    }
  }

  export class ResetMenuButton {
    public static readonly type = '[Backdrop] Reset Menu Button';
  }


  export class StateBecomeNotResetable {
    public static readonly type = '[Backdrop] State become not resetable';
  }

  export class StateBecomeResetable {
    public static readonly type = '[Backdrop] State become resetable';
  }
  export class SetUpCloseBackdropSubHeaderTitle {
    public static readonly type = '[Backdrop] Set Up Close Backdrop SubHeader Title';

    constructor(public backdropSubHeaderSettings: BackdropSubheaderSettings) {
    }
  }

  export class ResetSubHeader {
    public static readonly type = '[Backdrop] Reset Backdrop Sub Header';

  }
}
