import {Action, createSelector, Selector, State, StateContext, Store} from '@ngxs/store';
import {UserActions} from '../actions';
import {UserServiceApi} from '../../services/user-service-api.service';
import {catchError, filter, switchMap, tap} from 'rxjs/operators';
import {SnackBarService} from '../../services/snack-bar.service';
import {Navigate, RouterState} from '@ngxs/router-plugin';
import {Injectable} from '@angular/core';
import {WorkoutService} from '../../services/workout.service';
import {WorkoutProgramState, WorkoutProgramStateModel} from '../../training-store/states/workout-program/workout-program.state';
import {WorkoutProgramAction} from '../../training-store/actions/workout-programs.action';
import {WorkoutProgramEntity} from '../../../share/models/workout-programs';
import {DateService} from '../../../share/utils/date.service';
import {RouterStateModel} from '../../../router-state.model';
import {EntityStateModel} from '../../../share/store/entity.state';
import {WorkoutInstanceScheduler} from '../../../share/models/workout-instance-scheduler';
import {ScheduledDay} from '../../../share/models/workout/scheduled-day';
import {UserWorkoutProgram, UserWorkoutProgramDto} from '../../../share/models/user-workout-program';
import {AuthorizationUserState} from "../../../authorization/store/states/authorization-user.state";
import LoadRequiredDataAction = UserActions.LoadRequiredDataAction;
import {WorkoutState} from "../../training-store/states/workout.state";
import {WorkoutProgramScheduler} from "../../../share/models/workout-program-scheduler";
import {UserWorkoutInstanceEntity} from "../../../share/models/workout/workout-instance-entity";

export interface UserStateModel {
    userWorkoutProgram: UserWorkoutProgram;
}

const initialState = {
    userWorkoutProgram: null
};

@State<UserStateModel>({
    name: 'user',
    defaults: {...initialState}
})
@Injectable()
export class UserState {

    constructor(private userServiceApi: UserServiceApi,
                private workoutService: WorkoutService,
                private store: Store,
                private snackBarService: SnackBarService) {
    }

    public static getWorkoutInstanceByDayIndex() {
        return createSelector([UserState.getUserScheduler, RouterState.state],
            (sheduler: WorkoutProgramScheduler, routerState: RouterStateModel): WorkoutInstanceScheduler => {
                if (sheduler) {
                    const day = sheduler.listOfDays[routerState.params['dayIndex']]
                    const workoutInstance = day.userWorkoutInstanceList[0];
                    return {workoutInstance, day: day.date};
                }
            });
    }

    public static getWorkoutInstanceById(workoutInstanceId: string) {
        return createSelector([UserState.getUserScheduler],
            (userState: UserStateModel): UserWorkoutInstanceEntity => {
                if (userState) {
                    return userState.userWorkoutProgram.workoutProgramScheduler
                        .listOfDays.flatMap(day => day.userWorkoutInstanceList).find(wi => wi.id === workoutInstanceId)
                }
            });
    }

    public static getScheduledDayByWorkoutInstanceId(workoutInstanceId: string) {
        return createSelector([UserState.getUserScheduler],
            (userState: UserStateModel): ScheduledDay => {
                if (userState) {
                    return userState.userWorkoutProgram.workoutProgramScheduler
                        .listOfDays.find(day => day.userWorkoutInstanceList.find(wi => wi.id === workoutInstanceId) !== null)
                }
            });
    }

    static getSelectedEntity<R>() {
        return createSelector([this, RouterState.state],
            (state: EntityStateModel<R>, routerState: RouterStateModel) => state.entities &&
                state.entities[routerState.params[state.routerSelectedParams]]
        );
    }

    @Selector([UserState, WorkoutState])
    public static getUserTrainingProcessInfo(userState: UserStateModel) {
        return userState;
    }


    @Selector([UserState])
    public static getUserScheduler(userState: UserStateModel) {
        return userState.userWorkoutProgram.workoutProgramScheduler;
    }


    @Selector([UserState])
    public static getUserWorkoutProgramId(userState: UserStateModel) {
        return userState.userWorkoutProgram?.userWorkoutProgramId;
    }

    @Selector([UserState])
    public static getTrainingWorkoutProgramId(userState: UserStateModel) {
        return userState.userWorkoutProgram?.trainingWorkoutProgramId;
    }

    @Selector([UserState, WorkoutProgramState])
    public static getTrainingWorkoutProgram(userState: UserStateModel, wpStateModel: WorkoutProgramStateModel) {
        return wpStateModel.entities[userState.userWorkoutProgram?.trainingWorkoutProgramId];
    }

    @Selector([UserState, WorkoutProgramState])
    public static getUserWorkoutProgram(userState: UserStateModel, wpStateModel: WorkoutProgramStateModel) {
        userState.userWorkoutProgram
        const userWorkoutProgramDto: UserWorkoutProgramDto = {...userState.userWorkoutProgram,
            trainingWorkoutProgram: wpStateModel.entities[userState.userWorkoutProgram?.trainingWorkoutProgramId]}
        return userWorkoutProgramDto;
    }

    @Selector([UserState])
    public static getTodayWorkout(userState: UserStateModel) {
        return this.getToday(userState).userWorkoutInstanceList;
    }

    @Selector([UserState])
    public static getToday(userState: UserStateModel) {
        if (userState.userWorkoutProgram?.workoutProgramScheduler?.listOfDays) {
            const today = DateService.convertDataToBackEndPattern(new Date());
            return userState.userWorkoutProgram.workoutProgramScheduler.listOfDays
                .find(day => {
                    return DateService.convertDataToBackEndPattern(new Date(day.date + 'Z')) === today
                });
        }
    }

    @Action(UserActions.EnrollAction)
    public enrollOnWorkoutProgram(ctx: StateContext<UserStateModel>, {workoutProgramId}: UserActions.EnrollAction) {
        return this.userServiceApi.enrollOnProgram(workoutProgramId).pipe(
            tap(userTrainingInfo => {
                ctx.patchState({
                    userWorkoutProgram: userTrainingInfo.userWorkoutProgram
                });
                ctx.dispatch(new LoadRequiredDataAction());
                ctx.dispatch(new Navigate(['user']));
            }),
            catchError((err => ctx.dispatch(new UserActions.EnrollFailedAction(err))))
        );
    }

    @Action(UserActions.ResetState)
    public resetState(ctx: StateContext<UserStateModel>) {
        ctx.setState({
            ...initialState
        });
    }

    @Action(UserActions.StartWorkoutProgramAction)
    public startUserWorkoutProgram(ctx: StateContext<UserStateModel>) {
        return this.userServiceApi.startUserWorkoutProgram().pipe(
            tap(scheduler => {
                ctx.patchState({
                    userWorkoutProgram: {...ctx.getState().userWorkoutProgram, workoutProgramScheduler: scheduler}
                });
            }),
        );
    }


    @Action(UserActions.EnrollFailedAction)
    public enrollOnWorkoutProgramFailed(ctx: StateContext<UserStateModel>, action: UserActions.EnrollFailedAction) {
        this.snackBarService.openSnackBar(action.payload.error.message);
    }

    @Action(UserActions.UnsubscribeWorkoutProgramAction)
    public unsubscribeFromWorkoutProgram(ctx: StateContext<UserStateModel>) {
        return this.userServiceApi.unsubscribeFromWorkoutProgram().pipe(
            tap(() => {
                ctx.setState({
                    ...initialState
                });
            })
        );
    }

    @Action(UserActions.UserWorkoutProgramRestartAction)
    public userWorkoutProgramRestart(ctx: StateContext<UserStateModel>) {
        return this.userServiceApi.restartWorkoutProgram().pipe(
            tap(_ => {
                ctx.patchState({
                    userWorkoutProgram: {
                        ...ctx.getState().userWorkoutProgram,
                        workoutProgramScheduler: null,
                    }
                });
            })
        );
    }

    @Action(UserActions.LoadUserTrainingProcessInfo)
    public loadUserWorkoutProgram(ctx: StateContext<UserStateModel>) {
        return this.userServiceApi.getUserTrainingProcessInfo().pipe(
            tap(wp => {
                ctx.patchState(
                    {...wp}
                );
            })
        );
    }

    @Action(UserActions.LoadRequiredDataAction)
    public loadRequiredUserData(ctx: StateContext<UserStateModel>) {
        const currentUser = this.store.selectSnapshot(AuthorizationUserState.getCurrentUser)
        if (currentUser && currentUser.isAuthorized) {
            this.store.dispatch(new UserActions.LoadUserTrainingProcessInfo());
            return this.store.select(UserState.getUserWorkoutProgramId).pipe(
                filter(wpId => wpId !== '' && wpId !== null && wpId !== undefined),
                tap(() => this.store
                    .dispatch(new WorkoutProgramAction.LoadWorkoutProgramById(ctx.getState().userWorkoutProgram.userWorkoutProgramId))),
                switchMap((wpId: string) => this.store.select(WorkoutProgramState.getEntityById(wpId))),
                filter(workoutProgram => workoutProgram !== null && workoutProgram !== undefined),
                tap((workoutProgram) => this.workoutService
                    .loadWorkoutWithTheirExerciseByWorkoutIdList((workoutProgram as WorkoutProgramEntity).workoutsOnProgram))
            );
        }
    }

    @Action(UserActions.WorkoutInstanceFinished)
    public workoutInstanceFinish(ctx: StateContext<UserStateModel>,
                                 payload: UserActions.WorkoutInstanceFinished) {
        const scheduler = ctx.getState().userWorkoutProgram.workoutProgramScheduler;
        const listDays = ctx.getState().userWorkoutProgram.workoutProgramScheduler.listOfDays;
        const today = DateService.convertDataToBackEndPattern(new Date());
        const finishedWorkout = UserState.getScheduledDayByWorkoutInstanceId(payload.workoutInstanceId)(ctx.getState());
        const newWorkoutsToday: ScheduledDay = {
            ...finishedWorkout,
            userWorkoutInstanceList: [...finishedWorkout.userWorkoutInstanceList,
                {...finishedWorkout.userWorkoutInstanceList.find(w => w.id === payload.workoutInstanceId), status: "FINISHED"}]
        };
        const newListofDays = {...listDays.filter(day => day.date !== today), [today]: newWorkoutsToday};
        return ctx.patchState({
            userWorkoutProgram: {...ctx.getState().userWorkoutProgram, workoutProgramScheduler: {...scheduler, listOfDays: newListofDays}}
        });
    }


}
