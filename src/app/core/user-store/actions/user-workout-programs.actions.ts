export namespace UserActions {

  export class EnrollAction {
    public static readonly type = '[User] User enroll on workout program';

    constructor(public workoutProgramId: string) {
    }
  }

  export class StartWorkoutProgramAction {
    public static readonly type = '[User] Start workout program action';

    constructor() {
    }
  }

  export class EnrollFailedAction {
    public static readonly type = '[User] User enroll on workout program Failed';

    constructor(public payload: any) {
    }
  }

  export class UnsubscribeWorkoutProgramAction {
    public static readonly type = '[User] User unsubscribe from workout program';
  }

  export class UserWorkoutProgramRestartAction {
    public static readonly type = '[User] User Workout Program Restart';
  }

  export class WorkoutInstanceFinished {
    public static readonly type = '[User] Workout instance entity was finished';

    constructor(public workoutInstanceId: string, public day: string) {
    }
  }

  export class LoadUserTrainingProcessInfo {
    public static readonly type = '[User] Load User workout program';
  }

  export class ResetState {
    public static readonly type = '[User] Reset State';
  }

  export class LoadRequiredDataAction {
    public static readonly type = '[User] Load User required data';
  }
}



