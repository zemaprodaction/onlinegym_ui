import {Directive, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appToolbarContextHost]'
})
export class ToolbarContextHostDirective {

  constructor(public viewContainerRef: ViewContainerRef) {}

}
