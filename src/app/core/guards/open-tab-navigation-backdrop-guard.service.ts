import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {BackdropService} from '../services/backdrop/backdrop.service';
import {BackdropTabNavigationComponent} from '../../trainings/components';
import {of} from 'rxjs';
import {AuthGuard} from '../../share/guards/auth.guard';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OpenTabNavigationBackdropGuard implements CanActivate {
  constructor(private backdropService: BackdropService, private authGuard: AuthGuard) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.authGuard.canActivate(route, state).pipe(
      tap(guard => {
        if (guard) {
          this.backdropService.openTabsBackdrop(BackdropTabNavigationComponent,
            {
              webDownToHeight: 56,
              mobileDownToHeight: 56,
              nameBackdropPage: route.data.namePage,
              isShouldNotClose: true,
              navLinks: route.data.navLinks,
              componentName: BackdropTabNavigationComponent.name
            });
          return of(true);
        } else {
          return of(true);
        }
      })
    );
  }

  canActivateWithoutGuard(route: ActivatedRouteSnapshot) {
    this.backdropService.openTabsBackdrop(BackdropTabNavigationComponent,
      {
        webDownToHeight: 56,
        mobileDownToHeight: 56,
        nameBackdropPage: route.data.namePage,
        isShouldNotClose: true,
        navLinks: route.data.navLinks,
        componentName: BackdropTabNavigationComponent.name
      });
    return of(true);
  }
}
