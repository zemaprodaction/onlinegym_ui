import {Injectable} from '@angular/core';
import {CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {BackdropService} from '../services/backdrop/backdrop.service';

@Injectable({
  providedIn: 'root'
})
export class CloseBackdropGuard implements CanDeactivate<unknown> {

  constructor(private backdropService: BackdropService) {
  }

  canDeactivate(
    component: unknown,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    this.backdropService.closeBackdrop();
    return true;

  }

}
