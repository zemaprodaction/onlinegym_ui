import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate} from '@angular/router';
import {of} from 'rxjs';
import {Location} from "@angular/common";
import {Backdrop} from "../store/actions/backdrop.actions";
import {Store} from "@ngxs/store";
import {AppUiState} from "../store";

@Injectable({
  providedIn: 'root'
})
export class AddBackToToolbarGuard implements CanActivate {
  constructor(private store: Store, private _location: Location) {
  }

  canActivate(route: ActivatedRouteSnapshot) {
    const prevUrl = this.store.selectSnapshot(AppUiState.getPrevUrl)
    if(prevUrl !== null) {
      const callBack = () => this._location.back();
      this.store.dispatch(new Backdrop.UseNavigateBackButton(callBack));
    }
    return of(true);
  }
}
