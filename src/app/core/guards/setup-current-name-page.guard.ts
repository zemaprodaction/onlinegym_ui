import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate} from '@angular/router';
import {of} from 'rxjs';
import {Store} from '@ngxs/store';
import {SetCurrentPageName} from '../store/actions/app-ui.actions';
import {Backdrop} from '../store/actions/backdrop.actions';

@Injectable({
  providedIn: 'root'
})
export class SetupCurrentNamePageGuard implements CanActivate {

  constructor(private store: Store) {
  }

  canActivate(route: ActivatedRouteSnapshot) {
    if(route.data.currentNamePage) {
      this.store.dispatch(new SetCurrentPageName(route.data.currentNamePage));
    }
    if(route.data.backdropSubheaderSettings){
      this.store.dispatch(new Backdrop.SetUpCloseBackdropSubHeaderTitle(route.data.backdropSubheaderSettings));
    }
    return of(true);

  }
}
