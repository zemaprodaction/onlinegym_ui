import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate} from '@angular/router';
import {BackdropService} from '../services/backdrop/backdrop.service';
import {of} from 'rxjs';
import {ToolbarAction} from '../../share/models/toolbar-action';
import {BackdropSetting} from '../type/backdrop-setting';

@Injectable({
  providedIn: 'root'
})
export class AddActionToToolbarGuard implements CanActivate {
  constructor(private backdropService: BackdropService) {
  }

  canActivate(route: ActivatedRouteSnapshot) {
    const toolbarAction: ToolbarAction = route.data.action;
    const component = route.data.component;
    const settings = route.data.backdropSettings;
    const button = {...toolbarAction.button, callback: () => this.openFilter(component, settings)};
    this.backdropService.addActionToBackdrop({...toolbarAction, button});
    return of(true);

  }

  private openFilter(component: any, settings: BackdropSetting) {
    this.backdropService.openBackdropWithComponentCallback(component, settings);
  }
}
