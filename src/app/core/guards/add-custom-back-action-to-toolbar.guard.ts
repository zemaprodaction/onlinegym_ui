import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate} from '@angular/router';
import {of} from 'rxjs';
import {Backdrop} from "../store/actions/backdrop.actions";
import {Store} from "@ngxs/store";
import {Navigate} from "@ngxs/router-plugin";

@Injectable({
    providedIn: 'root'
})
export class AddCustomBackActionToToolbarGuard implements CanActivate {
    constructor(private store: Store) {
    }

    canActivate(route: ActivatedRouteSnapshot) {
        const callBack = () => this.store.dispatch(new Navigate(route.data.goToUrl));
        this.store.dispatch(new Backdrop.UseNavigateBackButton(callBack));
        return of(true);

    }
}
