import {fakeAsync, TestBed, tick} from '@angular/core/testing';

import {WorkoutService} from './workout.service';
import {NgxsModule, Store} from '@ngxs/store';
import {WorkoutState} from '../training-store/states/workout.state';
import {WorkoutAction} from '../training-store/actions/workouts.action';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TimerService} from './timer.service';
import {timer} from 'rxjs';

describe('TimerService', () => {
  let service: TimerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot([WorkoutState]),
        HttpClientTestingModule]
    });
    service = TestBed.inject(TimerService);

  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('when init service', () => {
    it('should dispatch LoadWorkoutByIdListAction action', fakeAsync(() => {
      service.startTraining();
      let number = 0;
      service.getTrainingTimer().subscribe(val => {
        number = val;
      });
      tick(3000);
      service.timerComplete();
      tick(1000);
      expect(number).toEqual(2);
    }));
  });

});
