import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import * as fromConstants from '../../share/constants/endpointConstants';
import {catchError, map} from 'rxjs/operators';
import {HttpClient, HttpParams} from '@angular/common/http';
import {ExerciseGroupingStatistic} from '../../share/models/exercise/exercise-grouping-statistic';

export enum StatisticDateRange {
  allTime = 'all_time',
  month = 'month',
  week = 'week',
  year = 'year'
}

@Injectable({
  providedIn: 'root'
})
export class ExerciseProgressService {

  constructor(private http: HttpClient) {
  }

  public getCharacteristicBaseOnExercise(dateRange: StatisticDateRange): Observable<ExerciseGroupingStatistic> {
    const params = new HttpParams().set('dateRange', dateRange);
    return this.http.get<ExerciseGroupingStatistic>(
      `${fromConstants.StatisticConstants.ROOT_STATISTIC_EXERCISE}/group_by_type`, {
        params
      })
      .pipe(
        map((result) => result['groupingStatistic']),
        catchError((error: any) => throwError(error))
      );
  }
}
