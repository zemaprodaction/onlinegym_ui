import {Injectable} from '@angular/core';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';


@Injectable({providedIn: 'root'})
export class IconService {

  constructor(private matIconRegistry: MatIconRegistry, private domSanitizer: DomSanitizer) {

  }

  public initializeRequredIcons() {
    this.matIconRegistry.addSvgIcon(
      'cardiogram',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/cardiogram.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'dumbbell',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/dumbbell.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'notepad',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/notepad.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'heart-statistic',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/heart.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'workout-program-calendar',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/workout-program-calendar.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'workout-program-required',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/workout-program-required.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'workout-program-watch',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/workout-program-watch.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'champion-cup',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/champion-cup.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'muscle',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/muscle.svg')
    );
  }
}
