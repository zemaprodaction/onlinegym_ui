import {Injectable} from '@angular/core';
import {Store} from '@ngxs/store';
import {WorkoutAction} from '../training-store/actions/workouts.action';
import {WorkoutState} from '../training-store/states/workout.state';

@Injectable({
  providedIn: 'root'
})
export class WorkoutService {

  constructor(private store: Store) {
  }

  public loadWorkoutWithTheirExerciseByWorkoutIdList(idList: string[]) {

    this.store.dispatch(new WorkoutAction.LoadWorkoutWithTheirExerciseByWorkoutIdList(idList));
  }

  public getWorkoutSnapshotById(id: string) {
    return this.store.selectSnapshot(WorkoutState.getWorkoutById(id));
  }

  public getWorkoutById(id: string) {
    return this.store.select(WorkoutState.getWorkoutById(id));
  }

}
