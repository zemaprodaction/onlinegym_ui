import {Injectable} from '@angular/core';
import {Store} from '@ngxs/store';
import {Observable} from 'rxjs';
import {MeasuresType} from '../../share/models/measuresType';
import {MeasureTypeState} from '../store/states/metadata-state/measure-types.state';
import {MetadataActions} from '../store/actions/metadata.actions';

@Injectable({
  providedIn: 'root'
})
export class MeasureService {

  constructor(private store: Store) {
  }

  getMeasureTypes(): Observable<MeasuresType[]> {
    return this.store.select(MeasureTypeState.getEntitiesAsArray<MeasuresType>());
  }

  loadMeasureService() {
    return this.store.dispatch(new MetadataActions.LoadMeasuresTypes());
  }
}
