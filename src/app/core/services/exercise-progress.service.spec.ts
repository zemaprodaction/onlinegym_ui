import {TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {ExerciseProgressService, StatisticDateRange} from './exercise-progress.service';
import * as fromConstants from '../../share/constants/endpointConstants';

describe('ExerciseProgresService', () => {
  let service: ExerciseProgressService;
  let http: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(ExerciseProgressService);
    http = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('when call get measure type metadata method', () => {
    it('should send request to specific url', () => {
      service.getCharacteristicBaseOnExercise(StatisticDateRange.allTime).subscribe((val) => {
        expect(val).toEqual([]);
      });
      const req = http.expectOne(`${fromConstants.StatisticConstants.ROOT_STATISTIC_EXERCISE}/group_by_type?dateRange=all_time`);
      expect(req.request.method).toEqual('GET');
      req.flush({});
    });
  });
});
