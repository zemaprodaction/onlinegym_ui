import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {WorkoutProgramEntity} from '../../share/models/workout-programs';
import {CrudService} from '../../trainings/service/crud.service';
import {TrainingEndpointConstants} from '../../share/constants/endpointConstants';


@Injectable({providedIn: 'root'})
export class WorkoutProgramServiceApi extends CrudService<WorkoutProgramEntity, string> {

  constructor(private http: HttpClient) {
    super(http, TrainingEndpointConstants.WORKOUT_PROGRAM);
  }
}
