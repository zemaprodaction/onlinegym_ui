import {Injectable} from '@angular/core';
import {BehaviorSubject, NEVER, Observable, of, timer} from 'rxjs';
import {switchMap, takeWhile, withLatestFrom} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class TimerService {

    private subscription: Observable<number>;
    private isComplete = false;
    private pause: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    constructor() {
    }

    public startTraining(fromTime: number = 0): Observable<number> {
            this.isComplete = false;
            this.subscription = timer(1000, 1000).pipe(
                withLatestFrom(this.pause),
                switchMap(([a1, a2]) => a2 ? NEVER : of(a1)),
                takeWhile(_ => !this.isComplete)
            );

        return this.subscription;
    }

    public getTrainingTimer(): Observable<number> {
        return this.subscription;
    }

    timerComplete() {
        this.isComplete = true;
    }

    timerPause() {
        this.pause.next(true);
    }

    timerResume() {
        this.pause.next(false);
    }
}
