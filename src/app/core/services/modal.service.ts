import {Injectable} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ComponentType} from '@angular/cdk/overlay';

@Injectable({
    providedIn: 'root'
})
export class ModalService {

    private isOpen = false

    constructor(public dialog: MatDialog) {
    }

    openDialog(componentRef: ComponentType<any>, message?: string, value?: any) {
        if (!this.isOpen) {
            this.isOpen = true
            return this.dialog.open(componentRef, {
                width: '300px',
                data: {value: value, message: message}
            }).afterClosed().subscribe(_ => {
                this.isOpen = false
            });
        }
    }

    openDialogWithCallBack(componentRef: ComponentType<any>) {
        this.isOpen = true
        return this.dialog.open(componentRef, {
            width: '300px',
        })
    }
}
