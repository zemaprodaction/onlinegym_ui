import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {TrainingEndpointConstants} from '../../share/constants/endpointConstants';
import {Observable} from 'rxjs';
import {CharacteristicPropertyFilter} from '../../share/models/characteristic-property-filter';

@Injectable({
  providedIn: 'root'
})
export class FilterService {

  constructor(private http: HttpClient) {
  }

  public getAllFiltersForWorkoutProgram(): Observable<CharacteristicPropertyFilter[]> {
    return this.http.get<CharacteristicPropertyFilter[]>(TrainingEndpointConstants.WORKOUT_PROGRAM_FILTERS);
  }
}
