import {Injectable} from '@angular/core';
import {Store} from '@ngxs/store';
import {ExerciseActions} from '../training-store/actions/exercise.action';

@Injectable({
  providedIn: 'root'
})
export class ExerciseService {

  constructor(private store: Store) {
  }

  public loadExerciseByIdListAction(idList: string[]) {
    this.store.dispatch(new ExerciseActions.LoadExerciseByIdListAction(idList));
  }

}
