import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {StatisticConstants} from '../../../share/constants/endpointConstants';
import {catchError} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import {CurrentWorkoutProgramStatistic} from '../../../user-progress/types/workout-program-statistic';

@Injectable({
  providedIn: 'root'
})
export class UserWorkoutProgramStatisticService {

  constructor(protected _http: HttpClient) {
  }

  getUserWorkoutProgramStatistic(userWorkoutProgramId: string): Observable<CurrentWorkoutProgramStatistic> {
    console.log(userWorkoutProgramId);
    return this._http.get<CurrentWorkoutProgramStatistic>
    (`${StatisticConstants.STATISTIC_WORKOUT_PROGRAM}/${userWorkoutProgramId}`)
      .pipe(catchError((error: any) => throwError(error.json())));
  }
}

