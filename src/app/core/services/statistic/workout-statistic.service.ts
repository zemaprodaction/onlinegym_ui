import {Injectable} from '@angular/core';
import {Store} from '@ngxs/store';
import {WorkoutProgressApiService} from './workout-progress-api.service';
import {map, switchMap} from 'rxjs/operators';
import {WorkoutState} from '../../training-store/states/workout.state';
import {Observable} from 'rxjs';
import {WorkoutRating} from '../../type/workout-rating';
import {WorkoutData} from '../../../share/models/workout-data';

@Injectable({
  providedIn: 'root'
})
export class WorkoutStatisticService {

  constructor(private store: Store, private workoutProgressServiceApi: WorkoutProgressApiService) {
  }

  getTheMostPopularWorkout(): Observable<WorkoutRating[]> {
    return this.workoutProgressServiceApi.getWorkoutProgramStatistic().pipe(
      switchMap(result => this.store.select(WorkoutState.getListWorkoutByListId(result.map(r => r.workoutId)))
        .pipe(
          map(workoutList => workoutList.map(workout => ({
            workoutId: workout.id,
            workout,
            rating: result.find(r => r.workoutId === workout.id).rating
          })))
        )),
    );
  }

  getUserHistory(take: number): Observable<WorkoutData[]> {
    return this.workoutProgressServiceApi.getUserHistory(take);
  }

  getAlUserHistory(): Observable<WorkoutData[]> {
    return this.workoutProgressServiceApi.getUserHistory(0);
  }
}
