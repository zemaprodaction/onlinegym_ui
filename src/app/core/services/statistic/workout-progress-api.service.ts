import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {StatisticConstants} from '../../../share/constants/endpointConstants';
import {catchError} from 'rxjs/operators';
import {WorkoutRating} from '../../type/workout-rating';
import {WorkoutData} from '../../../share/models/workout-data';

@Injectable({
  providedIn: 'root'
})
export class WorkoutProgressApiService {

  constructor(protected _http: HttpClient) {
  }

  getWorkoutProgramStatistic(): Observable<WorkoutRating[]> {
    return this._http.get<WorkoutRating[]>(StatisticConstants.WORKOUT_RATING)
      .pipe(catchError((error: any) => throwError(error.json())));
  }

  getUserHistory(take: number) {
    const params = new HttpParams().set('take', take.toString());
    return this._http.get<WorkoutData[]>(StatisticConstants.WORKOUT_HISTORY, {params})
      .pipe(catchError((error: any) => throwError(error.json())));
  }
}
