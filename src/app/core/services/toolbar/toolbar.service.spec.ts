import {ToolbarService} from './toolbar.service';
import {TestBed} from '@angular/core/testing';
import {NgxsModule, Store} from '@ngxs/store';
import {AppStates} from '../../store';
import {ToolbarAction} from '../../../share/models/toolbar-action';
import {ToolBarButton} from '../../../share/models/tool-bar-button';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('Toolbar service', () => {
  let service: ToolbarService;
  let store: Store;
  const mockToolbarState = {
    buttons: [new ToolbarAction(new ToolBarButton()), new ToolbarAction(new ToolBarButton())]
  };
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NgxsModule.forRoot(AppStates),
        HttpClientTestingModule
      ]
    });
    store = TestBed.inject(Store);
    store.reset({
      ...store.snapshot(),
      toolbar: mockToolbarState
    });

    service = TestBed.inject(ToolbarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
