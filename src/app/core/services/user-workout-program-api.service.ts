import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import * as fromConstants from '../../share/constants/endpointConstants';
import {catchError} from 'rxjs/operators';
import {UserWorkoutProgramResult} from '../../share/models/user-workout-program-result';
import {UserWorkoutProgramDto} from '../../share/models/user-workout-program-dto';

@Injectable({
  providedIn: 'root'
})
export class UserWorkoutProgramServiceApi {

  constructor(private http: HttpClient) {
  }

  public getActiveUserWorkoutProgramResult(): Observable<UserWorkoutProgramResult> {
    return this.http.get<UserWorkoutProgramResult>(`${fromConstants.UserEndpointConstants.USER_WORKOUT_PROGRAM_RESULT}`)
      .pipe(
        catchError((error: any) => throwError(error))
      );
  }

  public getArchiveUserWorkoutProgramResult(archiveId: string): Observable<UserWorkoutProgramResult> {
    return this.http.get<UserWorkoutProgramResult>(`${fromConstants
      .UserEndpointConstants.USER_ARCHIVE_WORKOUT_PROGRAM_RESULT}/${archiveId}`)
      .pipe(
        catchError((error: any) => throwError(error))
      );
  }

  public getUserWorkoutProgramArchive(): Observable<UserWorkoutProgramDto[]> {
    return this.http.get<UserWorkoutProgramDto[]>(`${fromConstants.UserEndpointConstants.USER_WORKOUT_PROGRAM_ARCHIVE}`)
      .pipe(
        catchError((error: any) => throwError(error))
      );
  }
}
