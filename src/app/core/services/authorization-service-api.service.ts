import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import * as fromConstants from '../../share/constants/endpointConstants';
import {User} from '../../share/models/user';

@Injectable({
    providedIn: 'root'
})
export class AuthorizationServiceApi {

    constructor(private http: HttpClient) {
    }

    public getAuthorization(): Observable<User> {
        return this.http.get<User>(fromConstants.AuthorizationConstants.AUTHORIZATION)
    }
}
