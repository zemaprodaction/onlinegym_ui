import {BackdropService} from './backdrop.service';
import {TestBed} from '@angular/core/testing';
import {NgxsModule, Store} from '@ngxs/store';
import {AppStates, BackdropState, BackdropStateModel, BackdropStatus} from '../../store';
import {Backdrop} from '../../store/actions/backdrop.actions';
import {BackdropSetting} from '../../type/backdrop-setting';
import {WorkoutProgramFilterComponent} from '../../../trainings/components/workout-program-filter/workout-program-filter.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';

const backdropSetting: BackdropSetting = {
  mobileDownToHeight: 100,
  webDownToHeight: 100,
  nameBackdropPage: 'test name page',
  isCloseable: true
};
describe('BackdropService', () => {
  let service: BackdropService;
  let store: Store;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot(AppStates), HttpClientTestingModule]
    });
    service = TestBed.inject(BackdropService);
    store = TestBed.inject(Store);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should change backdrop status to open', () => {
    let backdropState: BackdropStateModel = store.selectSnapshot(state => state.app.appUi.backdrop);
    expect(backdropState.backdropStateStatus).toEqual(BackdropStatus.CLOSE);
    store.dispatch(new Backdrop.Open(backdropSetting));
    backdropState = store.selectSnapshot(state => state.app.appUi.backdrop);
    expect(backdropState.backdropStateStatus).toEqual(BackdropStatus.OPEN);
  });

  it('should change backdrop status to close', () => {
    store.dispatch(new Backdrop.Open(backdropSetting));
    let backdropState = store.selectSnapshot(BackdropState.getStatus);
    expect(backdropState).toEqual(BackdropStatus.OPEN);
    store.dispatch(new Backdrop.CloseMenu());
    backdropState = store.selectSnapshot(BackdropState.getStatus);
    expect(backdropState).toEqual(BackdropStatus.CLOSE);
  });

  it('should keep backdrop setting', () => {
    store.reset({
      ...store.snapshot(),
      app: {
        appUi: {
          backdrop: {
            backdropStatus: BackdropStatus.CLOSE,
            backdropSetting
          }
        }
      }

    });
    const backdropState = store.selectSnapshot(BackdropState.getSetting);
    expect(backdropState).toEqual(backdropSetting);
  });

  it('should open backdrop with component', () => {

    spyOn(service, 'loadComponent');
    service.openBackdropWithComponentCallback(WorkoutProgramFilterComponent, {
        mobileDownToHeight: 100,
        nameBackdropPage: 'test name page',
        webDownToHeight: 100
      }
    );
    const backdropSettingActual = store.selectSnapshot(BackdropState.getSetting);
    expect(backdropSettingActual).toEqual(backdropSetting);
  });
});
