import {ComponentFactoryResolver, Injectable, Type} from '@angular/core';
import {ToolbarContextHostDirective} from '../../directives';
import {BackdropSetting} from '../../type/backdrop-setting';
import {Select, Store} from '@ngxs/store';
import {Backdrop} from '../../store/actions/backdrop.actions';
import {AppUiState, BackdropState} from '../../store';
import {Observable} from 'rxjs';
import {delay, take} from 'rxjs/operators';
import {SetCurrentPageName} from '../../store/actions/app-ui.actions';
import {Toolbar} from '../../store/actions/toolbar.action';
import {ToolbarAction} from '../../../share/models/toolbar-action';
import {NavigationItemsComponent} from '../../components/navigation-items/navigation-items.component';

@Injectable({
  providedIn: 'root'
})
export class BackdropService {

  @Select(AppUiState.currentPageName)
  currentPageName$: Observable<string>;

  private nowComponentLoad: any;
  private lastPageName;
  private toolbarContextHostDirective: ToolbarContextHostDirective;
  private componentStore: Map<string, Type<any>> = new Map();

  constructor(private store: Store,
              private componentFactoryResolver: ComponentFactoryResolver) {
  }

  setToolbarContextHostDirective(toolbarContextHostDirective: ToolbarContextHostDirective) {
    this.toolbarContextHostDirective = toolbarContextHostDirective;
  }

  public loadComponent(component: Type<any>) {
    if (this.nowComponentLoad !== component && component) {
      this.componentStore.set(component.name, component);
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);

      const viewContainerRef = this.toolbarContextHostDirective.viewContainerRef;

      viewContainerRef.clear();
      viewContainerRef.createComponent(componentFactory);
      this.nowComponentLoad = component;
    }
  }

  public openBackdropWithComponentCallback(component: Type<any>,
                                           backdropSetting: BackdropSetting) {
    this.currentPageName$.pipe(
      take(1),
    ).subscribe((name) => {
      this.store.dispatch(new SetCurrentPageName(backdropSetting.nameBackdropPage));
      this.lastPageName = name;
      this.loadComponent(component);
      this.store.dispatch(new Backdrop.Open({...backdropSetting, isCloseable: true}));
    });

  }

  public openBackdrop(component: string, backdropSetting: BackdropSetting) {

    this.currentPageName$.pipe(
      take(1),
    ).subscribe((name) => {
      this.store.dispatch(new SetCurrentPageName(backdropSetting.nameBackdropPage));
      this.lastPageName = name;
      this.loadComponent(this.componentStore.get(component));
      this.store.dispatch(new Backdrop.Open(backdropSetting));
    });
  }

  public openTabsBackdrop(component: Type<any>, backdropSetting: BackdropSetting) {
    this.currentPageName$.pipe(
      take(1),
      delay(350)
    ).subscribe((name) => {
      this.store.dispatch(new SetCurrentPageName(backdropSetting.nameBackdropPage));
      this.lastPageName = name;
      this.loadComponent(component);
      this.store.dispatch(new Backdrop.OpenTabNavigations(backdropSetting));
    });
  }

  public closeBackdropMenu() {
    const prevConf = this.store.selectSnapshot(BackdropState.getPreviousSettings);
    if (prevConf) {
      this.openTabsBackdrop(this.componentStore.get(prevConf.componentName), prevConf);
      this.store.dispatch(new Backdrop.ResetPrevConfig());
    } else {
      this.store.dispatch(new SetCurrentPageName(this.lastPageName));
      this.store.dispatch(new Backdrop.CloseMenu());
    }
  }

  public closeBackdrop() {
    this.store.dispatch(new SetCurrentPageName(this.lastPageName));
    this.store.dispatch(new Backdrop.HardClose());
  }

  public addActionToBackdrop(button: ToolbarAction) {
    this.store.dispatch(new Toolbar.LoadAction(button));
  }

  public resetButton() {
    this.store.dispatch(new Toolbar.ResetToolbarButton());
  }

  public changeBackdropToNavigatable() {
    this.store.dispatch(new Toolbar.ResetToolbarButton());
  }

  openMenu() {
    this.currentPageName$.pipe(
      take(1),
    ).subscribe((name) => {
      this.store.dispatch(new SetCurrentPageName('Меню'));
      this.lastPageName = name;
      this.loadComponent(NavigationItemsComponent);
      this.store.dispatch(new Backdrop.OpenMenu());
    });
  }
}
