import {TestBed} from '@angular/core/testing';

import {WorkoutService} from './workout.service';
import {NgxsModule, Store} from '@ngxs/store';
import {WorkoutState} from '../training-store/states/workout.state';
import {WorkoutAction} from '../training-store/actions/workouts.action';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('WorkoutService', () => {
  let service: WorkoutService;
  let store: Store;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot([WorkoutState]),
        HttpClientTestingModule]
    });
    service = TestBed.inject(WorkoutService);
    store = TestBed.inject(Store);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('when call loadWorkoutByIdList', () => {
    it('should dispatch LoadWorkoutByIdListAction action', () => {
      jest.spyOn(store, 'dispatch');
      service.loadWorkoutWithTheirExerciseByWorkoutIdList(['workout_1']);
      expect(store.dispatch)
        .toHaveBeenCalledWith(new WorkoutAction.LoadWorkoutWithTheirExerciseByWorkoutIdList(['workout_1']));
    });
  });

});
