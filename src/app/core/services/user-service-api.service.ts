import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import * as fromConstants from '../../share/constants/endpointConstants';
import {catchError} from 'rxjs/operators';
import {WorkoutProgramEntity} from '../../share/models/workout-programs';
import {UserTrainingInfo} from '../../share/models/user-training-info';
import {WorkoutProgramScheduler} from '../../share/models/workout-program-scheduler';
import {UserWorkoutProgram} from "../../share/models/user-workout-program";

@Injectable({
    providedIn: 'root'
})
export class UserServiceApi {

    constructor(private http: HttpClient) {
    }

    public enrollOnProgram(workoutProgramId: string): Observable<UserTrainingInfo> {
        return this.http.post<UserTrainingInfo>(`${fromConstants.UserEndpointConstants.ENROLL}`,
            {trainingWorkoutProgramId: workoutProgramId})
            .pipe(
                catchError((error: any) => throwError(error))
            );
    }

    public unsubscribeFromWorkoutProgram(): Observable<WorkoutProgramEntity> {
        return this.http.post<WorkoutProgramEntity>(`${fromConstants.UserEndpointConstants.UNSUBSCRIBE}`,
            {})
            .pipe(
                catchError((error: any) => throwError(error))
            )
            ;
    }
    public restartWorkoutProgram(): Observable<UserWorkoutProgram> {
        return this.http.post<UserWorkoutProgram>(`${fromConstants.UserEndpointConstants.RESTART_WP}`,
            {})
            .pipe(
                catchError((error: any) => throwError(error))
            )
            ;
    }

    public getUserTrainingProcessInfo(): Observable<UserTrainingInfo> {
        return this.http.get<UserTrainingInfo>(`${fromConstants.UserEndpointConstants.USER_PROFILE}`)
            .pipe(
                catchError((error: any) => throwError(error))
            );
    }

    public startUserWorkoutProgram(): Observable<WorkoutProgramScheduler> {
        return this.http.post<WorkoutProgramScheduler>(`${fromConstants.UserEndpointConstants.START_WORKOUT_PROGRAM}`,
            {})
            .pipe(
                catchError((error: any) => throwError(error))
            );
    }
}
