import {MonoTypeOperatorFunction} from 'rxjs';
import {filter, flatMap, map} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {ModalService} from './modal.service';
import {YouShouldLoginComponent} from '../../share/ui-components/dialogs';


@Injectable({
  providedIn: 'root'
})
export class RxjsCustomPipeService {

  constructor(private modalService: ModalService) {
  }

  areYouSurePipe<T>(message: string): MonoTypeOperatorFunction<T> {
    return input$ => input$.pipe(
      flatMap(value => {
        const dialogRef = this.modalService.openDialog(YouShouldLoginComponent, message, value);
        return dialogRef.afterClosed();
      }),
      filter(x => x != null),
      map(dialogResponce => dialogResponce['value'])
    );
  }
}

