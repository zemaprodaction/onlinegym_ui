import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import * as fromConstants from '../../share/constants/endpointConstants';
import {catchError} from 'rxjs/operators';
import {WorkoutInstanceEntity} from '../../share/models/workout/workout-instance-entity';
import {Exercise} from '../../share/models/exercise/exercise';
import {StatisticConstants, TrainingEndpointConstants} from '../../share/constants/endpointConstants';
import {WorkoutProgramStatistic} from '../../share/models/workout-program-statistic';
import {WorkoutResult} from '../../share/models/workout-result';


@Injectable({
  providedIn: 'root'
})
export class WorkoutProcessService {

  constructor(private http: HttpClient) { }
  private static createWorkoutInstanceUrl(workoutProgramId: string): string {
    return `${fromConstants.TrainingEndpointConstants.WORKOUT_INSTANCE.replace(
      fromConstants.TrainingParamConstants.WORKOUT_PROGRAM_ID_TEMPLATE, workoutProgramId)}`;
  }

  public updateExercise(exercise: Exercise): Observable<Exercise> {
    return this.http.put<Exercise>(TrainingEndpointConstants.EXERCISES, exercise)
      .pipe(catchError((error: any) => throwError(error.json())));
  }

  public updateStatistic(statistic: WorkoutProgramStatistic): Observable<WorkoutResult> {
    return this.http.post<WorkoutResult>(`${StatisticConstants.ROOT_STATISTIC_WORKOUT_PROGRAM}`, statistic)
      .pipe(catchError((error: any) => throwError(error.json())));
  }
}
