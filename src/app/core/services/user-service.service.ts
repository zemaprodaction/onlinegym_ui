import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Store} from '@ngxs/store';
import {UserState} from '../user-store/states/user.state';
import {WorkoutInstanceScheduler} from '../../share/models/workout-instance-scheduler';
import {filter} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private store: Store) {
  }

  public getWorkoutInstanceByParamFromRoute(): Observable<WorkoutInstanceScheduler> {
    return this.store.select(UserState.getWorkoutInstanceByDayIndex()).pipe(
      filter(wi => wi !== null && wi !== undefined));
  }
}
