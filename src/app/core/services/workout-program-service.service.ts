import {Injectable} from '@angular/core';
import {Store} from '@ngxs/store';
import {WorkoutProgramState} from '../training-store/states/workout-program/workout-program.state';


@Injectable({providedIn: 'root'})
export class WorkoutProgramService {

  constructor(private store: Store) {
  }

  getWorkoutProgramById(id: string) {
    return this.store.select(WorkoutProgramState.getWorkoutProgramById(id));
  }
}
