import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SnackBarContentComponent} from "../components/snack-bar-content/snack-bar-content.component";

@Injectable({
  providedIn: 'root'
})
export class SnackBarService {

  constructor(private snackBar: MatSnackBar) {
  }

  openSnackBar(response: any) {
      this.snackBar.openFromComponent(SnackBarContentComponent,  {
        data: response,
        duration: 3000});
  }
}
