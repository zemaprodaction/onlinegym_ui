import {TestBed} from '@angular/core/testing';
import {MeasureServiceApi} from './measure-service-api.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TrainingEndpointConstants} from '../../../share/constants/endpointConstants';

describe('MetadataService', () => {
  let service: MeasureServiceApi;
  let http: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(MeasureServiceApi);
    http = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('when call get measure type metadata method', () => {
    it('should send request to specific url', () => {
      service.findAll().subscribe((val) => {
        expect(val).toEqual([]);
      });
      const req = http.expectOne(TrainingEndpointConstants.METADATA_MEASURETYPE);
      expect(req.request.method).toEqual('GET');

      req.flush([]);
    });
  });
});
