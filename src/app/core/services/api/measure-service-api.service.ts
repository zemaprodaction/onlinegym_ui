import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {TrainingEndpointConstants} from '../../../share/constants/endpointConstants';
import {CrudService} from '../../../trainings/service/crud.service';
import {MeasuresType} from '../../../share/models/measuresType';

@Injectable({
  providedIn: 'root'
})
export class MeasureServiceApi extends CrudService<MeasuresType, string> {

  constructor(protected _http: HttpClient) {
    super(_http, TrainingEndpointConstants.METADATA_MEASURETYPE);
  }
}
