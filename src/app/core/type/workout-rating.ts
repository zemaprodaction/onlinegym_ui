import {Workout} from '../../share/models/workout/workoutEntity';

export interface WorkoutRating {
  workoutId: string;
  rating: number;
  workout: Workout;
}

