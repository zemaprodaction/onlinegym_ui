import {BackdropNavLink} from '../../share/models/backdrop-nav-link';

export class BackdropSetting {
  webDownToHeight?: number;
  customHeightOfBackdrop?: number;
  mobileDownToHeight?: number;
  nameBackdropPage?: string;
  isShouldNotClose? = false;
  navLinks?: BackdropNavLink[];
  namePage?: string;
  isCloseable?: boolean;
  isNavigable?: boolean;
  isNeedMenu?: boolean;
  componentName?: string;
}
