import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {AuthorizationService} from '../../authorization/services/authorization.service';
import {YouShouldLoginComponent} from "../../share/ui-components/dialogs";
import {ModalService} from "../services/modal.service";

@Injectable({
    providedIn: 'root'
})
export class AuthInterceptor implements HttpInterceptor {

    constructor(private authService: AuthorizationService, private modalService: ModalService) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(tap(() => {
            },
            (err: any) => {
                if (err instanceof HttpErrorResponse) {
                    if (err.status !== 401 && err.status !== 403) {
                        return;
                    }
                    this.authService.logoutUser();
                    this.modalService.openDialog(YouShouldLoginComponent);
                }
            }));
    }
}
