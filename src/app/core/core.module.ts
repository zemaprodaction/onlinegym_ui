import {NgModule, Optional, SkipSelf} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import * as fromComponents from './components';
import {NavigationComponent} from './components';
import * as fromDirectives from './directives';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {RouterModule} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {EnsureImportedOnceModule} from './ensure-imported-once-module';
import {LogButtonComponent} from './components/log-button/log-button.component';
import {NgxsModule} from '@ngxs/store';
import {TrainingStates} from './training-store';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {UserState} from './user-store/states/user.state';
import {CsrfInterceptor} from './interceptor/csrf.interceptor';
import {FabComponent} from './components/fab/fab.component';
import {CustomMenuButtonComponent} from './components/custom-menu-button/custom-menu-button.component';
import {ShareModule} from '../share/share.module';
import {AuthInterceptor} from './interceptor/auth.interceptor';
import {BrowserModule} from "@angular/platform-browser";
import { SnackBarContentComponent } from './components/snack-bar-content/snack-bar-content.component';


@NgModule({
  declarations: [
    ...fromComponents.components,
    ...fromDirectives.directives,
    LogButtonComponent,
    FabComponent,
    CustomMenuButtonComponent,
    SnackBarContentComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    MatToolbarModule,
    MatListModule,
    MatIconModule,
    RouterModule,
    BrowserAnimationsModule,
    MatSnackBarModule,
    FlexLayoutModule,
    NgxsModule.forFeature([...TrainingStates, UserState]),
    MatButtonModule,
    ShareModule,
    BrowserModule,
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: CsrfInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
  ],
  exports: [
    NavigationComponent,
    FabComponent,

  ]
})
export class CoreModule extends EnsureImportedOnceModule {
  public constructor(@SkipSelf() @Optional() parent: CoreModule) {
    super(parent);
  }
}
