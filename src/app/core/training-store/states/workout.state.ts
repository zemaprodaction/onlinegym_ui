import {Action, createSelector, State, StateContext} from '@ngxs/store';
import {WorkoutServiceApi} from '../../../trainings/service/workout-service-api.service';
import {defaultsEntityState, EntityState, EntityStateModel} from '../../../share/store/entity.state';
import {WorkoutAction} from '../actions/workouts.action';
import {Workout, WorkoutEntity} from '../../../share/models/workout/workoutEntity';
import {Injectable} from '@angular/core';
import {tap} from 'rxjs/operators';
import {ExerciseService} from '../../services/exercise.service';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface WorkoutStateModel extends EntityStateModel<Workout> {
}

@State<WorkoutStateModel>({
  name: 'workout',
  defaults: {
    ...defaultsEntityState,
    routerSelectedParams: 'workoutId'
  }
})
@Injectable()
export class WorkoutState extends EntityState<WorkoutEntity> {

  constructor(private workoutService: WorkoutServiceApi, private exerciseService: ExerciseService) {
    super(workoutService);
  }

  static getListWorkoutByListId(idList: string[]) {
    return createSelector([this],
      (state: EntityStateModel<Workout>) => idList.map(id => state.entities[id]));
  }

  static getWorkoutById(id: string) {
    return createSelector([this],
      (state: EntityStateModel<Workout>) => state.entities[id]);
  }

  @Action(WorkoutAction.LoadWorkouts)
  public addWorkout(ctx: StateContext<EntityStateModel<Workout>>) {
    return this.load(ctx);
  }

  @Action(WorkoutAction.LoadWorkoutByIdListAction)
  public loadWorkoutByIdList(ctx: StateContext<EntityStateModel<Workout>>, {payload}: WorkoutAction.LoadWorkoutByIdListAction) {
    return super.loadByIdList(ctx, payload);
  }

  @Action(WorkoutAction.LoadWorkoutWithTheirExerciseByWorkoutIdList)
  public loadWorkoutWithTheirExerciseByIdList(ctx: StateContext<EntityStateModel<Workout>>,
                                              {payload}: WorkoutAction.LoadWorkoutWithTheirExerciseByWorkoutIdList) {
    return super.loadByIdList(ctx, payload).pipe(
      tap(workouts => this.exerciseService
        .loadExerciseByIdListAction(this.getExerciseIdFromWorkoutList(workouts))
      )
    );
  }

  private getExerciseIdFromWorkoutList(workoutList: WorkoutEntity[]) {
    return workoutList.flatMap(workout => workout.listOfPhases)
      .flatMap(phase => phase.listOfExercises);
  }
}
