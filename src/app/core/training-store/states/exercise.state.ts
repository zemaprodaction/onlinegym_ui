import {Action, createSelector, State, StateContext} from '@ngxs/store';
import {TrainingParamConstants} from '../../../share/constants/endpointConstants';
import {defaultsEntityState, EntityState, EntityStateModel} from '../../../share/store/entity.state';
import {ExerciseServiceApi} from '../../../trainings/service/exercise-service-api.service';
import {ExerciseActions} from '../actions/exercise.action';
import {Exercise} from '../../../share/models/exercise/exercise';
import {Injectable} from '@angular/core';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ExerciseStateModel extends EntityStateModel<Exercise> {

}

@State({
  name: 'exercise',
  defaults: {
    ...defaultsEntityState,
    routerSelectedParams: TrainingParamConstants.EXERCISE_ID
  }
})
@Injectable()
export class ExerciseState extends EntityState<Exercise> {

  constructor(private exerciseService: ExerciseServiceApi) {
    super(exerciseService);
  }

  static selectExerciseById(exerciseId: string) {
    return createSelector([ExerciseState.getEntityById(exerciseId)],
      (exercise: Exercise) => exercise);
  }

  @Action(ExerciseActions.LoadExerciseAction)
  public loadExercise(ctx: StateContext<EntityStateModel<Exercise>>) {
    return this.load(ctx);
  }

  @Action(ExerciseActions.DeleteExerciseAction)
  public deleteExercise(ctx: StateContext<EntityStateModel<Exercise>>, action: ExerciseActions.DeleteExerciseAction) {
    return this.delete(ctx, action.payload);
  }

  @Action(ExerciseActions.LoadExerciseByIdListAction)
  public loadExerciseByIdList(ctx: StateContext<EntityStateModel<Exercise>>, {payload}: ExerciseActions.LoadExerciseByIdListAction) {
    return this.loadByIdList(ctx, payload);
  }
}
