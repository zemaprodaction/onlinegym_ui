import {NgxsModule, Store} from '@ngxs/store';
import {fakeAsync, TestBed, tick, waitForAsync} from '@angular/core/testing';
import {Navigate, NgxsRouterPluginModule, RouterStateSerializer} from '@ngxs/router-plugin';
import {Router, Routes} from '@angular/router';
import {Component} from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';
import {RouterTestingModule} from '@angular/router/testing';
import {trainingRoutesNames} from '../../../trainings/routes.names';
import {TrainingStates} from '../index';
import {CustomRouterStateSerializer} from '../../../router-state.serializer';
import {HttpClientModule} from '@angular/common/http';
import {WorkoutServiceApi} from '../../../trainings/service/workout-service-api.service';
import {of} from 'rxjs';
import {WorkoutState} from './workout.state';
import {WorkoutAction} from '../actions/workouts.action';
import {getMockWorkoutList} from '../../../share/unit-test-utils/unit-test-utils.spec';
import {ExerciseService} from '../../services/exercise.service';

@Component({
  selector: 'app-test',
  template: ''
})
class MockComponent {

}

@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>'
})
class MockAppComponent {

}

const ROUTERS: Routes = [
  {
    path: `${trainingRoutesNames.WORKOUT_INSTANCE_ID}`,
    component: MockComponent
  }
];
describe('Workout state', () => {
  let store: Store;
  let router: Router;
  let fixture;
  let workoutService: WorkoutServiceApi;
  let exerciseService: ExerciseService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [MockComponent, MockAppComponent],
      imports: [NgxsModule.forRoot(TrainingStates,
        {
          selectorOptions: {
            suppressErrors: false,
            injectContainerState: false
          }
        }),
        HttpClientModule,
        NgxsRouterPluginModule.forRoot(),
        RouterTestingModule.withRoutes(ROUTERS)],
      providers: [
        {provide: APP_BASE_HREF, useValue: '/'},
        {provide: RouterStateSerializer, useClass: CustomRouterStateSerializer}
      ],
    }).compileComponents();
    store = TestBed.inject(Store);
    router = TestBed.inject(Router);
    fixture = TestBed.createComponent(MockAppComponent);
    workoutService = TestBed.inject(WorkoutServiceApi);
    exerciseService = TestBed.inject(ExerciseService);

    jest.spyOn(workoutService, 'findAll').mockReturnValue(of(getMockWorkoutList()));
    fixture.ngZone.run(() => router.initialNavigation());
  }));

  describe('when dispatch Load Workout action twice', () => {
    it('should not load data', fakeAsync(() => {
      store.dispatch(new WorkoutAction.LoadWorkouts());
      store.dispatch(new WorkoutAction.LoadWorkouts());
      tick();
      expect(workoutService.findAll).toHaveBeenCalledTimes(1);
    }));
  });


  describe('when was dispatched LoadWorkoutByIdListAction', () => {
    it('should call service api methode for load specific workouts', () => {
      const idList = ['id1', 'id2'];
      jest.spyOn(workoutService, 'findByIdList').mockReturnValue(of(getMockWorkoutList()));
      store.dispatch(new WorkoutAction.LoadWorkoutByIdListAction(idList));
      expect(workoutService.findByIdList).toHaveBeenCalledWith(idList);
    });
  });

  describe('when was dispatched LoadWorkoutWithTheirExerciseByWorkoutIdList', () => {
    beforeEach(waitForAsync(() => {
      jest.spyOn(workoutService, 'findByIdList').mockReturnValue(of(getMockWorkoutList()));
      jest.spyOn(exerciseService, 'loadExerciseByIdListAction');
    }));
    it('should load workout by id list and exercise for these workouts', () => {
      const idList = ['workout_1', 'workout2'];
      jest.spyOn(workoutService, 'findByIdList').mockReturnValue(of(getMockWorkoutList()));
      jest.spyOn(exerciseService, 'loadExerciseByIdListAction');
      store.dispatch(new WorkoutAction.LoadWorkoutWithTheirExerciseByWorkoutIdList(idList));
      expect(workoutService.findByIdList).toHaveBeenCalledWith(idList);
      expect(exerciseService.loadExerciseByIdListAction)
        .toHaveBeenLastCalledWith(['exercise_1', 'exercise_1', 'exercise_2']);
    });
  });
});
