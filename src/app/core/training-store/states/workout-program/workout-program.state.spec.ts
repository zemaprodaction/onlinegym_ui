import {NgxsModule, Store} from '@ngxs/store';
import {fakeAsync, TestBed, tick, waitForAsync} from '@angular/core/testing';
import {NgxsRouterPluginModule} from '@ngxs/router-plugin';
import {Component} from '@angular/core';
import {RouterTestingModule} from '@angular/router/testing';
import {TrainingStates} from '../../index';
import {of} from 'rxjs';
import {WorkoutProgramServiceApi} from '../../../services/workout-program-service-api.service';
import {
  WorkoutProgram,
  WorkoutProgramCharacteristicConstants,
  WorkoutProgramEntity
} from '../../../../share/models/workout-programs';
import {WorkoutProgramAction} from '../../actions/workout-programs.action';
import {WorkoutProgramState} from './workout-program.state';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {CharacteristicPropertyFilterChosen} from '../../../../share/models/characteristic-property-filter-chosen';

@Component({
  selector: 'app-test',
  template: ''
})
class MockComponent {

}

export function getMockWorkoutProgramEntityList() {
  const workout1: WorkoutProgramEntity = {
    id: 'workout_program_1',
    listOfWeeks: [
      {
        listOfDays: [
          {
            listOfScheduledWorkouts: [
              {
                id: 'workout_instance_test_id'
              }
            ]
          }
        ]
      }
    ],
    characteristic: {
      workoutPlaces: {
        value: 'gym',
        nativeValue: '',
      },
      equipmentNecessity: {
        value: 'required',
        nativeValue: '',
      },
      workoutDifficulty: {
        value: 'hard',
        nativeValue: '',
      },
      workoutType: {
        value: 'streinght',
        nativeValue: '',
      }
    },
    workoutsOnProgram: ['work1', 'work2']
  };
  const workout2: WorkoutProgramEntity = {
    id: 'workout_program_2',
    listOfWeeks: [],
    workoutsOnProgram: ['work1', 'work2'],
    characteristic: {
      workoutPlaces: {
        nativeValue: '',
        value: 'home'
      },
      equipmentNecessity: {
        nativeValue: '',
        value: 'required'
      },
      workoutDifficulty: {
        nativeValue: '',
        value: 'hard'
      },
      workoutType: {
        nativeValue: '',
        value: 'health'
      }
    }
  };
  return [workout1, workout2];
}

describe('Workout Program state', () => {
  let store: Store;
  let workoutProgramService: WorkoutProgramServiceApi;


  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot(TrainingStates,
        {
          selectorOptions: {
            suppressErrors: false,
            injectContainerState: false
          }
        }),
        HttpClientTestingModule,
        RouterTestingModule,
        NgxsRouterPluginModule.forRoot()],
    }).compileComponents();
    store = TestBed.inject(Store);
    workoutProgramService = TestBed.inject(WorkoutProgramServiceApi);
    jest.spyOn(workoutProgramService, 'findAll').mockReturnValue(of(getMockWorkoutProgramEntityList()));
    jest.spyOn(workoutProgramService, 'findOne');
    store.reset({
      ...store.snapshot(),
      training: {
        workoutProgramFilter: {
          ids: []
        },
        workout: {
          entities: {
            work1: {id: 'work1'},
            work2: {id: 'work2'}
          }
        },
        workoutProgram: {}
      }
    });
  }));

  describe('when dispatch Workout Program Load action', () => {
    it('should load all workout program in normalize state', fakeAsync(() => {
        store.dispatch(new WorkoutProgramAction.LoadWorkoutPrograms());
        tick();
        const workoutProgramState = store.selectSnapshot(state => state.training.workoutProgram);
        expect(workoutProgramState.ids).toEqual(['workout_program_1', 'workout_program_2']);
      }
    ));
  });

  describe('when twice dispatch Workout Program Load action', () => {
    it('should take workout program from store', fakeAsync(() => {
        store.dispatch(new WorkoutProgramAction.LoadWorkoutPrograms());
        tick();
        store.dispatch(new WorkoutProgramAction.LoadWorkoutPrograms());
        expect(workoutProgramService.findAll).toHaveBeenCalledTimes(1);
        const workoutProgramState = store.selectSnapshot(state => state.training.workoutProgram);
        expect(workoutProgramState.ids).toEqual(['workout_program_1', 'workout_program_2']);
      }
    ));
  });

  describe('when dispatch Workout Program Load By Id action and workout program exists in store', () => {
    it('should take workout program from store', fakeAsync(() => {
        store.dispatch(new WorkoutProgramAction.LoadWorkoutPrograms());
        tick();
        store.dispatch(new WorkoutProgramAction.LoadWorkoutProgramById('workout_program_1'));
        store.dispatch(new WorkoutProgramAction.LoadWorkoutProgramById('workout_program_2'));
        store.dispatch(new WorkoutProgramAction.LoadWorkoutProgramById('workout_program_3'));
        tick();
        const workoutProgramState = store.selectSnapshot(state => state.training.workoutProgram);
        expect(workoutProgramState.ids).toEqual(['workout_program_1', 'workout_program_2']);
      }
    ));
  });

  it('should find denormilize workout program', (() => {
      store.dispatch(new WorkoutProgramAction.LoadWorkoutPrograms());
      const workoutProgram: WorkoutProgram = store.selectSnapshot(
        WorkoutProgramState.getWorkoutProgramById('workout_program_1'));
      expect(workoutProgram.workoutsOnProgram).toEqual([{id: 'work1'}, {id: 'work2'}]);
    }
  ));

  it('should return workout programs with apply filter', fakeAsync(() => {
      store.dispatch(new WorkoutProgramAction.LoadWorkoutPrograms());
      tick();
      let filteredWorkoutPrograms = store.selectSnapshot(WorkoutProgramState.getFilteredWorkoutPrograms);
      expect(filteredWorkoutPrograms.length).toEqual(2);
      const chosenFilter: CharacteristicPropertyFilterChosen = {
        value: 'home',
        filterPath: WorkoutProgramCharacteristicConstants.workoutPlaces
      };
      store.dispatch(new WorkoutProgramAction.AddWorkoutProgramFilter(chosenFilter));
      tick();
      filteredWorkoutPrograms = store.selectSnapshot(WorkoutProgramState.getFilteredWorkoutPrograms);
      expect(filteredWorkoutPrograms.length).toEqual(1);
    }
  ));

  it('should return array with one workout program with type cardio and place home', fakeAsync(() => {
      store.dispatch(new WorkoutProgramAction.LoadWorkoutPrograms());
      tick();
      let filteredWorkoutPrograms = store.selectSnapshot(WorkoutProgramState.getFilteredWorkoutPrograms);
      expect(filteredWorkoutPrograms.length).toEqual(2);

      const chosenFilter: CharacteristicPropertyFilterChosen = {
        value: 'home',
        filterPath: WorkoutProgramCharacteristicConstants.workoutPlaces
      };
      store.dispatch(new WorkoutProgramAction.AddWorkoutProgramFilter(chosenFilter));

      const chosenFilter2: CharacteristicPropertyFilterChosen = {
        value: 'health',
        filterPath: WorkoutProgramCharacteristicConstants.workoutType
      };
      store.dispatch(new WorkoutProgramAction.AddWorkoutProgramFilter(chosenFilter));

      const chosenFilter3: CharacteristicPropertyFilterChosen = {
        value: 'cardio',
        filterPath: WorkoutProgramCharacteristicConstants.workoutType
      };
      tick();

      filteredWorkoutPrograms = store.selectSnapshot(WorkoutProgramState.getFilteredWorkoutPrograms);
      expect(filteredWorkoutPrograms.length).toEqual(1);
    }
  ));
});
