import {NgxsModule, Store} from '@ngxs/store';
import {fakeAsync, TestBed, tick, waitForAsync} from '@angular/core/testing';
import {NgxsRouterPluginModule} from '@ngxs/router-plugin';
import {RouterTestingModule} from '@angular/router/testing';
import {TrainingStates} from '../../index';
import {HttpClientModule} from '@angular/common/http';
import {WorkoutProgramAction} from '../../actions/workout-programs.action';
import {WorkoutProperty} from '../../../../share/models/workout-propert';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {CharacteristicPropertyFilterChosen} from '../../../../share/models/characteristic-property-filter-chosen';


describe('Workout Program state', () => {
  let store: Store;
  const chosenFilter: CharacteristicPropertyFilterChosen = {
    value: 'testValue',
    filterPath: 'workoutType'
  };
  const chosenFilterOption2: CharacteristicPropertyFilterChosen = {
    value: 'testValue2',
    filterPath: 'workoutType'
  };
  const chosenFilter2: CharacteristicPropertyFilterChosen = {
    value: 'testValue',
    filterPath: 'workoutDifficulty'
  };
  new WorkoutProperty('test', 'testCode', 'test_id');
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot(TrainingStates,
        {
          selectorOptions: {
            suppressErrors: false,
            injectContainerState: false
          }
        }),
        HttpClientModule,
        RouterTestingModule,
        NgxsRouterPluginModule.forRoot()],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    }).compileComponents();
    store = TestBed.inject(Store);
    store.dispatch(new WorkoutProgramAction.AddWorkoutProgramFilter(
      chosenFilter));
  }));


  it('should add filter to workout program state', fakeAsync(() => {
      const workoutProgramState = store.selectSnapshot(state => state.training);
      expect(workoutProgramState.workoutProgramFilter)
        .toEqual({workoutType: new Set(['testValue']), ids: new Set(['workoutType'])});
    }
  ));

  it('should add 2 different filter to workout program state', fakeAsync(() => {
      store.dispatch(new WorkoutProgramAction.AddWorkoutProgramFilter(
        chosenFilter2));
      tick();
      const workoutProgramState = store.selectSnapshot(state => state.training);
      expect(workoutProgramState.workoutProgramFilter.ids.size).toEqual(2);
      expect(workoutProgramState.workoutProgramFilter.ids).toEqual(new Set(['workoutType', 'workoutDifficulty']));
    }
  ));

  it('should add 2 simulate filter in one type filter and add value to array to workout program state', fakeAsync(() => {
      store.dispatch(new WorkoutProgramAction.AddWorkoutProgramFilter(
        chosenFilterOption2));
      store.dispatch(new WorkoutProgramAction.AddWorkoutProgramFilter(
        chosenFilterOption2));
      tick();
      const workoutProgramState = store.selectSnapshot(state => state.training);
      expect(workoutProgramState.workoutProgramFilter.workoutType.size).toEqual(2);
      expect(workoutProgramState.workoutProgramFilter.workoutType).toEqual(new Set(['testValue', 'testValue2']));
    }
  ));

  it('should remove filter value from filter type', fakeAsync(() => {
      store.dispatch(new WorkoutProgramAction.AddWorkoutProgramFilter(
        chosenFilter2));
      store.dispatch(new WorkoutProgramAction.AddWorkoutProgramFilter(
        chosenFilterOption2));

      store.dispatch(new WorkoutProgramAction.RemoveWorkoutProgramFilter(chosenFilter));
      tick();
      let workoutProgramState = store.selectSnapshot(state => state.training);
      expect(workoutProgramState.workoutProgramFilter.workoutType.size).toEqual(1);
      expect(workoutProgramState.workoutProgramFilter.workoutType).toEqual(new Set(['testValue2']));

      store.dispatch(new WorkoutProgramAction.RemoveWorkoutProgramFilter(chosenFilter2));
      tick();
      workoutProgramState = store.selectSnapshot(state => state.training);
      expect(workoutProgramState.workoutProgramFilter.workoutDifficulty.size).toEqual(0);
      expect(workoutProgramState.workoutProgramFilter.workoutDifficulty).toEqual(new Set([]));
    }
  ));
});
