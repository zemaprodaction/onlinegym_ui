import {Action, createSelector, Selector, State, StateContext} from '@ngxs/store';
import {defaultsEntityState, EntityState, EntityStateModel} from '../../../../share/store/entity.state';
import {WorkoutProgramServiceApi} from '../../../services/workout-program-service-api.service';
import {WorkoutProgramAction} from '../../actions/workout-programs.action';
import {WorkoutProgramFilterState, WorkoutProgramFilterStateModel} from './workout-program-filter.state';
import {WorkoutProgram, WorkoutProgramEntity} from '../../../../share/models/workout-programs';
import {Injectable} from '@angular/core';
import {WorkoutState, WorkoutStateModel} from '../workout.state';
import {CharacteristicProperty} from '../../../../share/models/characteristic-property';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface WorkoutProgramStateModel extends EntityStateModel<WorkoutProgramEntity> {
}

@State<WorkoutProgramStateModel>({
  name: 'workoutProgram',
  defaults: {
    ...defaultsEntityState,
    routerSelectedParams: 'workoutProgramId'
  },
  children: [WorkoutProgramFilterState]
})
@Injectable()
export class WorkoutProgramState extends EntityState<WorkoutProgramEntity> {

  constructor(private workoutProgramService: WorkoutProgramServiceApi) {
    super(workoutProgramService);
  }

  static getWorkoutProgramById(id: string) {
    return createSelector([WorkoutProgramState.getEntityById(id), WorkoutState],
      (wPE: WorkoutProgramEntity, workoutState: WorkoutStateModel): WorkoutProgram => {
        if (wPE) {
          const workoutOnProgram = wPE.workoutsOnProgram.map(workoutId =>
            workoutState.entities[workoutId]
          );
          return {...wPE, workoutsOnProgram: workoutOnProgram};
        }
      });
  }

  private static isPassFilters(workoutProgram: WorkoutProgramEntity, wpfState: WorkoutProgramFilterStateModel) {
    let isPass = true;
    for (const item of wpfState.ids) {
      if (wpfState[item].size === 0) {
        isPass = true;
      } else {
        const value = (workoutProgram.characteristic[item] as CharacteristicProperty).value;
        const isArray = this.isArray(value);
        if (isArray) {
          isPass = (value as Array<string>).some(val => wpfState[item].has(val));
        } else {
          isPass = wpfState[item].has(value as string);
        }
      }
      if (!isPass) {
        return false;
      }
    }
    return isPass;
  }

  private static isArray(value: any) {
    return Array.isArray(value);
  }

  @Selector([WorkoutProgramState, WorkoutProgramFilterState])
  public static getFilteredWorkoutPrograms(wpState: WorkoutProgramStateModel, wpfState: WorkoutProgramFilterStateModel) {
    return wpState.ids.map(id => wpState.entities[id]).filter(wp => this.isPassFilters(wp, wpfState));
  }

  @Selector([WorkoutProgramState.getSelectedEntity()])
  public static getSelectedWorkoutPrograms(workoutProgram: WorkoutProgramEntity) {
    return workoutProgram;
  }


  @Action(WorkoutProgramAction.LoadWorkoutPrograms)
  public addWorkoutProgram(ctx: StateContext<EntityStateModel<WorkoutProgramEntity>>) {
    return this.load(ctx);
  }

  @Action(WorkoutProgramAction.LoadWorkoutProgramById)
  public addWorkoutProgramById(ctx: StateContext<EntityStateModel<WorkoutProgramEntity>>,
                               {id}: WorkoutProgramAction.LoadWorkoutProgramById) {
    return this.loadById(ctx, id);
  }


}
