import {Action, Selector, State, StateContext} from '@ngxs/store';
import {WorkoutProgramAction} from '../../actions/workout-programs.action';
import {Injectable} from '@angular/core';

export interface WorkoutProgramFilterStateModel {
  [id: string]: Set<string>;

  ids: Set<string>;
}

const initialState = {
  ids: new Set([])
};

@State<WorkoutProgramFilterStateModel>({
  name: 'workoutProgramFilter',

  defaults: {
    ...initialState
  }
})
@Injectable()
export class WorkoutProgramFilterState {


  @Selector([WorkoutProgramFilterState])
  public static getAllFilterValue(state: WorkoutProgramFilterStateModel) {
    const values = [];
    state.ids.forEach(id => values.push(...state[id]));
    return values;
  }

  @Action(WorkoutProgramAction.AddWorkoutProgramFilter)
  public addWorkoutWorkoutProgramFilter(ctx: StateContext<WorkoutProgramFilterStateModel>,
                                        action: WorkoutProgramAction.AddWorkoutProgramFilter) {

    const filterField = action.payload.filterPath;
    const state = ctx.getState();
    const particularFilterValues = state[filterField] || [];
    ctx.patchState({
      ids: new Set([...state.ids, filterField]),
      [filterField]: new Set([...particularFilterValues, action.payload.value])
    });
  }

  @Action(WorkoutProgramAction.RemoveWorkoutProgramFilter)
  public removeWorkoutWorkoutProgramFilter(ctx: StateContext<WorkoutProgramFilterStateModel>,
                                           action: WorkoutProgramAction.RemoveWorkoutProgramFilter) {

    const filterField = action.payload.filterPath;
    const state = ctx.getState();
    const newFilterTypeState = [...state[action.payload.filterPath]]
      .filter(val => val !== action.payload.value);
    ctx.patchState({
      [filterField]: new Set(newFilterTypeState)
    });
  }

  @Action(WorkoutProgramAction.ResetFilters)
  public resetFilters(ctx: StateContext<WorkoutProgramFilterStateModel>) {
    ctx.setState({...initialState});
  }
}
