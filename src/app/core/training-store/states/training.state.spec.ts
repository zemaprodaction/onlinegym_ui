import {NgxsModule, Store} from '@ngxs/store';
import {TestBed, waitForAsync} from '@angular/core/testing';
import {TrainingStates} from '../index';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('Training state', () => {
  let store: Store;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot(TrainingStates,
        {
          selectorOptions: {
            suppressErrors: false,
            injectContainerState: false
          }
        }), HttpClientTestingModule]
    }).compileComponents();
    store = TestBed.inject(Store);
  }));

  it('Training store should contains specific children store', (() => {
      const trainingState = store.selectSnapshot(state => state.training);
      expect(trainingState.exercise).toBeTruthy();
      expect(trainingState.workout).toBeTruthy();
      expect(trainingState.workoutProgram).toBeTruthy();
    }
  ));
});
