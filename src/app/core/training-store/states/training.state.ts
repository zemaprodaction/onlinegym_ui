import {State} from '@ngxs/store';
import {ExerciseState} from './exercise.state';
import {WorkoutState} from './workout.state';
import {WorkoutProgramState} from './workout-program/workout-program.state';
import {Injectable} from '@angular/core';
import {WorkoutProgramFilterState} from './workout-program/workout-program-filter.state';

@State({
  name: 'training',
  children: [ExerciseState, WorkoutState, WorkoutProgramState, WorkoutProgramFilterState]
})
@Injectable()
export class TrainingState {

}
