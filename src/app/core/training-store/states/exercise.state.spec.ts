import {NgxsModule, Store} from '@ngxs/store';
import {fakeAsync, TestBed, tick, waitForAsync} from '@angular/core/testing';
import {Navigate, NgxsRouterPluginModule, RouterStateSerializer} from '@ngxs/router-plugin';
import {Router, Routes} from '@angular/router';
import {Component} from '@angular/core';
import {APP_BASE_HREF, Location} from '@angular/common';
import {RouterTestingModule} from '@angular/router/testing';
import {trainingRoutesNames} from '../../../trainings/routes.names';
import {Exercise} from '../../../share/models/exercise/exercise';
import {ExerciseState} from './exercise.state';
import {TrainingStates} from '../index';
import {CustomRouterStateSerializer} from '../../../router-state.serializer';
import {of} from 'rxjs';
import {HttpClientModule} from '@angular/common/http';
import {ExerciseServiceApi} from '../../../trainings/service/exercise-service-api.service';
import {ExerciseActions} from '../actions/exercise.action';

@Component({
  selector: 'app-test',
  template: ''
})
class MockExerciseComponent {

}

@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>'
})
class MockAppComponent {

}

const ROUTERS: Routes = [
  {
    path: `${trainingRoutesNames.EXERCISE_ID}`,
    component: MockExerciseComponent
  }
];
describe('Exercise state', () => {
  let store: Store;
  let router: Router;
  let fixture;
  let location: Location;
  let exerciseService: ExerciseServiceApi;

  function getMockExercise(): Exercise {
    return {
      additionalInfo: null,
      detailedDescription: '',
      imageBundle: null,
      exerciseType: null,
      id: 'exercise_1',
      name: 'Exercise 1'
    };
  }

  function getMockExerciseList(): Exercise[] {
    return [{
      additionalInfo: null,
      detailedDescription: '',
      imageBundle: null,
      exerciseType: null,
      id: 'exercise_1',
      name: 'Exercise 1'
    }, {
      additionalInfo: null,
      detailedDescription: '',
      imageBundle: null,
      exerciseType: null,
      id: 'exercise_2',
      name: 'Exercise 2'
    }];

  }

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [MockExerciseComponent, MockAppComponent],
      imports: [NgxsModule.forRoot(TrainingStates,
        {
          selectorOptions: {
            suppressErrors: false,
            injectContainerState: false
          }
        }),
        HttpClientModule,
        NgxsRouterPluginModule.forRoot(),
        RouterTestingModule.withRoutes(ROUTERS)],
      providers: [
        {provide: APP_BASE_HREF, useValue: '/'},
        {provide: RouterStateSerializer, useClass: CustomRouterStateSerializer}
      ],
    }).compileComponents();
    location = TestBed.inject(Location);
    store = TestBed.inject(Store);
    router = TestBed.inject(Router);
    fixture = TestBed.createComponent(MockAppComponent);
    exerciseService = TestBed.inject(ExerciseServiceApi);
    jest.spyOn(exerciseService, 'findAll').mockReturnValue(of(getMockExerciseList()));
    jest.spyOn(exerciseService, 'delete').mockReturnValue(of('exercise_1'));
    fixture.ngZone.run(() => router.initialNavigation());
  }));

  describe('when dispatch Load Exercise action twice', () => {
    it('should not load data', fakeAsync(() => {
      store.dispatch(new ExerciseActions.LoadExerciseAction());
      store.dispatch(new ExerciseActions.LoadExerciseAction());
      tick();
      expect(exerciseService.findAll).toHaveBeenCalledTimes(1);
    }));
  });

  it('should return exercise by id from router', fakeAsync(() => {
      store.dispatch(new ExerciseActions.LoadExerciseAction());
      store.dispatch(new Navigate(['exercise', 'exercise_1']));
      tick();
      const selectedExercise = store.selectSnapshot(ExerciseState.getSelectedEntity());
      expect(selectedExercise).toEqual(getMockExercise());
    }
  ));

  it('should load exercise in normalize state', fakeAsync(() => {
      store.dispatch(new ExerciseActions.LoadExerciseAction());
      tick();
      const selectedExercise = store.selectSnapshot(state => state.training.exercise);
      expect(selectedExercise.ids).toEqual(['exercise_1', 'exercise_2']);
    }
  ));

  it('should delete exercise from normalize state', fakeAsync(() => {
      store.dispatch(new ExerciseActions.LoadExerciseAction());
      tick();
      let selectedExercise = store.selectSnapshot(state => state.training.exercise);
      expect(selectedExercise.ids).toEqual(['exercise_1', 'exercise_2']);

      store.dispatch(new ExerciseActions.DeleteExerciseAction('exercise_1'));
      tick();
      selectedExercise = store.selectSnapshot(state => state.training.exercise);
      expect(selectedExercise.ids).toEqual(['exercise_2']);
    }
  ));

  describe('when dispatch Load Exercise By Id List Action', () => {
    it('should call exercise service api method for load exercises by list id', () => {
      jest.spyOn(exerciseService, 'findByIdList');
      store.dispatch(new ExerciseActions.LoadExerciseByIdListAction(['id1', 'id2']));
      expect(exerciseService.findByIdList).toHaveBeenCalledWith(['id1', 'id2']);
    });
  });

});
