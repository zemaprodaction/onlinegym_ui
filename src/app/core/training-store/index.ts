import {TrainingState} from './states/training.state';
import {ExerciseState} from './states/exercise.state';
import {WorkoutState} from './states/workout.state';
import {WorkoutProgramState} from './states/workout-program/workout-program.state';
import {WorkoutProgramFilterState} from './states/workout-program/workout-program-filter.state';

export const TrainingStates = [TrainingState, ExerciseState, WorkoutState, WorkoutProgramState,
  WorkoutProgramFilterState];
