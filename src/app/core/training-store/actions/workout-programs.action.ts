import {CharacteristicPropertyFilterChosen} from '../../../share/models/characteristic-property-filter-chosen';

export namespace WorkoutProgramAction {
  export class LoadWorkoutPrograms {
    public static readonly type = '[WorkoutPrograms] Load Workout Programs';

    constructor() {
    }
  }

  export class LoadWorkoutProgramById {
    public static readonly type = '[WorkoutPrograms] Load Workout Program by id';

    constructor(public id: string) {
    }
  }

  export class AddWorkoutProgramFilter {
    public static readonly type = '[WorkoutProgramsFilter] Add Workout Program Filter';

    constructor(public payload: CharacteristicPropertyFilterChosen) {
    }
  }

  export class RemoveWorkoutProgramFilter {
    public static readonly type = '[WorkoutProgramsFilter] Remove Workout Program Filter';

    constructor(public payload: CharacteristicPropertyFilterChosen) {
    }
  }


  export class ResetFilters {
    public static readonly type = '[WorkoutProgramsFilter] Reset All Filters';
  }

}


