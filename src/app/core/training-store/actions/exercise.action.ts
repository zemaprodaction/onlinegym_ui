export namespace ExerciseActions {
  export class LoadExerciseAction {
    public static readonly type = '[Exercise] Add Exercise';
  }

  export class DeleteExerciseAction {
    public static readonly type = '[Exercise] Delete Exercise';

    constructor(public payload: string) {
    }
  }

  export class LoadExerciseByIdListAction {
    public static readonly type = '[Exercise] Load Exercise By Id List';

    constructor(public payload: string[]) {
    }
  }
}



