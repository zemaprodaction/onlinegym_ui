export namespace WorkoutAction {
  export class LoadWorkouts {
    public static readonly type = '[Workouts] Load Workout';

    constructor() {
    }
  }

  export class LoadWorkoutByIdListAction {
    public static readonly type = '[Workouts] Load Workout By List Of id';

    constructor(public payload: string[]) {
    }
  }

  export class LoadWorkoutWithTheirExerciseByWorkoutIdList {
    public static readonly type = '[Workouts] Load Workout With Their Exercise By List Of workout ids';

    constructor(public payload: string[]) {
    }
  }

}
