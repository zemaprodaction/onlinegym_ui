export class NavLink {
  constructor(public url: string, public name: string, public svgIcon: string) {}
}
