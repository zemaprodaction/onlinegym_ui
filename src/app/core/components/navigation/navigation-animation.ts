import {animate, animateChild, query, state, style, transition, trigger} from '@angular/animations';

export const navigationAnimations = [
  trigger('openClose', [
      state('open', style({
        transform: 'translateY({{downTo}}px)',
      }), {params: {downTo: '168px'}}),
      state('navigation_open', style({
        position: 'relative',
        transform: 'translateY({{downTo}}px)',
      }), {params: {downTo: '168px'}}),
      state('close', style({
        transform: 'translateY(0)',
      })),
      state('tab_navigation_open', style({
        transform: 'translateY(48px)',
      })),
      state('navigation_close', style({
        top: '0',
      })),
      state('full_width', style({
        minHeight: '2000px',
        top: '-50px',
      })),
      state('navigation_full_width', style({
        width: '0',
      })),
      transition('open => close', [
        animate('250ms ease-in'),
        query('@routerTransition', animateChild()),
      ]),
      transition('close => open', [
        animate('200ms ease-in'),
        query('@routerTransition', animateChild()),
      ]),
      transition('close => tab_navigation_open', [
        animate('200ms ease-in'),
        query('@routerTransition', animateChild()),
      ]),
      transition('close <=> open', [
        animate('200ms ease-in'),
        query('@routerTransition', animateChild()),
      ]),
      transition('tab_navigation_open <=> open', [
        animate('200ms ease-in'),
        query('@routerTransition', animateChild()),
      ]),
      transition('* => full_width', [
        animate('200ms ease-in'),
        query('@routerTransition', animateChild()),
      ]),
      transition('* => navigation_full_width', [
        animate('200ms ease-in'),
        query('@routerTransition', animateChild()),
      ]),
      transition('navigation_close <=> navigation_open', [
        animate('200ms ease-in'),
        query('@routerTransition', animateChild()),
      ]),
    ]
  ),
  trigger('openCloseNavigation', [
      state('open', style({
        transform: 'translateY({{downTo}}px)',
      }), {params: {downTo: '168px'}}),
      state('navigation_open', style({
        transform: 'translateY({{downTo}}px)',
      }), {params: {downTo: '168px'}}),
      state('close', style({
        transform: 'translateY(0)',
      })),
      state('tab_navigation_open', style({
        transform: 'translateY(48px)',
      })),
      state('navigation_close', style({
        top: '72px'
      })),
      state('full_width', style({
        minHeight: '2000px',
        top: '-50px',
      })),
      state('navigation_full_width', style({
        width: '0',
      })),
      transition('open => close', [
        animate('250ms ease-in'),
      ]),
      transition('close => open', [
        animate('200ms ease-in'),
      ]),
      transition('close => tab_navigation_open', [
        animate('200ms ease-in'),
      ]),
      transition('close <=> open', [
        animate('200ms ease-in'),
      ]),
      transition('tab_navigation_open <=> open', [
        animate('200ms ease-in'),
      ]),
      transition('* => full_width', [
        animate('200ms ease-in'),
      ]),
      transition('* => navigation_full_width', [
        animate('200ms ease-in'),
      ]),
      transition('navigation_close <=> navigation_open', [
        animate('200ms ease-in'),
      ]),
    ]
  )
];
