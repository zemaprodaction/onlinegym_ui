import {AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {Observable} from 'rxjs';
import {NavLink} from './nav-link';
import {User} from '../../../share/models/user';
import {ToolbarAction} from '../../../share/models/toolbar-action';
import {navigationAnimations} from './navigation-animation';
import {filter, map, withLatestFrom} from 'rxjs/operators';
import {ToolbarContextHostDirective} from '../../directives';
import {BackdropSetting} from '../../type/backdrop-setting';
import {Select, Store} from '@ngxs/store';
import {AppUiState, BackdropState, BackdropStatus} from '../../store';
import {BackdropService} from '../../services/backdrop/backdrop.service';
import {BackdropSubheaderSettings} from '../../../share/models/backdrop-subheader-settings';
import {FabSettings} from '../../../share/models/fab-settings';
import {RouterOutlet} from '@angular/router';
import {routeTransitionAnimations} from '../../../route-transition-animations';


@Component({
    selector: 'app-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.scss'],
    animations: [routeTransitionAnimations, navigationAnimations],
})
export class NavigationComponent implements OnInit, AfterViewInit {

    @Select(AppUiState.fabSettings)
    fabSetting: Observable<FabSettings>;

    @Input()
    user!: User;

    @Input()
    links: NavLink[] = [];

    @ViewChild('backdropFront') elementView: ElementRef;

    @ViewChild(ToolbarContextHostDirective, {static: true})
    toolbarContextHostDirective!: ToolbarContextHostDirective;

    @Select(BackdropState.getStatus)
    $backDropStatus: Observable<BackdropStatus>;

    @Select(BackdropState.getSetting)
    $backdropSetting: Observable<BackdropSetting>;

    @Select(BackdropState.getBackdroSubHeader)
    $backdropSubHeaderSetting: Observable<BackdropSubheaderSettings>;

    $backdropDownToHeight: Observable<number>;

    $backdropAnimateStatus: Observable<BackdropStatus>;

    component: ToolbarAction;

    contentHeight: number;

    constructor(private breakpointObserver: BreakpointObserver,
                private _store: Store,
                private backdropService: BackdropService) {
        this.$backdropDownToHeight = this.$backdropSetting.pipe(
            filter((setting) => setting !== null),
            withLatestFrom(breakpointObserver.observe([Breakpoints.XSmall])),
            map(([setting, observ]) => observ.matches ? setting.mobileDownToHeight : setting.webDownToHeight),
        );
    }

    ngAfterViewInit() {
        this.contentHeight = this.elementView.nativeElement.offsetHeight;
    }

    ngOnInit() {
        this.backdropService.setToolbarContextHostDirective(this.toolbarContextHostDirective);
        this.$backdropAnimateStatus = this.$backDropStatus;
    }

    toggle($event: ToolbarAction) {
        $event.button.callback(this.toolbarContextHostDirective);
    }

    prepareRoute(outlet: RouterOutlet) {
        return outlet &&
            outlet.activatedRouteData &&
            outlet.activatedRouteData['animationState'];
    }

    closeBackdrop() {
        this.backdropService.closeBackdropMenu();
    }

}
