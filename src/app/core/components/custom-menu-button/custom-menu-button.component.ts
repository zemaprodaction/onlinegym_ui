import {Component, Input, OnInit} from '@angular/core';
import {BackdropMenuButton} from '../../../share/models/backdrop-menu-button';

@Component({
  selector: 'app-custom-menu-button',
  templateUrl: './custom-menu-button.component.html',
  styleUrls: ['./custom-menu-button.component.scss']
})
export class CustomMenuButtonComponent implements OnInit {

  @Input()
  menuButton: BackdropMenuButton;

  constructor() {
  }

  ngOnInit(): void {
  }

}
