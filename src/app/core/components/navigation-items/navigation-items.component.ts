import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';


@Component({
  selector: 'app-navigation-items',
  templateUrl: './navigation-items.component.html',
  styleUrls: ['./navigation-items.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavigationItemsComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {

  }
}
