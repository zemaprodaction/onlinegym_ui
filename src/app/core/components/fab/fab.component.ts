import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {FabSettings} from '../../../share/models/fab-settings';

@Component({
  selector: 'app-fab',
  templateUrl: './fab.component.html',
  styleUrls: ['./fab.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FabComponent implements OnInit {

  @Input()
  fabSettings: FabSettings;

  constructor() { }

  ngOnInit(): void {
  }

}
