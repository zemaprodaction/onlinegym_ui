import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ToolbarAction} from '../../../share/models/toolbar-action';
import {NavigationItemsComponent} from '../navigation-items/navigation-items.component';
import {ToolBarButton} from '../../../share/models/tool-bar-button';
import {Select, Store} from '@ngxs/store';
import {AppService} from '../../store/app.service';
import {Observable} from 'rxjs';
import {BackdropService} from '../../services/backdrop/backdrop.service';
import {AppUiState, BackdropState, BackdropStatus, ToolbarState} from '../../store';
import {User} from '../../../share/models/user';
import {Navigate} from '@ngxs/router-plugin';
import {environment} from '../../../../environments/environment';
import {BackdropSetting} from '../../type/backdrop-setting';
import {HttpClient} from '@angular/common/http';
import {BackdropMenuButton} from '../../../share/models/backdrop-menu-button';
import {AuthorizationUserState} from "../../../authorization/store/states/authorization-user.state";

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToolbarComponent implements OnInit {

  @Select(AppUiState.currentPageName)
  currentPageName$: Observable<string>;

  @Select(ToolbarState.actions)
  toolbarActions$: Observable<ToolbarAction[]>;

  @Select(ToolbarState.getIsShowLoginButton)
  isShowLoginAction$: Observable<ToolbarAction[]>;

  @Select(BackdropState.getMenuButton)
  menuButton$: Observable<BackdropMenuButton>;

  @Select(AuthorizationUserState.getCurrentUser)
  user$: Observable<User>;

  @Input()
  backdropStatus: BackdropStatus;

  @Input()
  backdropSettings: BackdropSetting = {};

  @Output() public sidenavToggle = new EventEmitter();
  @Output() public actionClick = new EventEmitter<ToolbarAction>();

  menuButtonAction: ToolbarAction;
  closeMenuButton: ToolbarAction;

  constructor(private appService: AppService, private backdropService: BackdropService,
              private store: Store, private http: HttpClient
  ) {
    this.menuButtonAction = {
      button: new ToolBarButton($localize`:@@menu:Menu`, '', 'filter_alt',
        () => this.openBackDropWithMenu()),
      component: NavigationItemsComponent,
      status: BackdropStatus.OPEN,
      isUseBackdropContext: true,
    };
    this.closeMenuButton = {
      button: new ToolBarButton($localize`:@@close:Close`, '', 'close', () => this.closeBackdropMenu()),
      component: NavigationItemsComponent,
      status: BackdropStatus.CLOSE,
      isUseBackdropContext: true,
    };

  }

  ngOnInit(): void {
  }

  getLoginLogoutButton(user: User) {
    const loginCallback = () => window.location.href = `${environment.api.baseUrl}/oauth2/authorization/keycloak`;
    const logoutCallback = () => this.logout();
    if (user && user.isAuthorized) {
      return new ToolbarAction(new ToolBarButton($localize`:@@toolbar.logout:Logout`, '/logout',
        'logout', logoutCallback), false);
    } else {
      return new ToolbarAction(
        new ToolBarButton($localize`:@@login:Login`,
          `${environment.api.baseUrl}/oauth2/authorization/keycloak`, 'person', loginCallback), null);
    }
  }

  logout() {
    this.http.post(`${environment.api.baseUrl}/logout`, {}).subscribe(() => {
      window.location.reload();
    });
  }

  back() {
    this.store.dispatch(new Navigate(['/training/workout-program']));
  }

  backDropStatusIsOpen(status: BackdropStatus) {
    return status === BackdropStatus.OPEN;
  }

  public onToggleSidenav = (toolbarAction: ToolbarAction) => {
    this.actionClick.emit(toolbarAction);
  };

  onActionClick($event: ToolbarAction) {
    this.actionClick.emit($event);
  }

  private openBackDropWithMenu() {
    this.backdropService.openMenu();

  }

  private closeBackdropMenu() {
    this.backdropService.closeBackdropMenu();
  }
}
