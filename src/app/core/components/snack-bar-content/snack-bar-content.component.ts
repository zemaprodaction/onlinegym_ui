import {Component, Inject, NgZone, OnInit, Renderer2} from '@angular/core';
import {MAT_SNACK_BAR_DATA, MatSnackBarRef} from "@angular/material/snack-bar";

@Component({
    selector: 'app-snack-bar-content',
    templateUrl: './snack-bar-content.component.html',
    styleUrls: ['./snack-bar-content.component.scss']
})
export class SnackBarContentComponent implements OnInit {

    constructor(
        @Inject(MAT_SNACK_BAR_DATA) public data,
        private _snackRef: MatSnackBarRef<SnackBarContentComponent>,
        private _zone: NgZone
    ) {
    }

    ngOnInit(): void {
    }

    dismiss() {
        this._zone.run(() => {
            this._snackRef.dismiss();
        })
    }

}
