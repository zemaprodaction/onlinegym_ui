import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ToolbarAction} from '../../../share/models/toolbar-action';

@Component({
  selector: 'app-log-button',
  templateUrl: './log-button.component.html',
  styleUrls: ['./log-button.component.scss']
})
export class LogButtonComponent implements OnInit {

  @Input()
  logButton: ToolbarAction;
  @Output() public actionClick = new EventEmitter<ToolbarAction>();

  constructor() {
  }

  ngOnInit(): void {
  }

  onActionClick($event: ToolbarAction) {
    this.actionClick.emit($event);
  }

}
