export const appRoutesNames = {
  WORKOUT_PROGRAMS: 'workout-programs',
  ADMIN: 'admin',
  USER: 'user',
  TRAINING_PROCESS: 'training-process',
  PROGRESS: 'progress'
};
