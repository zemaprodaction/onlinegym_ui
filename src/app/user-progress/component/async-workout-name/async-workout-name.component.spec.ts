import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AsyncWorkoutNameComponent } from './async-workout-name.component';

describe('AsyncWorkoutNameComponent', () => {
  let component: AsyncWorkoutNameComponent;
  let fixture: ComponentFixture<AsyncWorkoutNameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AsyncWorkoutNameComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AsyncWorkoutNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
