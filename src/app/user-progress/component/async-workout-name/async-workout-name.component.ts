import {Component, Input, OnInit} from '@angular/core';
import {Workout} from '../../../share/models/workout/workoutEntity';

@Component({
  selector: 'app-async-workout-name',
  templateUrl: './async-workout-name.component.html',
  styleUrls: ['./async-workout-name.component.scss']
})
export class AsyncWorkoutNameComponent implements OnInit {

  @Input()
  workout: Workout;

  constructor() {
  }

  ngOnInit(): void {
  }

}
