import {Component, Inject, OnInit} from '@angular/core';
import {MAT_BOTTOM_SHEET_DATA} from '@angular/material/bottom-sheet';
import {UserWorkoutProgramServiceApi} from '../../../core/services/user-workout-program-api.service';
import {Observable} from 'rxjs';
import {UserWorkoutProgramResult} from '../../../share/models/user-workout-program-result';

@Component({
  selector: 'app-workout-program-result-bottomsheet-card',
  templateUrl: './workout-program-result-bottomsheet-card.component.html',
  styleUrls: ['./workout-program-result-bottomsheet-card.component.scss']
})
export class WorkoutProgramResultBottomsheetCardComponent implements OnInit {

  header = $localize`:@@results:Results`
  results$: Observable<UserWorkoutProgramResult>;

  constructor(@Inject(MAT_BOTTOM_SHEET_DATA) public data: { archiveId: string }, private service: UserWorkoutProgramServiceApi) {
  }

  ngOnInit(): void {
    this.results$ = this.service.getArchiveUserWorkoutProgramResult(this.data.archiveId);
  }

}
