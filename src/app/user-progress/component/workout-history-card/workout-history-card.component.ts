import {Component, Input, OnInit} from '@angular/core';
import {WorkoutData} from '../../../share/models/workout-data';
import {WorkoutHistoryDialogComponent} from '../workout-history-dialog/workout-history-dialog.component';
import {MatBottomSheet} from '@angular/material/bottom-sheet';

@Component({
  selector: 'app-workout-history-card',
  templateUrl: './workout-history-card.component.html',
  styleUrls: ['./workout-history-card.component.scss']
})
export class WorkoutHistoryCardComponent implements OnInit {

  @Input()
  workoutDataList: WorkoutData[];

  constructor(private _bottomSheet: MatBottomSheet) {
  }

  ngOnInit(): void {
  }

  openDialog() {
    this._bottomSheet.open(WorkoutHistoryDialogComponent, {
      panelClass: 'bottom-sheet-full-width'
    });
  }
}
