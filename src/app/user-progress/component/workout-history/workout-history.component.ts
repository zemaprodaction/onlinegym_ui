import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {WorkoutData} from '../../../share/models/workout-data';
import {Store} from '@ngxs/store';
import {WorkoutState} from '../../../core/training-store/states/workout.state';
import {filter, tap} from 'rxjs/operators';

@Component({
  selector: 'app-workout-history',
  templateUrl: './workout-history.component.html',
  styleUrls: ['./workout-history.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkoutHistoryComponent implements OnInit {

  @Input()
  workoutDataList: WorkoutData[];

  constructor(private store: Store) {
  }

  ngOnInit(): void {
  }

  getName(id: string) {
    return this.store.select(WorkoutState.getWorkoutById(id)).pipe(
      filter(v => v !== undefined),
    );
  }

}
