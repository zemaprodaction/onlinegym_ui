import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {UserWorkoutProgramStatisticService} from '../../../core/services/statistic/user-workout-program-statistic.service';
import {Select} from '@ngxs/store';
import {filter, map, share, switchMap} from 'rxjs/operators';
import {UserState} from '../../../core/user-store/states/user.state';
import {WorkoutProgram} from '../../../share/models/workout-programs';
import {CurrentWorkoutProgramStatistic} from '../../types/workout-program-statistic';
import {WorkoutData} from '../../../share/models/workout-data';
import {WorkoutStatisticService} from '../../../core/services/statistic/workout-statistic.service';
import {UserWorkoutProgramResult} from '../../../share/models/user-workout-program-result';
import {UserWorkoutProgramServiceApi} from '../../../core/services/user-workout-program-api.service';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-user-mainly-progress',
  templateUrl: './user-mainly-progress.component.html',
  styleUrls: ['./user-mainly-progress.component.scss']
})
export class UserMainlyProgressComponent implements OnInit {

  @Select(UserState.getTrainingWorkoutProgram)
  $currentWorkoutProgram: Observable<WorkoutProgram>;

  workoutProgramStatistic$: Observable<CurrentWorkoutProgramStatistic>;
  percentCompleate$: Observable<number>;
  workoutHistory$: Observable<WorkoutData[]>;

  results$: Observable<UserWorkoutProgramResult>;
  constructor(private workoutProgramStatisticService: UserWorkoutProgramStatisticService,
              private service: UserWorkoutProgramServiceApi,
              private router: Router, protected route: ActivatedRoute,
              private workoutStatisticService: WorkoutStatisticService) {
    this.results$ = this.service.getActiveUserWorkoutProgramResult();
    this.workoutProgramStatistic$ = this.$currentWorkoutProgram.pipe(
      share(),
      filter(wp => wp !== null && wp !== undefined),
      switchMap(currentWp => workoutProgramStatisticService.getUserWorkoutProgramStatistic(currentWp.id))
    );
    this.percentCompleate$ = this.workoutProgramStatistic$.pipe(
      map(stat => stat.averagePercentComplete)
    );
    this.workoutHistory$ = this.workoutStatisticService.getUserHistory(3);
  }

  ngOnInit(): void {
  }

  swipeLeft() {
    this.router.navigate(["../workout-program"], {relativeTo: this.route});
  }
}
