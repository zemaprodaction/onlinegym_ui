import {Component, OnInit} from '@angular/core';
import {WorkoutStatisticService} from '../../../core/services/statistic/workout-statistic.service';
import {Observable} from 'rxjs';
import {WorkoutData} from '../../../share/models/workout-data';
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';

@Component({
  selector: 'app-workout-history-dialog',
  templateUrl: './workout-history-dialog.component.html',
  styleUrls: ['./workout-history-dialog.component.scss']
})
export class WorkoutHistoryDialogComponent implements OnInit {

  workoutHistory$: Observable<WorkoutData[]>;

  constructor(private workoutStatisticService: WorkoutStatisticService,
              public bottomSheet: MatBottomSheet,
              private _bottomSheetRef: MatBottomSheetRef<WorkoutHistoryDialogComponent>) { }

  ngOnInit(): void {
    this.workoutHistory$ = this.workoutStatisticService.getAlUserHistory();
  }

  onNoClick(): void {
    this._bottomSheetRef.dismiss();
  }

}
