import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {WorkoutProgramProgress} from '../../../share/models/workout-program-progress';

@Component({
  selector: 'app-workout-program-complete-percent-card',
  templateUrl: './workout-program-complete-percent-card.component.html',
  styleUrls: ['./workout-program-complete-percent-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkoutProgramCompletePercentCardComponent implements OnInit {

  @Input()
  userWorkoutProgramStatistic: WorkoutProgramProgress;

  constructor() {
  }

  ngOnInit(): void {
  }

}
