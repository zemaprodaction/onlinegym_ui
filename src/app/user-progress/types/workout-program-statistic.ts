export class UserWorkoutProgramStatistic {
  workoutProgramId: string;
  finishedWorkoutInstance: string[];
  userId: string;
}

export interface CurrentWorkoutProgramStatistic {
  averagePercentComplete: number;
  workoutProgramStatistic: UserWorkoutProgramStatistic;
  percentDone: number;
}
