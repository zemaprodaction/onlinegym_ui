export interface GroupingStatistic {
  name: string;
  value: number;
  measurePostfix: string;
}
