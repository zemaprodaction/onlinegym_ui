export interface WorkoutHistoryDay {
  workoutCompletePercent: number;
  workoutInstanceId: string;
  date?: Date;
  numberRestDaysBeforeWorkout: number;
  workoutId: string;
}
