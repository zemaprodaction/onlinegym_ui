import {GroupingStatistic} from './grouping-statistic';
import {WorkoutHistoryDay} from './workout-history-day';

export interface UserProgress {
  groupingStatistic: GroupingStatistic[];
  workoutHistory: WorkoutHistoryDay[];
}
