export const USER_PROGRESS_ROUTER_PARAMS = {
  WORKOUT_RESULT_ID: 'workoutResultId',
  WORKOUT_ID: 'workoutId',
  WORKOUT_DATA_STATISTIC_ID: 'id',
};
