import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {WorkoutProcessResultComponent} from './container';
import {WorkoutResultExistsGuard} from './guard/workout-result-exists.guard';
import {USER_PROGRESS_ROUTER_PARAMS} from './endpoint-constant';
import {OpenTabNavigationBackdropGuard} from '../core/guards/open-tab-navigation-backdrop-guard.service';
import {UserProgressContainerComponent} from './container/user-progress-container/user-progress-container.component';
import {UserMainlyProgressComponent} from './component/user-mainly-progress/user-mainly-progress.component';
import {ExerciseUserProgressComponent} from './container/exersice-user-progress/exercise-user-progress.component';
import {WorkoutProgramProgressComponent} from './container/workout-program-progress/workout-program-progress.component';
import {WorkoutProgressComponent} from './container/workout-progress/workout-progress.component';
import {CloseBackdropGuard} from '../core/guards/close-backdrop.guard';
import {ResetFabGuard} from '../trainings/guards/reset-fab.guard';
import {AuthGuard} from "../share/guards/auth.guard";

export const USER_ROUTES: Routes = [
    {
        path: `workout_result/:${USER_PROGRESS_ROUTER_PARAMS.WORKOUT_DATA_STATISTIC_ID}`,
        component: WorkoutProcessResultComponent,
        canActivate: [WorkoutResultExistsGuard, AuthGuard],
        data: {
            animationState: 'workout-result-page'
        }
    },
    {
        path: ``,
        component: UserProgressContainerComponent,
        children: [
            {path: '', redirectTo: 'workout-program'},
            {path: 'mainly', component: UserMainlyProgressComponent, data: {animationState: '1'}},
            {path: 'workout-program', component: WorkoutProgramProgressComponent, data: {animationState: '2'}},
            {path: 'workout', component: WorkoutProgressComponent, data: {animationState: '3'}},
            {path: 'exercise', component: ExerciseUserProgressComponent, data: {animationState: '4'}},
        ],
        canActivate: [OpenTabNavigationBackdropGuard],
        canDeactivate: [CloseBackdropGuard, ResetFabGuard],
        data: {
            currentNamePage: $localize`:@@page-name.workout-programs:Workout programs`,
            namePage: $localize`:@@progress:Progress`,

            navLinks: [
                // {
                //     path: 'mainly',
                //     label: $localize`:@@mainly:Mainly`
                // },
                {
                    path: 'workout-program',
                    label: $localize`:@@programs:Programs`
                },
                {
                    path: 'workout',
                    label: $localize`:@@workouts:Workouts`
                },
                {
                    path: 'exercise',
                    label: $localize`:@@exercises:Exercises`
                }
            ],
            animationState: 'statistic'
        }
    },
];

export const routing: ModuleWithProviders<RouterModule> = RouterModule.forChild(USER_ROUTES);
