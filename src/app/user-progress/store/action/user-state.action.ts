import {WorkoutResult} from '../../../share/models/workout-result';

export namespace UserStatisticActions {

  export class AddWorkoutResult {
    public static readonly type = '[User Statistic] Add Workout Result';

    constructor(public workoutResult: WorkoutResult) {
    }
  }

  export class LoadWorkoutResult {
    public static readonly type = '[User Statistic] Load Workout Result';
    constructor(public workoutDataStatisticId: string) {
    }
  }
  export class ResetAction {
    public static readonly type = '[User Statistic] Reset State';
    constructor() {
    }
  }
}


