import {Action, Selector, State, StateContext} from '@ngxs/store';
import {defaultsEntityState, EntityState, EntityStateModel} from '../../../share/store/entity.state';
import {UserStatisticActions} from '../action/user-state.action';
import {WorkoutResult} from '../../../share/models/workout-result';
import {Injectable} from '@angular/core';
import {UserProgressServiceApi} from '../../service/user-progress-api.service';
import {tap} from 'rxjs/operators';
import {USER_PROGRESS_ROUTER_PARAMS} from '../../endpoint-constant';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface UserStatisticStateModel extends EntityStateModel<WorkoutResult> {

}

@State({
    name: 'userStatistic',
    defaults: {
        ...defaultsEntityState,
        idParam: 'userWorkoutInstanceRowStatisticId',
        routerSelectedParams: USER_PROGRESS_ROUTER_PARAMS.WORKOUT_DATA_STATISTIC_ID
    }
})
@Injectable()
export class UserStatisticState extends EntityState<WorkoutResult> {

    constructor(private userProgressServiceApi: UserProgressServiceApi) {
        super(null);
    }

    @Selector([UserStatisticState.getSelectedEntity()])
    static getSelectedWorkoutResult<R>(selectedEntity: WorkoutResult) {
        if (selectedEntity) {
            return selectedEntity;
        }
    }

    @Action(UserStatisticActions.AddWorkoutResult)
    public addWorkoutResult(ctx: StateContext<EntityStateModel<WorkoutResult>>, action: UserStatisticActions.AddWorkoutResult) {
        this.addOneWithId(ctx, action.workoutResult, action.workoutResult.userWorkoutInstanceRowStatisticId);
    }

    @Action(UserStatisticActions.LoadWorkoutResult)
    public loadWorkoutResult(ctx: StateContext<EntityStateModel<WorkoutResult>>,
                             {workoutDataStatisticId}: UserStatisticActions.LoadWorkoutResult) {
        return this.loadByWorkoutAndWorkoutInstanceInstanceId(ctx, workoutDataStatisticId);
    }

    @Action(UserStatisticActions.ResetAction)
    public reset(ctx: StateContext<EntityStateModel<WorkoutResult>>) {
        ctx.setState({
            ...defaultsEntityState,
        });
    }

    public loadByWorkoutAndWorkoutInstanceInstanceId(ctx: StateContext<EntityStateModel<WorkoutResult>>,
                                                     workoutDataStatisticId: string) {
        return this.userProgressServiceApi.getWorkoutResultSummary(workoutDataStatisticId).pipe(
            tap(entity => {
                const state = ctx.getState();
                const newState = {
                    ...state,
                    entities: {...state.entities, [entity.userWorkoutInstanceRowStatisticId]: entity},
                    ids: [...state.ids, entity.userWorkoutInstanceRowStatisticId]
                };
                ctx.setState(newState);
            })
        );
    }


}
