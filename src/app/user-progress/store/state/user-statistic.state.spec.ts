import {NgxsModule, Store} from '@ngxs/store';
import {fakeAsync, TestBed, tick, waitForAsync} from '@angular/core/testing';
import {UserStatisticState} from './user-statistic.state';
import {UserStatisticActions} from '../action/user-state.action';
import {workoutResultTestData} from '../../test-utils/test.data.spec';
import {NgxsRouterPluginModule, RouterStateSerializer} from '@ngxs/router-plugin';
import {APP_BASE_HREF} from '@angular/common';
import {CustomRouterStateSerializer} from '../../../router-state.serializer';
import {MockComponent} from '../../../share/unit-test-utils/unit-test-utils.spec';
import {RouterTestingModule} from '@angular/router/testing';
import {trainingRoutesNames} from '../../../trainings/routes.names';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('User statistic state', () => {
  let store: Store;
  let fixture;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [MockComponent],
      imports: [NgxsModule.forRoot([UserStatisticState],
        {
          selectorOptions: {
            suppressErrors: false,
            injectContainerState: false
          }
        }),
        NgxsRouterPluginModule.forRoot(),
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([
            {
              path: `${trainingRoutesNames.WORKOUT_RESULT_ID}`,
              component: MockComponent
            }
          ]
        )],
      providers: [
        {provide: APP_BASE_HREF, useValue: '/'},
        {provide: RouterStateSerializer, useClass: CustomRouterStateSerializer}
      ],
    }).compileComponents();
    store = TestBed.inject(Store);
    fixture = TestBed.createComponent(MockComponent);
  }));


  it('should add normilize workout result when dispatch addWorkoutResult action', fakeAsync(() => {
      store.dispatch(new UserStatisticActions.AddWorkoutResult(workoutResultTestData));
      tick();
      const statisticState = store.selectSnapshot(state => state.userStatistic);
      const workoutResultId = workoutResultTestData.workoutInstanceId;
      expect(statisticState.entities[workoutResultTestData.id]).toEqual(workoutResultTestData);
    }
  ));
});
