import {WorkoutResult} from '../../share/models/workout-result';

export class WorkoutResultUtils {
  public static generateWorkoutResultIdByWorkoutResult(workoutResult: WorkoutResult) {
    return this.generateWorkoutResultIdByDayAndId(workoutResult.date, workoutResult.workoutInstanceId);
  }

  public static generateWorkoutResultIdByDayAndId(date: string, workoutInstanceId: string) {
    return date + '_' + workoutInstanceId;
  }
}
