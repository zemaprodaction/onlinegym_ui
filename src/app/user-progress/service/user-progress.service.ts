import {Injectable} from '@angular/core';
import {Store} from '@ngxs/store';
import {WorkoutResult} from '../../share/models/workout-result';
import {Observable} from 'rxjs';
import {UserStatisticState} from '../store/state/user-statistic.state';
import {UserStatisticActions} from '../store/action/user-state.action';

@Injectable({
  providedIn: 'root'
})
export class UserProgressService {

  constructor(private store: Store) {
  }

  loadWorkoutResultById(workoutDataStatisticId: string) {
    this.store.dispatch(new UserStatisticActions.LoadWorkoutResult(workoutDataStatisticId));
  }

  findWorkoutResultById(workoutResultId: string): Observable<WorkoutResult> {
    return this.store.select(UserStatisticState.getEntityById(workoutResultId));
  }
  findWorkoutResultByIdSnapshot(workoutResultId: string): WorkoutResult {
    return this.store.selectSnapshot(UserStatisticState.getEntityById(workoutResultId));
  }
}
