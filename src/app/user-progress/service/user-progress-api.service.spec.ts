import {TestBed} from '@angular/core/testing';

import {UserProgressServiceApi} from './user-progress-api.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {StatisticConstants} from '../../share/constants/endpointConstants';
import {NgxsModule, Store} from '@ngxs/store';

import {MatSnackBarModule} from '@angular/material/snack-bar';
import {userProgress} from '../../share/unit-test-utils/unit-test-utils.spec';
import {UserState} from '../../core/user-store/states/user.state';


describe('UserProgressService', () => {
  let service: UserProgressServiceApi;
  let http: HttpTestingController;
  let store: Store;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatSnackBarModule,
        NgxsModule.forRoot([UserState])
      ],
    });
    service = TestBed.inject(UserProgressServiceApi);
    http = TestBed.inject(HttpTestingController);
    store = TestBed.inject(Store);

    store.reset({
      ...store.snapshot(),
      user: {
        workoutProgram: {
          id: 'workout_program_test_id'
        }
      }
    });
  });

  afterEach(() => {
    http.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return user progress from backend', () => {
    service.getUserProgress().subscribe((usrProgress) => {
      expect(usrProgress).toEqual(userProgress);
    });

    const req = http.expectOne(`${StatisticConstants.USER_PROGRESS}`);
    expect(req.request.method).toEqual('GET');

    req.flush({...userProgress});
  });
});
