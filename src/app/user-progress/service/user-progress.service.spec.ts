import {TestBed} from '@angular/core/testing';

import {UserProgressService} from './user-progress.service';
import {NgxsModule, Store} from '@ngxs/store';
import {WorkoutResult} from '../../share/models/workout-result';
import {UserStatisticState} from '../store/state/user-statistic.state';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('UserProgressServiceService', () => {
  let service: UserProgressService;
  let store: Store;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot([UserStatisticState]), HttpClientTestingModule]
    });
    service = TestBed.inject(UserProgressService);
    store = TestBed.inject(Store);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('when store has specific workout result', () => {
    let workoutResult: WorkoutResult;
    beforeEach(() => {
      workoutResult = {
        id: '',
        workoutInstanceId: 'workout_result_id',
        date: 'test_data'
      };
      store.reset({
        userStatistic: {
          entities: {
            ['test_data_workout_result_id']: workoutResult
          }
        }
      });
    });
  });

  describe('when store has NOT specific workout result', () => {
    let workoutResult: WorkoutResult;
    beforeEach(() => {
      workoutResult = {
        id: '',
        workoutInstanceId: 'workout_result_id',
        date: 'test_data'
      };
      store.reset({
        userStatistic: {
          entities: {
            ['test_anauther_workoutid']: workoutResult
          }
        }
      });
    });

    it('should return null when call find method', done => {
      const result = service
        .findWorkoutResultById(workoutResult.workoutInstanceId);
      result.subscribe(res => {
        expect(res).toBeUndefined();
        done();
      });
    });
  });
});
