import {Injectable} from '@angular/core';
import {UserProgress} from '../types/user-progress';
import {Observable, throwError} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {StatisticConstants} from '../../share/constants/endpointConstants';
import {catchError, filter, switchMap} from 'rxjs/operators';
import {Select} from '@ngxs/store';
import {WorkoutProgramEntity} from '../../share/models/workout-programs';
import {WorkoutResult} from '../../share/models/workout-result';
import {UserState} from '../../core/user-store/states/user.state';
import {WorkoutProgramProgress} from '../../share/models/workout-program-progress';

@Injectable({
  providedIn: 'root'
})
export class UserProgressServiceApi {

  @Select(UserState.getTrainingWorkoutProgram)
  userWorkoutProgram$: Observable<WorkoutProgramEntity>;

  constructor(private http: HttpClient) {
  }

  getUserProgress() {
    return this.http.get<UserProgress>(`${StatisticConstants.USER_PROGRESS}`)
      .pipe(catchError((error: any) => throwError(error.json())));

  }

  getUserWorkoutProgramStatistic() {
    return this.userWorkoutProgram$.pipe(
      filter(userWp => userWp !== null && userWp !== undefined),
      switchMap(userWP => this.http.get<WorkoutProgramProgress>(`${StatisticConstants.USER_PROGRESS_CURRENT_WORKOUT_PROGRAM}/${userWP.id}`)
        .pipe(
          catchError((error: any) => throwError(error.json())))
      ));
  }

  getWorkoutResultSummary(workoutDataStatisticId: string): Observable<WorkoutResult> {
    return this.http.get<WorkoutResult>
    (`${StatisticConstants.ROOT_STATISTIC_WORKOUT_INSTANCE}/${workoutDataStatisticId}/summary`)
      .pipe(catchError((error: any) => throwError(error.json())
      ));
  }
}
