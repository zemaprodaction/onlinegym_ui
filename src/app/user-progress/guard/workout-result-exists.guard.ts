import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {UserProgressService} from '../service/user-progress.service';
import {tap} from 'rxjs/operators';
import {USER_PROGRESS_ROUTER_PARAMS} from '../endpoint-constant';
import {AuthGuard} from '../../share/guards/auth.guard';

@Injectable({
  providedIn: 'root'
})
export class WorkoutResultExistsGuard implements CanActivate {

  constructor(private userProgressService: UserProgressService, private authGuard: AuthGuard) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> {
    return this.authGuard.canActivate(next, state).pipe(
      tap(_ => {
        const workoutDataStatisticId = next.params[USER_PROGRESS_ROUTER_PARAMS.WORKOUT_DATA_STATISTIC_ID];
        const result = this.userProgressService.findWorkoutResultByIdSnapshot(workoutDataStatisticId);
        if (!result) {
          this.userProgressService.loadWorkoutResultById(workoutDataStatisticId);
        }
      }));
  }
}
