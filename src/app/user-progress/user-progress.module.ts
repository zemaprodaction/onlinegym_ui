import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ShareModule} from '../share/share.module';
import {routing} from './user-progress-router';
import {FlexModule, GridModule} from '@angular/flex-layout';
import {MatCardModule} from '@angular/material/card';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatButtonModule} from '@angular/material/button';
import {NgxsModule} from '@ngxs/store';
import {UserStatisticState} from './store/state/user-statistic.state';
import {MatListModule} from '@angular/material/list';
import {fromContainer} from './container';
import {WorkoutProgramCompletePercentCardComponent} from './component/workou-program-complete-percent-card/workout-program-complete-percent-card.component';
import {UserProgressContainerComponent} from './container/user-progress-container/user-progress-container.component';
import {UserMainlyProgressComponent} from './component/user-mainly-progress/user-mainly-progress.component';
import {ExerciseUserProgressComponent} from './container/exersice-user-progress/exercise-user-progress.component';
import {WorkoutProgramProgressComponent} from './container/workout-program-progress/workout-program-progress.component';
import {WorkoutProgressComponent} from './container/workout-progress/workout-progress.component';
import {WorkoutHistoryComponent} from './component/workout-history/workout-history.component';
import {WorkoutHistoryDialogComponent} from './component/workout-history-dialog/workout-history-dialog.component';
import {WorkoutHistoryCardComponent} from './component/workout-history-card/workout-history-card.component';
import { AsyncWorkoutNameComponent } from './component/async-workout-name/async-workout-name.component';
import {MatIconModule} from '@angular/material/icon';
import { WorkoutProgramResultBottomsheetCardComponent } from './component/workout-program-result-bottomsheet-card/workout-program-result-bottomsheet-card.component';
import {MatDialogModule} from "@angular/material/dialog";

@NgModule({
  declarations: [...fromContainer,
    WorkoutProgramCompletePercentCardComponent,
    UserProgressContainerComponent,
    UserMainlyProgressComponent, ExerciseUserProgressComponent,
    WorkoutProgramProgressComponent, WorkoutProgressComponent,
    WorkoutHistoryComponent, WorkoutHistoryDialogComponent, WorkoutHistoryCardComponent,
    AsyncWorkoutNameComponent, WorkoutProgramResultBottomsheetCardComponent],
    imports: [
        CommonModule,
        ShareModule,
        routing,
        GridModule,
        NgxsModule.forFeature([UserStatisticState]),
        MatCardModule,
        FlexModule,
        MatProgressBarModule,
        MatButtonModule,
        MatListModule,
        MatIconModule,
        MatDialogModule,
    ]
})
export class UserProgressModule {
}
