import {CardWithResultTrainingComponent} from './card-with-result-training/card-with-result-training.component';
import {WorkoutProcessResultComponent} from './workout-process-result/workout-process-result.component';

export const fromContainer = [CardWithResultTrainingComponent, WorkoutProcessResultComponent];

export * from './card-with-result-training/card-with-result-training.component';
export * from './workout-process-result/workout-process-result.component';
