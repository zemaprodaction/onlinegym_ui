import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-exercise-user-progress',
  templateUrl: './exercise-user-progress.component.html',
  styleUrls: ['./exercise-user-progress.component.scss']
})
export class ExerciseUserProgressComponent implements OnInit {

  constructor(private router: Router, protected route: ActivatedRoute) {
  }

  ngOnInit(): void {

  }
  swipeRight() {
    this.router.navigate(["../workout"], {relativeTo: this.route});
  }
}
