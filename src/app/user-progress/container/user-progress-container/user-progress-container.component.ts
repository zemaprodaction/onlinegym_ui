import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {RouterOutlet} from '@angular/router';
import {tabAnimation} from '../../../route-transition-animations';

@Component({
  selector: 'app-user-progress-container',
  templateUrl: './user-progress-container.component.html',
  styleUrls: ['./user-progress-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [tabAnimation]
})
export class UserProgressContainerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet &&
      outlet.activatedRouteData &&
      outlet.activatedRouteData['animationState'];
  }

}
