import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {filter, map} from 'rxjs/operators';
import {WorkoutResult} from '../../../share/models/workout-result';
import {WorkoutCharateristic} from '../../../share/models/workout-charateristic';
import {Store} from '@ngxs/store';
import {UserStatisticState} from '../../store/state/user-statistic.state';
import {ExerciseState} from '../../../core/training-store/states/exercise.state';
import {ExerciseSummary} from '../../../share/models/exercise-summary';
import {MeasureTypeState} from '../../../core/store/states/metadata-state/measure-types.state';
import {MeasuresType} from '../../../share/models/measuresType';

@Component({
  selector: 'app-workout-process-result',
  templateUrl: './workout-process-result.component.html',
  styleUrls: ['./workout-process-result.component.scss']
})
export class WorkoutProcessResultComponent implements OnInit {

  workoutResult$: Observable<WorkoutResult>;

  exerciseSummary$: Observable<ExerciseSummary[]>;
  commonCharacteristics$: Observable<WorkoutCharateristic[]>;
  charResultData$: Observable<{ labels: string[]; data: number[] }>;

  //man
  imgSrc = 'https://drive.google.com/uc?export=download&id=1OQ5Bgm29FDXo-h5stZFOdz8SUIhIGcZB';
  //woman
  //    imgSrc = 'https://drive.google.com/uc?export=download&id=1EH39g4BfNkEWW238BA9msUthpGPfhdL6';

  constructor(private store: Store) {
    this.workoutResult$ = this.store.select(UserStatisticState.getSelectedWorkoutResult).pipe(
      filter(wr => wr !== null && wr !== undefined),
    );
    this.exerciseSummary$ = this.workoutResult$.pipe(
      filter(wr => (wr != null)),
      map(workoutResult => workoutResult.exerciseSummary.map(es => {
          const newSummary = es.summary.map(s => {
            const newMap = new Map<string, number>();
            Object.keys(s).forEach((value) => {
              const newKey = store.selectSnapshot(MeasureTypeState.getEntityById<MeasuresType>(value));
              newMap.set(newKey.name, s[value]);
            });
            return newMap;
          });
          return {
            ...es,
            summary: newSummary,
            exercise: this.store.selectSnapshot(ExerciseState.selectExerciseById(es.exerciseId))
          };
        }
      ))
    );
    this.charResultData$ = this.getCharResultData();
    this.commonCharacteristics$ = this.workoutResult$.pipe(
      filter(wr => (wr != null)),
      map(wr => WorkoutProcessResultComponent.initCommonCharateristicArr(wr))
    );
  }


  private static initCommonCharateristicArr(wr: WorkoutResult) {
    return [new WorkoutCharateristic('упражнений', wr.exerciseCount),
      new WorkoutCharateristic('кДж', wr.totalWork),
      new WorkoutCharateristic('длительность', wr.totalWorkoutTime)];
  }

  ngOnInit(): void {
  }


  private getCharResultData() {
    return this.workoutResult$.pipe(
      filter(wr => (wr != null)),
      map(value => {
        let countOfDay = 1;
        const labels = value.workoutProgress.map(() => 'День' + countOfDay++);
        return {labels, data: value.workoutProgress};
      }),
    );
  }
}
