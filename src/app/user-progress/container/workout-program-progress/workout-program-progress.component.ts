import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Select, Store} from '@ngxs/store';
import {UserState} from '../../../core/user-store/states/user.state';
import {WorkoutProgram} from '../../../share/models/workout-programs';
import {UserWorkoutProgramResult} from '../../../share/models/user-workout-program-result';
import {UserWorkoutProgramServiceApi} from '../../../core/services/user-workout-program-api.service';
import {Backdrop} from '../../../core/store/actions/backdrop.actions';
import {combineAll, map, switchMap} from 'rxjs/operators';
import {WorkoutProgramState} from '../../../core/training-store/states/workout-program/workout-program.state';
import {WorkoutProgramAction} from '../../../core/training-store/actions/workout-programs.action';
import {AuxiliaryComponentService} from '../../../user/training-process/services/auxiliary-component.service';
import {WorkoutProgramResultBottomsheetCardComponent} from '../../component/workout-program-result-bottomsheet-card/workout-program-result-bottomsheet-card.component';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-workout-program-progress',
  templateUrl: './workout-program-progress.component.html',
  styleUrls: ['./workout-program-progress.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkoutProgramProgressComponent implements OnInit {

  @Select(UserState.getTrainingWorkoutProgram)
  $currentWorkoutProgram: Observable<WorkoutProgram>;

  results$: Observable<UserWorkoutProgramResult>;

  $archive: Observable<any[]>;

  constructor(private service: UserWorkoutProgramServiceApi,
              private auxiliaryComponentService: AuxiliaryComponentService,
              private router: Router, protected route: ActivatedRoute,
              private store: Store) {
    this.store.dispatch(new Backdrop.SetUpCloseBackdropSubHeaderTitle({
      openTitle: $localize`:@@programStatistics:Program statistics`,
      closeTitle: $localize`:@@programStatistics:Program statistics`
    }));
  }

  ngOnInit(): void {
    this.store.dispatch(new WorkoutProgramAction.LoadWorkoutPrograms());
    this.results$ = this.service.getActiveUserWorkoutProgramResult();
    this.$archive = this.service.getUserWorkoutProgramArchive().pipe(
      switchMap(dto => dto.map(d => this.store.select(WorkoutProgramState.getWorkoutProgramById(d.userWorkoutProgramId)).pipe(
          map(wp => ({...wp, startDate: d.startDate, endDate: d.endDate, archiveId: d.id})),
        ))),
      combineAll()
    );

  }

  openBottomsheet(archiveId: string) {
    this.auxiliaryComponentService.infoBottomSheet(WorkoutProgramResultBottomsheetCardComponent, {archiveId});
  }

  swipeLeft() {
    this.router.navigate(["../workout"], {relativeTo: this.route});
  }

}
