import {Component, Input, OnInit} from '@angular/core';
import {ExerciseSummary} from '../../../share/models/exercise-summary';

@Component({
  selector: 'app-card-with-result-training',
  templateUrl: './card-with-result-training.component.html',
  styleUrls: ['./card-with-result-training.component.scss']
})
export class CardWithResultTrainingComponent implements OnInit {

  @Input()
  exerciseSummaryList: ExerciseSummary[] = [];

  ngOnInit(): void {
  }

}
