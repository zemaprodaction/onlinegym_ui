import {Component, OnInit} from '@angular/core';
import {WorkoutStatisticService} from '../../../core/services/statistic/workout-statistic.service';
import {Observable} from 'rxjs';
import {WorkoutRating} from '../../../core/type/workout-rating';
import {WorkoutData} from '../../../share/models/workout-data';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'app-workout-progress',
    templateUrl: './workout-progress.component.html',
    styleUrls: ['./workout-progress.component.scss']
})
export class WorkoutProgressComponent implements OnInit {

    workoutRating$: Observable<WorkoutRating[]>;
    workoutHistory$: Observable<WorkoutData[]>;

    constructor(private workoutStatisticService: WorkoutStatisticService,
                private router: Router, protected route: ActivatedRoute) {
        this.workoutRating$ = this.workoutStatisticService.getTheMostPopularWorkout();
        this.workoutHistory$ = this.workoutStatisticService.getUserHistory(3);
    }

    ngOnInit(): void {
    }

    swipeLeft() {
        this.router.navigate(["../exercise"], {relativeTo: this.route});
    }

    swipeRight() {
        this.router.navigate(["../workout-program"], {relativeTo: this.route});
    }
}
