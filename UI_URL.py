import os
url=os.environ['UI_URI']
with open ('src/environments/environment.defaults.ts', 'r') as f:
  old_data = f.read()
new_data = old_data.replace('http://localhost:8082', url)
with open ('src/environments/environment.defaults.ts', 'w') as f:
  f.write(new_data)
print('OKAI')