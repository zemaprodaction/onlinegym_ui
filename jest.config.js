const {pathsToModuleNameMapper} = require('ts-jest/utils');
const {compilerOptions} = require('./tsconfig.spec.json');

const esModules = ['@agm', 'ngx-bootstrap', 'lodash-es', 'ng2-charts'].join('|');
module.exports = {
  preset: 'jest-preset-angular',
  roots: ['<rootDir>/src/'],
  testMatch: ['**/+(*.)+(spec).+(ts)'],
  setupFilesAfterEnv: ['<rootDir>/src/test.ts'],
  collectCoverage: true,
  coverageReporters: ['html'],
  coverageDirectory: 'coverage/my-app',
  moduleNameMapper: {
    "^lodash-es$": "lodash"
  },
  "transformIgnorePatterns": [
    "<rootDir>/node_modules/(?!lodash-es)"
  ]
};
