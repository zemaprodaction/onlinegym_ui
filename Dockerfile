FROM node:14.9.0-alpine3.10 as builder
WORKDIR /app
COPY package*.json ./
RUN npm config set registry http://registry.npmjs.org/ && npm install --no-package-lock
COPY . .
RUN npm run build --localize
CMD ["npm", "start"]
